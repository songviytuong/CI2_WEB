<?php

class SEO_model extends CI_Model {

    var $tablename = 'auc_seo';

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function getSEODefault($id = 1) {
        $this->db->select('*');
        $this->db->where('ID', $id);
        $result = $this->db->get($this->tablename)->row_array();
        return $result;
    }

    public function defineSEODefault($id = 1) {
        $row = $this->getSEODefault($id);
        $res = array();
        $res['MetaID'] = $row['ID'];
        $res['MetaTitle'] = $row['Title'];
        $res['MetaKeywords'] = $row['Keywords'];
        $res['MetaDescription'] = $row['Description'];
        $res['MetaPicture'] = $row['Thumb'];
        return $res;
    }

}
