<?php

class Languages_model extends CI_Model {

    var $tablename = 'ttp_languages';
    var $tablename_translate = 'ttp_languages_translate';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getLanguageByLangID($lang) {
        $LangArr = $this->getLanguageBySession($lang);
        $LangID = $LangArr['LangID'];
        $this->db->select('*');
        $this->db->where('LangID', $LangID);
        $result = $this->db->get($this->tablename_translate)->result();
        return $result;
    }

    function getLanguageBySession($lang) {
        $this->db->select('*');
        $this->db->where('LangName', $lang);
        $result = $this->db->get($this->tablename)->row_array();
        return $result;
    }

    function getLangID($lang) {
        $this->db->select('LangID');
        $this->db->where('LangName', $lang);
        $result = $this->db->get($this->tablename)->row();
        return $result->LangID;
    }

    function getLangNameByLocal($local) {
        $this->db->select('LangID,LangName');
        $this->db->where('LangLocal', $local);
        $result = $this->db->get($this->tablename)->row_array();
        return $result;
    }

    function getAllLanguages($orderby = 'LangPosition', $sort = 'ASC') {
        $this->db->select('*');
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function defineAllLanguage() {
        $data = $this->getAllLanguages();
        $res = array();
        foreach ($data as $key => $row) {
            $res[$key]['ID'] = $row->LangID;
            $res[$key]['Local'] = $row->LangLocal;
            $res[$key]['Icon'] = $row->LangIcon;
            $res[$key]['Flag'] = $row->LangFlag;
            $res[$key]['Name'] = $row->LangName;
        }
        return $res;
    }

    function getAllTranslates($orderby = 'LangID', $sort = 'DESC') {
        $this->db->select('*');
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename_translate)->result();
        return $result;
    }

}