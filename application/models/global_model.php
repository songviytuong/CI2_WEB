<?php

class Global_model extends CI_Model {

    var $tablename = 'ttp_ip';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    function getNewUrl($old_url) {
        $arr = array(
            'about-us' => 'gioi-thieu-auchan',
            'gia-tri-cot-loi' => 'gioi-thieu-auchan',
            'bang-tin-hoat-dong' => 'gioi-thieu-auchan',
            'our-products' => 'khuyen-mai',
            
            'my-pham' => 'khuyen-mai',
            'hai-san-tuoi-song' => 'khuyen-mai',
            'my-pham' => 'khuyen-mai',
            'banh-mi-truyen-thong' => 'khuyen-mai',
            'thit' => 'khuyen-mai',
            'rau-cu-hoa-va-cay' => 'khuyen-mai',
            
            'product-discounts' => 'the-thanh-vien',
            'co-hoi-nghe-nghiep' => 'tuyen-dung',
            'where-to-find-us' => 'he-thong-sieu-thi-auchan',
            'dieu-khoan-chinh-sach' => 'dieu-khoan-va-chinh-sach',
        );
        if (isset($arr[$old_url])) {
            $new_url = ROOT_URL . '/';
            $new_url .= $arr[$old_url];
            $new_url .= page_extension;
            return $new_url;
        } else {
            return FALSE;
        }
    }

    function getReferer() {
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "";
        if ($referer != '') {
            $referer = explode('/', $referer);
            $referer = isset($referer[2]) ? $referer[2] : 'UNKNOWN';
        } else {
            $referer = TOOLS_URL;
        }
        return $referer;
    }

    function getUserAgent() {
        $u_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "UNKNOWN";
        return $u_agent;
    }

    function getYourIP() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    function getIPFromDB() {
        $this->db->select('ip');
        $this->db->where('type', 0);
        $result = $this->db->get($this->tablename)->result_array();
        return $result;
    }

    function troubleshoot() {
        $arr = $this->getIPFromDB();
        foreach ($arr as $key => $ip) {
            $ips[$key] = $ip['ip'];
        }
        $your_ip = $this->getYourIP();

        $res = array();
        if (!in_array($your_ip, $ips) && (!in_array($your_ip, array('127.0.0.1')))) {
            $res['accept'] = FALSE;
            $res['ip'] = $your_ip;
        }
        return $res;
    }

    function getType() {
        $your_ip = $this->getYourIP();
        $this->db->select('type');
        $this->db->where('ip', $your_ip);
        $data = $this->db->get($this->tablename)->row();
        $data = ($data) ? $data->type : 2;
        return $data;
    }

    function addIP($type = 0, $localip = null) {
        if ($localip) {
            $your_ip = $localip;
        } else {
            $your_ip = $this->getYourIP();
        }
        $query = $this->db->query("SELECT id FROM ttp_ip
                           WHERE ip = " . $this->db->escape($your_ip) . " limit 1");
        $data['ip'] = $your_ip;
        $data['type'] = $type;
        $data['created'] = date('Y-m-d H:i:s');
        if ($query->num_rows() == 0) {
            $this->db->insert($this->tablename, $data);
        } else {
            $this->db->where('ip', $your_ip);
            $this->db->update($this->tablename, $data);
        }
        return;
    }

    function removeIP($localip = null) {
        if ($localip) {
            $your_ip = $localip;
        } else {
            $your_ip = $this->getYourIP();
        }
        $this->db->where('ip', $your_ip);
        $this->db->delete($this->tablename);
    }

    function limit_text($text, $limit) {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }
        return $text;
    }

    function truncate($string, $limit, $break = ".", $pad = "...") {

        if (strlen($string) <= $limit)
            return $string;
        if (false !== ($max = strpos($string, $break, $limit))) {
            if ($max < strlen($string) - 1) {
                $string = substr($string, 0, $max) . $pad;
            }
        }
        return $string;
    }

    function strip_tags_content($text, $tags = '', $invert = FALSE) {
        preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
        $tags = array_unique($tags[1]);

        if (is_array($tags) AND count($tags) > 0) {
            if ($invert == FALSE) {
                return preg_replace('@<(?!(?:' . implode('|', $tags) . ')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
            } else {
                return preg_replace('@<(' . implode('|', $tags) . ')\b.*?>.*?</\1>@si', '', $text);
            }
        } elseif ($invert == FALSE) {
            return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        return $text;
    }

    function strip_tags($text) {
        $txt = $this->strip_tags_content($text);
        $txt = str_replace("(", "", $txt);
        $txt = str_replace(")", "", $txt);
        $txt = str_replace(" ", "", $txt);
        return $txt;
    }

}