<!DOCTYPE html>
<html>
    <head>
        <title>Website is Coming Soon</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="author" content="Lee Peace">
        <meta name="robots" content="all">
        <link data-noprefix  href='http://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800' rel='stylesheet' type='text/css'>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?= UNDER_URL; ?>/css/animate.css">
        <link rel="stylesheet" href="<?= UNDER_URL; ?>/css/stylesheet.css">
    </head>
    <body class="style1">
        <div id="preloader">
            <div id="status">&nbsp;</div>
            <noscript>JavaScript is off. Please enable to view full site.</noscript>
        </div>
        <div class="wrapper">
            <div class="page-holder">
                <h1>Under Construction</h1>
                <h3 class="subtitle">Register your IP to Login our Website!</h3>
                <!-- Countdown dashboard start -->
                <div id="countdown_dashboard">
                    <div class="dash weeks_dash">
                        <span class="dash_title">weeks</span>
                        <div class="digit">0</div>
                        <div class="digit">0</div>
                    </div>

                    <div class="dash days_dash">
                        <span class="dash_title">days</span>
                        <div class="digit">0</div>
                        <div class="digit">0</div>
                    </div>

                    <div class="dash hours_dash">
                        <span class="dash_title">hours</span>
                        <div class="digit">0</div>
                        <div class="digit">0</div>
                    </div>

                    <div class="dash minutes_dash">
                        <span class="dash_title">minutes</span>
                        <div class="digit">0</div>
                        <div class="digit">0</div>
                    </div>

                    <div class="dash seconds_dash">
                        <span class="dash_title">seconds</span>
                        <div class="digit">0</div>
                        <div class="digit">0</div>
                    </div>

                </div>
                <!-- Countdown dashboard end -->
                <div class="form-holder">
                    <div class="message-box"></div>
                    <form>
                        <?php
                        $this->load->model('global_model', 'global');
                        $your_ip = $this->global->getYourIP();
                        ?>
                        <div class="form-group">
                            <div class="input-group">
                                <input class="le-input required" name="ip" data-placeholder="Your IP" type="text" value="<?= $your_ip; ?>">
                                <div class="input-group-addon"><button class="le-btn">Register</button></div>
                            </div>
                        </div>
                    </form>
                    <div id="loading" class="pull-right">
                        <img alt="" src="<?= UNDER_URL; ?>/images/loader.gif" />
                    </div>
                </div>
            </div>
            <div id="sky">
                <div id="clouds" ></div>
                <div id="plane" ></div>
                <div id="baloon1"></div>
                <div id="baloon2"></div>
                <div id="sea" ></div>
                <div id="wave" ></div>
            </div>
            <div id="land"></div>
            <div id="bucket"></div>
            <div id="boot"></div>
            <div id="suitcase">
                <div id="hat"></div>
            </div>
        </div>
        <a class="goto-top" href="#gotop"></a>
        <script src="<?= UNDER_URL; ?>/js/jquery-1.11.1.min.js"></script>
        <script src="<?= UNDER_URL; ?>/js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="<?= UNDER_URL; ?>/js/css_browser_selector.js"></script>
        <script type="text/javascript" src="<?= UNDER_URL; ?>/js/prefixfree.min.js"></script>
        <script type="text/javascript" src="<?= UNDER_URL; ?>/js/pace.min.js"></script>
        <script type="text/javascript" src="<?= UNDER_URL; ?>/js/jquery.validate.js"></script>
        <script type="text/javascript" src="<?= UNDER_URL; ?>/js/jquery.spritely.js"></script>
        <script type="text/javascript" src="<?= UNDER_URL; ?>/js/jquery.lwtCountdown-1.0.js"></script>
        <script type="text/javascript" src="<?= UNDER_URL; ?>/js/fontsmoothie.min.js"></script>
        <script type="text/javascript" src="<?= UNDER_URL; ?>/js/script.js"></script>
    </body>
</html>