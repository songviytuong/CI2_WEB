<?php echo $_doctype; ?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet">
        <?php echo $_title; ?>
        <base href="<?php echo base_url(); ?>" >
        <meta name="description" content="<?php echo $MetaDescription; ?>" />
        <meta name="keywords" content="<?php echo $MetaKeywords; ?>" />
        <?php echo $MetaExtend ?>
        <meta http-equiv='Refresh' content='1200; url=<?php echo current_url() ?>'>
        <meta name="robots" content="index,follow,all" />
        <meta name="revisit-after" content="1 days" />
        <link type="image/x-icon" href="public/site/images/fav.ico" rel="shortcut icon">
        <link href="public/site/css/bootstrap.min.css" rel="stylesheet">
        <link href="public/site/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="public/site/css/animate.min.css" rel="stylesheet">
        <link href="public/site/css/style.css" rel="stylesheet">

    </head>
    <body>
        <?php echo $content; ?>
        <div id="order-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade"></div>
        <script src="public/site/js/jquery.min.js"></script>
        <script src="public/site/js/tether.min.js" type="text/javascript"></script>
        <script src="public/site/js/bootstrap.min.js"></script>
        <script src="public/site/js/script.js"></script>
    </body>
</html>
