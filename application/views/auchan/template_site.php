<?php echo $_doctype; ?>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="google-site-verification" content="-9OIMhMxg9J95dluhkVoW2IrakRnUwfDa_Iu55UJo7w" />
        <?= SEOPlugin::getBaseUrl(); ?>
        <title><?= SEOPlugin::getTitle(); ?></title>
        <?php if (SEOPlugin::getKeywords()) { ?>
            <meta name="keywords" content="<?= SEOPlugin::getKeywords(); ?>" />     
        <?php } ?>   
        <meta name="description" content="<?= SEOPlugin::getDescription(); ?>" />
        <meta property='og:title' content="<?= SEOPlugin::getTitle(); ?>" />
        <meta property='og:description' content="<?= SEOPlugin::getDescription(); ?>" />
        <?= SEOPlugin::getSocialImage(); ?>
        <link rel="apple-touch-icon" sizes="180x180" href="<?= THEMES_URL; ?>/images/fav/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="<?= THEMES_URL; ?>/images/fav/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="<?= THEMES_URL; ?>/images/fav/favicon-16x16.png" />
        <link rel="manifest" href="<?= THEMES_URL; ?>/images/fav/manifest.json" />
        <link rel="mask-icon" href="<?= THEMES_URL; ?>/images/fav/safari-pinned-tab.svg" color="#ff0000" />
        <meta name="theme-color" content="#ffffff" />
        <?php
//        $this->session->sess_destroy();
        $session_id = $this->session->userdata('session_id');
        $session_id = 'auc' . substr($session_id, -8);
        $seed = str_split('0123456789');
        shuffle($seed);
        $rand = '';
        foreach (array_rand($seed, 10) as $k)
            $rand .= $seed[$k];
        ?>
        <link href="<?= THEMES_URL; ?>/css/base.css?v=<?= $session_id; ?>" rel="stylesheet" type="text/css" />
        <link href="<?= THEMES_URL; ?>/css/main.min.css?v=<?= $session_id; ?>" rel="stylesheet" type="text/css" />
        <link href="<?= THEMES_URL; ?>/css/custom.css?v=<?= $session_id; ?>" rel="stylesheet" type="text/css" />
        <?php if (ENVIRONMENT == 'production') : ?>
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=vietnamese" rel="stylesheet" />
            <?php
            if (Globals::getConfig("Analytics")) {
                echo Globals::getConfig("Analytics") != '' ? "<script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', '" . Globals::getConfig("Analytics") . "', 'auto');
          ga('send', 'pageview');

        </script>" : "";
            }
            ?>
        <?php endif; //ENVIRONMENT ?>

    </head>

    <body class="<?= isset($page_class) ? $page_class : ''; ?>">
        <?php echo $header; ?>
        <?php echo $content; ?>
        <?php echo $footer; ?>

        <script src="<?= THEMES_URL; ?>/js/jquery.min.js?v=<?= $session_id; ?>" type="text/javascript"></script>
        <script src="<?= THEMES_URL; ?>/js/tether.min.js?v=<?= $session_id; ?>" type="text/javascript"></script>
        <script src="<?= THEMES_URL; ?>/js/bootstrap.min.js?v=<?= $session_id; ?>" type="text/javascript"></script>
        <script src="<?= THEMES_URL; ?>/js/placeholderTypewriter.js?v=<?= $session_id; ?>" type="text/javascript"></script>
        <script src="<?= THEMES_URL; ?>/js/script.js?v=<?= $session_id; ?>"></script>
        <?php if (lazyload) : ?>
            <script src="<?= THEMES_URL; ?>/js/lazyload.min.js?v=<?= $session_id; ?>"></script>
            <script>$("img.lazyload").lazyload();</script>
        <?php endif; ?>
        <?php if (MapResizer) : ?>
            <script src="<?= THEMES_URL; ?>/js/imageMapResizer.min.js"></script>
            <script>$('map').imageMapResize();</script>
        <?php endif; ?>
        <?php if (is_array($js_to_load)) : ?>
            <?php foreach ($js_to_load as $row): ?>
                <script src="<?= $row; ?>" type="text/javascript" ></script>
            <?php endforeach; ?>
        <?php endif; ?>

        <script>
                $(document).ready(function () {
                    var turn_popup = "<?= (!empty(Globals::getConfig("Popup")) == 1) ? 'on' : 'off'; ?>";
                    if (turn_popup == 'on') {
                        $('#popup-modal').modal('show');
                    }
                    var placeholderText = [
                        "Bạn muốn tìm gì trên Auchan à ?",
                        "Trái cây & Rau củ quả phải không ?",
                        "Thịt & hải sản à ?",
                        "Bánh hay đồ uống phải không ?",
                        "Nguyên liệu nấu ăn à ?",
                        "Đồ dùng nhà bếp phải không ?",
                    ];
                    $('#placeholder-type-writter').placeholderTypewriter({text: placeholderText});
                });
        </script>
        <script src="<?= THEMES_URL; ?>/js/global.js?v=<?= $session_id; ?>"></script>
        <script src="<?= THEMES_URL; ?>/js/custom.js?v=<?= $session_id; ?>" type="text/javascript"></script>
        <?php if (COMMENTOFF && COMMENT == 'FB') : ?>
            <div id="fb-root"></div>
            <script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                        return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
        <?php endif; ?>
    </body>
</html>