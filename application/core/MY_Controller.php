<?php

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->initiate_cache();
    }

    private function initiate_cache() {
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
    }

}
