<?php

class Globals {

    static $_config = array();
    protected $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('static_model', 'STATIC');
        $data = $this->CI->STATIC->getConfig();
        self::$_config = json_decode($data->Data);
        $this->getConfig();
    }

    public static function getConfig($params = "") {
        if ($params) {
            return self::$_config->$params;
        } else {
            return self::$_config;
        }
    }

    public static function getContentByCurl($url) {
        try {
            $curlHandle = curl_init(); // init curl
            curl_setopt($curlHandle, CURLOPT_URL, $url); // set the url to fetch
            curl_setopt($curlHandle, CURLOPT_HEADER, 0);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_BINARYTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_TIMEOUT, 300);
            curl_setopt($curlHandle, CURLOPT_FOLLOWLOCATION, 1);
            $content = curl_exec($curlHandle);
            curl_close($curlHandle);
        } catch (Exception $ex) {
            return "";
        }
        return $content;
    }

    public static function timeBetweenTwoDate($date1, $date2) {
        $start = strtotime($date1);
        $end = strtotime($date2);
        if ($start == $end)
            return "";
        if ($start > $end) {
            $temp = $start;
            $start = $end;
            $end = $temp;
        }
        $seconds = 0;
        while ($start < $end) {
            $seconds++;
            $start++;
        }
        $one_day = 24 * 60 * 60;
        $one_hour = 60 * 60;
        $one_minute = 60;

        $result = array();
        $result['days'] = (int) ($seconds / $one_day);
        $result['hours'] = (int) ( ( $seconds % $one_day ) / $one_hour);
        $result['minutes'] = (int) ( ( ( $seconds % $one_day ) % $one_hour ) / $one_minute);
        $result['seconds'] = (int) ( ( ( $seconds % $one_day ) % $one_hour ) % $one_minute);

        $result['hours'] = $result['hours'] + $result['days'] * 24;
        return $result;
    }

    public function compareDate($date1, $date2) {
        if (strtotime($date1) > strtotime($date2))
            return 1;
        elseif (strtotime($date1) == strtotime($date2))
            return 0;
        return -1;
    }

    public static function link_to_title($link) {
        return str_replace(array("http://", "/", "&", "?", ";", "=", "/"), "_", $link);
    }

    public static function addDayToDate($date, $operator = '+', $day, $format = 'Y-m-d') {
        $date = $date . " " . $operator . $day . " day";
        $date = strtotime($date);
        return date($format, $date);
    }

    /**
     * Them 1 ngay vao ngay hien tai, Fix loi thang 2
     * format: Y-m-d H:i:s
     *
     * @param Datetime $date
     */
    static function addMonthToDate($date, $numofmonths) {
        $datetime = $date;
        for ($i = 0; $i < $numofmonths; $i++) {
            $time = strtotime($datetime);
            $day = date("d", $time);
            $month = intval(date("m", $time));
            $year = date("Y", $time);
            $hour = date("H", $time);
            $minutes = date("i", $time);
            $seconds = date("s", $time);
            if ($month == 1) {
                $maxDayOfFeb = date("t", strtotime("$year-02-01 00:00:00"));
//                $date = new DateTime("$year-$month-$day $hour:$minutes:$seconds");
//                $datetime = $date->format("Y-m-d H:i:s");
//                $date->modify("+$maxDayOfFeb day");
//                $datetime = $date->format("Y-m-d H:i:s");
                $month += 1;
                $maxDayOfFeb = date("t", strtotime("$year-02-01 00:00:00"));
                $maxDayOfFeb -= 1;
                $date = new DateTime("$year-$month-$maxDayOfFeb $hour:$minutes:$seconds");
                $datetime = $date->format("Y-m-d H:i:s");
            } else {
                $date = new DateTime("$year-$month-$day $hour:$minutes:$seconds");
                $date->modify("+1 month");
                $datetime = $date->format("Y-m-d H:i:s");
            }
        }
        $time = new DateTime($datetime);
        return $time;
    }

    public static function getPromotion($pDetail, $bbcode) {
        $brands = array("Apple", "Sony", "Samsung", "Wiko", "Oppo", "Microsoft", "Lg", "Blackberry", "Mobell", "Hp", "Philips", "Lenovo", "Asus");
        $bonus = array("khuyến mãi", "ưu đãi", "chương trình khuyến mãi", "chương trình ưu đãi");
        if ($bbcode == 1) {
            $links = array(
                "[url=http://www.hnammobile.com/tin-tuc/mua-dien-thoai-tra-gop-tai-hnam-mobile-.6206.html]mua điện thoại trả góp[/url]",
                "[url=http://www.hnammobile.com/tin-tuc/mua-dien-thoai-tra-gop-tai-hnam-mobile-.6206.html]mua trả góp điện thoại[/url]",
                "[url=http://www.hnammobile.com]điện thoại giá rẻ[/url]",
                "[url=http://www.hnammobile.com/dien-thoai/]điện thoại[/url]",
                "[url=http://www.hnammobile.com/dien-thoai/]điện thoại di động[/url]",
                "[url=http://www.hnammobile.com/dien-thoai/]smartphone[/url]",
                "[url=http://www.hnammobile.com/dien-thoai/]smartphone chính hãng[/url]",
                "[url=http://www.hnammobile.com/dien-thoai/]smartphone giá rẻ[/url]",
                "[url=http://www.hnammobile.com/may-tinh-bang/]máy tính bảng[/url]",
                "[url=http://www.hnammobile.com/may-tinh-bang/]máy tính bảng giá rẻ[/url]",
                "[url=http://www.hnammobile.com/loai-dien-thoai/kho-may-cu.53.html]điện thoại cũ[/url]",
//                "[url=http://www.hnammobile.com/loai-dien-thoai/blackberry.45.html]điện thoại blackberry[/url]",
//                "[url=http://www.hnammobile.com/loai-dien-thoai/blackberry.45.html]điện thoại blackberry[/url]",
//                "[url=http://www.hnammobile.com/loai-dien-thoai/blackberry.45.html]điện thoại blackberry[/url]",
            );
        } else {
            $links = array(
                "<a href='http://www.hnammobile.com/tin-tuc/mua-dien-thoai-tra-gop-tai-hnam-mobile-.6206.html'>mua điện thoại trả góp</a>",
                "<a href='http://www.hnammobile.com/tin-tuc/mua-dien-thoai-tra-gop-tai-hnam-mobile-.6206.html'>mua trả góp điện thoại</a>",
                "<a href='http://www.hnammobile.com'>điện thoại giá rẻ</a>",
                "<a href='http://www.hnammobile.com/dien-thoai/'>điện thoại</a>",
                "<a href='http://www.hnammobile.com/dien-thoai/'>điện thoại di động</a>",
                "<a href='http://www.hnammobile.com/dien-thoai/'>smartphone chính hãng</a>",
                "<a href='http://www.hnammobile.com/dien-thoai/'>smartphone giá rẻ</a>",
                "<a href='http://www.hnammobile.com/may-tinh-bang/'>máy tính bảng</a>",
                "<a href='http://www.hnammobile.com/may-tinh-bang/'>máy tính bảng giá rẻ</a>",
                "<a href='http://www.hnammobile.com/loai-dien-thoai/kho-may-cu.53.html'>điện thoại cũ</a>",
//                "<a href='http://www.hnammobile.com/loai-dien-thoai/blackberry.45.html'>điện thoại blackberry</a>",
//                "<a href='http://www.hnammobile.com/loai-dien-thoai/blackberry.45.html'>điện thoại blackberry</a>",
//                "<a href='http://www.hnammobile.com/loai-dien-thoai/blackberry.45.html'>điện thoại blackberry</a>",
            );
        }

        shuffle($links);
        shuffle($brands);
        $_link = $links[0];
        $_brand1 = $brands[0];
        $_brand2 = $brands[1];
        $_brand3 = $brands[2];
        $_brand4 = $brands[3];
        $_title = $pDetail["title"];
        shuffle($bonus);
        $_bonus = $bonus[0];

        $p1 = array(
            "Song song với những dịch vụ khách hàng nhiều $_bonus, hệ thống điện thoại chính hãng Hnam Mobile cung cấp $_link còn triển khai loạt chương trình khuyến mãi kết hợp với những thương hiệu công nghệ hàng đầu thế giới như “Cưỡi SH – Rước Vespa – Lướt Wiko miễn phí” cùng Wiko; “Đón năm mới – Nhận quà công nghệ sành điệu” với $_brand1… ",
            "Đồng thời, khi đến với Hnam Mobile quý khách hàng còn có thể chọn mua cho mình hàng loạt sản phẩm $_link cấu hình mạnh, thương hiệu tốt, có giá giảm sốc tại Hnam Mobile như: $_title hay loạt sản phẩm từ $_brand1, $_brand2…",
        );
        $p2 = array(
            "Cùng với những dịch vụ khách hàng tuyệt vời, Hnam Mobile vói các sản phẩm triển khai nhiều chương trình khuyến mãi kết hợp cùng những thương hiệu nổi tiếng như $_brand1, $_brand2, $_brand3, $_brand4…",
            "Đồng thời, ghé đến Hnam Mobile, bạn sẽ có nhiều lựa chọn với những sản phẩm cấu hình tốt, thương hiệu mạnh, giá giảm sốc tại Hnam Mobile như: $_brand1, $_brand2, $_brand3, $_brand4…",
        );
        $p3 = array();

        shuffle($p1);
        shuffle($p2);
        shuffle($p3);

        $p1 = $p1[0];
        $p2 = $p2[0];
        $p3 = $p3[0];
        $arr = array($p1, $p2, $p3);
        shuffle($arr);

        $prefixs = array("Giới thiệu", "Thông tin về", "Những");
        shuffle($prefixs);
        $prefix = $prefixs[0];
        if ($bbcode == 1) {
            $title = "[b]$prefix chương trình khuyến mại[/b]";
        } else {
            $title = "<b>$prefix chương trình khuyến mại</b>";
        }
        return $title . "<p>" . implode("</p><p>", $arr) . "</p>";
    }

}

?>