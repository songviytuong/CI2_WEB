<?php

/*
 * Lee Peace
 * Email: songviytuong@gmail.com
 * 07-11-2017
 * 
 */

class SMSPlugin {

    static $_client_url = "";
    static $_username = "";
    static $_password = "";
    static $_brandname = "";    # Brandname of parnert
    static $_message = "";      # Lenght of SMS <160 character : 1 sms
    static $_phone = "";        # pre-fix is "0" or "84"
    static $_type = 0;          # 0: Advertising - 1: customer support
    static $_token = "";        # must be unique time()
    protected $CI;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->CI = & get_instance();
        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $this->setType(1);
        $this->setToken(time());
    }

    public static function setClientUrl($_client_url) {
        self::$_client_url = $_client_url;
    }

    public static function getClientUrl() {
        return self::$_client_url;
    }

    public static function setUsername($_username) {
        self::$_username = $_username;
    }

    public static function getUsername() {
        return self::$_username;
    }

    public static function setPassword($_password) {
        self::$_password = $_password;
    }

    public static function getPassword() {
        return self::$_password;
    }

    public static function setBrandname($_brandname) {
        self::$_brandname = $_brandname;
    }

    public static function getBrandname() {
        return self::$_brandname;
    }

    public static function setMessage($_message) {
        self::$_message = $_message;
    }

    public static function getMessage() {
        return self::$_message;
    }

    public static function setType($_type) {
        self::$_type = $_type;
    }

    public static function getType() {
        return self::$_type;
    }

    public static function setPhone($_phone) {
        self::$_phone = $_phone;
    }

    public static function getPhone() {
        return self::$_phone;
    }

    public static function setToken($_token) {
        self::$_token = $_token;
    }

    public static function getToken() {
        return self::$_token;
    }

    public static function SendSMS() {
        $client = new SoapClient(self::getClientUrl());
        $result = $client->send(
            array(
                "USERNAME" => self::getUsername(),
                "PASSWORD" => self::getPassword(),
                "BRANDNAME" => self::getBrandname(),
                "MESSAGE" => self::getMessage(),
                "TYPE" => self::getType(),
                "PHONE" => self::getPhone(),
                "IDREQ" => self::getToken()
            )
        );
        return $result;
    }

    public static function Response($code) {
        $arr = array(
            '-4' => 'Template Wrong',
            '-3' => 'System error',
            '-2' => 'Wrong user or password',
            '-1' => 'Sending error (Message content unicode character)',
            '0' => 'Success',
            '2' => 'Not enough account',
            '3' => 'Reciever is null',
            '4' => 'Invalid reciever',
            '5' => 'Target is null',
            '6' => 'Error:[error detail]',
            '7' => 'Over quota',
            '99' => 'Error in processing : [error detail]',
            '100' => 'Authentication fail',
            '101' => 'Authentication User is deactived',
            '102' => 'Authentication User is expired',
            '103' => 'Authentication User is locked',
            '104' => 'Template not actived',
            '105' => 'Template does not exist',
            '108' => 'Msisdn in blackList',
            '304' => 'Send the same content in short time',
            '400' => 'Not enough money',
            '900' => 'System is error',
            '901' => 'Lenght of messages is 612 with noneUnicode message and 266 with Unicode message',
            '902' => 'Number of msisdn must be >0',
            '904' => 'Brandname inactive'
        );
        if ($code) {
            return $arr[$code];
        } else {
            return null;
        }
    }

}
