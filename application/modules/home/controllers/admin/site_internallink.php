<?php 
class Site_internallink extends Admin_Controller { 
	public $user;
	public $classname="site_internallink";
	public $limit = 15;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
		$session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
		if($this->user->IsAdmin!=1){
			$this->lib->published_module($this->classname);
		}
		$this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }
	
	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$page = $this->uri->segment(5);
		$start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $this->template->add_title('Internal links | Site Config');
		$limit_str = "limit $start,$this->limit";
		$nav = $this->db->query("select count(1) as nav from ttp_footer_links a,ttp_footer_categories_links b where a.CategoriesID=b.ID")->row();
        $nav = $nav ? $nav->nav : 0 ;
		$object = $this->db->query("select a.*,b.Title as CategoriesTitle from ttp_footer_links a,ttp_footer_categories_links b where a.CategoriesID=b.ID $limit_str")->result();
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/site_internallink/',
			'data'		=>	$object,
			'start'		=>	$start,
			'nav'		=>	$this->lib->nav(base_url().ADMINPATH.'/home/site_internallink/index',5,$nav,$this->limit)
		);
		$this->template->write_view('content','admin/site_internallink_home',$data); 
		$this->template->render();
	}

	public function search(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		if(isset($_GET['keyword'])){
			$keyword = $this->lib->fill_data($_GET['keyword']);
			$str = "select a.*,b.Title as CategoriesTitle from ttp_footer_links a,ttp_footer_categories_links b where a.CategoriesID=b.ID";
			$str = $keyword != '' ? $str." and (a.Title like '%".urldecode($keyword)."%' or b.Title like '%".urldecode($keyword)."%')" : $str ;
			$this->template->add_title('Tìm kiếm dữ liệu');
			$data=array(
				'data'	=> $this->db->query($str)->result(),
				'nav'	=> '',
				'start'	=> 0,
				'base_link' =>	base_url().ADMINPATH.'/home/site_internallink/',
			);
			$this->template->write_view('content','admin/site_internallink_home',$data);
			$this->template->render();
			return;
		}
		redirect(ADMINPATH.'/home/site_internallink/');
	}

	public function delete($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$this->db->query("delete from ttp_footer_links where ID=$id");
		}
		$return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
		redirect($return);
	}
	

	public function add(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$this->template->add_title('Add Link | Config Site');
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/site_internallink/'
		);
		$this->template->write_view('content','admin/site_internallink_add',$data);
		$this->template->render();
	}

	public function addnew(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '' ;
		$Title_en = isset($_POST['Title_en']) ? $this->lib->fill_data($_POST['Title_en']) : '' ;
		$Link = isset($_POST['Link']) ? $this->lib->fill_data($_POST['Link']) : '' ;
		$Link_en = isset($_POST['Link_en']) ? $this->lib->fill_data($_POST['Link_en']) : '' ;
		$Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0 ;
		$CategoriesID = isset($_POST['CategoriesID']) ? $this->lib->fill_data($_POST['CategoriesID']) : 0 ;
		if($Title!='' && $Link!='' && $CategoriesID!=0){
			$data = array(
				'Link'			=> $Link,
				'Link_en'		=> $Link_en,
				'Published'		=> $Published,
				'Title'			=> $Title,
				'Title_en'		=> $Title_en,
				'CategoriesID'	=> $CategoriesID,
				'Created'		=> date("Y-m-d H:i:s",time()),
				'LastEdited'	=> date("Y-m-d H:i:s",time())
			);
			$this->db->insert("ttp_footer_links",$data);
		}
		redirect(ADMINPATH.'/home/site_internallink/');
	}

	public function edit($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$result = $this->db->query("select * from ttp_footer_links where ID=$id")->row();
			if(!$result) show_error();
			$this->template->add_title('Edit Link | Config Site');
			$data = array(
				'base_link' =>	base_url().ADMINPATH.'/home/site_internallink/',
				'data'		=>	$result
			);
			$this->template->write_view('content','admin/site_internallink_edit',$data);
			$this->template->render();
		}
	}

	public function update(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		$ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
		$Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '' ;
		$Title_en = isset($_POST['Title_en']) ? $this->lib->fill_data($_POST['Title_en']) : '' ;
		$Link = isset($_POST['Link']) ? $this->lib->fill_data($_POST['Link']) : '' ;
		$Link_en = isset($_POST['Link_en']) ? $this->lib->fill_data($_POST['Link_en']) : '' ;
		$Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0 ;
		$CategoriesID = isset($_POST['CategoriesID']) ? $this->lib->fill_data($_POST['CategoriesID']) : 0 ;
		if($Title!='' && $Link!='' && $CategoriesID!=0){
			$data = array(
				'Link'			=> $Link,
				'Link_en'		=> $Link_en,
				'Published'		=> $Published,
				'Title'			=> $Title,
				'Title_en'		=> $Title_en,
				'CategoriesID'	=> $CategoriesID,
				'LastEdited'	=> date("Y-m-d H:i:s",time())
			);
			$this->db->where("ID",$ID);
			$this->db->update("ttp_footer_links",$data);
		}
		redirect(ADMINPATH.'/home/site_internallink/');
	}

}
?>