<?php 
class Gallery_album extends Admin_Controller { 
	public $user;
	public $classname="gallery_album";
	public $limit = 15;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
		$session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
		if($this->user->IsAdmin!=1){
			$this->lib->published_module($this->classname);
		}
		$this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }
	
	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$page = $this->uri->segment(5);
		$start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $this->template->add_title('Album list | Gallery');
		$limit_str = "limit $start,$this->limit";
		$nav = $this->db->query("select count(1) as nav from ttp_album")->row();
        $nav = $nav ? $nav->nav : 0 ;
		$object = $this->db->query("select * from ttp_album order by Created DESC $limit_str")->result();
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/gallery_album/',
			'data'		=>	$object,
			'start'		=>	$start,
			'nav'		=>	$this->lib->nav(base_url().ADMINPATH.'/home/gallery_album/index',5,$nav,$this->limit)
		);
		$this->template->write_view('content','admin/gallery_album_home',$data); 
		$this->template->render();
	}

	public function search(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		if(isset($_GET['keyword'])){
			$keyword = $this->lib->fill_data($_GET['keyword']);
			$str = "select * from ttp_album";
			$str = $keyword != '' ? $str." where Title like '%".urldecode($keyword)."%'" : $str ;
			$this->template->add_title('Tìm kiếm dữ liệu');
			$data=array(
				'data'	=> $this->db->query($str)->result(),
				'nav'	=> '',
				'start'	=> 0,
				'base_link' =>	base_url().ADMINPATH.'/home/gallery_album/',
			);
			$this->template->write_view('content','admin/gallery_album_home',$data);
			$this->template->render();
			return;
		}
		redirect(ADMINPATH.'/home/gallery_album/');
	}

	public function delete($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$this->db->query("delete from ttp_album where ID=$id");
		}
		$return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
		redirect($return);
	}
	

	public function add(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$this->template->add_title('Add Album | Gallery');
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/gallery_album/'
		);
		$this->template->write_view('content','admin/gallery_album_add',$data);
		$this->template->render();
	}

	public function addnew(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
		$Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0 ;
		if($Title!=''){
			$data = array(
				'Published'		=> $Published,
				'Title'			=> $Title,
				'Created'		=> date("Y-m-d H:i:s"),
				'LastEdited'	=> date("Y-m-d H:i:s")
			);
			$Max = $this->db->query("select max(ID) as ID from ttp_album")->row();
			$NewID = $Max ? $Max->ID+1 : 1;
			if(isset($_FILES['Images_upload'])){
				if(count($_FILES['Images_upload']['name'])>0){
					$this->upload_to = "album_thumb/$NewID";
					$reciver = $this->UploadMultiImages();
					if(isset($reciver['Thumb'])){
						foreach($reciver['Thumb'] as $item){
							$cropimage = explode(",",IMAGECROP);
							foreach($cropimage as $row){
								$size = explode("x", $row);
								$width = isset($size[0]) ? (int)$size[0] : 0 ;
								$height = isset($size[1]) ? (int)$size[1] : 0 ;
								if($width>0 && $height>0) $this->lib->cropimage($item,$width,$height);
							}
						}
					}
					if(isset($reciver['ID'])){
						$data['Data'] = json_encode($reciver['ID']);
					}
				}
			}
			$this->db->insert("ttp_album",$data);
		}
		redirect(ADMINPATH.'/home/gallery_album/');
	}

	public function edit($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$result = $this->db->query("select * from ttp_album where ID=$id")->row();
			if(!$result) return;
			$this->template->add_title('Edit Album | Gallery');
			$data = array(
				'base_link' =>	base_url().ADMINPATH.'/home/gallery_album/',
				'data'		=>	$result
			);
			$this->template->write_view('content','admin/gallery_album_edit',$data);
			$this->template->render();
		}
	}

	public function update(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		$ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
		$Title = isset($_POST['Title']) ? $this->security->xss_clean($_POST['Title']) : '' ;
		$Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0 ;
		if($Title!=''){	
			$data = array(
				'Published'		=> $Published,
				'Title'			=> $Title,
				'LastEdited'	=> date("Y-m-d H:i:s")
			);
			$this->db->where("ID",$ID);
			$this->db->update("ttp_album",$data);
		
		}
		redirect(ADMINPATH.'/home/gallery_album/');
	}

	public function remove($album=0,$id=0){
		if(is_numeric($album) && is_numeric($id)){
			if($album>0 && $id>0){
				$result = $this->db->query("select * from ttp_album where ID=$album")->row();
				if($result){
					$data = json_decode($result->Data,true);
					if(($key = array_search($id, $data)) !== false) {
					    unset($data[$key]);
					}
					$data = json_encode($data);
					$this->db->query("update ttp_album set Data='$data' where ID=$album");
				}
			}
		}
		redirect(ADMINPATH.'/home/gallery_album/edit/'.$album);
	}

	public function loadimage(){
		$ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
		$result = $this->db->query("select * from ttp_album where ID=$ID")->row();
		if($result){
			if(isset($_FILES['Images_upload'])){
				if(count($_FILES['Images_upload']['name'])>0){
					$this->upload_to = "album_thumb/$ID";
					$reciver = $this->UploadMultiImages();
					if(isset($reciver['Thumb'])){
						foreach($reciver['Thumb'] as $item){
							$cropimage = explode(",",IMAGECROP);
							foreach($cropimage as $row){
								$size = explode("x", $row);
								$width = isset($size[0]) ? (int)$size[0] : 0 ;
								$height = isset($size[1]) ? (int)$size[1] : 0 ;
								if($width>0 && $height>0) $this->lib->cropimage($item,$width,$height);
							}
						}
					}
					if(isset($reciver['ID'])){
						$json = json_decode($result->Data,true);
						$json = is_array($json) ? $json : array() ;
						$merge = array_merge($json,$reciver['ID']);
						$data['Data'] = json_encode($merge);
						$this->db->where("ID",$ID);
						$this->db->update("ttp_album",$data);
						echo "true";
						return;
					}
				}
			}
		}
		echo "false";
	}

	public function move(){
		$Album = $this->uri->segment(5);
		$ID = $this->uri->segment(6);
		$current = $this->uri->segment(7);
		$Move = $this->uri->segment(8);
		if(is_numeric($Album) && $Album>0){
			$result = $this->db->query("select * from ttp_album where ID=$Album")->row();
			if($result){
				$json = json_decode($result->Data,true);
				if(isset($json[$Move])){
					$temp = $json[$Move];
					$json[$Move] = $ID;
					$json[$current] = $temp;
				}
				$data['Data'] = json_encode($json);
				$this->db->where("ID",$Album);
				$this->db->update("ttp_album",$data);
			}
		}
		$link = ADMINPATH.'/home/gallery_album/edit/'.$Album;
		redirect($link);
	}
}
?>