<?php 
class System_ajax extends Admin_Controller { 
	public $user;
	public $classname="home";
	public $limit = 10;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

	public function system_user_loadrole_by_id($ID=0){
		if(is_numeric($ID)){
			$result = $this->db->query("select * from ttp_role where ID=$ID")->row();
			$this->load->view("admin/system_ajax_loadrole_by_id",array("data" => $result));
		}
	}

	public function site_load_view_by_position_banner($ID=0,$lang="vi"){
		if(is_numeric($ID)){
			$view = $lang=="vi" ? "vi" : "en" ;
			$this->load->view("admin/site_ajax_load_view_position",array("ID"=>$ID,"lang"=>$view));
		}
	}

	public function site_load_view_by_ID_banner($ID="",$lang="vi"){
		if(is_numeric($ID)){
			$view = $lang=="vi" ? "vi" : "en" ;
			$data = $this->db->query("select * from ttp_banner where ID=$ID")->row();
			$this->load->view("admin/site_ajax_load_view_id_banner",array("data"=>$data,"lang"=>$view));
		}
	}

	public function site_load_view_brown_images($start=0){
		$start = is_numeric($start) ? $start : 0 ;
		$data = $this->db->query("select ID,Thumb from ttp_images order by ID DESC limit $start,24")->result();
		$this->load->view("admin/site_ajax_load_brown_images",array('data'=>$data,'page'=>$start));
	}

	public function site_load_option_input($name="",$value=""){
		if($name!='' && $value!=''){
			$this->load->view("admin/site_ajax_load_option_input",array("name"=>$name,"value"=>$value));
		}else{
			echo "false";
		}
	}

	public function site_load_alias(){
		$data = isset($_POST['data']) ? $_POST['data'] : "" ;
		$ID = isset($_POST['ID']) ? $_POST['ID'] : "" ;
		$ob = isset($_POST['ob']) ? $_POST['ob'] : "" ;
		$bonus_post = "";
		$bonus_cat = "";
		$bonus_page = "";
		if($ob!=''){
			$bonus_post = $ID>0 && $ob=="post" ? " and ID!=$ID" : "" ;
			$bonus_cat = $ID>0 && $ob=="cat" ? " and ID!=$ID" : "" ;
			$bonus_page = $ID>0 && $ob=="page" ? " and ID!=$ID" : "" ;
			$bonus_tags = $ID>0 && $ob=="tag" ? " and ID!=$ID" : "" ;
		}
		if($data!=''){
			$data = $this->lib->alias($data);
			$check = $this->db->query("select Alias from ttp_pagelinks where (Alias='$data' or Alias_en='$data') $bonus_page")->row();
			if($check){
				$data = $data."-".time();
				echo $data;return;
			}
			$check = $this->db->query("select Alias from ttp_categories where (Alias='$data' or Alias_en='$data') $bonus_cat")->row();
			if($check){
				$data = $data."-".time();
				echo $data;return;
			}
			$check = $this->db->query("select Alias from ttp_tags where Alias='$data' $bonus_tags")->row();
			if($check){
				$data = $data."-".time();
				echo $data;return;
			}
			$check = $this->db->query("select Alias from ttp_post where (Alias='$data' or Alias_en='$data') $bonus_post")->row();
			if($check){
				$data = $data."-".time();
				echo $data;return;
			}
			echo $data;
		}else{
			echo "false";
		}
	}

	public function remove_image_from_album($ID=0,$Album=0){
		if(is_numeric($ID) && $ID>0 && is_numeric($Album) && $Album>0){
			$result = $this->db->query("select * from ttp_album where ID=$Album")->row();
			if($result){
				$json = json_decode($result->Data,true);
				if(($key = array_search($ID, $json)) !== false) {
				    unset($json[$key]);
				}
				$json = json_encode($json);
				$this->db->query("update ttp_album set Data='$json' where ID=$Album");
				echo "true";
			}else{
				echo "false";
			}
		}else{
			echo "false";
		}
	}

	public function show_album_list(){
		$result = $this->db->query("select Title,ID from ttp_album")->result();
		if(count($result)>0){
			foreach($result as $row){
				echo "<p><a onclick='add_album(this)' data='$row->ID'>$row->Title</a></p>";
			}
		}else{
			echo "<p>Album list is empty .</p>";
		}
	}

	public function add_image_from_album($ID=0,$Album=0){
		if(is_numeric($ID) && $ID>0 && is_numeric($Album) && $Album>0){
			$result = $this->db->query("select * from ttp_album where ID=$Album")->row();
			if($result){
				$json = json_decode($result->Data,true);
				if(!in_array($ID,$json)){
					$json[] = $ID;
				}
				$json = json_encode($json);
				$this->db->query("update ttp_album set Data='$json' where ID=$Album");
				echo "true";
			}else{
				echo "false";
			}
		}else{
			echo "false";
		}
	}
}
?>