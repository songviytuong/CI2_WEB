<?php 
class Home extends CI_Controller { 
    public $limit = 20;
    public $upload_to = '';
    public function __construct() { 
        parent::__construct();
        $this->load->library('template'); 
        $this->template->set_template('site');
        $staticconfig = $this->db->query("select * from ttp_static_config where ID=1")->row();
        $logo = $this->db->query("select * from ttp_banner where PositionID=2 and Published=1 order by ID DESC")->row();
        $data = array(
            "static" => $staticconfig,
            "logo"   => $logo
        );
        $this->template->write_view('header','header',$data);
        $this->template->write_view('footer','footer',$data);
        $this->template->add_doctype();
	}

    public function make_folder($str,$folder='assets/'){
        if($str!=''){
            if(!file_exists($folder.$str)){
                mkdir("./$folder".$str, 0777, true);
            }
        }
    }

    public function downloadimage_by_link($image=''){
        $this->upload_to = date('Y').'/'.date('Y-m');
        $this->make_folder($this->upload_to,'assets/');
        @$file = file_get_contents($image);
        if($file!=''){
            $name = explode('/',$image);
            $name = isset($name[count($name)-1]) ? $name[count($name)-1] : '' ;
            if($name!=''){
                file_put_contents("assets/".$this->upload_to.'/'.$name, $file);
                $cropimage = explode(",",IMAGECROP);
                foreach($cropimage as $row){
                    $size = explode("x", $row);
                    $width = isset($size[0]) ? (int)$size[0] : 0 ;
                    $height = isset($size[1]) ? (int)$size[1] : 0 ;
                    if($width>0 && $height>0) $this->lib->cropimage("assets/".$this->upload_to.'/'.$name,$width,$height);
                }
                return "assets/".$this->upload_to.'/'.$name;
            }
        }else{
            return '';
        }
    }

    public function postsite(){
        $getcat = isset($_GET['getcat']) ? $_GET['getcat'] : '' ;
        if($getcat=='true'){
            $result = $this->db->query("select * from ttp_categories")->result();
            if(count($result)>0){
                echo "<ul>";
                foreach($result as $row){
                    echo "<li><input type='checkbox' value='$row->ID' /> $row->Title";
                }
                echo "</ul>";
            }
        }
        $transfer = isset($_GET['trans']) ? $_GET['trans'] : "" ;
        if($transfer=="true"){
            $keyaccept = sha1("@hoathienphu.com.vn");
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "None" ;
            $key = isset($_POST['key']) ? $_POST['key'] : "None" ;
            if($keyaccept==$key && $referer=="http://online.sacngockhang.com/"){
                $post_thumb = isset($_POST['post_thumb']) ? $_POST['post_thumb'] : "" ;
                $post_title = isset($_POST['post_title']) ? $_POST['post_title'] : "" ;
                $post_content = isset($_POST['post_content']) ? $_POST['post_content'] : "" ;
                $post_description = isset($_POST['post_description']) ? $_POST['post_description'] : "" ;
                $post_categories = isset($_POST['post_categories']) ? $_POST['post_categories'] : 0 ;
                if($post_title!='' && $post_content!='' && $post_categories>0){
                    $alias = $this->lib->alias($post_title);
                    $check = $this->db->query("select * from ttp_post where Alias='$alias'")->row();
                    $alias = $check ? $alias.'-'.time() : $alias ;
                    $thumb = $this->downloadimage_by_link($post_thumb);
                    $data = array(
                        'Title'         => $post_title,
                        'Description'   => $post_description,
                        'Introtext'     => $post_content,
                        'CategoriesID'  => $post_categories,
                        'Published'     => 0,
                        'Alias'         => $alias,
                        'Created'       => date('Y-m-d H:i:s'),
                        'LastEdited'    => date('Y-m-d H:i:s'),
                        'Thumb'         => $thumb
                    );
                    $this->db->insert("ttp_post",$data);

                    /*** End Insert ***/
                    echo "true";
                }else{
                    echo "false";
                }
            }else{
                if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARTDED_FOR'] != '') {
                    $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip_address = $_SERVER['REMOTE_ADDR'];
                }
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                @file_put_contents("log_connect.txt","******** Error authentication *********\nTime : ".date("d/m/Y H:i:s",time())."\n"."Referer : $referer \nKey : $key \nIp : $ip_address \n \n",FILE_APPEND);
                echo "false";
            }
        }
    }

    public function accept_deny(){
        $this->load->view('accept_deny');
    }

    public function checkstatus(){
        echo "true";
    }

    public function next_order(){
        $token = $this->session->userdata('token');
        if($token==1){
            $page = isset($_POST['page']) ? (int)$_POST['page'] : 0 ;
            $limit = $page*6;
            $order = $this->db->query("select Name,Ngaydathang,ID,Total,Chiphi,Chietkhau from ttp_report_order order by ID DESC limit $limit,6")->result();
            $orderlist = array();
            $productslist = array();
            if(count($order)>0){
                foreach($order as $row){
                    $orderlist[] = $row->ID;
                }
                $temp = implode(',', $orderlist);
                $products = $this->db->query("select a.Title,b.Amount,b.Total,b.OrderID from ttp_report_products a,ttp_report_orderdetails b where a.ID=b.ProductsID and b.OrderID in($temp)")->result();
                if(count($products)>0){
                    foreach($products as $item){
                        $gift = $item->Total==0 ? 'tặng ' : '';
                        if(isset($productslist[$item->OrderID])){
                            $productslist[$item->OrderID][] = $gift.$item->Amount.' '.$item->Title;
                        }else{
                            $productslist[$item->OrderID] = array();
                            $productslist[$item->OrderID][] = $gift.$item->Amount.' '.$item->Title;
                        }
                    }
                }
                foreach($order as $row){
                    echo '<div class="item-order">
                          <b>'.$row->Name.'</b>
                          <small> đặt hàng cách đây '.$this->lib->get_times_remains($row->Ngaydathang).'</small>';
                    $total = $row->Total-$row->Chietkhau+$row->Chiphi;
                    echo "<p>Đơn hàng trị giá <b>".number_format($total,0,'','.')."đ</b> bao gồm (";
                    echo isset($productslist[$row->ID]) ? implode(', ',$productslist[$row->ID]) : '' ;
                    echo ")</p>";
                    echo '</div>';
                }
            }
        }else{
            redirect("http://google.com");
        }
    }
	
	public function index($lang='vi')
    {
        $token = $this->session->set_userdata('token',1);
        $ob = $this->db->query("select * from ttp_pagelinks where Classname='index' limit 0,1")->row();
        if(!$ob){echo "Not found page index" ;return;}
	    $title = $ob->MetaTitle!='' ? $ob->MetaTitle : $ob->Title ;
        $description = $ob->MetaDescription!='' ? $ob->MetaDescription : $ob->Title ;
        $keywords = $ob->MetaKeywords!='' ? $ob->MetaKeywords : $ob->Title ;
        $banner = $this->db->query("select * from ttp_banner where PositionID=1 and Published=1 order by ID DESC")->result();
        $categories_home = $this->db->query("select * from ttp_categories where Published=1 and Data like '%\"showhome\":\"true\"%' order by ID DESC limit 0,5")->result();
        $categories_special = $this->db->query("select * from ttp_categories where Published=1 and Data like '%\"block\":\"customers\"%'")->row();
        $ID_Special = $categories_special ? $categories_special->ID : 0 ;
        $post_categories_special = $this->db->query("select * from ttp_post where Published=1 and CategoriesID=$ID_Special and Data like '%\"hot\":\"true\"%' order by ID DESC limit 0,3")->result();
        $order = $this->db->query("select Name,Ngaydathang,ID,Total,Chiphi,Chietkhau from ttp_report_order where Total>0 order by ID DESC limit 0,6")->result();
        $products = $this->db->query("select a.Title,a.Description,a.Alias,a.Donvi,a.PrimaryImage as Thumb,a.Price,a.ID from ttp_report_products a,ttp_report_products_website b where a.ID=b.ProductsID and b.Website='http://sacngockhang.com/' order by b.STT ASC")->result();
        $data = array(
            'language'          => $lang,
            'banner'            => $banner,
            'post_categories'   => $categories_home,
            'categories_special'=> $categories_special,
            'post_special'      => $post_categories_special,
            'order'             => $order,
            'products'          => $products
        );
        $this->template->add_title($title);
        $this->template->write('MetaKeywords',$keywords);
        $this->template->write('MetaDescription',$description);
        $this->template->write_view('content','home',$data);
        $this->template->render();
    }

    public function question_and_answer($ob=''){
        if(!is_object($ob)) {
            $ob = $this->db->query("select * from ttp_pagelinks where Classname='question_and_answer'")->row();
            if(!$ob){
                $this->errorpage();
            }
        }
        $title = $ob->MetaTitle!='' ? $ob->MetaTitle : $ob->Title ;
        $description = $ob->MetaDescription!='' ? $ob->MetaDescription : $ob->Title ;
        $keywords = $ob->MetaKeywords!='' ? $ob->MetaKeywords : $ob->Title ;
        $categories = $this->db->query("select * from ttp_categories where PagelinksID=$ob->ID")->result();
        $post = $this->db->query("select * from ttp_post where PagelinksID=$ob->ID and Published=1")->result();
        $this->template->add_title($title);
        $this->template->write('MetaKeywords',$keywords);
        $this->template->write('MetaDescription',$description);
        $this->template->write('MetaExtend','<link rel="canonical" href="'.base_url().$ob->Alias.'.html" />');
        $this->template->write_view('content','question_and_answer',
            array(
                'data'      => $ob,
                'categories'=> $categories,
                'post'      => $post
            )
        );
        $this->template->render();
    }

    public function news($ob){
        $title = $ob->MetaTitle!='' ? $ob->MetaTitle : $ob->Title ;
        $description = $ob->MetaDescription!='' ? $ob->MetaDescription : $ob->Title ;
        $keywords = $ob->MetaKeywords!='' ? $ob->MetaKeywords : $ob->Title ;
        $categories = $this->db->query("select * from ttp_categories where PagelinksID=$ob->ID")->result();
        $this->template->add_title($title);
        $this->template->write('MetaKeywords',$keywords);
        $this->template->write('MetaDescription',$description);
        $this->template->write('MetaExtend','<link rel="canonical" href="'.base_url().$ob->Alias.'.html" />');
        $this->template->write_view('content','news',
            array(
                'data'      =>$ob,
                'categories'=>$categories
            )
        );
        $this->template->render();
    }

    public function products($object){
        $ob = $this->db->query("select a.Title,a.Description,a.Alias,a.Donvi,a.PrimaryImage as Thumb,a.Price,a.ID from ttp_report_products a,ttp_report_products_website b where a.ID=b.ProductsID and a.Status=1 and a.Published=1 and b.Website='http://sacngockhang.com/' order by b.STT ASC")->result();
        $this->template->add_title($object->MetaTitle);
        $this->template->write('MetaKeywords',$object->MetaKeywords);
        $this->template->write('MetaDescription',$object->MetaDescription);
        $this->template->write('MetaExtend','<link rel="canonical" href="'.base_url().$object->Alias.'.html" />');
        $this->template->write_view('content','products',array('data'=>$object,'products'=>$ob));
        $this->template->render();   
    }

    public function details_products(){
        $segment = $this->uri->segment(2);
        $segment = str_replace('.html','', $segment);
        $products = $this->db->query("select a.*,b.Title as TradeMark from ttp_report_products a,ttp_report_trademark b where a.TrademarkID=b.ID and a.Published=1 and a.Status=1 and a.Alias='$segment'")->row();
        if($products){
            $ext = '';
            $ext.= "<meta property='og:title' content='$products->Title' />";
            $ext.= "<meta property='og:description' content='$products->Description' />";
            $ext.= file_exists($products->PrimaryImage) ? "<meta property='og:image' content='".base_url()."$products->PrimaryImage' />" : "" ;
            $HasBuy = $this->db->query("select count(1) as HasBuy from ttp_report_orderdetails where ProductsID=$products->ID")->row();
            $HasBuy = $HasBuy ? $HasBuy->HasBuy : 0 ;
            $Combo = $this->db->query("select count(1) as Combo from ttp_report_orderdetails a,ttp_report_products_bundle b where b.Src_ProductsID=a.ProductsID and b.Des_ProductsID=$products->ID")->row();
            $Combo = $Combo ? $Combo->Combo : 0 ;
            $TotalBuy = $HasBuy+$Combo;
            $this->template->add_title($products->MetaTitle !='' ? $products->MetaTitle : $products->Title);
            $this->template->write('MetaExtend',$ext);
            $this->template->write('MetaKeywords',$products->MetaKeywords);
            $this->template->write('MetaDescription',$products->MetaDescription);
            $this->template->write('MetaExtend','<link rel="canonical" href="'.base_url().'san-pham/'.$products->Alias.'.html" />');
            $this->template->write_view('content','details_products',
                array(
                    'data'      =>  $products,
                    'TotalBuy'  =>  $TotalBuy
                )
            );
            $this->template->render();
        }else{
            redirect('san-pham.html');
        }
    }

    public function checklink(){
        $Alias = $this->uri->uri_string();
        $Alias = str_replace('.html','',$Alias);
        $check = $this->db->query("select * from ttp_post where Alias='$Alias' and Published=1")->row();
        if($check){
            $this->details($check);
        }else{
            $check = $this->db->query("select * from ttp_pagelinks where Alias='$Alias' and Published=1")->row();
            if($check){
                if($check->Classname=='products'){
                    $this->products($check);
                }elseif($check->Classname=='news'){
                    $this->news($check);
                }elseif($check->Classname=='question_and_answer'){
                    $this->question_and_answer($check);
                }
            }else{
                $segment = explode('/',$Alias);
                $page = count($segment)>0 ? $segment[count($segment)-1] : 0 ;
                if($page>0){
                    $Alias = str_replace("/$page","",$Alias);
                }
                $check = $this->db->query("select * from ttp_categories where Alias='$Alias' and Published=1")->row();
                if($check){
                    $this->categories($check);
                }else{
                    $check = $this->db->query("select * from ttp_tags where Alias='$Alias' and Published=1")->row();
                    if($check){
                        $this->tags($check);
                    }else{
                        redirect('');
                    }
                }
            }
        }
    }

    public function details($ob,$lang="vi"){
        $this->db->query("update ttp_post set View = View+1 where ID=$ob->ID");
        if($lang=="vi"){
            $title = $ob->MetaTitle!='' ? $ob->MetaTitle : $ob->Title ;
            $description = $ob->MetaDescription!='' ? $ob->MetaDescription : $ob->Title ;
            $keywords = $ob->MetaKeywords!='' ? $ob->MetaKeywords : $ob->Title ;
        }else{
            $title = $ob->Title ;
            $description = $ob->Title ;
            $keywords = $ob->Title ;
        }
        $ext = $ob->MetaExt!='' ? $ob->MetaExt : "";
        $ext.= "<meta property='og:title' content='$title' />";
        $ext.= "<meta property='og:description' content='$description' />";
        $ext.= file_exists($ob->Thumb) ? "<meta property='og:image' content='".base_url()."$ob->Thumb' />" : "" ;
        $ext.= '<link rel="canonical" href="'.base_url().$ob->Alias.'.html" />';
        $this->template->add_title($title);
        $this->template->write('MetaKeywords',$keywords);
        $this->template->write('MetaDescription',$description);
        $this->template->write('MetaExtend',$ext);
        $banner = $this->db->query("select * from ttp_banner where PositionID=3 and Published=1 order by ID DESC")->row();
        $relative = $this->db->query("select * from ttp_post where Published=1 and CategoriesID=$ob->CategoriesID and Thumb!='' and ID!=$ob->ID and Thumb!='' order by ID DESC limit 0,3")->result();
        if(count($relative)==0){
            $relative = $this->db->query("select * from ttp_post where Published=1 and Thumb!='' and ID!=$ob->ID order by ID DESC limit 0,3")->result();  
        }
        $categories_home = $this->db->query("select * from ttp_categories where Published=1 and Data like '%\"showhome\":\"true\"%' order by ID DESC limit 0,5")->result();
        $data = array(
            'data'      => $ob,
            'relative'  => $relative,
            'post_categories'   => $categories_home,
            'banner'    => $banner
        );
        $this->template->write_view('content','detail',$data);
        $this->template->render();
    }

    public function preview($id=0){
        if(!isset($_SESSION['data'])){
            $_SESSION['data'] = $_POST;
        }
        print_r($_SESSION['data']);
        return;
        $this->template->add_title("PREVIEW POST");
        $data = array(
            'data'=>""
        );
        $this->template->write_view('content','detail',$data);
        $this->template->render();
    }

    public function categories($ob,$lang="vi"){
        $result = $this->db->query("select * from ttp_post where CategoriesID=$ob->ID and Thumb!='' and Published=1")->result();
        $data = array(
            'data'=>$ob,
            'post'=>$result
        );
        $json = json_decode($ob->Data);
        $metatitle = isset($json->metatitle) ? $json->metatitle : "" ;
        $metadescription = isset($json->metadescription) ? $json->metadescription : "" ;
        $metakeywords = isset($json->metakeywords) ? $json->metakeywords : "" ;
        $title = $metatitle!='' ? $metatitle : $ob->Title ;
        $description = $metadescription!='' ? $metadescription : $ob->Title ;
        $keywords = $metakeywords!='' ? $metakeywords : $ob->Title ;
        $this->template->add_title($title);
        $this->template->write('MetaKeywords',$keywords);
        $this->template->write('MetaDescription',$description);
        $this->template->write_view('content','categories',$data);
        $this->template->render();
    }

    public function tags($ob,$lang="vi"){
        if($lang=="en"){
            $link = "en/".$this->uri->segment(2);
            $page = $this->uri->segment(3);
            $langnume = 3;
        }else{
            $link = $this->uri->segment(1);
            $page = $this->uri->segment(2);
            $langnume = 2;
        }
        $start = is_numeric($page) ? $page : 0 ;
        $limit_str = "limit $start,$this->limit";
        if(!is_numeric($start)) $start=0;
        $nav = $this->db->query("select count(1) as nav from ttp_post where Thumb!='' and Published=1 and Tags like '%[$ob->ID]%'")->row();
        $nav = $nav ? $nav->nav : 0 ;
        if($lang=="en"){
            $result = $this->db->query("select ID,Title_en as Title,Description_en as Description,Alias_en as Alias,Thumb,Created from ttp_post where Thumb!='' and Published=1 and Tags like '%[$ob->ID]%' order by ID DESC $limit_str")->result();
        }else{
            $result = $this->db->query("select * from ttp_post where Thumb!='' and Published=1 and Tags like '%[$ob->ID]%' order by ID DESC $limit_str")->result();
        }
        $data = array(
            'categories'=> $ob,
            'data'      => $result,
            'result'    => $nav,
            'nav'       => $this->lib->nav(base_url().$link,$langnume,$nav,$this->limit),
            'lang'      => $lang
        );
        $json = json_decode($ob->Data);
        $metatitle = isset($json->metatitle) ? $json->metatitle : "" ;
        $metadescription = isset($json->metadescription) ? $json->metadescription : "" ;
        $metakeywords = isset($json->metakeywords) ? $json->metakeywords : "" ;
        $title = $metatitle!='' ? $metatitle : $ob->Title ;
        $description = $metadescription!='' ? $metadescription : $ob->Title ;
        $keywords = $metakeywords!='' ? $metakeywords : $ob->Title ;
        $this->template->add_title($title);
        $this->template->write('MetaKeywords',$keywords);
        $this->template->write('MetaDescription',$description);
        $this->template->write_view('content','tags',$data);
        $this->template->render();
    }

    public function errorpage(){
        $this->load->view('404');
    }
}
?>