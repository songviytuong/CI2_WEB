<div class="row">
  <div class="col-xs-12">
    <div class="embed-responsive embed-responsive-16by9">
      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/pOlHWy9wlNg"></iframe>
    </div>
    <br>
  </div>
  <div class="col-xs-4">
    <div class="embed-responsive embed-responsive-16by9">
        <iframe src="https://www.youtube.com/embed/7FP0R66VkZU" allowfullscreen></iframe>
    </div>
  </div>
  <div class="col-xs-4">
    <div class="embed-responsive embed-responsive-16by9">
        <iframe src="https://www.youtube.com/embed/6gaH8vfYYXo" allowfullscreen></iframe>
    </div>
  </div>
  <div class="col-xs-4">
    <div class="embed-responsive embed-responsive-16by9">
        <iframe src="https://www.youtube.com/embed/r5HHapANIyo" allowfullscreen></iframe>
    </div>
  </div>
</div> 