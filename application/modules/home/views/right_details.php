<div class="row">
    <div class="col-xs-12">
        <?php 
        if($banner){
            $jsondata = json_decode($banner->Data);
            echo '<a href="'.$jsondata->Link.'"><img src="'.CDN.$banner->Thumb.'" class="img-responsive" /></a>';
        }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <h3>CLIP VỀ CHÚNG TÔI</h3>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/pOlHWy9wlNg"></iframe>
        </div>
        <br>
    </div>
</div>
<?php 
    if(count($post_categories)>0){
        foreach($post_categories as $row){
            echo '<div class="row"><div class="col-xs-12"><h3>'.$row->Title.'</h3><hr></div></div>';
            $post = $this->db->query("select Alias,Title,Thumb,CategoriesID from ttp_post where Published=1 and CategoriesID=$row->ID order by ID DESC limit 0,3")->result();
            if(count($post)>0){
                echo "<div class='row'>";
                foreach($post as $item){
                    echo '<div class="col-xs-12 col-sm-4 col-md-12" style="margin-bottom:10px;">
                            <div class="row">
                                <div class="col-xs-4 col-sm-12 col-md-5">
                                    <img src="'.CDN.$item->Thumb.'" alt="'.$item->Title.'" class="img-responsive">
                                </div>
                                <div class="col-xs-8 col-sm-12 col-md-7">
                                    <p style="margin-top:5px;"><a class="color-base" title="'.$item->Title.'" href="'.$item->Alias.'.html">'.$item->Title.'</a></p>
                                </div>
                            </div>
                        </div>';
                }
                echo "</div>";
            }
        }
    }
    ?>