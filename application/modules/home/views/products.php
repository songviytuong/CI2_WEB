<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 breakcrumb">
                <a href="<?php echo base_url() ?>"><span class="glyphicon glyphicon-home"></span></a>
                <a href="<?php echo base_url() ?>">Trang chủ</a>
                <a><span class="glyphicon glyphicon-menu-right"></span></a>
                <a href="<?php echo $data->Alias ?>"><?php echo $data->Title ?></a>
            </div>
        </div>
    </div>
</div>
<?php 
if(count($products)>0){
?>
<div class="content bg-white">
    <div class="container">
        <div class="row">
            <?php 
            foreach($products as $key=>$row){
              if($key==2) break;
              $price = number_format($row->Price,0,'','.');
            ?>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-5">
                        <a href="<?php echo $data->Alias.'/'.$row->Alias ?>"><img src="<?php echo CDN.$row->Thumb ?>" class="img-responsive w100" /></a>
                    </div>
                    <div class="col-xs-12 col-sm-7">
                        <h3><?php echo $row->Title ?></h3>
                        <p class="price"><b><?php echo $price ?>đ</b> / <?php echo $row->Donvi ?></p>
                        <p><?php echo $row->Description ?></p>
                        <br>
                        <p><button type="button" onclick="add_to_cart(this,'add_cart',<?php echo $row->ID ?>,1)" class="btn-lg btn-danger">Đặt mua sản phẩm này</button></p>
                    </div>
                </div>
            </div>
            <?php 
            }
            ?>
        </div>
    </div>
</div>
<?php 
}

if(count($products)>2){
?>
<div class="content">
    <div class="container">
        <div class="row">
            <?php 
            foreach($products as $key=>$row){
              if($key<2) continue;
              $price = number_format($row->Price,0,'','.');
            ?>
            <div class="col-xs-12 col-sm-12 col-md-4">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-12">
                        <a href="<?php echo 'san-pham/'.$row->Alias ?>"><img src="<?php echo CDN.$row->Thumb ?>" class="img-responsive w100" /></a>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-12">
                        <h3><?php echo $row->Title ?></h3>
                        <p class="price"><b><?php echo $price ?>đ</b> / <?php echo $row->Donvi ?></p>
                        <p><?php echo $row->Description ?></p>
                        <br>
                        <p><button onclick="add_to_cart(this,'add_cart',<?php echo $row->ID ?>,1)" class="btn-lg btn-danger">Đặt mua sản phẩm này</button></p>
                    </div>
                </div>
            </div>
            <?php 
            }
            ?>
        </div>
    </div>
</div>
<?php 
}
?>