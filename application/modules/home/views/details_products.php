<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 breakcrumb">
                <a href="<?php echo base_url() ?>"><span class="glyphicon glyphicon-home"></span></a>
                <a href="<?php echo base_url() ?>">Trang chủ</a>
                <a><span class="glyphicon glyphicon-menu-right"></span></a>
                <a href="<?php echo base_url().'san-pham' ?>">Sản phẩm</a>
                <a><span class="glyphicon glyphicon-menu-right"></span></a>
                <a href="<?php echo 'san-pham/'.$data->Alias ?>"><?php echo $data->Title ?></a>
            </div>
        </div>
    </div>
</div>

<div class="content bg-white">
    <div class="container">
        <div class="row details_products_box">
            <div class="col-xs-12 col-sm-5 col-md-5">
                <img src="<?php echo CDN.$data->PrimaryImage ?>" alt="" class="img-responsive">
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 info_details_products_box">
                <h1><?php echo $data->Title ?></h1>
                <p>Thương hiệu : <?php echo $data->TradeMark ?></p>
                <p><?php echo $data->Description ?></p>
                <hr>
                <div class="row">
                    <div class="col-xs-8 col-sm-8 col-md-7">
                        <div class="price">
                            <b><?php echo number_format($data->Price,0,'','.') ?>đ</b> / <?php echo $data->Donvi ?>
                        </div>
                        <div class="add_cart">
                            <button onclick="add_to_cart(this,'add_cart',<?php echo $data->ID ?>,1)" class="btn-lg btn-danger">ĐẶT MUA SẢN PHẨM NÀY</button>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-5">
                        <div class="has_buy">
                            <b><?php echo number_format($TotalBuy,0,'','.') ?></b>
                            <small>đã mua</small>
                        </div>
                    </div>
                </div>

                <div class="alert alert-success"><i class="fa fa-truck" aria-hidden="true"></i> Giao hàng miễn phí toàn quốc cho đơn hàng từ <b>500.000đ</b> khi đặt hàng trực tiếp trên website.</div>
                <p>Mọi thông tin thắc mắc về đặt hàng hoặc hỗ trợ tư vấn sản phẩm vui lòng liên hệ tổng đài số <b class="text-danger">19006033</b>.</p>
            </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php echo str_replace('src="/assets/','src="'.CDN.'/assets/', $data->Content) ?>
            </div>
        </div>
    </div>
</div>
<div class="content bg-white">
    <div class="container">
        <div class="row details_products_box">
            <div class="col-xs-12 col-sm-5 col-md-5">
                <img src="<?php echo CDN.$data->PrimaryImage ?>" alt="" class="img-responsive">
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 info_details_products_box">
                <h1><?php echo $data->Title ?></h1>
                <p>Thương hiệu : <?php echo $data->TradeMark ?></p>
                <p><?php echo $data->Description ?></p>
                <hr>
                <div class="row">
                    <div class="col-xs-8 col-sm-8 col-md-7">
                        <div class="price">
                            <b><?php echo number_format($data->Price,0,'','.') ?>đ</b> / <?php echo $data->Donvi ?>
                        </div>
                        <div class="add_cart">
                            <button onclick="add_to_cart(this,'add_cart',<?php echo $data->ID ?>,1)" class="btn-lg btn-danger">ĐẶT MUA SẢN PHẨM NÀY</button>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-5">
                        <div class="has_buy">
                            <b><?php echo number_format($TotalBuy,0,'','.') ?></b>
                            <small>đã mua</small>
                        </div>
                    </div>
                </div>
                <div class="alert alert-success"><i class="fa fa-truck" aria-hidden="true"></i> Giao hàng miễn phí toàn quốc cho đơn hàng từ <b>500.000đ</b> khi đặt hàng trực tiếp trên website.</div>
                <p>Mọi thông tin thắc mắc về đặt hàng hoặc hỗ trợ tư vấn sản phẩm vui lòng liên hệ tổng đài số <b class="text-danger">19006033</b>.</p>
            </div>
        </div>
    </div>
</div>