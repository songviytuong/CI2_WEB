<?php 
  $json = $static ? json_decode($static->Data) : (object) array() ;
?>
<div class="content bg-white block-list-info">
    <div class="container">
        <div class="row hidden-xs hidden-sm">
            <div class="col-xs-12 col-sm-3 col-md-3">
                <i class="fa fa-check-circle-o"></i>
                <div class="description media-body">
                    <h4>CHẤT LƯỢNG</h4>
                    <p>Đem đến cho người dùng chất lượng sản phẩm tốt nhất&nbsp;<br></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3">
                <i class="fa fa-shopping-cart"></i>
                <div class="description media-body">
                    <h4>LỰA CHỌN</h4>
                    <p>Sản phẩm phân phối trực tiếp đến tay người tiêu dùng.<br></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3">
                <i class="fa fa-truck"></i>
                <div class="description media-body">
                    <h4>GIAO HÀNG</h4>
                    <p>Giao hàng cho tất cả các đơn hàng trên toàn quốc</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3">
                <i class="fa fa-question-circle"></i>
                <div class="description media-body">
                    <h4>TƯ VẤN</h4>
                    <p>Hổ trợ tư vấn khách hàng qua tổng đài <b class="text-danger"><?php echo isset($json->Hotline) ? $json->Hotline : '' ; ?></b></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="fb-page" data-href="https://www.facebook.com/sacngockhang/" data-tabs="timeline" data-height="220" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/sacngockhang/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/sacngockhang/">Sắc Ngọc Khang</a></blockquote></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <img src="public/site/images/logo-htp.png" class="img-responsive" />
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <img src="<?php echo $logo ? CDN.$logo->Thumb : '' ; ?>" class="img-responsive" />
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <img src="http://online.gov.vn/Images/dathongbao.png" class="img-responsive" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <hr>
                        <p>Địa chỉ trụ sở : số 10 Nguyễn Cửu Đàm, phường Tân Sơn Nhì, quận Tân Phú, TPHCM.</p>
                        <p>Tổng đài tư vấn / đặt hàng : <b class="text-danger"><?php echo isset($json->Hotline) ? $json->Hotline : '' ; ?></b></p>
                        <p>MST/ĐKKD/QĐTL : 0307205818</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a id="alo-phoneIcon" data-toggle="modal" data-target="#callme-modal" class="alo-phone alo-green alo-show">
    <div class="alo-ph-circle"></div>
    <div class="alo-ph-circle-fill"></div>
    <div class="alo-ph-img-circle"><span class="glyphicon glyphicon-earphone"></span></div><span class="alo-ph-text"><?php echo isset($json->Hotline) ? $json->Hotline : '' ; ?></span>
</a>
<a class="messenger_icon" href="https://www.messenger.com/t/141058836006826" target="_blank">Nhắn tin với chúng tôi</a>
<div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10&appId=196633390834520";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>