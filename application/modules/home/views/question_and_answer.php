<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 breakcrumb">
                <a href="<?php echo base_url() ?>"><span class="glyphicon glyphicon-home"></span></a>
                <a href="<?php echo base_url() ?>">Trang chủ</a>
                <a><span class="glyphicon glyphicon-menu-right"></span></a>
                <a href="<?php echo $data->Alias ?>"><?php echo $data->Title ?></a>
            </div>
        </div>
    </div>
</div>
<div class="content bg-white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-7">
                <img src="public/site/images/expert-img.png" class="img-responsive" />
            </div>
            <div class="col-xs-12 col-sm-6 col-md-5 bg-eee">
                <div class="row">
                  <div class="col-xs-12"><h3 class="title" id="info"><i class="fa fa-info" style="margin-right:10px;"></i> VẤN ĐỀ CỦA BẠN</h3><p>Bạn đang gặp vấn đề về da như nám sạm, tàn nhang,... ? Đừng lo lắng, hãy để lại thông tin dưới đây tổng đài <b class="text-danger">19006033</b> của chúng tôi sẽ gọi lại trực tiếp tư vấn cho bạn .<br><small>(*) Mọi thông tin mà bạn nhập vào chúng tôi cam kết sẽ bảo mật tuyệt đối.</small></p><hr></div>
                </div>
                <div class="row">
                  <form id="form-callme">
                    <div class="hidden col-xs-12" id="box-alert-callme">
                      <div class="alert alert-danger"><span></span></div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                      <p>Họ và tên của bạn (*)</p>
                      <input type="text" class="form-control" name="Name" placeholder="" />
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                      <p>Số điện thoại (*)</p>
                      <input type="text" class="form-control" name="Phone" placeholder="" />
                      <br>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <p>Mô tả ngắn về vấn đề của bạn (*)</p>
                      <textarea class="form-control" rows="5" name="Note"></textarea>
                      <br>
                      <a type="type" class="btn-lg btn-success" onclick="send_callme(this,'form-callme','send_callme')">Gửi cho chúng tôi</a>
                    </div>
                  </form>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <h3>Tổng hợp những câu hỏi thường gặp</h3>
            <p style="font-size:15px;">Kiểm tra xem tình trạng bạn đang gặp phải có nằm trong danh sách dưới đây hay không ? Nếu không có bạn vui lòng điền vào form bên trên để được tổng đài của chúng tôi gọi lại tư vấn hỗ trợ hoặc gọi trực tiếp vào tổng đài số <b class="text-danger">19006033</b> để được tư vấn ngay.</p>
            <p>(*) Nhấp vào tiêu đề câu hỏi để xem nội dung giải đáp.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <?php 
              if(count($post)>0){
                  foreach($post as $key=>$row){
                    $active = $key==0 ? 'in' : '' ;
              ?>
                    <div class="panel panel-success">
                      <div class="panel-heading" role="tab" id="heading<?php echo $key ?>">
                        <h4 class="panel-title">
                          <a style="display:block;" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $key ?>" aria-expanded="true" aria-controls="collapse<?php echo $key ?>">
                            <i class="fa fa-info"></i>
                            <?php echo $row->Title ?>
                          </a>
                        </h4>
                      </div>
                      <div id="collapse<?php echo $key ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $key ?>">
                        <div class="panel-body">
                          <?php echo $row->Introtext ?>
                        </div>
                      </div>
                    </div>
              <?php 
                  }
              }
              ?>
            </div>
          </div>
          <div class="col-xs-12 text-center">
            <button onclick="$('input[name=\'Name\']').focus()" class="btn-lg btn-danger"><i class="fa fa-arrow-up"></i> TÔI GẶP VẤN ĐỀ KHÁC</button>
          </div>
        </div>
    </div>
</div>