<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 breakcrumb">
                <a href="<?php echo base_url() ?>"><span class="glyphicon glyphicon-home"></span></a>
                <a href="<?php echo base_url() ?>">Trang chủ</a>
                <a><span class="glyphicon glyphicon-menu-right"></span></a>
                <a href="<?php echo $data->Alias ?>.html"><?php echo $data->Title ?></a>
            </div>
        </div>
    </div>
</div>
<div class="content bg-white news_box">
    <div class="container">
        <div class="row">
            <?php 
            if(count($post)>0){
                foreach($post as $item){
                    echo '<div class="col-xs-6 col-sm-4 col-md-3">';
                    echo '<a href="'.$item->Alias.'.html"><img class="img-responsive" src="'.CDN.$item->Thumb.'" alt="'.$item->Title.'" /></a>';
                    echo '<p style="margin-top:10px"><a href="'.$item->Alias.'.html">'.$item->Title.'</a></p>';
                    echo '<p>'.$this->lib->splitString($item->Description,110).'</p>';
                    echo '</div>';
                }
            }
            ?>
        </div>
    </div>
</div>