<?php 
  $arr_ID = "$data->ID";
?>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 breakcrumb">
                <a href="<?php echo base_url() ?>"><span class="glyphicon glyphicon-home"></span></a>
                <a href="<?php echo base_url() ?>">Trang chủ</a>
                <a><span class="glyphicon glyphicon-menu-right"></span></a>
                <?php 
                if($data->CategoriesID>0){
                    $categories = $this->db->query("select Title,Alias from ttp_categories where ID=$data->CategoriesID")->row();
                    if($categories){
                        $link = $categories->Alias ;
                        echo "<a href='$link.html'>$categories->Title</a>";
                    }
                }
                ?>
                <a><span class="glyphicon glyphicon-menu-right"></span></a>
                <a class="active"><?php echo $data->Title ?></a>
            </div>
        </div>
    </div>
</div>
<div class="content bg-white">
    <div class="container">
        <div class="row">
            <div class="main-content col-xs-12 col-sm-12 col-md-8">
              <div class="detail-content">
                <h3 class="post-title" style="font-size: 26px;"><?php echo $data ? $data->Title : "" ; ?></h3>
                <div class="top-meta">
                  <div class="meta-info">
                    <p><span class="post-date"><?php echo $data ? date('d/m/Y H:i:s',strtotime($data->Created)) : "" ; ?></span></p>
                  </div>
                  <div class="default-share"></div>
                </div>
                <div class="post-content-wrap" style="text-align: justify;">
                  <p class="post-excerpt"><?php echo $data->Description ?></p>
                  <?php
                  $json = json_decode($data->Data);
                  if(isset($json->video)){
                      $video = isset($json->video) ? '<div class="embed-responsive embed-responsive-16by9" style="margin-bottom:15px"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$json->video.'" allowfullscreen></iframe></div>' : "" ;
                      echo $video;
                  }
                  ?>
                  <div class="post-content">
                    <?php 
                    $data->Introtext = str_replace(' src=',' class="img-thumbnail img-responsive" src=',$data->Introtext);
                    echo str_replace(' src="/assets/',' src="'.CDN.'assets/',$data->Introtext);
                    ?>
                  </div>
                </div>
                <div class="post-source-tags">
                    <?php 
                    if($data->Tags!=''){
                        $data->Tags = str_replace("[","",$data->Tags);
                        $data->Tags = str_replace("]","",$data->Tags);
                        $tags = $data->Tags;
                        if($tags!=''){
                            $result = $this->db->query("select * from ttp_tags where ID in($tags)")->result();
                            if(count($result)>0){
                                echo '<ul class="post-tags post-small-box list-unstyled"><li><span>TAGS</span></li>';
                                foreach($result as $item){
                                    echo "<li><a href='$item->Alias.html'>$item->Title</a></li>";
                                }
                                echo '</ul>';
                            }
                        }
                    }
                    ?>
                </div>
                <div class="post-source">
                  <?php 
                  $marketing = $this->db->query("select a.* from ttp_post a,ttp_categories b,ttp_pagelinks c where a.CategoriesID=b.ID and b.PagelinksID=c.ID and c.Classname='marketing' and a.Published=1")->row();
                    $Introtext = $marketing ? str_replace(' src=',' class="img-thumbnail img-responsive" src=',$marketing->Introtext) : "";
                    echo $Introtext=='' ? '' : '<div class="alert alert-info">'.str_replace(' src="/assets/',' src="'.CDN.'assets/',$Introtext).'</div>';
                    ?>
                </div>
              </div>
              <!-- End detail-content-->
              <div class="relate-post main-content-box">
                <hr>
                <h3 class="blog-box-title"><span>Bài viết liên quan</span></h3>
                <div class="row">
                  <?php 
                  if(count($relative)>0){
                      foreach ($relative as $row) {
                          $json = json_decode($row->Data);
                          $link = isset($json->changelink) ? $json->changelink : $row->Alias ;
                          $image = CDN.$row->Thumb;
                          echo '<div class="col-xs-4 post-item"><a href="'.$link.'" class="post-img"><img alt="'.$row->Title.'" src="'.$image.'" class="img-responsive"></a>
                            <a style="display:block;margin-top:10px;margin-bottom:5px;" href="'.$link.'.html">'.$row->Title.'</a>
                            <p class="hidden-xs">'.$this->lib->splitString($row->Description,100).'</p>
                          </div>';
                          $arr_ID.=",".$row->ID;
                      }
                  }
                  ?>
                  
                </div>
              </div>
              <!-- End relate-post    -->
            </div>
            <div class="sidebar col-xs-12 col-sm-12 col-md-4">
                <?php 
                $this->load->view('right_details');
                ?>
            </div>
        </div>
    </div>
</div>