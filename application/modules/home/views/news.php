<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 breakcrumb">
                <a href="<?php echo base_url() ?>"><span class="glyphicon glyphicon-home"></span></a>
                <a href="<?php echo base_url() ?>">Trang chủ</a>
                <a><span class="glyphicon glyphicon-menu-right"></span></a>
                <a href="<?php echo $data->Alias ?>.html"><?php echo $data->Title ?></a>
            </div>
        </div>
    </div>
</div>
<div class="content bg-white news_box">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-7">
               <?php $this->load->view("primary_categories"); ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-5 bg-eee">
                <div class="row">
                  <div class="col-xs-12"><h3 class="title"><i class="fa fa-info" style="margin-right:10px;"></i> VẤN ĐỀ CỦA BẠN</h3><p>Bạn đang gặp vấn đề về da như nám sạm, tàn nhang,... ? Đừng lo lắng, hãy để lại thông tin dưới đây tổng đài <b class="text-danger">19006033</b> của chúng tôi sẽ gọi lại trực tiếp tư vấn cho bạn .<br><small>(*) Mọi thông tin mà bạn nhập vào chúng tôi cam kết sẽ bảo mật tuyệt đối.</small></p><hr></div>
                </div>
                <div class="row">
                  <form id="form-callme">
                    <div class="hidden col-xs-12" id="box-alert-callme">
                      <div class="alert alert-danger"><span></span></div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                      <p>Họ và tên của bạn (*)</p>
                      <input type="text" class="form-control" name="Name" placeholder="" />
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                      <p>Số điện thoại (*)</p>
                      <input type="text" class="form-control" name="Phone" placeholder="" />
                      <br>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <p>Mô tả ngắn về vấn đề của bạn (*)</p>
                      <textarea class="form-control" rows="5" name="Note"></textarea>
                      <br>
                      <a type="type" class="btn-lg btn-success" onclick="send_callme(this,'form-callme','send_callme')">Gửi cho chúng tôi</a>
                    </div>
                  </form>
                </div>
            </div>
        </div>
        <?php 
        if(count($categories)>0){
            foreach($categories as $key=>$row){
                echo '<div class="row"><div class="col-xs-12"><h3>'.$row->Title.'</h3><hr></div></div>';
                echo '<div class="row">';
                $limit = $key<1 ? 12 : 4 ;
                $post = $this->db->query("select * from ttp_post where CategoriesID=$row->ID and Published=1 and Thumb!='' order by ID DESC limit 0,$limit")->result();
                if(count($post)>0){
                    foreach($post as $item){
                        echo '<div class="col-xs-6 col-sm-4 col-md-3">';
                        echo '<a href="'.$item->Alias.'.html"><img class="img-responsive" src="'.CDN.$item->Thumb.'" alt="'.$item->Title.'" /></a>';
                        echo '<p style="margin-top:10px"><a href="'.$item->Alias.'.html">'.$item->Title.'</a></p>';
                        echo '<p>'.$this->lib->splitString($item->Description,110).'</p>';
                        echo '</div>';
                    }
                }
                echo '</div>';
            }
        }
        ?>
    </div>
</div>