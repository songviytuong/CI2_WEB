<div class="slider">
    <div class="container-fluid">
        <div class="row">
            <?php
            if(count($banner)>0){
                foreach($banner as $row){
                    $json = json_decode($row->Data);
                    $Title= isset($json->Title) ? $json->Title : "" ;
                    $Description= isset($json->Description) ? $json->Description : "" ;
                    $Link= isset($json->Link) ? $json->Link : "" ;
                    echo "<a href='$Link'><img src='".CDN."$row->Thumb' alt='$Title' class='img-responsive w100' /></a>";
                }
            }
            ?>
        </div>
    </div>
</div>
<?php 
if(count($products)>0){
?>
<div class="content bg-white">
    <div class="container">
        <div class="row">
            <?php 
            foreach($products as $key=>$row){
              if($key==2) break;
              $price = number_format($row->Price,0,'','.');
            ?>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-5">
                        <a href="<?php echo 'san-pham/'.$row->Alias ?>.html"><img src="<?php echo CDN.$row->Thumb ?>" class="img-responsive w100" /></a>
                    </div>
                    <div class="col-xs-12 col-sm-7">
                        <h3><?php echo str_replace("Sắc Ngọc Khang","<br>Sắc Ngọc Khang",$row->Title) ?></h3>
                        <p class="price"><b><?php echo $price ?>đ</b> / <?php echo $row->Donvi ?></p>
                        <p><?php echo $row->Description ?></p>
                        <br>
                        <p><button type="button" onclick="add_to_cart(this,'add_cart',<?php echo $row->ID ?>,1)" class="btn-lg btn-danger">Đặt mua sản phẩm này</button></p>
                    </div>
                </div>
            </div>
            <?php 
            }
            ?>
        </div>
    </div>
</div>
<?php 
}

if(count($products)>2){
?>
<div class="content">
    <div class="container">
        <div class="row">
            <?php 
            foreach($products as $key=>$row){
              if($key<2) continue;
              $price = number_format($row->Price,0,'','.');
            ?>
            <div class="col-xs-12 col-sm-12 col-md-4">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-12">
                        <a href="<?php echo 'san-pham/'.$row->Alias ?>.html"><img src="<?php echo CDN.$row->Thumb ?>" class="img-responsive w100" /></a>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-12">
                        <h3><?php echo $row->Title ?></h3>
                        <p class="price"><b><?php echo $price ?>đ</b> / <?php echo $row->Donvi ?></p>
                        <p><?php echo $row->Description ?></p>
                        <br>
                        <p><button onclick="add_to_cart(this,'add_cart',<?php echo $row->ID ?>,1)" class="btn-lg btn-danger">Đặt mua sản phẩm này</button></p>
                    </div>
                </div>
            </div>
            <?php 
            }
            ?>
        </div>
    </div>
</div>
<?php 
}
?>
<div class="content bg-white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="text-center"><?php echo $categories_special ? $categories_special->Title : '' ?></h2>
                <p class="text-center" style="margin-bottom:50px;"><?php echo $categories_special ? $categories_special->Description : '' ?></p>
            </div>
        </div>
        <div class="row">
            <?php 
            if(count($post_special)>0){
                foreach($post_special as $row){
            ?>
            <div class="col-sm-12 col-sm-4 col-md-4">
                <div class="col-xs-8 col-sm-8 col-md-6 col-md-offset-3 col-sm-offset-2 col-xs-offset-2">
                    <img src="<?php echo CDN.$row->Thumb ?>" alt="" class="img-responsive img-circle">
                </div>
                <div class="clearfix"></div>
                <h3 class="text-center"><?php echo $row->Title ?></h3>
                <p class="text-center"><?php echo $row->Description ?></p>
            </div>
            <?php 
                }
            }
            ?>
        </div>
    </div>
</div>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <?php 
                if(count($post_categories)>0){
                    foreach($post_categories as $row){
                        echo '<div class="row"><div class="col-xs-12"><h3>'.$row->Title.'</h3></div></div>';
                        $post = $this->db->query("select Alias,Title,Thumb,CategoriesID from ttp_post where Published=1 and CategoriesID=$row->ID order by ID DESC limit 0,3")->result();
                        if(count($post)>0){
                            echo '<div class="row">';
                            foreach($post as $item){
                                echo '<div class="col-xs-4 col-sm-4 col-md-4">
                                  <img src="'.CDN.$item->Thumb.'" alt="'.$item->Title.'" class="img-responsive">
                                  <p style="margin-top:5px;"><a class="color-base" title="'.$item->Title.'" href="'.$item->Alias.'.html">'.$item->Title.'</a></p>
                              </div>';
                            }
                            echo '</div>';
                        }
                    }
                }
                ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>CLIP VỀ CHÚNG TÔI</h3>
                    </div>
                </div>
                <?php $this->load->view("primary_categories"); ?>
            </div>
        </div>
    </div>
</div>
<div class="content bg-white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <h4 style="margin-top: 0px;">ĐỊA CHỈ TRỤ SỞ CHÍNH</h4>
                <img src="public/site/images/map.jpg" alt="" class="img-responsive" style="margin-bottom: 20px;">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <h4 style="margin-top: 0px;">CẢM ƠN KHÁCH HÀNG</h4>
                <div class="box-item-order" id="box-item-order">
                    <?php 
                    $orderlist = array();
                    $productslist = array();
                    if(count($order)>0){
                        foreach($order as $row){
                            $orderlist[] = $row->ID;
                        }
                        $temp = implode(',', $orderlist);
                        $products = $this->db->query("select a.Title,b.Amount,b.Total,b.OrderID from ttp_report_products a,ttp_report_orderdetails b where a.ID=b.ProductsID and b.OrderID in($temp)")->result();
                        if(count($products)>0){
                            foreach($products as $item){
                                $gift = $item->Total==0 ? 'tặng ' : '';
                                if(isset($productslist[$item->OrderID])){
                                    $productslist[$item->OrderID][] = $gift.$item->Amount.' '.$item->Title;
                                }else{
                                    $productslist[$item->OrderID] = array();
                                    $productslist[$item->OrderID][] = $gift.$item->Amount.' '.$item->Title;
                                }
                            }
                        }
                        foreach($order as $row){
                            echo '<div class="item-order">
                                  <b>'.$row->Name.'</b>
                                  <small> đặt hàng cách đây '.$this->lib->get_times_remains($row->Ngaydathang).'</small>';
                            $total = $row->Total-$row->Chietkhau+$row->Chiphi;
                            echo "<p>Đơn hàng trị giá <b>".number_format($total,0,'','.')."đ</b> bao gồm (";
                            echo isset($productslist[$row->ID]) ? implode(', ',$productslist[$row->ID]) : '' ;
                            echo ")</p>";
                            echo '</div>';
                        }
                    }
                    ?>
                </div>
                <button class="btn btn-primary" style="margin-top:10px;" onclick="load_box_check_order(this,'load_box_check_order')"><span class="glyphicon glyphicon-map-marker"></span> Kiểm tra đơn hàng</button>
            </div>
        </div>
    </div>
</div>
