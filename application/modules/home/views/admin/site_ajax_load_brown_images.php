<?php 
if(count($data)>0){
	$i=0;
	foreach($data as $row){
		if(file_exists($row->Thumb)){
			$image = $this->lib->getfilesize($row->Thumb,270,151);
			echo "<div class='col-md-2 col-sm-2 col-xs-2' style='margin:10px 0px'>";
			echo "<a onclick='choose_image_from_server(this)' data='$row->Thumb'><img src='$image' class='img-responsive' /></a>";
			echo "</div>";
			$i++;
		}
	}
}
$max = $this->db->query("select count(1) as ID from ttp_images")->row();
$next = $max->ID>($page+24) ? $page+24 : $page;
$stop = $page+$i;
$classnext = $max->ID>($page+24) ? '' : 'class="disabled"' ;
?>
<div class="position_nav">
	<nav style="text-align:center">
		<ul class='pagination'>
			<li><a onclick="load_image_in_page(<?php echo $page==0 ? 0 : $page-24 ; ?>)">« Prev</a></li>
			<li <?php echo $classnext ?>><a onclick="load_image_in_page(<?php echo $next ?>)">Next »</a></li>
		</ul>
	</nav>
	<div class="col-md-12 col-sm-12 col-xs-12" style='text-align:center'><small>Image <?php echo $page." - ".$stop ?> / Total <?php echo $max->ID ?> images</small></div>
</div>