<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="title-header">Điều chỉnh thông tin trang</h3>
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update" ?>" method="post">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel tab1 tabcontent">
                                <div class="x_content">
                                    <br>
                                        <input type='hidden' name="ID" value="<?php echo $data->ID ?>" />
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Mã trang <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="ClassName" value="<?php echo $data->Classname ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Đường dẫn đến trang 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="Link" value="<?php echo $data->Alias ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tiêu đề trang <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" required="required" class="form-control col-md-7 col-xs-12" name="Title" value="<?php echo $data->Title ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Trạng thái kích hoạt</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div>
                                                    <label style="padding-top:7px">
                                                        <input type="radio" name="Published" value="1" <?php echo $data->Published==1 ? "checked='checked'" : '' ; ?>> &nbsp; Kích hoạt &nbsp;
                                                        <input type="radio" name="Published" value="0" <?php echo $data->Published==0 ? "checked='checked'" : '' ; ?>> Ngưng kích hoạt
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Hiển thị ngay sau trang <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="STT" class="form-control">
                                                    <option value='0'>-- Hiển thị đầu tiên --</option>
                                                    <?php 
                                                        $result = $this->db->query("select STT,Title from ttp_pagelinks order by STT ASC")->result();
                                                        if(count($result)>0){
                                                            foreach($result as $row){
                                                                $before = $data->STT-1==$row->STT ? "selected='selected'" : "" ;
                                                                echo "<option value='$row->STT' $before>$row->Title</option>";
                                                                
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                </div>
                                <div class="x_title">
                                    <h3>Các thẻ mô tả cho công cụ tìm kiếm</h3>
                                    <hr>
                                </div>
                                <div class="x_content">
                                    <br />
                                    
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Thẻ tiêu đề
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="MetaTitle" value="<?php echo $data->MetaTitle ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Thẻ từ khóa</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="resizable_textarea form-control" name="MetaKeywords" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo $data->MetaKeywords ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Thẻ mô tả ngắn</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="resizable_textarea form-control" name="MetaDescription" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo $data->MetaDescription ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Thẻ mở rộng khác</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="resizable_textarea form-control" name="MetaExt" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo $data->MetaExt ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Thực hiện tạo mới</button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
