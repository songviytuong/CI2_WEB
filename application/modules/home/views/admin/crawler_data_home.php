<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="title-header">Cấu hình robot lấy tin</h3>
                            <hr>
                            <a href="<?php echo $base_link . "add" ?>" class="btn btn-success" role="button"><i class="fa fa-plus"></i> Lấy dữ liệu</a>
                            <a href="<?php echo $base_link . "public_data" ?>" class="btn btn-primary" role="button"><i class="fa fa-upload"></i> Hiển thị dữ liệu</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <table id="example" class="table table-bordered">
                        <thead>
                            <tr class="headings">
                                <th>STT</th>
                                <th>Destination </th>
                                <th>Created </th>
                                <th>Status </th>
                                <th class=" no-link last"><span class="nobr">Action</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if(count($data)>0){
                                $i=$start;
                                foreach($data as $row){ 
                                    $json = json_decode($row->Data,true);
                                    $i++;
                            ?>
                                    <tr class="even pointer">
                                        <td class="a-center ">
                                            <?php echo $i; ?>
                                        </td>
                                        <td>
                                            <?php echo "<a href='{$base_link}edit/$row->ID'>".$json['Title'].'</a>'; ?><br>
                                            <?php 
                                                $site = explode("/",$row->Destination);
                                                $type = isset($site[2]) ? $site[2] : '' ;
                                                echo isset($json['Categories']) ? "<small>$type » ".str_replace(" / "," » ",$json['Categories'])."</small>" : "<small>$type</small>";
                                            ?>
                                        </td>
                                        <td><?php echo $row->Created; ?></td>
                                        <?php 
                                        switch ($row->Published) {
                                            case 0:
                                                $published="Available";
                                                break;
                                            case 1:
                                                $published="Published";
                                                break;
                                            default:
                                                $published="Empty";
                                                break;
                                        }
                                        ?>
                                        <td class="a-right a-right "><?php echo $published; ?></td>
                                        <td class="last" style="min-width: 150px;">
                                            <a href='<?php echo $base_link . "edit/$row->ID" ?>'><i class="fa fa-pencil-square-o"></i> Edit </a>
                                            &nbsp;&nbsp;&nbsp;
                                            <a href='<?php echo $base_link."delete/$row->ID" ?>' class='delete_link'><i class='fa fa-trash-o'></i> Delete</a>
                                        </td>
                                    </tr>
                            <?php 
                                }
                            }else{
                                ?>
                                <tr class="even pointer">
                                    <td class="a-center" colspan="5" style="text-align:center">Data is empty .</td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                    <?php if(count($data)>0) echo $nav; ?>
                </div>
            </div>
        </div>
