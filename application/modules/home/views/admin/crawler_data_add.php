<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="title-header">Lấy tin từ hệ thống khác</h3>
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."addnew" ?>" method="post" enctype="multipart/form-data">
                        <div class="x_content"> 
                            <div class="form-group">
                                <label class="col-md-6 col-sm-6 col-xs-12">Nhập danh sách các link cần lấy dữ liệu</label>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <textarea class="resizable_textarea form-control" id="textarea_data" name="Destination" style="word-wrap: break-word; resize: horizontal; min-height: 300px;"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <button type="submit" class="btn btn-success">Tiếp tục <span class="glyphicon glyphicon-step-forward"></span></button>
                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-transfer"></span> Quét link</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Quét link từ website khác</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="reciver_data col-xs-12"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="alert alert-danger hidden"></div>
                            </div>
                            <div class="col-xs-12">
                                <p>Link đích muốn robot quét lấy link</p>
                            </div>
                            <div class="col-xs-12">
                                <input type="text" name="Destination_link" id="Destination_link" class="form-control col-md-12 col-xs-12" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="Crawl_links">Thực hiện quét</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
</div>

<style>
    .row_links{}
    .row_links a{display:block;padding:3px 0px;border-bottom:1px solid #EEE;color:#999;}
    .row_links a:hover{border-bottom:1px solid #E1e1e1;text-decoration: none;color:#444;}
</style>
<script>

    $("#Crawl_links").click(function(){
        var link = $("#Destination_link").val();
        if(link!=''){
            $.ajax({
                url: "<?php echo $base_link.'/crawl_links_from_string' ?>",
                dataType: "html",
                type: "POST",
                data: "str="+link,
                beforeSend: function(){
                    $(".reciver_data").html("<p>Loading ...</p>");
                },
                success: function(result){
                    if(result!='false'){
                        $(".reciver_data").html(result);
                    }else{
                        $(".reciver_data").html("<p>Not found data from this string .</p>");
                    }
                }
            });
        }else{
            $(this).parents(".modal").find(".alert-danger").removeClass('hidden').html('Vui lòng nhập link để robot quét');
        }
    });

    function uselinks(ob){
        var html = $(ob).html();
        var oldval = $("#textarea_data").val();
        $("#textarea_data").val(oldval+"\n"+html);
        $(ob).remove();
    }
</script>