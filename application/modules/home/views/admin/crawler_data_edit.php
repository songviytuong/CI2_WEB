<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <form data-parsley-validate class="form-horizontal form-label-left" id="formdata" action="<?php echo $base_link ."update" ?>" method="post" enctype="multipart/form-data">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="tab1 tabcontent">
                                    <div class="x_title">
                                        <h3>Điều chỉnh bài viết <button type="submit" class="btn btn-success pull-right" style="margin-right:0px">Thực hiện thay đổi</button></h3>
                                        <hr>
                                    </div>
                                    <?php 
                                    $json = json_decode($data->Data,true);
                                    ?>
                                    <div class="x_content">
                                            <input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
                                            <div class="form-group">
                                                <label class="col-md-12 col-sm-12 col-xs-12">Tiêu đề bài viết (<span class="required">*</span>)</label>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <input type="text" required="required" class="form-control col-md-12 col-xs-12" name="Title" value="<?php echo isset($json['Title']) ? trim(str_replace('"',"'", $json['Title'])) : '' ; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12 col-sm-12 col-xs-12">Mô tả ngắn bài viết</label>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <textarea class="resizable_textarea form-control" name="Description" id="Description" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 130px;"><?php echo isset($json['Description']) ? trim($json['Description']) : '' ; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <textarea class="resizable_textarea form-control ckeditor" name="Introtext" id="Introtext"><?php echo isset($json['Introtext']) ? $json['Introtext'] : '' ; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>