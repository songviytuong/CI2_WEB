<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="x_panel">
                        <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update" ?>" method="post" enctype="multipart/form-data">
                            <div class="x_content">
                                <input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
                                <!-- Tags  -->
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <div class="x_title">
                                        <h3>Preview <small> / view image</small></h3>
                                        <hr>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="dvPreview">
                                            <?php 
                                            if(file_exists($data->Thumb)){
                                                echo "<img src='$data->Thumb' class='img-responsive' />";
                                            }else{
                                                echo '<span style="padding-top:9px;display:block">No image selected</span>';    
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <div class="x_title">
                                        <h3>Information <small> / information image</small></h3>
                                        <hr>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Created : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div style='padding-top:9px'>
                                                <?php echo $data->Created ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">LastEdited : </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div style='padding-top:9px'>
                                                <?php echo $data->LastEdited ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Image Name</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div>
                                                <input class="form-control col-md-12 col-sm-12 col-xs-12" type='text' name="Name" value='<?php echo $data->Name ?>' required />
                                            </div>
                                        </div>
                                    </div>
                                    <?php 
                                    $album = $this->db->query("select Title,ID from ttp_album where Data like '%\"$data->ID\"%'")->result();
                                        if(count($album)>0){
                                            echo '<div class="form-group" id="destination_add_album">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Inner Album</label>';
                                            $i=1;
                                            $jsondata = array();
                                            foreach($album as $row){
                                                $jsondata[] = '"a'.$row->ID.'"';
                                                if($i==2){
                                                    echo '<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>';
                                                }
                                                echo '<div class="col-md-9 col-sm-9 col-xs-12">
                                                        <div style="padding-top:9px">
                                                            '.$row->Title.' <a class="removefromalbum" onclick="removefromalbum(this)" data="'.$row->ID.'">[x]</a>
                                                        </div>
                                                    </div>';
                                                $i=2;
                                            }
                                            echo "</div>";
                                            echo "<div class='form-group add_album'><span class='col-md-3 col-sm-3 col-xs-12'></span><a id='add_album' class='col-md-9 col-sm-9 col-xs-12'><i class='fa fa-plus-square'></i> Add album</a><div class='box_select_album'></div></div>";
                                        }
                                    ?>
                                    <div class="clearfix" style="margin-bottom:10px"></div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Change Image</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div>
                                                <label>
                                                    <span class="btn btn-file" style="padding-left:0px;">
                                                        <input type="file" name="Image_upload" id="choosefile" />
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <button type="submit" class="btn btn-success">Thực hiện thay đổi</button>
                                        </div>
                                    </div>
                                    <nav>
                                        <ul class='pagination'>
                                            <?php 
                                            $prev = $this->db->query("select ID from ttp_images where ID>$data->ID order by ID ASC")->row();
                                            echo $prev ? "<li><a href='{$base_link}edit/$prev->ID'>« Prev</a></li>" : "" ;
                                            $next = $this->db->query("select ID from ttp_images where ID<$data->ID order by ID DESC")->row();
                                            echo $next ? "<li><a href='{$base_link}edit/$next->ID'>Next »</a></li>" : "" ;
                                            
                                            ?>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    nav{text-align: right;}
    a{cursor: pointer}
    a.removefromalbum:hover{color:#F00;}
    .add_album{position:relative;}
</style>
<script>
    $("#choosefile").change(function(){
        var Fileinput = document.getElementById("choosefile");
        var file = Fileinput.files[0];
        var imageType = /image.*/
        var dvPreview = $(".dvPreview");
        if(file.type.match(imageType)){
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = $("<img />");
                img.attr("class","img-responsive");
                img.attr("src", e.target.result);
                dvPreview.html(img);
            }
            reader.readAsDataURL(file);
        }else{
            console.log("Not an Image");
        }
    });

    function removefromalbum(ob){
        var ID = $(ob).attr("data");
        var parent = $(ob).parent('div').parent('div');
        if(ID!=''){
            $.ajax({
                url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/remove_image_from_album/'.$data->ID ?>"+"/"+ID, 
                success: function(result){
                    if(result!='fasle'){
                        parent.fadeOut();
                    }else{
                        alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu . Vui lòng kiểm tra lại đường truyền .");
                    }
                }
            });
        }
    };

    var status = 0;
    $("#add_album").click(function(){
        if(status==0){
            $.ajax({
                url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/show_album_list/' ?>", 
                success: function(result){
                    $(".box_select_album").html(result);
                    $(".box_select_album").toggle();
                }
            });
        }else{
            $(".box_select_album").toggle();
        }
    });

    var array = [<?php echo implode(",", $jsondata) ?>];
    function add_album(ob){
        var data = $(ob).html();
        var ID = $(ob).attr('data');
        if(array.indexOf("a"+ID)==-1){
            $.ajax({
                url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/add_image_from_album/'.$data->ID ?>"+"/"+ID,
                success: function(result){
                    $("#destination_add_album").append("<span class='col-md-3 col-sm-3 col-xs-12'></span><div class='col-md-9 col-sm-9 col-xs-12'><div style='padding-top:9px'>"+data+" <a class='removefromalbum' onclick='removefromalbum(this)'' data='"+ID+"'>[x]</a></div></div>");
                    array.push(data);
                }
            });
        }
        $(".box_select_album").toggle();
    }
</script>