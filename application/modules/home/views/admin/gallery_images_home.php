<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="title-header">Danh sách hình ảnh</h3>
                            <hr>
                            <a href="<?php echo $base_link . "add" ?>" class="btn btn-success pull-left"><i class="fa fa-plus"></i> Upload Image</a>
                        </div>
                    </div>
                    <br>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                    <?php 
                    if(count($data)>0){
                        foreach($data as $row){
                            $img = $this->lib->getfilesize($row->Thumb,270,151);
                            echo "<div class='col-md-2 col-sm-3 col-xs-4 custom'>
                                    <a href='{$base_link}edit/$row->ID'><img src='$img' class='img-responsive' /></a>
                                    <a class='delete_link' href='{$base_link}delete/$row->ID'><i class='fa fa-trash-o'></i></a>
                                </div>";
                        }
                    }
                    ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php 
                    echo $nav;
                ?>
            </div>
        </div>
    </div>
</div>
<style>
    .custom{margin-bottom: 15px; position: relative;overflow:hidden;min-height:160px;}
    .custom img{box-sizing:border-box;border:1px solid #E1e1e1;}
    .custom .delete_link{position: absolute;top: -20px;right: 15px;background: #2A3F54;padding: 2px 5px;opacity: 0;transition:all 0.5s;-webkit-transition:all 0.5s;-moz-transition:all 0.5s;}
    .custom:hover .delete_link{opacity: 1;top:0px;}
    .custom .delete_link:hover{background: #eee}
    .custom .delete_link:hover i{color:#2A3F54;}
    .custom .delete_link i{color:#FFF;}
    nav{text-align:center;}
</style>