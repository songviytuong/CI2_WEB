<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="transfer_post">
                            <div class="row">
                                <div class="col-xs-12 col-md-4 col-sm-4">
                                    <div class="x_title">
                                        <h4>Trang website đích</h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <select id="Site" class="form-control">
                                                <option value="">-- Chọn website đích --</option>
                                                <option value="http://tools.ucancook.vn/postsite">ucancook.vn</option>
                                            </select>
                                        </div>
                                        <!-- end left -->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 cat_block">
                                            <div class="x_title">
                                                <h4>Danh mục bài viết</h4>
                                                <hr>
                                            </div>
                                            <div class="cat_reciver"><p>Vui lòng chọn website để thấy thông tin danh mục</p></div>
                                            <button class="btn btn-success" id="transfer">Thực hiện gửi tin</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-8 col-sm-6">
                                            <h4>Danh sách bài viết</h4>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <select style="margin-top:10px;" class="form-control" id="change_site">
                                                <option value="">-- Lọc tất cả trang --</option>
                                                <?php 
                                                $site = $this->db->query("select * from ttp_crawler_site")->result();
                                                if(count($site)>0){
                                                    foreach($site as $row){
                                                        echo "<option value='$row->Site'>$row->Site</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <table id="crawl_data_post" class="table table-bordered">
                                        <thead>
                                            <tr class="headings">
                                                <th>STT</th>
                                                <th>Bài viết </th>
                                                <th>Xem trước </th>
                                                <th class="no-link last" style="text-align:center"><input class="checkfull_option" type="checkbox" /></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if(count($data)>0){
                                                $i=0;
                                                foreach($data as $row){ 
                                                    $json = json_decode($row->Data,true);
                                                    $i++;
                                            ?>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <?php echo $i; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $json['Title']; ?><br>
                                                            <?php 
                                                                $site = explode("/",$row->Destination);
                                                                $type = isset($site[2]) ? $site[2] : '' ;
                                                                echo isset($json['Categories']) ? "<small>$type » ".str_replace(" / "," » ",$json['Categories'])."</small>" : "<small>$type</small>";
                                                            ?>
                                                        </td>
                                                        <td class="a-right a-right <?php echo "status".$row->ID ?>" style="text-align:center"><a class="crawl_preview" data="<?php echo $row->ID ?>"><i class="fa fa-search"></i></a></td>
                                                        <td class="last" style="text-align:center"><input class="rowcheckbox <?php echo "row".$row->ID ?>" type="checkbox" value="<?php echo $row->ID ?>" /></td>
                                                    </tr>
                                            <?php 
                                                }
                                                if(count($data)==4){
                                                    echo '<tr class="even pointer"><td colspan="4" style="text-align:center"><a onclick="loadmore(this)" data="'.$i.'">Load more post</a></td></tr>';
                                                }
                                            }else{
                                                ?>
                                                <tr class="even pointer">
                                                    <td class="a-center" colspan="4" style="text-align:center">No post available .</td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Xem trước bài viết</h4>
            </div>
            <div class="modal-body">
                <div class="row crawl_preview_box">
                    <div class="col-xs-12">
                        <div class="content_crawl_box" style="line-height: 25px;text-align: justify;"></div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<style type="text/css">
    a{cursor: pointer}
    .cat_block{}
    .cat_reciver{}
    .cat_reciver ul{padding-left:0px;}
    .cat_reciver ul li{margin:0px; padding:5px 0px;display: block}
    .cat_reciver ul li input{margin-right:5px;}
    .cat_reciver ul li ul{padding-left:20px;}
</style>
<script>
    $("#change_site").change(function(){
        var data = $(this).val();
        $.ajax({
            url: "<?php echo $base_link.'/load_post_available_from_site' ?>",
            dataType: "html",
            type: "POST",
            data: "Site="+data,
            beforeSend: function(){
                $("table tbody").html("<tr><td colspan='4'><p>Loading ...</p></td></tr>");
            },
            success: function(result){
                if(result!='false'){
                    $("table tbody").html(result);
                }else{
                    $("table tbody").html("<p>No post from this site .</p>");
                }
            }
        });
    });

    $(".checkfull_option").change(function(){
        if(this.checked===true){
            $(".rowcheckbox").prop('checked', true);
        }else{
            $(".rowcheckbox").prop('checked', false);
        }
    });

    $("#Site").change(function(){
        var data = $(this).val();
        if(data!=''){
            $.ajax({
                url: "<?php echo $base_link.'/get_categories_from_site' ?>",
                dataType: "html",
                type: "POST",
                data: "Site="+data,
                beforeSend: function(){
                    $(".cat_reciver").html("<p>Loading ...</p>");
                },
                success: function(result){
                    if(result!='false'){
                        $(".cat_reciver").html(result);
                    }else{
                        $(".cat_reciver").html("<p>No categories on this site .</p>");
                    }
                }
            });
        }else{
            $(".cat_reciver").html("<p>Choose site to load categories</p>");
        }
    });

    $(".crawl_preview").click(function(){
        var data = $(this).attr("data");
        if(data!=''){
            $.ajax({
                url: "<?php echo $base_link.'/get_crawl_post_by_id' ?>",
                dataType: "html",
                type: "POST",
                data: "ID="+data,
                beforeSend: function(){
                    $(".crawl_preview_box .content_crawl_box").html("<p>Loading ...</p>");
                },
                success: function(result){
                    if(result!='false'){
                        $(".crawl_preview_box .content_crawl_box").html(result);
                        $(".crawl_preview_box").show();
                    }else{
                        $(".crawl_preview_box .content_crawl_box").html("<p>This post is not available .</p>");
                    }
                    $("#myModal").modal("show");
                }
            });
        }
    });

    $("#close_crawl_preview").click(function(){
        $(".crawl_preview_box").hide();
    });

    $("#transfer").click(function(){
        var site = $("#Site").val();
        var post = [];
        var cat = [];
        $(this).addClass("saving");
        $(".rowcheckbox").each(function() {
           if ($(this).is(":checked")) {
               post.push($(this).val());
           }
        });
        $(".cat_reciver input").each(function() {
           if ($(this).is(":checked")) {
               cat.push($(this).val());
           }
        });
        temppost = post.join(",");
        cat = cat.join(",");
        if(temppost!='' && cat!='' && site!=''){
            send_post(0,post,site,cat);
        }else{
            alert("Vui lòng chọn bài viết và danh mục để thực hiện transfer về site đích !");
            $("#transfer").html("Transfer");
            $("#transfer").removeClass("saving");
        }
    });

    function send_post(num,post,site,cat){
        if(post.length==num){
            $("#transfer").html("Transfer");
            $("#transfer").removeClass("saving");
            return false;
        }
        $(".status"+post[num]).html("Sending ...");
        $.ajax({
            url: "<?php echo $base_link.'/send_post' ?>",
            dataType: "html",
            type: "POST",
            data: "Site="+site+"&post="+post[num]+"&cat="+cat,
            beforeSend: function(){
                $("#transfer").html("Post "+post[num]+" sending ...");
            },
            success: function(result){
                $(".row"+post[num]).parent("td").parent("tr").fadeOut();
                $(".row"+post[num]).remove();
            }
        }).always(function(){
            send_post(++num,post,site,cat);
        });
    }

    function loadmore(ob){
        var site = $("#change_site").val();
        var stt = $(ob).attr('data');
        var parent = $(ob).parent("td").parent("tr");
        $.ajax({
            url: "<?php echo $base_link.'/load_more_post' ?>",
            dataType: "html",
            type: "POST",
            data: "Site="+site+"&stt="+stt,
            beforeSend: function(){
                $(ob).parent("td").html("<p>Loading ...</p>");
            },
            success: function(result){
                parent.remove();
                if(result!='false'){
                    $("table tbody").append(result);
                }else{
                    alert("No post");
                }
            }
        });
    }
</script>