<?php 
    $segment = $this->uri->segment(3);
    $temp_current_group = explode("_",$segment);
    $current_group = isset($temp_current_group[0]) ? $temp_current_group[0] : '' ;
    $current_page = isset($temp_current_group[1]) ? $temp_current_group[1] : '' ;
    $permission = json_decode($user->DetailRole,true);
?>
<div class="menu-primary">
    <ul>
        <li <?php echo $segment == "" ? "class='active'" : '' ; ?>>
            <a><span class="glyphicon glyphicon-stats"></span> <span>Tổng quan</span></a></li>
        <li <?php echo $current_group == "system" ? "class='active'" : '' ; ?>>
            <a><span class="glyphicon glyphicon-cog"></span> <span>Hệ thống</span></a>
            <ul>
                <li class="sub-title">Hệ thống</li>
                <?php 
                if($user->IsAdmin==1){
                ?>
                <li <?php echo $current_page == "module" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/system_module">Permission list</a></li>
                <li <?php echo $current_page == "role" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/system_role">Role list</a></li>
                <li <?php echo $current_page == "user" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/system_user">User list</a></li>
                <li <?php echo $current_page == "email" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/system_email">Email Setting</a></li>
                <?php 
                }else{
                    if(array_key_exists('system_module',$permission)){
                    ?>
                        <li <?php echo $current_page == "module" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/system_module">Permission list</a></li>
                    <?php
                    }
                    if(array_key_exists('system_role',$permission)){
                    ?>
                        <li <?php echo $current_page == "role" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/system_role">Role list</a></li>
                    <?php
                    }
                    if(array_key_exists('system_user',$permission)){
                    ?>
                        <li <?php echo $current_page == "user" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/system_user">User list</a></li>
                    <?php
                    }
                    if(array_key_exists('system_email',$permission)){
                    ?>
                        <li <?php echo $current_page == "email" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/system_email">Email Setting</a></li>
                    <?php
                    }
                }
                ?>
            </ul>
        </li>
        <li <?php echo $current_group == "site" ? "class='active'" : '' ; ?>><a><span class="glyphicon glyphicon-globe"></span> <span>Website</span></a>
            <ul>
                <li class="sub-title">Website</li>
                <?php 
                if($user->IsAdmin==1){
                ?>
                    <li <?php echo $current_page == "staticconfig" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_staticconfig">Static config</a></li>
                    <li <?php echo $current_page == "pagelinks" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_pagelinks">Page links</a></li>
                    <li <?php echo $current_page == "categories" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_categories">Categories Pages</a></li>
                    <li <?php echo $current_page == "post" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_post">Post Pages</a></li>
                    <li <?php echo $current_page == "banner" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_banner">Banner list</a></li>
                    <li <?php echo $current_page == "internallink" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_internallink">Site links footer</a></li>
                    <li <?php echo $current_page == "tags" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_tags">Tags</a></li>
                <?php 
                }else{
                    if(array_key_exists('site_staticconfig',$permission)){
                    ?>
                        <li <?php echo $current_page == "staticconfig" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_staticconfig">Static config</a></li>
                    <?php
                    }
                    if(array_key_exists('site_pagelinks',$permission)){
                    ?>
                        <li <?php echo $current_page == "pagelinks" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_pagelinks">Page links</a></li>
                    <?php
                    }
                    if(array_key_exists('site_categories',$permission)){
                    ?>
                        <li <?php echo $current_page == "categories" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_categories">Categories Pages</a></li>
                    <?php
                    }
                    if(array_key_exists('site_post',$permission)){
                    ?>
                        <li <?php echo $current_page == "post" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_post">Post Pages</a></li>
                    <?php
                    }
                    if(array_key_exists('site_banner',$permission)){
                    ?>
                        <li <?php echo $current_page == "banner" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_banner">Banner list</a></li>
                    <?php
                    }
                    if(array_key_exists('site_internallink',$permission)){
                    ?>
                        <li <?php echo $current_page == "internallink" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_internallink">Site links footer</a></li>
                    <?php
                    }
                    if(array_key_exists('site_tags',$permission)){
                    ?>
                        <li <?php echo $current_page == "tags" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/site_tags">Tags</a></li>
                    <?php
                    }
                }
                ?>
            </ul>
        </li>
        <li <?php echo $current_group == "gallery" ? "class='active'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/gallery_images"><span class="glyphicon glyphicon-picture"></span> <span>Tài liệu</span></a></li>
        <li <?php echo $current_group == "crawler" ? "class='active'" : '' ; ?>><a><span class="glyphicon glyphicon-send"></span> <span>Lấy bài</span></a>
            <ul>
                <li class="sub-title">Lấy tin từ website khác</li>
                <?php 
                if($user->IsAdmin==1){
                ?>
                    <li <?php echo $current_page == "setting" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/crawler_setting">Bot setting</a></li>
                    <li <?php echo $current_page == "data" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/crawler_data">Crawl data</a></li>
                    <li <?php echo $current_page == "site" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/crawler_site">Crawl site</a></li>
                <?php 
                }else{
                    if(array_key_exists('crawler_setting',$permission)){
                    ?>
                        <li <?php echo $current_page == "setting" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/crawler_setting">Setting</a></li>
                    <?php
                    }
                    if(array_key_exists('crawler_data',$permission)){
                    ?>
                        <li <?php echo $current_page == "data" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/crawler_data">Crawl data</a></li>
                    <?php
                    }
                    if(array_key_exists('crawler_site',$permission)){
                    ?>
                        <li <?php echo $current_page == "site" ? "class='current-page'" : '' ; ?>><a href="<?php echo base_url().ADMINPATH ?>/home/crawler_site">Crawl site</a></li>
                    <?php
                    }
                }
                ?>
            </ul>
        </li>
    </ul>
</div>