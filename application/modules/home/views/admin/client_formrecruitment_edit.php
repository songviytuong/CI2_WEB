<?php 
$jsonpost = json_decode($data->postData);
$json = json_decode($data->Data);
?>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>CLIENT INTERACTION GROUP</h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <form action="<?php echo $base_link . 'search'; ?>" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Search for..." required>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <form class="form-horizontal">
	            <div class="x_panel">
		            <div class='col-md-6 col-sm-6 col-xs-12'>
		                <div class="x_title">
		                    <h2>RECRUITMENT <small> / Position</small></h2>
		                    <div class="clearfix"></div>
		                </div>
		                <div class="x_content">
		                    <div class="form-group row">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Title 
		                        </label>
		                        <div class="col-md-9 col-sm-9 col-xs-12" style="padding-top:8px">
		                            <?php echo $data->Title ?>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Position 
		                        </label>
		                        <div class="col-md-9 col-sm-9 col-xs-12" style="padding-top:8px">
		                            <?php echo isset($jsonpost->position) ? $jsonpost->position : '' ; ?>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Area 
		                        </label>
		                        <div class="col-md-9 col-sm-9 col-xs-12" style="padding-top:8px">
		                            <?php echo isset($jsonpost->area) ? $jsonpost->area : '' ; ?>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Worktime 
		                        </label>
		                        <div class="col-md-9 col-sm-9 col-xs-12" style="padding-top:8px">
		                            <?php echo isset($jsonpost->worktime) ? $jsonpost->worktime : '' ; ?>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Deathline 
		                        </label>
		                        <div class="col-md-9 col-sm-9 col-xs-12" style="padding-top:8px">
		                            <?php echo isset($jsonpost->deathline) ? $jsonpost->deathline : '' ; ?>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Description 
		                        </label>
		                        <div class="col-md-9 col-sm-9 col-xs-12" style="padding-top:8px">
		                            <?php echo isset($data->Description) ? $data->Description : '' ; ?>
		                        </div>
		                    </div>
		                </div>
		                
		            </div>
		            <div class='col-md-6 col-sm-6 col-xs-12'>
		            	<div class="x_title" id="title_ext">
		                    <h2>Candidates <small> / Information</small></h2>
		                    <div class="clearfix"></div>
		                </div>
		                <div class="x_content">
		                	<div class="form-group row">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name 
		                        </label>
		                        <div class="col-md-9 col-sm-9 col-xs-12" style="padding-top:8px">
		                            <?php echo $data->Name ?>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email </label>
		                        <div class="col-md-9 col-sm-9 col-xs-12" style="padding-top:8px">
		                            <?php echo $data->Email ?>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Note</label>
		                        <div class="col-md-9 col-sm-9 col-xs-12" style="padding-top:8px">
		                            <?php echo isset($json->Ghichu) ? $json->Ghichu : "" ; ?>
		                        </div>
		                    </div>
		                </div>
		                <div class="x_title">
		                    <h2>Attachment <small> / Candidates CV</small></h2>
		                    <div class="clearfix"></div>
		                </div>
		                <div class="x_content">
		                	<div class="form-group row">
		                		<?php 
		                		if(isset($json->CV)){
		                			if(count($json->CV)>0){
		                				$i=1;
		                				foreach($json->CV as $row){
		                					$temp = explode('/',$row);
		                					$temp = count($temp>0) ? $temp[count($temp)-1] : "" ;
		                					if($temp!=''){
		                						$ext = explode(".",$temp);
		                						$ext = count($ext)>0 ? $ext[count($ext)-1] : "" ;
		                						switch ($ext) {
		                							case 'doc':
		                								$icon = '<i class="fa fa-file-word-o"></i>';
		                								break;
		                							case 'docx':
		                								$icon = '<i class="fa fa-file-word-o"></i>';
		                								break;
		                							case 'pdf':
		                								$icon = '<i class="fa fa-file-pdf-o"></i>';
		                								break;	
		                							default:
		                								$icon = '<i class="fa fa-file-text-o"></i>';
		                								break;
		                						}
			                					echo '<div class="col-md-12 col-sm-12 col-xs-12" style="padding-top:8px;border-bottom:1px solid #E1e1e1;padding-bottom:5px">';
			                					echo "<div class='col-md-6 col-sm-6 col-xs-12'>$icon <a href='$row' title='Click to download this file'>File_CV_$i</a></div>";
			                					echo "<div class='col-md-6 col-sm-6 col-xs-12'><a href='$row' title='Click to download this file'>Download</a></div>";
			                					echo '</div>';
			                					$i++;
		                					}
		                				}
		                			}else{
		                				echo "<div class='col-md-12 col-sm-12 col-xs-12'> No attachments</div>";
		                			}
		                		}else{
		                			echo "<div class='col-md-12 col-sm-12 col-xs-12'> No attachments</div>";
		                		}
		                		?>
		                    </div>
		                </div>
		            </div>

	            </div>
	        </form>
        </div>
    </div>
</div>