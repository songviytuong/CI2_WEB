<?php 
$json = json_decode($data->Data);
?>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>CLIENT INTERACTION GROUP</h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <form action="<?php echo $base_link . 'search'; ?>" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Search for..." required>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <form class="form-horizontal">
	            <div class="x_panel">
	                <div class="x_title">
	                    <h2>CONTACT FORM <small> / Note</small></h2>
	                    <div class="clearfix"></div>
	                </div>
	                <div class="x_content">
	                    <div class="form-group row">
	                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name 
	                        </label>
	                        <div class="col-md-9 col-sm-9 col-xs-12" style="padding-top:8px">
	                            <?php echo $data->Name ?>
	                        </div>
	                    </div>
	                    <div class="form-group row">
	                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email 
	                        </label>
	                        <div class="col-md-9 col-sm-9 col-xs-12" style="padding-top:8px">
	                            <?php echo $data->Email; ?>
	                        </div>
	                    </div>
	                    <div class="form-group row">
	                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Note 
	                        </label>
	                        <div class="col-md-9 col-sm-9 col-xs-12" style="padding-top:8px">
	                            <?php echo isset($json->Note) ? $json->Note : 'Data empty .' ; ?>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </form>
        </div>
    </div>
</div>