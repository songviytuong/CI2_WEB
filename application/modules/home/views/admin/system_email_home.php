<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="title-header">Cấu hình hệ thống Email</h3>
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Nhận Email</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Gửi Email</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Cấu hình Email</a></li>
                    </ul>
                    <!-- Tab panes -->
                      <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update" ?>" method="post" enctype="multipart/form-data">
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <br>
                                <div class="x_content">
                                    <div class="form-group">
                                        <label class="col-md-12 col-sm-12 col-xs-12">Email người nhận thông tin (<span class="required">*</span>)</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control col-md-12 col-xs-12" name="Email_reciver" value="<?php echo isset($data->Email_reciver) ? $data->Email_reciver : "" ; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12 col-sm-12 col-xs-12">Tiêu đề email muốn nhận (<span class="required">*</span>)</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control col-md-12 col-xs-12" name="Reciver_title" value="<?php echo isset($data->Reciver_title) ? $data->Reciver_title : "" ; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <br>
                                <div class="x_content">
                                    <div class="form-group">
                                        <label class="col-md-12 col-sm-12 col-xs-12">Tiêu đề email gửi đi (<span class="required">*</span>)</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" required="required" class="form-control col-md-12 col-xs-12" name="Sender_title" value="<?php echo isset($data->Sender_title) ? $data->Sender_title : "" ; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12 col-sm-12 col-xs-12">Nội dung muốn gửi (<span class="required">*</span>)</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <textarea class="resizable_textarea form-control ckeditor" name="Sender_content"><?php echo isset($data->Sender_content) ? $data->Sender_content : "" ; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="messages">
                                <br>
                                <div class="x_content">
                                    <div class="form-group">
                                        <label class="col-md-12 col-sm-12 col-xs-12">Protocol</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control col-md-12 col-xs-12" name="Protocol" value="<?php echo isset($data->Protocol) ? $data->Protocol : "" ; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12 col-sm-12 col-xs-12">SMTP_host</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control col-md-12 col-xs-12" name="SMTP_host" value="<?php echo isset($data->SMTP_host) ? $data->SMTP_host : "" ; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12 col-sm-12 col-xs-12">SMTP_port</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control col-md-12 col-xs-12" name="SMTP_port" value="<?php echo isset($data->SMTP_port) ? $data->SMTP_port : "" ; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12 col-sm-12 col-xs-12">SMTP_user</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control col-md-12 col-xs-12" name="SMTP_user" value="<?php echo isset($data->SMTP_user) ? $data->SMTP_user : "" ; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12 col-sm-12 col-xs-12">SMTP_password</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="password" class="form-control col-md-12 col-xs-12" name="SMTP_password" value="<?php echo isset($data->SMTP_password) ? $data->SMTP_password : "" ; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="settings">
                                <br>
                                <div class="x_content">
                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="public/themes_email/themes_01.html" target="_blank"><img src="public/themes_email/thumb_themes_01.PNG" class="img-responsive" /></a>
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center">
                                            <input type="radio" name="Themes" value="themes_01" <?php echo isset($data->Themes) && @$data->Themes=="themes_01" ? "checked='checked'" : "" ; ?>/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-success">Cập nhật thông tin</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
