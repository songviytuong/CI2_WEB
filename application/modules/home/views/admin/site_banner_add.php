<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="title-header">Tạo mới banner </h3>
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."addnew" ?>" method="post" enctype="multipart/form-data">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel tab1 tabcontent">
                                <div class="x_content"> 
                                    <br>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Chọn loại banner</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="PositionID" class="form-control" id="PositionSelect">
                                                <?php 
                                                $result = $this->db->query("select * from ttp_banner_position")->result();
                                                if(count($result)>0){
                                                    foreach($result as $row){
                                                        echo "<option value='$row->ID'>$row->Title</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" id="recive_ajax">

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Chọn hình ảnh từ náy tính</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div>
                                                <label>
                                                    <span class="btn btn-file"><input type="file" name="Image_upload" id="choosefile" />
                                                    </span>
                                                </label>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Xem trước hình ảnh</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="dvPreview"><span style="padding-top:9px;display:block">No image selected</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <button type="submit" class="btn btn-success">Thực hiện tạo mới</button>
                                        </div>
                                </div>
                            </div>
                            <!-- end VIETNAM -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $.ajax({
            url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/site_load_view_by_position_banner/1' ?>", 
            success: function(result){
                $("#recive_ajax").html(result);
            }
        });

        $.ajax({
            url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/site_load_view_by_position_banner/1/en' ?>", 
            success: function(result){
                $("#recive_ajax_en").html(result);
            }
        });

        $("#PositionSelect").change(function(){
            var ID = $(this).val();
            $.ajax({
                url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/site_load_view_by_position_banner/' ?>"+ID, 
                success: function(result){
                    $("#recive_ajax").html(result);
                }
            });
            $.ajax({
                url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/site_load_view_by_position_banner/' ?>"+ID+"/en", 
                success: function(result){
                    $("#recive_ajax_en").html(result);
                }
            });
        });



        $("#choosefile").change(function(){
            var Fileinput = document.getElementById("choosefile");
            var file = Fileinput.files[0];
            var imageType = /image.*/
            var dvPreview = $(".dvPreview");
            if(file.type.match(imageType)){
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr("style", "max-height:300px;max-width: 400px");
                    img.attr("src", e.target.result);
                    dvPreview.html(img);
                }
                reader.readAsDataURL(file);
            }else{
                console.log("Not an Image");
            }
        });
    });
</script>