<?php 
	$temp = trim($name);
	$temp = strtolower($temp);
	$form = "";
	if($value=="inputtext"){
		$form = '<input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="'.$name.'">';
	}
	if($value=="textarea"){
		$form = '<textarea class="resizable_textarea form-control" name="'.$name.'" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"></textarea>';
	}
	if($temp=='position' || $temp=='worktime' || $temp=='changelink' || $temp=='year' || $temp=='number' || $temp=='timeline' || $temp=='block' || $temp=='deathline' || $temp=='video'){
		$form = '<input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="'.$name.'">';
	}
	if($temp=='album'){
		$form = '<select name="album" class="form-control">';
		$result = $this->db->query("select * from ttp_album")->result();
		if(count($result)>0){
			foreach ($result as $row) {
				$form.="<option value='$row->ID'>$row->Title</option>";
			}
		}
		$form .='</select>';
	}
	if($temp=='hot' || $temp=='useslide' || $temp=='showhome' || $temp=='featured' || $temp=='specialrecruitment'){
		$form = '<div>
                    <label style="padding-top:7px">
                        <input type="radio" name="'.$temp.'" value="true" checked="checked"> &nbsp; Enable &nbsp;
                        <input type="radio" name="'.$temp.'" value="false"> Disable
                    </label>
                </div>';
	}
	if($temp == "area"){
		$array = array(
			0=>'Tp. Hồ Chí Minh',1=>'Hà Nội',2=>'Bình Dương',3=>'Đà Nẵng',4=>'Hải Phòng',5=>'Long An',6=>'Bà Rịa Vũng Tàu',7=>'An Giang',8=>'Bắc Giang',9=>'Bắc Kạn',10=>'Bạc Liêu',
			11=>'Bắc Ninh',12=>'Bến Tre',13=>'Bình Định',14=>'Bình Phước',15=>'Bình Thuận ',16=>'Cà Mau',17=>'Cần Thơ',18=>'Cao Bằng',19=>'Đắk Lắk',20=>'Đắk Nông',21=>'Điện Biên',
			22=>'Đồng Nai',23=>'Đồng Tháp',24=>'Gia Lai',25=>'Hà Giang',26=>'Hà Nam',27=>'Hà Tĩnh',28=>'Hải Dương',29=>'Hậu Giang',30=>'Hòa Bình',31=>'Hưng Yên',32=>'Khánh Hòa',
			33=>'Kiên Giang',34=>'Kon Tum',35=>'Lai Châu',36=>'Lâm Đồng',37=>'Lạng Sơn',38=>'Lào Cai',39=>'Nam Định',40=>'Nghệ An',41=>'Ninh Bình',42=>'Ninh Thuận',43=>'Phú Thọ',
			44=>'Phú Yên',45=>'Quảng Bình',46=>'Quảng Nam',47=>'Quảng Ngãi',48=>'Quảng Ninh',49=>'Quảng Trị',50=>'Sóc Trăng',51=>'Sơn La',52=>'Tây Ninh',53=>'Thái Bình',54=>'Thái Nguyên',
			55=>'Thanh Hóa',56=>'Thừa Thiên Huế',57=>'Tiền Giang',58=>'Trà Vinh',59=>'Tuyên Quang',60=>'Vĩnh Long',61=>'Vĩnh Phúc',62=>'Yên Bái');
		$form = '<select name="area" class="form-control">';
		foreach($array as $row){
			$form.="<option value='$row'>$row</option>";
		}
		$form .='</select>';
	}

if($temp!='recruitment' && $temp!='achievement'){
?>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x] </a><?php echo $name; ?></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo $form; ?>
    </div>
</div>
<?php 
	if($temp=="changelink"){
		echo '<div class="form-group">
		    <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x] </a>changelink_en</label>
		    <div class="col-md-6 col-sm-6 col-xs-12">
		        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="changelink_en">
		    </div>
		</div>';
	}
}else{
	if($temp=='recruitment'){
		$array = array(
				0=>'Tp. Hồ Chí Minh',1=>'Hà Nội',2=>'Bình Dương',3=>'Đà Nẵng',4=>'Hải Phòng',5=>'Long An',6=>'Bà Rịa Vũng Tàu',7=>'An Giang',8=>'Bắc Giang',9=>'Bắc Kạn',10=>'Bạc Liêu',
				11=>'Bắc Ninh',12=>'Bến Tre',13=>'Bình Định',14=>'Bình Phước',15=>'Bình Thuận ',16=>'Cà Mau',17=>'Cần Thơ',18=>'Cao Bằng',19=>'Đắk Lắk',20=>'Đắk Nông',21=>'Điện Biên',
				22=>'Đồng Nai',23=>'Đồng Tháp',24=>'Gia Lai',25=>'Hà Giang',26=>'Hà Nam',27=>'Hà Tĩnh',28=>'Hải Dương',29=>'Hậu Giang',30=>'Hòa Bình',31=>'Hưng Yên',32=>'Khánh Hòa',
				33=>'Kiên Giang',34=>'Kon Tum',35=>'Lai Châu',36=>'Lâm Đồng',37=>'Lạng Sơn',38=>'Lào Cai',39=>'Nam Định',40=>'Nghệ An',41=>'Ninh Bình',42=>'Ninh Thuận',43=>'Phú Thọ',
				44=>'Phú Yên',45=>'Quảng Bình',46=>'Quảng Nam',47=>'Quảng Ngãi',48=>'Quảng Ninh',49=>'Quảng Trị',50=>'Sóc Trăng',51=>'Sơn La',52=>'Tây Ninh',53=>'Thái Bình',54=>'Thái Nguyên',
				55=>'Thanh Hóa',56=>'Thừa Thiên Huế',57=>'Tiền Giang',58=>'Trà Vinh',59=>'Tuyên Quang',60=>'Vĩnh Long',61=>'Vĩnh Phúc',62=>'Yên Bái');
		$form = '<select name="area" class="form-control">';
		foreach($array as $row){
			$form.="<option value='$row'>$row</option>";
		}
		$form .='</select>';
		echo '<div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x] Area</a></label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			    	'.$form.'
			    </div>
			</div>';
		echo '<div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x] Deathline</a></label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			    	<input type="text" class="form-control col-md-7 col-xs-12" name="deathline">
			    </div>
			</div>';
		echo '<div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12"></label><div class="col-md-6 col-sm-6 col-xs-12">VIETNAMESE VALUE</div></div>';
		echo '<div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x] Position</a></label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			    	<input type="text" class="form-control col-md-7 col-xs-12" name="position">
			    </div>
			</div>';
		echo '<div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x] Worktime</a></label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			    	<input type="text" class="form-control col-md-7 col-xs-12" name="worktime">
			    </div>
			</div>';
		echo '<div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12"></label><div class="col-md-6 col-sm-6 col-xs-12">ENGLISH VALUE</div></div>';
		echo '<div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x] Position</a></label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			    	<input type="text" class="form-control col-md-7 col-xs-12" name="position_en">
			    </div>
			</div>';
		echo '<div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x] Worktime</a></label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			    	<input type="text" class="form-control col-md-7 col-xs-12" name="worktime_en">
			    </div>
			</div>';
	}
	if($temp=='achievement'){
		echo '<div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x] Year</a></label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			    	<input type="text" class="form-control col-md-7 col-xs-12" name="year">
			    </div>
			</div>';
		echo '<div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x] Timeline</a></label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			    	<input type="text" class="form-control col-md-7 col-xs-12" name="timeline">
			    </div>
			</div>';
		echo '<div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x] Number</a></label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			    	<input type="text" class="form-control col-md-7 col-xs-12" name="number">
			    </div>
			</div>';
		echo '<div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x] Changelink</a></label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			    	<input type="text" class="form-control col-md-7 col-xs-12" name="changelink">
			    </div>
			</div>';
		echo '<div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x] Changelink_en</a></label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			    	<input type="text" class="form-control col-md-7 col-xs-12" name="changelink_en">
			    </div>
			</div>';
	}
}
?>