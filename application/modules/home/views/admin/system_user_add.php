<?php 
    $error = isset($_GET['error']) ? $_GET['error'] : "" ;
    echo $error=="user_exists" ? "<script>alert('Username already exists !');</script>" : "" ;
?>
<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="title-header">Tạo mới tài khoản</h3>
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="x_panel">
                        <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."addnew" ?>" method="post" enctype="multipart/form-data">
                        <div class="x_content">
                            <br />
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Loại tài khoản <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="UserType" class="form-control col-md-7 col-xs-12">
                                            <?php 
                                            $data = $this->lib->get_config_define('position','users',1,'id');
                                            if(count($data)>0){
                                                foreach($data as $row){
                                                    echo "<option value='$row->code'>$row->name</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Phòng ban <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="DepartmentID" class="form-control col-md-7 col-xs-12">
                                            <?php 
                                            $department = $this->db->query("select * from ttp_department")->result();
                                            if(count($department)>0){
                                                foreach($department as $row){
                                                    echo "<option value='$row->ID'>$row->Code - $row->Title</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tài khoản đăng nhập <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="UserName">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Mật khẩu sử dụng <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" required="required" class="form-control col-md-7 col-xs-12" name="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email" required="required" class="form-control col-md-7 col-xs-12" name="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Họ <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="LastName">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="FirstName">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Trang mặc định tài khoản <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="HomePage">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Cho phép đăng nhập từ xa <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="AllowRemote" class="form-control col-md-7 col-xs-12">
                                            <option value='1'>Kích hoạt</option>
                                            <option value='0' selected="selected">Ngưng kích hoạt</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Giới tính</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div>
                                            <label style="padding-top:7px">
                                                <input type="radio" name="Gender" value="1" checked="checked"> &nbsp; Nam &nbsp;
                                                <input type="radio" name="Gender" value="0"> &nbsp; Nữ &nbsp;
                                                <input type="radio" name="Gender" value="2"> &nbsp; Khác &nbsp;
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Trạng thái sử dụng</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div>
                                            <label style="padding-top:7px">
                                                <input type="radio" name="Published" value="1" checked="checked"> &nbsp; Kích hoạt &nbsp;
                                                <input type="radio" name="Published" value="0"> Ngưng kích hoạt
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Kênh bán hàng</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div>
                                            <label style="padding-top:7px">
                                                <input type="radio" name="Chanel" value="0" checked="checked"> &nbsp; Online &nbsp;
                                                <input type="radio" name="Chanel" value="1"> MT & GT &nbsp;
                                                <input type="radio" name="Chanel" value="2">  &nbsp; Khác
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <br>
                        </div>
                        <!-- Image Thumb  -->
                        <div class="x_title">
                            <h3>Hình ảnh đại diện <small></small></h3>
                            <hr>
                        </div>
                        <div class="x_content"> 
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Chọn hình ảnh đại diện</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div>
                                        <label>
                                            <span class="btn btn-file">
                                                <input type="file" name="Image_upload" id="choosefile" />
                                            </span>
                                        </label>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Xem trước khi tải lên</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="dvPreview"><span style="padding-top:9px;display:block">No image selected</span></div>
                                </div>
                            </div>
                            <br>
                        </div>
                        <!-- Role  -->
                        <div class="x_title">
                            <h3>Quyền hạn sử dụng</h3>
                            <hr>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Chọn nhóm quyền</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div>
                                        <label>
                                            <select name="RoleID" class="form-control" id="RoleDetails">
                                                <?php 
                                                $result = $this->db->query("select * from ttp_role")->result();
                                                if(count($result)>0){
                                                    $temp =1 ;
                                                    foreach($result as $row){
                                                        if($temp==1) $data=$row;
                                                        $status = $row->Published==1 ? "Enable" : "Disable" ;
                                                        echo "<option value='$row->ID'>$row->Title - $status</option>";
                                                        $temp++;
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12" id="recive_ajax">
                                    <?php 
                                    $result = $this->db->query("select * from ttp_sitetree")->result();
                                    if(count($result)>0){
                                        echo '<table class="table table-bordered">';
                                        echo '<thead>
                                                <tr class="headings">
                                                    <th>STT</th>
                                                    <th>Module </th>
                                                    <th>Read</th>
                                                    <th>Write</th>
                                                    <th>Modify</th>
                                                    <th>Delete</th>
                                                    <th>Full</th>
                                                </tr>
                                            </thead><tbody>';
                                        $i=0;
                                        $permission = $data ? json_decode($data->DetailRole,true) : "" ;
                                        foreach($result as $row){
                                            $i++;
                                            $read='';
                                            $write='';
                                            $modify='';
                                            $delete='';
                                            $full='';
                                            $temp=0;
                                            if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('r',$permission[$row->Classname])){
                                                $read="checked='checked'";
                                                $temp++;
                                            }
                                            if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('w',$permission[$row->Classname])){
                                                $write="checked='checked'";
                                                $temp++;
                                            }
                                            if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('m',$permission[$row->Classname])){
                                                $modify="checked='checked'";
                                                $temp++;
                                            }
                                            if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('d',$permission[$row->Classname])){
                                                $delete="checked='checked'";
                                                $temp++;
                                            }
                                            if($temp==4){
                                                $full="checked='checked'";
                                            }
                                        ?>
                                        
                                            <tr class="even pointer">
                                                <td class="a-center ">
                                                    <?php echo $i; ?>
                                                </td>
                                                <td><?php echo $row->Title; ?></td>
                                                <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="r" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $read ?> /></td>
                                                <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="w" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $write ?> /></td>
                                                <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="m" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $modify ?> /></td>
                                                <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="d" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $delete ?> /></td>
                                                <td><input type='checkbox' class='checkfull' data="<?php echo $row->Classname ?>" <?php echo $full ?> /></td>
                                                
                                            </tr>

                                        <?php 
                                        }
                                        echo "</tbody></table>";
                                    }
                                ?>
                                </div>
                            </div>
                            <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <button type="submit" class="btn btn-success">Thực hiện tạo mới</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#RoleDetails").change(function(){
            var ID = $(this).val();
            $.ajax({
                url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/system_user_loadrole_by_id/' ?>"+ID, 
                success: function(result){
                    $("#recive_ajax").html(result);
                }
            });
        });

        $("#choosefile").change(function(){
            var Fileinput = document.getElementById("choosefile");
            var file = Fileinput.files[0];
            var imageType = /image.*/
            var dvPreview = $(".dvPreview");
            if(file.type.match(imageType)){
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr("style", "max-height:100px;max-width: 100px");
                    img.attr("src", e.target.result);
                    dvPreview.html(img);
                }
                reader.readAsDataURL(file);
            }else{
                console.log("Not an Image");
            }
        });
    });
</script>