<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2">
                <img src="http://online.sacngockhang.com/public/admin/images/logo.png" class="img-logo" />
            </div>
            <div class="col-xs-5">
                <div class="form-control circle-radius search-box">
                    <i class="fa fa-search"></i>
                    <input type="text" class="form-control" placeholder="Search...">
                </div>
            </div>
            <div class="col-xs-5 user-info">
                <div class="pull-right">
                    <a href="<?php echo base_url() . ADMINPATH . "/home/profile"; ?>">Hello, <b><?php echo $user->FirstName . " " .$user->LastName ?></b></a>
                    <a><img src="<?php echo file_exists($user->Thumb) ? base_url().$user->Thumb : base_url()."public/admin/images/user.png" ; ?>" alt="<?php echo $user->UserName ?>" class="img img-circle" /></a>
                    <a href=""><i class="fa fa-comments" aria-hidden="true"></i></a>
                    <a href="<?php echo base_url() . ADMINPATH . "/logout"; ?>"">Logout <i class="fa fa-power-off" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>