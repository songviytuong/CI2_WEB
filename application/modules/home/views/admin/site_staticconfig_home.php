<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="title-header">Cấu hình dữ liệu tĩnh</h3>
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <br>
                    <div class="x_content">
                        <br />
                        <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update" ?>" method="post">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên công ty</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="Title" value="<?php echo isset($data['Title']) ? $data['Title'] : "" ; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Hotline</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="Hotline" value="<?php echo isset($data['Hotline']) ? $data['Hotline'] : "" ; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="Email" value="<?php echo isset($data['Email']) ? $data['Email'] : "" ; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Địa chỉ</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="resizable_textarea form-control" name="Address" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo isset($data['Address']) ? $data['Address'] : "" ; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Giấy chứng nhận</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="resizable_textarea form-control" name="Certificate" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo isset($data['Certificate']) ? $data['Certificate'] : "" ; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Copyright</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="resizable_textarea form-control" name="Copyright" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo isset($data['Copyright']) ? $data['Copyright'] : "" ; ?></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="x_title">
                                <h3>Mã nhúng Google Analytics</small></h3>
                                <hr>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Google Analytics ID</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="Analytics" value="<?php echo isset($data['Analytics']) ? $data['Analytics'] : "" ; ?>" placeholder='Example: UA-61245331-1'>
                                </div>
                            </div>
                            <br>
                            <div class="x_title">
                                <h3>Danh sách IP không bắt nhập mã token</small></h3>
                                <hr>
                            </div>
                            <div class="form-group">
                                <?php 
                                $ip = file_get_contents('ipaccept.txt');
                                $ip = explode('|',$ip);
                                if(count($ip)>0){
                                    foreach ($ip as $value) {
                                        echo "<div class='col-xs-2'>$value</div>";
                                    }
                                }

                                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                                    $myip = $_SERVER['HTTP_CLIENT_IP'];
                                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                                    $myip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                                } else {
                                    $myip = $_SERVER['REMOTE_ADDR'];
                                }
                                echo "<div class='col-xs-12'><hr>Bạn đang truy cập bằng IP : ".$myip ." <a class='btn-sm' style='color:#27c' href='".ADMINPATH."/home/site_staticconfig/addmyiptolist'>>> Thêm IP này vào danh sách</a></div>";
                                ?>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <button type="submit" class="btn btn-success">Cập nhật thông tin</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
