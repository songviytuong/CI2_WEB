<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="title-header">Danh sách các webiste lấy tin</h3>
                            <?php 
                            echo "<small>Tất cả dữ liệu ($total)</small>";
                            if(count($group)>0){
                                foreach($group as $row){
                                    $name = $row->Published==1 ? "Enabled" : "Disabled" ;
                                    echo " | <small>$name ($row->Total)</small>";
                                }
                            }
                            ?>
                            <hr>
                            <a href="<?php echo $base_link . "add" ?>" class="btn btn-success pull-left"><i class="fa fa-plus"></i> Tạo mới</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="headings">
                                                <th class="checkbox-button"><input type="checkbox" id="checkall"></th>
                                                <th>Site Name </th>
                                                <th>Published </th>
                                                <th class=" no-link last"><span class="nobr">Action</span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if(count($data)>0){
                                                $i=$start;
                                                foreach($data as $row){ 
                                                    $i++;
                                            ?>
                                                    <tr class="even pointer">
                                                        <td><input type="checkbox"></td>
                                                        <td><?php echo "<a href='{$base_link}edit/$row->ID'>".$row->Site.'</a>'; ?></td>
                                                        <td class="a-right a-right "><?php echo $row->Published==1 ? "Enable" : "Disable" ; ?></td>
                                                        <td class=" last">
                                                            <a href='<?php echo $base_link . "edit/$row->ID" ?>'><i class="fa fa-pencil-square-o"></i> Edit </a>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <a href='<?php echo $base_link . "delete/$row->ID" ?>' class='delete_link'><i class="fa fa-trash-o"></i> Delete</a>
                                                        </td>
                                                    </tr>
                                            <?php 
                                                }
                                            }else{
                                                echo "<tr><td colspan='4' style='text-align:center'>Data is empty .</td></tr>";
                                            }
                                            ?>
                                        </tbody>
                    </table>
                    <?php if(count($data)>0) echo $nav; ?>
                </div>
            </div>
        </div>
    </div>
</div>
