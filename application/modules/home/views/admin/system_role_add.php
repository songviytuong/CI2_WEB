<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="title-header">Tạo mới nhóm quyền</h3>
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <br />
                    <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."addnew" ?>" method="post">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên nhóm quyền <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="Title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Trạng thái kích hoạt</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div>
                                    <label style="padding-top:7px">
                                        <input type="radio" name="Status" value="1" checked="checked"> &nbsp; Kích hoạt &nbsp;
                                        <input type="radio" name="Status" value="0"> Ngưng kích hoạt
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php 
                                $result = $this->db->query("select * from ttp_sitetree")->result();
                                if(count($result)>0){
                                    echo '<table class="table table-bordered">';
                                    echo '<thead>
                                            <tr class="headings">
                                                <th>STT</th>
                                                <th>Module </th>
                                                <th>Read</th>
                                                <th>Write</th>
                                                <th>Modify</th>
                                                <th>Delete</th>
                                                <th>Full</th>
                                                <th>Published</th>
                                            </tr>
                                        </thead><tbody>';
                                    $i=0;
                                    foreach($result as $row){
                                        $i++;
                                    ?>
                                    
                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <?php echo $i; ?>
                                            </td>
                                            <td><?php echo $row->Title; ?></td>
                                            <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="r" class="<?php echo "checkbox_$row->Classname" ?>" /></td>
                                            <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="w" class="<?php echo "checkbox_$row->Classname" ?>" /></td>
                                            <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="m" class="<?php echo "checkbox_$row->Classname" ?>" /></td>
                                            <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="d" class="<?php echo "checkbox_$row->Classname" ?>" /></td>
                                            <td><input type='checkbox' class='checkfull' data="<?php echo $row->Classname ?>" /></td>
                                            <td class="a-right a-right "><?php echo $row->Published==1 ? "Enabled" : "Disabled" ; ?></td>
                                            
                                        </tr>

                                    <?php 
                                    }
                                    echo "</tbody></table>";
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button type="submit" class="btn btn-success">Thực hiện tạo mới</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
