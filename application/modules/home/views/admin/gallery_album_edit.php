<?php 
    $error = isset($_GET['error']) ? $_GET['error'] : "" ;
    echo $error=="user_exists" ? "<script>alert('Username already exists !');</script>" : "" ;
?>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>GALLERY GROUP</h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <form action="<?php echo $base_link . 'search'; ?>" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Search for..." required>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>ALBUM <small> / Add new album</small></h2>
                    <div class="clearfix"></div>
                </div>
                <form id="formdata" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update" ?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
                    <div class="x_content"> 
                        <div class="form-group">
                            <label class=" col-md-3 col-sm-3 col-xs-12">Published</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div>
                                    <label style="padding-top:7px">
                                        <input type="radio" name="Published" value="1" <?php echo $data->Published==1 ? "checked='checked'" : '' ; ?>> &nbsp; Enable &nbsp;
                                        <input type="radio" name="Published" value="0" <?php echo $data->Published==0 ? "checked='checked'" : '' ; ?>> Disable
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 col-sm-6 col-xs-12">Title (<span class="required">*</span>)</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="CreateLink" required="required" class="form-control col-md-12 col-xs-12" name="Title" value='<?php echo $data->Title ?>'>
                            </div>
                            <div class="col-md-1 col-sm-1 col-xs-12">
                                <div>
                                    <label>
                                        <span class="btn btn-default btn-file btn-primary">
                                            Browse <input type="file" name="Images_upload[]" id="choosefile" multiple />
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                        <div class="form-group dvPreview">
                            <div id="dvPreview">
                                
                            </div>
                            <?php 
                            $IDfile = json_decode($data->Data,true);
                            $listid = count($IDfile)>0 ? implode(",",$IDfile) : "0" ;
                            $file = $this->db->query("select * from ttp_images where ID in($listid)")->result();
                            if(count($file)>0){
                                echo "<div class='row rowheader'>
                                <div class='col-md-1 col-sm-1 col-xs-12'>Preview</div>
                                <div class='col-md-4 col-sm-4 col-xs-12'>Image Name</div>
                                <div class='col-md-3 col-sm-3 col-xs-12'>Image Created</div>
                                <div class='col-md-2 col-sm-2 col-xs-12'>Action</div>
                                <div class='col-md-2 col-sm-2 col-xs-12'>Up / Down</div>
                            </div>";
                                foreach($file as $row){
                                    $key = array_search($row->ID,$IDfile);
                                    $dataup = $key==0 ? 0 : $key-1;
                                    $datadown = $key+1;
                                    $image = $this->lib->getfilesize($row->Thumb,113,63);
                                    $IDfile[$key] = "<div class='row filerow file$key'>
                                            <div class='col-md-1 col-sm-1 col-xs-12'><img src='$image' class='img-responsive' /></div>
                                            <div class='col-md-4 col-sm-4 col-xs-12'>$row->Name</div>
                                            <div class='col-md-3 col-sm-3 col-xs-12'>$row->Created</div>
                                            <div class='col-md-2 col-sm-2 col-xs-12'><a title='Remove this image in album' href='{$base_link}remove/$data->ID/$row->ID'>[x] Remove</a>
                                            </div>
                                            <div class='col-md-2 col-sm-2 col-xs-12'>
                                                <a href='{$base_link}move/$data->ID/$row->ID/$key/$dataup' class='rowup' dataup='$dataup' data='$key'><i class='fa fa-chevron-up'></i></a>
                                                <a href='{$base_link}move/$data->ID/$row->ID/$key/$datadown' class='rowdown' data='$key' datadown='$datadown'><i class='fa fa-chevron-down'></i></a>
                                            </div>
                                        </div>";
                                }
                                echo implode("",$IDfile);
                            }else{
                                echo "<div class='row rowheader'><div class='col-md-12 col-sm-12 col-xs-12'>Album is empty .</div></div>";
                            }
                            ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .filerow{padding:3px 0px;border-bottom:1px solid #E1e1e1;min-height: 50px}
    .rowheader{font-weight: bold;padding:5px 0px;border-bottom: 1px solid #CCC;margin-bottom:10px;}
    .filerow:hover{border-bottom:1px solid #ccc;background: #F5F5F5}
    .filerow i{margin-right:5px;}
    .filerow a{cursor: pointer}
</style>
<script>
    $(document).ready(function(){
        $("#choosefile").change(function(){
            var Fileinput = document.getElementById("choosefile");
            var numfile = Fileinput.files.length;
            var fd = new FormData(document.getElementById("formdata"));
            if (fd && numfile>0) {
                $.ajax({
                    url: "<?php echo $base_link.'loadimage' ?>",
                    type: "POST",
                    data: fd,
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    beforeSend: function(result) {
                        $(".dvPreview").html("<p style='text-align:center'><img src='public/admin/images/loading.gif' style='width: 15px;margin-right: 8px;' />Images uploading ...</p>");
                    },
                    success: function(result) {
                        location.reload();
                    },       
                    error: function(result) {

                    }       
                });
            }
        });
    });
</script>