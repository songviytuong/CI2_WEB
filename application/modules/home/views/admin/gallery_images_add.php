<div class="content">
    <div class="container-fluid">
        <div class="content-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="x_panel">
                        <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."addnew" ?>" method="post" enctype="multipart/form-data">
                            <div class="x_content"> 
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <span class="btn-file-upload">
                                            <i class="fa fa-plus"></i>
                                            <input type="file" name="Images_upload[]" id="choosefile" multiple />
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group dvPreview">
                                    <div class="col-xs-12">
                                        <div class='row rowheader'>
                                            <div class='col-md-1 col-sm-1 col-xs-12'>Preview</div>
                                            <div class='col-md-5 col-sm-5 col-xs-12'>Image Name</div>
                                            <div class='col-md-3 col-sm-2 col-xs-12'>Image Type</div>
                                            <div class='col-md-3 col-sm-2 col-xs-12'>Image Size</div>
                                        </div>
                                        <div id="dvPreview"></div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <div class="row">
                                        <button type="submit" class="btn btn-success">Thực hiện upload</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .dvPreview{display:none;}
    .filerow{padding:3px 0px;min-height: 50px}
    .rowheader{font-weight: bold;padding:5px 0px;}
    .filerow:hover{}
    .filerow i{margin-right:5px;}
    .filerow a{cursor: pointer}
</style>
<script>
    $(document).ready(function(){
        $("#choosefile").change(function(){
            var Fileinput = document.getElementById("choosefile");
            var numfile = Fileinput.files.length;
            var dvPreview = $("#dvPreview");
            dvPreview.html("");
            if(numfile>0){
                var j=0;
                for (var i = 0; i < numfile; i++) {
                    var reader = new FileReader();
                    var file = Fileinput.files[i];
                    var imageType = /image.*/;
                    var head = "";
                    if(file.type.match(imageType)){
                        reader.onloadend = function (e) {
                            var file = Fileinput.files[j];
                            dvPreview.append("<div class='row filerow file"+j+"'><div class='col-md-1 col-sm-1 col-xs-12'><img src='"+e.target.result+"' class='img-responsive' /></div><div class='col-md-5 col-sm-5 col-xs-12'>"+file.name+"</div><div class='col-md-3 col-sm-3 col-xs-12'>"+file.type+"</div><div class='col-md-3 col-sm-3 col-xs-12'>"+file.size+"</div></div>");
                            j++;
                        }
                    }else{
                        console.log("Not an Image");
                    }
                    reader.readAsDataURL(file);
                }
                $(".dvPreview").fadeIn('slow');
            }
        });
    });
</script>