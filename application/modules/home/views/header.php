<?php 
  $json = $static ? json_decode($static->Data) : (object) array() ;
?>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6"><b>GIAO HÀNG MIỄN PHÍ</b> TOÀN QUỐC CHO ĐƠN HÀNG TỪ 500.000Đ</div>
            <div class="col-xs-12 col-sm-6 col-md-6 text-right hidden-xs"><a onclick="load_box_check_order(this,'load_box_check_order')"><span class="glyphicon glyphicon-map-marker"></span> Kiểm tra đơn hàng</a><i class="fa fa-phone"></i> HOTLINE <?php echo isset($json->Hotline) ? $json->Hotline : '' ; ?></div>
        </div>
    </div>
</div>
<div class="menu-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-7 col-sm-3 col-md-2">
                <a href="<?php echo base_url() ?>"><img src="<?php echo $logo ? CDN.$logo->Thumb : '' ; ?>" class="img-responsive" /></a>
            </div>
            <div class="col-xs-5 col-sm-9 col-md-10 pull-right">
                <input type="checkbox" class="hidden" id="openmenu">
                <label for="openmenu" class="hidden-lg hidden-md openmenu"><span class="glyphicon glyphicon-menu-hamburger"></span></label>
                <ul class="text-right hidden-xs hidden-sm menu-box">
                    <?php 
                    $menu = $this->db->query("select Title,Alias,Classname from ttp_pagelinks where Published=1 and Classname!='index' order by STT ASC limit 0,6")->result();
                    foreach($menu as $row){
                        echo '<li><a href="'.$row->Alias.'.html" class="bounceInDown animated">'.$row->Title.'</a></li>';
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>