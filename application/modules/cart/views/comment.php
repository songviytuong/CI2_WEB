<ul class="media-list">
    <?php 
    if(count($Comments)>0){
        foreach($Comments as $key=>$row){
            $class= $key>2 ? "hidden" : "";
        ?>
        <li class="media <?php echo $class ?>">
            <div class="media-body">
                <h6 class="media-heading"> <b><?php echo $row->Name ?> </b><span>| Ngày <?php echo date('d/m/Y',strtotime($row->Create)) ?> vào lúc <?php echo date('H:i',strtotime($row->Create)) ?></span></h6>
                <p><?php echo $row->Comment ?></p>
            </div>
        </li>
        <?php 
        }
    }
    ?>
</ul>
<?php 
if(count($Comments)>3){
?>
<p class="text-center"><a onclick="show_more_comment(this)" class="btn-link"> <i class="snk snk-right-dir"></i>Xem thêm bình luận (<?php echo count($Comments)-3 ?>)</a></p>
<?php 
}
?>