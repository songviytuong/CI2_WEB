<div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
        <h5 class="modal-title"> <span>ĐẶT HÀNG THÀNH CÔNG</span></h5>
      </div>
      <div class="modal-body text-center">
        <p>Cảm ơn quý khách hàng đã quan tâm và sử dụng sản phẩm Sắc Ngọc Khang của công ty cổ phần dược phẩm Hoa Thiên Phú. Bộ phận chăm sóc khách hàng của công ty sẽ liên hệ giao hàng cho quý khách trong thời gian sớm nhất. Trân trọng !</p>
          <?php 
          if($Total>=300000){
            ?>
            <p class="alert alert-success">Chúc mừng bạn !!! Đơn hàng của bạn được may mắn nhận được một lượt quay thưởng nhận quà từ chương trình quay thưởng của http://sacngockhang.com.</p>
            <a href="<?php echo base_url().'uudaisacngockhang/event_luckydraw' ?>" class="btn btn-danger" style="color:#fff;border-radius:20px;">Quay thưởng nhận quà</a>
            <?php
          }else{
          ?>
            <a href="<?php echo base_url() ?>" class="btn btn-primary" style="color:#fff;border-radius:20px;">TRỞ LẠI TRANG CHỦ</a>
            <a href="https://www.facebook.com/sharer.php?u=<?php echo base_url() ?>" class="btn btn-info" style="color:#fff;border-radius:20px;">CHIA SẺ THÔNG TIN NÀY</a>
          <?php 
          }
          ?>
      </div>
    </div>
</div>