<div role="document" class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title"> <span>Kiểm tra đơn hàng</span></h4>
          </div>
          <div class="modal-body">
            <div class="row hidden" id="error-box">
                <div class="col-xs-12">
                    <div class="alert alert-danger"><i class="snk snk-info-circled"></i><span></span></div>
                </div>
            </div>
            <div class="row">
            	<form id="check_order">
            	<div class="col-xs-12 col-sm-6">
            		<p>Mã đơn hàng</p>
            		<input type="text" class="form-control" name="OrderID" autofocus="true" placeholder="Ví dụ : DH1708_53901" />
            	</div>
            	<div class="col-xs-12 col-sm-6">
            		<p>Số điện thoại người nhận</p>
            		<input type="text" class="form-control" name="OrderPhone" />
            	</div>
            	</form>
            </div>
            <div class="row">
            	<div class="col-xs-12">
            		<p style="margin-top:20px;">(*) Nếu Quý khách thực hiện kiểm tra đơn hàng không thành công vui lòng liên hệ tổng đài <b class="text-danger">1900 6033</b> để được trung tâm hỗ trợ trực tuyến hỗ trợ nhanh nhất.</p>
            	</div>
            </div>
          </div>
          <div class="modal-footer text-center">
            <button type="button" class="btn btn-success btn-lg btn-block" onclick="check_order(this,'check_order','check_order')">Thực hiện kiểm tra</button>
          </div>
    </div>
</div>
