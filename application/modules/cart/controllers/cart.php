<?php 
class Cart extends CI_Controller { 
    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
	}
    
    public $data = array();
    
    public function add_cart(){
        $cart = $this->session->userdata("cart_funel");
        $cart = $cart!='' ? $cart : array() ;
        $ProductsID = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $Amount = isset($_POST['amount']) ? (int)$_POST['amount'] : 1 ;
        $Amount = $Amount<0 ? 1 : $Amount ;
        if($ProductsID>0){
            $Products = $this->db->query("select * from ttp_report_products where ID=$ProductsID")->row();
            if($Products){
                $cart[$ProductsID] = array(
                    'Title' => $Products->Title,
                    'Price' => $Products->Price,
                    'Image' => CDN.$Products->PrimaryImage,
                    'Amount'=> $Amount
                );
                $this->click_position(2,count($cart),1,$Products->Title);
                $this->session->set_userdata("cart_funel",$cart);
            }
        }
        echo json_encode(array('error'=>false,'message'=>'add cart success'));
    }

    public function check_order(){
        $phone = isset($_POST['OrderPhone']) ? strip_tags(trim($_POST['OrderPhone'])) : '' ;
        $phone = preg_replace('/[^0-9]+/i', '', $phone);
        $phone = mysql_real_escape_string($phone);
        $OrderID = isset($_POST['OrderID']) ? mysql_real_escape_string(strip_tags(trim($_POST['OrderID']))) : '' ;
        $response = array('error'=>false,'message'=>'','focus'=>'');
        if(strlen($phone)<10 || strlen($phone)>11){$response['error'] = true;$response['focus'] = 'Phone';$response['message'] = 'Số điện thoại không hợp lệ.';}
        if($OrderID=='' || strlen($OrderID)<2){$response['error'] = true;$response['focus'] = 'OrderID';$response['message'] = 'Mã đơn hàng không hợp lệ';}
        if($response['error']==false){
            $check = $this->db->query("select ID from ttp_report_order where MaDH = '$OrderID' and Phone='$phone'")->row();
            if(!$check){
                $response['error'] = true;
                $response['message'] = 'Không tìm thấy đơn hàng theo yêu cầu của bạn !';
            }else{
                $this->session->set_userdata('order_check',$check->ID);
            }
        }
        echo json_encode($response);
    }

    public function load_order_details(){
        $order = $this->session->userdata('order_check');
        $order = (int)$order;
        $check = $this->db->query("select * from ttp_report_order where ID = $order")->row();
        if($check){
            $details = $this->db->query("select a.Title,b.Amount,b.Price,b.PriceDown,b.Total from ttp_report_products a,ttp_report_orderdetails b where a.ID=b.ProductsID and b.OrderID=$check->ID")->result();
            $this->load->view('order',array('data'=>$check,'details'=>$details));
        }
    }

    public function load_box_check_order(){
        $this->load->view("load_box_check_order");
    }
    
    public function send_comment(){
        $Name = isset($_POST['Name']) ? strip_tags($_POST['Name']) : '' ;
        $FunelID = isset($_POST['FunelID']) ? (int)$_POST['FunelID'] : 0 ;
        $Phone = isset($_POST['Phone']) ? strip_tags($_POST['Phone']) : '' ;
        $Comments = isset($_POST['Comments']) ? strip_tags($_POST['Comments']) : '' ;
        $response = array('error'=>false,'message'=>'','focus'=>'');
        if($Comments==''){$response['error'] = true;$response['focus'] = 'Comments';$response['message'] = 'Vui lòng nhập bình luận';}
        if($Name==''){$response['error'] = true;$response['focus'] = 'Name';$response['message'] = 'Vui lòng nhập tên';}
        if($response['error']==false){
            $data = array(
                'Name'      => $Name,
                'Phone'     => $Phone,
                'FunelID'   => $FunelID,
                'Comment'   => $Comments,
                'Create'    => date('Y-m-d H:i:s'),
                'Status'    => 1
            );
            $this->db->insert('ttp_funels_comments',$data);
        }
        echo json_encode($response);
    }
    
    public function send_callme(){
        $Name = isset($_POST['Name']) ? strip_tags($_POST['Name']) : '' ;
        $Phone = isset($_POST['Phone']) ? strip_tags($_POST['Phone']) : '' ;
        $Comments = isset($_POST['Note']) ? strip_tags($_POST['Note']) : '' ;
        $response = array('error'=>false,'message'=>'','focus'=>'');
        if($Comments==''){$response['error'] = true;$response['focus'] = 'Note';$response['message'] = 'Vui lòng nhập nội dung cấn tư vấn';}
        if($Phone==''){$response['error'] = true;$response['focus'] = 'Phone';$response['message'] = 'Vui lòng nhập số điện thoại';}
        if($Name==''){$response['error'] = true;$response['focus'] = 'Name';$response['message'] = 'Vui lòng nhập tên';}
        if($response['error']==false){
            $data = array(
                'Name'      => $Name,
                'Phone'     => $Phone,
                'Note'      => $Comments,
                'Created'   => date('Y-m-d H:i:s')
            );
            $this->db->insert('ttp_report_customer_callme',$data);
            $response['message'] = 'Chúng tôi đã nhận được thông tin của bạn. Chuyên gia chăm sóc khách hàng của chúng tôi sẽ gọi điện giải đáp vấn đề của bạn trong thời gian sớm nhất.';
        }
        echo json_encode($response);
    }
    
    public function load_order(){
        $warehouse = $this->db->query("select * from ttp_report_warehouse where Published=1")->result();
        $city = $this->db->query("select * from ttp_report_city")->result();
        $district = $this->db->query("select * from ttp_report_district where CityID=30")->result();
        $this->data = array(
            'cart'      => $this->session->userdata("cart_funel"),
            'warehouse' => $warehouse,
            'city'      => $city,
            'district'   => $district
        );
        $this->load->view("cart",$this->data);
    }
    
    public function load_comment(){
        $funelID = isset($_POST['FunnelID']) ? (int)$_POST['FunnelID'] : 0 ;
        $this->data['Comments'] = $this->db->query("select * from ttp_funels_comments where FunelID=$funelID order by ID DESC")->result();
        $this->load->view("comment",$this->data);
    }
    
    public function thanks_page(){
        $session = $this->session->userdata("customer");
        $total = isset($session['Total']) ? $session['Total'] : 0 ;
        $this->load->view("thanks",array('Total'=>$total));
    }
    
    public function remove_products(){
        $response = array('error'=>false,'message'=>'remove cart success');
        $cart = $this->session->userdata("cart_funel");
        $cart = $cart!='' ? $cart : array() ;
        if(count($cart)>1){
            $ProductsID = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
            if($ProductsID>0){
                unset($cart[$ProductsID]);
                $this->session->set_userdata("cart_funel",$cart);
            }
        }else{
            $response = array('error'=>true,'message'=>'Đơn hàng phải có ít nhất 1 sản phẩm');
        }
        echo json_encode($response);
    }
    
    public function load_district_from_city(){
        $CityID = isset($_POST['CityID']) ? (int)$_POST['CityID'] : 0 ;
        $District = $this->db->query("select * from ttp_report_district where CityID=$CityID")->result();
        if(count($District)>0){
            foreach($District as $row){
                echo "<option value='$row->ID'>$row->Title</option>";
            }
        }
    }
    
    public function click_position($click=1,$position=0,$funelID=0,$title=''){
        $position = isset($_POST['position']) ? (int)$_POST['position'] : $position ;
        $funelID = isset($_POST['funel']) ? (int)$_POST['funel'] : $funelID ;
        $title = isset($_POST['title']) ? $_POST['title'] : $title ;
        if($funelID>0){
            $data = array(
                'FunelsID'  => $funelID,
                'Click'     => $click,
                'TitleButton'=> $title,
                'Position'  => $position,
                'YesNo'     => 1,
                'Created'   => date('Y-m-d H:i:s')
            );
            $this->db->insert('ttp_funels_report',$data);
        }
        if($click==1)
        echo json_encode(array('error'=>false,'message'=>''));
    }
    
    public function create_order(){
        $coupon_code = isset($_POST['coupon_code']) ? mysql_real_escape_string(trim($_POST['coupon_code'])) : '' ;
        $CityID = isset($_POST['CityID']) ? (int)$_POST['CityID'] : 0 ;
        $DistrictID = isset($_POST['DistrictID']) ? (int)$_POST['DistrictID'] : 0 ;
        $FunelID = isset($_POST['FunelID']) ? (int)$_POST['FunelID'] : 0 ;
        $AddressOrder = isset($_POST['AddressOrder']) ? strip_tags(trim($_POST['AddressOrder'])) : '' ;
		$phone = isset($_POST['Phone']) ? strip_tags(trim($_POST['Phone'])) : '' ;
		$phone = preg_replace('/[^0-9]+/i', '', $phone);
		$name = isset($_POST['Name']) ? strip_tags(trim($_POST['Name'])) : '' ;
		$note = isset($_POST['Note']) ? strip_tags(trim($_POST['Note'])) : '' ;
        $response = array('error'=>false,'message'=>'','focus'=>'');
		if($AddressOrder==''){$response['error'] = true;$response['focus'] = 'AddressOrder';$response['message'] = 'Địa chỉ nhận hàng không hợp lệ';}
		if($CityID==0){$response['error'] = true;$response['focus'] = 'CityID';$response['message'] = 'Tỉnh/ Thành phố buộc phải chọn';}
		if($DistrictID==0){$response['error'] = true;$response['focus'] = 'DistrictID';$response['message'] = 'Quận/ Huyện buộc phải chọn';}
		if(strlen($phone)<10 || strlen($phone)>11){$response['error'] = true;$response['focus'] = 'Phone';$response['message'] = 'Số điện thoại không hợp lệ.';}
        if($name=='' || strlen($name)<2){$response['error'] = true;$response['focus'] = 'Name';$response['message'] = 'Họ tên không hợp lệ';}
		if($response['error']==false){
            $check_customer = $this->db->query("select * from ttp_report_customer where Phone1='$phone'")->row();
            if($check_customer){
                $CustomerID = $check_customer->ID;
            }else{
                $arr = array(
                    'Phone1'    => $phone,
                    'Name'      => $name,
                    'Address'   => $AddressOrder
                );
                $this->db->insert("ttp_report_customer",$arr);
                $CustomerID = $this->db->insert_id();
            }
			$monthuser = date("m",time());
	        $yearuser = date("Y",time());
	        $userorder = $this->db->query("select count(ID) as SL from ttp_report_order where MONTH(Ngaydathang)=$monthuser and YEAR(Ngaydathang)=$yearuser")->row();
            $User = $this->db->query("select a.Phone,b.UserID,b.ID from ttp_user a,ttp_report_order_asign b where a.ID=b.UserID and b.Status=1")->row();
            $UserID = $User ? $User->UserID : 2 ;
	        $yearuser = date("y",time());
	        $userorder = $userorder ? $userorder->SL+1+$monthuser+$yearuser : 1+$monthuser+$yearuser ;
	        $monyear = date('ym');
	        $idbyUser = str_pad($userorder, 6, '0', STR_PAD_LEFT);
	        $MaDH = 'DH'.$monyear."_".$idbyUser;
            
			$data = array(
				'MaDH'			=> $MaDH,
				'AddressOrder'	=> $AddressOrder,
				'UserID'		=> $UserID,
				'CityID'		=> $CityID,
				'DistrictID'	=> $DistrictID,
				'CustomerID'	=> $CustomerID,
				'Note'			=> $note,
				'Ghichu'		=> $note,
				'SourceID'		=> 9,
				'KenhbanhangID'	=> 3,
				'Name'			=> $name,
				'Phone'			=> $phone,
				'Status'		=> 2,
				'Ngaydathang'	=> date('Y-m-d H:i:s'),
				'HistoryEdited'	=> date('Y-m-d H:i:s'),
				'KhoID'			=> 1,
				'SendSMS'		=> 1,
                'FunelID'       => $FunelID
			);
			$this->db->insert("ttp_report_order",$data);
			$orderid = $this->db->insert_id();
			$data_history = array(
				'OrderID'	=> $orderid,
				'Thoigian'	=> date('Y-m-d H:i:s'),
				'Status'	=> 2,
				'Ghichu'	=> 'Tạo đơn hàng từ website '.base_url(),
				'UserID'	=> $UserID
			);
			$this->db->insert('ttp_report_orderhistory',$data_history);
			
			$data = $this->session->userdata("cart_funel");
			$total = 0;
			$totalamount = 0;
			$content = "";
			if(count($data)>0){
				$details = array_keys($data);
				$details = implode(',',$details);
				if($details!=''){
					$datadetails = $this->db->query("select a.ID,a.Price,a.SpecialStartday,a.SpecialStopday,a.SpecialPrice,a.Title from ttp_report_products a where ID in($details)")->result();
					if(count($datadetails)>0){
						$details_insert = array();
						foreach($datadetails as $row){
							$price = time()>=strtotime($row->SpecialStartday) && time()<=strtotime($row->SpecialStopday) ? $row->SpecialPrice : $row->Price ;
							$quantity = isset($data[$row->ID]['Amount']) ? $data[$row->ID]['Amount'] : 0 ;
							$total_row = $price*$quantity;
							$total = $total+$total_row;
							if($quantity>0){
								$details_insert[] = "($orderid,$row->ID,$price,$quantity,$total_row)";
								$totalamount = $totalamount+$quantity;
								$content.="$quantity $row->Title";
							}
						}
						if(count($details_insert)>0){
							$sql = "insert into ttp_report_orderdetails(OrderID,ProductsID,Price,Amount,Total) values".implode(',',$details_insert);
							$this->db->query($sql);
						}
					}
				}
				$content.= "";
                $value_coupon = $this->value_coupon($coupon_code,$total);
                $value_fee = $this->value_fee($DistrictID,$total,$value_coupon['value']);
                $this->db->query("update ttp_report_order set SoluongSP=$totalamount,Total=$total,Chietkhau=".$value_coupon['value'].",VoucherID=".$value_coupon['ID'].",Chiphi=$value_fee where ID=$orderid");
				$this->session->set_userdata("cart_funel",array());
				$name = $this->lib->asciiCharacter($name);
                $content  = $this->lib->asciiCharacter($content);
                $send = $User ? $User->Phone : '84966865632';
                $data = array(
                    'u'     => 'hoathien',
                    'pwd'   => 'r5c8h',
                    'from'  => 'HoaThienPhu',
                    'phone' => $send,
                    'sms'   => 'Khach hang '.$name.' - '.$phone." dat hang ".$content
                );
                $this->db->query("update ttp_report_order_asign set Status=0");
                $next = $User ? $User->ID + 1 : 1 ;
                $next = $next>5 ? 1 : $next ;
                $this->db->query("update ttp_report_order_asign set Status=1 where ID=$next");
                file_get_contents("http://sms.vietguys.biz/api/?".http_build_query($data));
                if(($total-$value_coupon['value'])>=300000){
                    $this->db->query("update ttp_report_customer set ScoreLuckydraw=ScoreLuckydraw+1 where ID=$CustomerID");
                    $this->session->set_userdata("customer",array('Phone'=>$phone,'ID'=>$CustomerID,'City'=>$CityID,'OrderID'=>$orderid,'Total'=>$total-$value_coupon['value']+$value_fee));
                }
			}
        }
        echo json_encode($response);
    }

    public function getfee(){
        $id = isset($_POST['district']) ? (int)$_POST['district'] : 0 ;
        $total = isset($_POST['total']) ? (int)$_POST['total'] : 0 ;
        $coupon = isset($_POST['coupon']) ? (int)$_POST['coupon'] : 0 ;
        $total = $total-$coupon;
        $check = $this->db->query("select * from ttp_report_district where ID=$id")->row();
        $fee = 0;
        if($check){
            if($total<=499000){
                $fee = $check->PriceCost;
            }
            if($total>=500000 && $total<2000000){
                $fee = $check->PriceCost1;
            }
            if($total>=2000000){
                $fee = $check->PriceCost2;
            }
        }
        echo json_encode(array("fee"=>$fee>0 ? "Chi phí vận chuyển phát sinh cho khu vực bạn vừa chọn : <b>".number_format($fee)."đ</b>" : "Đơn hàng của bạn được miễn phí vận chuyển giao hàng tận nơi.","number_fee"=>$fee,"total"=>$fee+$total));
    }

    public function value_fee($id=0,$total=0,$coupon=0){
        $total = $total-$coupon;
        $check = $this->db->query("select * from ttp_report_district where ID=$id")->row();
        $fee = 0;
        if($check){
            if($total<=499000){
                $fee = $check->PriceCost;
            }
            if($total>=500000 && $total<2000000){
                $fee = $check->PriceCost1;
            }
            if($total>=2000000){
                $fee = $check->PriceCost2;
            }
        }
        return $fee;
    }

    public function value_coupon($coupon='',$Total=0){
        $check = $this->db->query("select * from ttp_report_voucher where Code='$coupon' and Startday<='".date('Y-m-d H:i:s')."' and Stopday>='".date('Y-m-d H:i:s')."' and Status=0")->row();
        if($check){
            if($check->PriceReduce>0){
                $value = $check->PriceReduce;
            }
            if($check->PercentReduce>0){
                $value = $check->PercentReduce*100/$Total;
            }
            if($check->PolicyID>0){
                $policy = $this->db->query("select * from ttp_report_voucher_policy where ID=$check->PolicyID")->row();
                $data = $policy ? json_decode($policy->Data,true) : array();
                $expression = $this->lib->expression();
                $valid = true;
                if(count($data)>0){
                    foreach($data as $key=>$row){
                        if(isset($expression[$key])){
                            if(count($row)>0){
                                foreach($row as $exp=>$item){
                                    if(isset($expression[$exp]) && count($item)>1){
                                        if($valid==true){
                                            $valid = $this->lib->valid_expression($$exp,$item);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if($valid==false){
                    return array('value'=>0,'ID'=>0);
                }else{
                    if($check->UseOne==0){
                        $this->db->where("ID",$check->ID);
                        $this->db->update('ttp_report_voucher',array('Status'=>1));
                    }
                    return array('value'=>$value,'ID'=>$check->ID);
                }
            }
        }
        return array('value'=>0,'ID'=>0);
    }

    public function check_coupon_code(){
        $coupon = isset($_POST['coupon']) ? mysql_real_escape_string(trim($_POST['coupon'])) : 0 ;
        $Total = isset($_POST['total']) ? (int)$_POST['total'] : 0 ;
        $fee = isset($_POST['fee']) ? (int)$_POST['fee'] : 0 ;
        $Total = $Total+$fee;
        $check = $this->db->query("select * from ttp_report_voucher where Code='$coupon' and Startday<='".date('Y-m-d H:i:s')."' and Stopday>='".date('Y-m-d H:i:s')."' and Status=0")->row();
        $response = array("error"=>true,"focus"=>"#coupon_code","message"=>"Mã giảm giá bạn vừa nhập không hợp lệ.","data"=>"");
        if($check){
            $response["error"] = false;
            if($check->PriceReduce>0){
                $response["message"] = "Mã của bạn được giảm ".number_format($check->PriceReduce)."đ";
                $response["coupon"] = $check->PriceReduce;
            }
            if($check->PercentReduce>0){
                $response["message"] = "Mã của bạn được giảm ".number_format($check->PercentReduce)."%";
                $response["coupon"] = $check->PercentReduce*100/$total;
            }
            if($check->PolicyID>0){
                $policy = $this->db->query("select * from ttp_report_voucher_policy where ID=$check->PolicyID")->row();
                $data = $policy ? json_decode($policy->Data,true) : array();
                $expression = $this->lib->expression();
                $policydata = array();
                $valid = true;
                if(count($data)>0){
                    foreach($data as $key=>$row){
                        if(isset($expression[$key])){
                            if(count($row)>0){
                                foreach($row as $exp=>$item){
                                    if(isset($expression[$exp]) && count($item)>1){
                                        $policydata[] = $expression[$exp].' '.$expression[$key].' '.$expression[$item[0]].' '.$item[1];
                                        if($valid==true){
                                            $valid = $this->lib->valid_expression($$exp,$item);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if($valid==false){
                    $response["error"] = true;
                    $policydata = implode(',', $policydata);
                    $response["message"] .= " chỉ áp dụng cho ".$policydata;
                    $response["message"] = "<i class='fa fa-warning'></i> ".$response["message"]; 
                }else{
                    $response["message"] = "<i class='fa fa-check'></i> ".$response["message"].".";
                }
                $response["valid"] = $valid;
            }
        }
        echo json_encode($response);
    }
	
	public function sendmail($Title="HTP | Khách hàng đặt hàng từ website",$message=""){
        if($Title!="" && $message!=''){
            $config = $this->db->query("select * from ttp_email where Published=1 limit 0,1")->row();
            if($config){
                $json = $config->Data!='' ? json_decode($config->Data) : (object)array();
                $this->load->library("email");
                $this->load->library("my_email");
                $data = array(
                    'message'       =>$message,
                    'user'          =>$json->SMTP_user,
                    'password'      =>$json->SMTP_password,
                    'protocol'      =>$json->Protocol,
                    'smtp_host'     =>$json->SMTP_host,
                    'smtp_port'     =>$json->SMTP_port,
                    'from_sender'   =>$json->SMTP_user,
                    'subject_sender'=>$Title,
                    'to_receiver'   =>$json->Email_reciver,
					'name_sender'	=>$Title
                );
                $this->my_email->config($data);
                $this->my_email->sendmail();
            }
        }
    }
}
?>
