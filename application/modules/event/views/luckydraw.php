    <div class="luckydraw-page">
      <div class="container">
        
        <div class="row">
          <div class="col-xs-12">
            <div class="draw">
              <div class="wheel">
                <canvas id="canvas" width="700" height="700"></canvas>
                <a onClick="startSpin(this)" class="btn-start"><span class="pulse">Bắt đầu</span></a>
                <div class="gra"></div>
                <div class="gra-relative"></div>
                <div class="gra-2 pulse"></div>
              </div>
            </div>
            <div class="rule-box">
              <h4>Khách hàng may mắn mới nhất</h4>
              <ul>
                <?php
                $result = $this->db->query("select a.Name,a.Phone1,b.Created from ttp_report_customer a,ttp_result_luckydraw b where a.ID=b.CustomerID and b.ResultCode>0 order by b.ID DESC limit 0,20")->result();
                if(count($result)>0){
                  $i=1;
                  foreach($result as $row){
                      if($row->Name!=''){
                      $row->Phone1 = substr_replace($row->Phone1, str_repeat("x", 6), 0, 6);
                      echo "<li><span class='number'>$i</span><span class='span1'>$row->Name</span><span class='span2'>".date('d/m/Y H:i:s',strtotime($row->Created))."</span><span class='span2' style='margin-right:20px'>$row->Phone1</span></li>";
                      $i++;
                      }
                  }
                }
                ?>
              </ul>
            </div>
          </div>
        </div>
          <h2 class="title"></h2>
        <div class="panel panel-default hidden-xs">
          <div class="panel-heading">Danh sách quà tặng</div>
          <div class="panel-body">
            <div class="full-product-box row">
              <?php
              $prize = $this->db->query("select * from ttp_define where `group`='prize' and `type`='lucky_draw' and position=0")->result();
              if(count($prize)>0){
                  foreach($prize as $row){
                      echo '<div class="col-xs-6 col-sm-3">
                        <div class="product-item"><a class="product-img img-responsive"><img alt="Product img" src="'.$row->description.'"></a>
                          <p class="title"> <a class="text">'.$row->name.'</a></p>
                          <p class="meta"><span class="price"><b>'.$row->color.'</b></span></p>
                        </div>
                      </div>';
                  }
              }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url() ?>public/site/js/Winwheel.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
    <script>
        var theWheel = new Winwheel({'drawMode' : 'image','numSegments'  : 8,'segments'     :[{'text' : '7'},{'text' : '8'},{'text' : '1'},{'text' : '2'},{'text' : '3'},{'text' : '4'},{'text' : '5'},{'text' : '6'}],'animation' :{'type': 'spinToStop','easing': 'Power2.easeInOut','duration' : 8,'spins': 1,'callbackFinished' : 'alertPrize()'}});
        var loadedImg = new Image();
        loadedImg.onload = function(){theWheel.wheelImage = loadedImg;theWheel.draw();}
        loadedImg.src = "<?php echo base_url() ?>public/site/images/luckydraw/draw-b-oldg.png";
        var wheelPower    = 3;
        var wheelSpinning = false;
        var gift = 0;
        function startSpin(){
            var stopAt = theWheel.getRandomForSegment(2);
            theWheel.animation.stopAngle = stopAt;
            theWheel.animation.spins = 25;
            theWheel.startAnimation();
            wheelSpinning = true;
            
//            if (wheelSpinning == false){
//                $.post('<?php echo base_url() ?>uudaisacngockhang/segmentNumber',{},function(result){
//                    if(result.error==false){
//                      gift = result.number;
//                      var stopAt = theWheel.getRandomForSegment(result.number);
//                      theWheel.animation.stopAngle = stopAt;
//                      theWheel.animation.spins = 25;
//                      theWheel.startAnimation();
//                      wheelSpinning = true;
//                    }else{
//                      alert(result.message);
//                    }
//                },"json");
//            }
        }

        function resetWheel(){
            theWheel.stopAnimation(false);
            theWheel.rotationAngle = 0;
            theWheel.draw();
            wheelSpinning = false;
        }

        function alertPrize(){
//            var winningSegment = theWheel.getIndicatedSegment();
            $.get("<?php echo base_url() ?>event/showGift",{gift:gift},function(result){
                $("#order-modal").html(result);
            });
            $("#order-modal").modal("show");
        }

        function notenought(){
          alert("Xin lỗi ! Bạn đã hết lượt quay cho trò chơi này !");
        }
    </script>

<style>
  .footer{display: none}
</style>
