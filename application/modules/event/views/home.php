<div class="content bg-event bg-white">
	<div class="container">
		<div class="title-event-regis"></div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 rules">
				<h3 class="title">THỂ LỆ CHƯƠNG TRÌNH</h3>
				<hr>
				<ul>
					<li>500 quý khách hàng đầu tiên khi tham gia chương trình sau đó điền đầy đủ thông tin Quý khách sẽ nhận ngay ưu đãi giảm giá được gửi về số điện thoại đã đăng kí với một trong những mã ưu đãi sau:
						<ul>
							<li>5% ,  50.000 VNĐ  : áp dụng cho đơn hàng từ 499.000</li>
							<li>10% , 100.000 VNĐ : áp dụng cho đơn hàng từ 999.000</li>
							<li>15% , 200.000 VNĐ : áp dụng cho đơn hàng từ 1.499.000 – 10.000.000</li>
						</ul>
					</li>	
					<li>Mỗi khách hàng chỉ được nhận DUY NHẤT  1 mã ưu đãi khi tham gia chương trình</li>
					<li>Quý khách dùng để nhập vào vị trí “Mã ưu đãi” khi đặt hàng trên website http://sacngockhang.com</li>
					<li>Thời hạn sử dụng Mã ưu đãi từ 1/9/2017 – 30/9/2017</li>
					<li>ĐẶC BIỆT sau khi đặt hàng thành công với hóa đơn trên 300.000, Quý khách hàng nhận được 1 lượt quay TRÚNG THƯỞNG những phần quà giá trị chỉ có tại website Sắc Ngọc Khang đấy nhé  (Lưu ý: Số lượng QUÀ TẶNG CÓ HẠN và không có giá trị quy đổi thành tiền hay sản phẩm khác)</li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<h3 class="title">ĐĂNG KÝ THAM GIA</h3>
				<hr>
				<form id="event_form">
					<p>Họ tên của bạn</p>
					<input type="text" class="form-control" name="Name" placeholder="Ví dụ : Nguyễn Thị Phương Thảo">
					<p>Số điện thoại</p>
					<input type="text" class="form-control" name="Phone" placeholder="Ví dụ : 0901876549">
					<p>Địa chỉ email</p>
					<input type="text" class="form-control" name="Email" placeholder="Ví dụ : thaonguyen@sacngockhang.com">
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6">
							<p>Mã bảo vệ</p>
							<div id="imgcapt" style="margin-bottom:10px;">
							<?php echo $cap['image']; ?>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6">
							<p>Nhập mã bảo vệ</p>
							<input type="text" class="form-control" name="Captcha" placeholder="" style="height:45px;border-radius:0px;">
						</div>
					</div>
					<div id="error-box" class="hidden">
						<div class="alert alert-danger"><span></span></div>
					</div>
					<button type="button" class="btn btn-success" onclick="event_form(this,'event_form','uudaisacngockhang/event_form')">Gửi mã cho tôi</button>
				</form>
				<canvas id="gift-item"></canvas>
			</div>
			<div class="col-xs-12 another-products">
				<div class="row"><h3 class="text-center">SẢN PHẨM CỦA CHÚNG TÔI</h3></div>
				<div class="row no-gutters">
		            <div class="col-sm-4 col-md-4">
		              <div class="item">
		                  <img src="http://giaiphap.sacngockhang.com/public/site/clickfunel/images/asset/Giai-phap-co-ban.png" class="img img-responsive">
		                <div class="plan-title">
		                  <p class="plan-info"><b>Giải pháp cơ bản</b><span class="text-muted">Khởi đầu trải nghiệm trong 1 tháng</span></p>
		                  <h3 class="plan-pricing">649,000 VNĐ</h3>
		                </div>
		                <ul class="list-unstyled">
		                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>03 Hộp Sắc Ngọc Khang vi tảo lục <br>(60 viên / hộp)</span></li>
		                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>01 Sữa rửa mặt Sắc Ngọc Khang 100g</span></li>
		                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>01 Nước hoa hồng Sắc Ngọc Khang 145ml</span></li>
		                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>01 Kem dưỡng da Sắc Ngọc Khang 10g</span></li>
		                </ul>
		                <p><i class="snk snk-truck"></i>Giao hàng tận nơi</p>
		                <p style="color:#d71149"> <i class="snk snk-heart"></i>Giá gốc <span style="text-decoration: line-through;font-style: italic;">832,000đ</span><br> Tiết kiệm ngay <b>183,000đ</b></p><a onclick="add_to_cart(this,'add_cart',19,1)" class="btn-lg btn-danger btn-block text-center"> <i class="snk snk-right"> </i>ĐẶT MUA</a>
		              </div>
		            </div>
		            <div class="col-sm-4 col-md-4 premium-plan">
		              <div class="item"><span class="bagde left-badge">Best Value</span>
		                  <img src="http://giaiphap.sacngockhang.com/public/site/clickfunel/images/asset/Giai-phap-ngan-ngua.png" class="img img-responsive">
		                <div class="plan-title">
		                  <p class="plan-info"><b>Giải pháp ngăn ngừa</b><span class="text-muted">Hiệu quả tối ưu trong 2 tháng</span></p>
		                  <h3 class="plan-pricing">1,249,000 VNĐ									</h3>
		                </div>
		                <ul class="list-unstyled">
		                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>06 Hộp Sắc Ngọc Khang vi tảo lục <br>(60 viên / hộp)</span></li>
		                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>02 Sữa rửa mặt Sắc Ngọc Khang 100g</span></li>
		                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>02 Nước hoa hồng Sắc Ngọc Khang 145ml</span></li>
		                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>02 Kem dưỡng da Sắc Ngọc Khang 10g</span></li>
		                </ul>
		                <p><i class="snk snk-truck"></i>Giao hàng tận nơi</p>
		                <p style="color:#d71149"> <i class="snk snk-heart"></i>Giá gốc <span style="text-decoration: line-through;font-style: italic;">1,664,000đ</span><br> Tiết kiệm ngay <b>415,000đ</b></p>
		                  <p> <i class="snk snk-gift"></i>Tặng túi thời trang canvas trị giá 99,000 (áp dụng 500 khách hàng đầu tiên)</p><a onclick="add_to_cart(this,'add_cart',21,1)" class="btn-lg btn-danger btn-block text-center"> <i class="snk snk-right"> </i>ĐẶT MUA</a>
		              </div>
		            </div>
		            <div class="col-sm-4 col-md-4 diamond-plan">
		              <div class="item">
		                  <img src="http://giaiphap.sacngockhang.com/public/site/clickfunel/images/asset/Giai-phap-cai-thien.png" class="img img-responsive">
		                <div class="plan-title">
		                  <p class="plan-info"><b>Giải pháp cải thiện</b><span class="text-muted">Kết quả tối đa trong 3 tháng</span></p>
		                  <h3 class="plan-pricing">1,849,000 VNĐ</h3>
		                </div>
		                <ul class="list-unstyled">
		                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>09 Hộp Sắc Ngọc Khang vi tảo lục <br>(60 viên / hộp)</span></li>
		                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>03 Sữa rửa mặt Sắc Ngọc Khang 100g</span></li>
		                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>03 Nước hoa hồng Sắc Ngọc Khang 145ml</span></li>
		                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>03 Kem dưỡng da Sắc Ngọc Khang 10g</span></li>
		                </ul>
		                <p><i class="snk snk-truck"></i>Giao hàng tận nơi</p>
		                <p style="color:#d71149"> <i class="snk snk-heart"></i>Giá gốc <span style="text-decoration: line-through;font-style: italic;">2,496,000đ</span><br> Tiết kiệm ngay <b>647,000đ</b></p>
		                <p> <i class="snk snk-gift"></i>Tặng túi du lịch trị giá 199,000 (áp dụng 200 khách hàng đầu tiên)</p><a onclick="add_to_cart(this,'add_cart',22,1)" class="btn-lg btn-danger btn-block text-center"> <i class="snk snk-right"> </i>ĐẶT MUA</a>
		              </div>
		            </div>
		          </div>
			</div>
		</div>
	</div>
</div>
<script>
	var canvas = document.getElementById("gift-item");
	canvas.height = 0;
	var ctx = canvas.getContext("2d");
	ctx.width = canvas.width;
	ctx.height = canvas.height;
	var gift = [
		"5phan-tram.png",
		"10phan-tram.png",
		"15phan-tram.png",
		"50000.png",
		"100000.png",
		"200000.png"
	];
	var key = 0;
	var src = 0;
	var flag = 0;
	var stop = '';
	function draw() {
		if(flag!=0){return;}
		if(stop == gift[src]){flag=1;}
		requestAnimationFrame(draw);
		var img = new Image();
	  	img.onload = function() {
			ctx.clearRect(0, 0, canvas.width, canvas.height);
		    ctx.drawImage(img, 0, 0);
		    ctx.stroke();
	  	};
	  	img.src = "<?php echo base_url() ?>public/site/images/"+gift[src];
		if(key%6==0){src++;}
		if(src==gift.length){src=0;}
		key++;
	}

	function recivegift(gift){
		stop = gift+".png";
		draw();
	}
</script>
