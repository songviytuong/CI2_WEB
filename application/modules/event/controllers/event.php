<?php 
class Event extends CI_Controller { 
    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->load->library('template'); 
        $this->template->set_template('site');
        $staticconfig = $this->db->query("select * from ttp_static_config where ID=1")->row();
        $logo = $this->db->query("select * from ttp_banner where PositionID=2 and Published=1 order by ID DESC")->row();
        $data = array(
            "static" => $staticconfig,
            "logo"   => $logo,
            "hidden" => true
        );
        $this->template->write_view('header','header',$data);
        $this->template->write_view('footer','footer',$data);
        $this->template->add_doctype();
	}
    
    public $data = array();
    public $gift = array();

    public function get_devices(){
        $this->load->library('user_agent');
        if ($this->agent->is_browser()){
            $agent = $this->agent->browser().' '.$this->agent->version();
        }elseif ($this->agent->is_robot()){
            $agent = $this->agent->robot();
        }elseif ($this->agent->is_mobile()){
            $agent = $this->agent->mobile();
        }else{
            $agent = 'Unidentified User Agent';
        }
        return $agent;
    }

    public function save_event_analytics(){
        $Referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/' ;
        $Referer = $Referer!='/' ? parse_url($Referer, PHP_URL_HOST) : '/';
        $device = $this->get_devices();
        if($Referer!='sacngockhang.com' && $Referer!='www.sacngockhang.com'){
            $data = array(
                'IP'        =>$_SERVER['REMOTE_ADDR'],
                'Device'    =>$device,
                'Referer'   =>$Referer,
                'Created'   =>date('Y-m-d H:i:s'),
                'EventID'   =>1
            );
            $this->db->insert('ttp_event_analytics',$data);
        }
    }

    public function index(){
        $this->save_event_analytics();
        $this->load->helper('captcha');
        $vals = array(
            'img_path'   => APPPATH.'../assets/captcha/images/',
            'img_url'    => base_url().'assets/captcha/images/',
            'font_path'  => APPPATH.'../public/site/fonts/VNAVAN.TTF',
            'img_width'  => '170',
            'img_height' => '45',
            'expiration' => 7200
        );
        $products = $this->db->query("select a.Title,a.Description,a.Alias,a.Donvi,a.PrimaryImage as Thumb,a.Price,a.ID from ttp_report_products a,ttp_report_products_website b where a.ID=b.ProductsID and b.Website='http://sacngockhang.com/' order by b.STT ASC")->result();
        $cap = create_captcha($vals);
        $this->data = array(
            'cap'       => $cap,
            'products'  => $products
        );
        $this->session->set_userdata("captcha",$cap);
    	$this->template->add_title("Chương trình nhận mã ưu đãi cho khách hàng đăng ký lần đâu | Sắc Ngọc Khang");
        $this->template->write('MetaKeywords',"");
        $this->template->write('MetaDescription',"");
        $this->template->write_view('content','home',$this->data);
        $this->template->render();
    }

    public function event_form(){
        $response = array('error'=>false,'message'=>'','focus'=>'');
        $Captcha = isset($_POST['Captcha']) ? mysql_real_escape_string(strtolower($_POST['Captcha'])) : '' ;
        $Name = isset($_POST['Name']) ? mysql_real_escape_string(trim($_POST['Name'])) : '' ;
        $Name = strip_tags($Name);
        $Phone = isset($_POST['Phone']) ? mysql_real_escape_string(trim($_POST['Phone'])) : '' ;
        $Phone = preg_replace('/[^0-9]+/i', '', $Phone);
        $Email = isset($_POST['Email']) ? mysql_real_escape_string(trim($_POST['Email'])) : '' ;
        $cap = $this->session->userdata("captcha");
        $cap['word'] = $cap['word']!='' ? strtolower($cap['word']) : '' ;
        if($response['error']==false && $Name==''){
            $response['error'] = true;
            $response['focus'] = 'Name';
            $response['message'] = 'Họ tên không hợp lệ.';
        }
        if($response['error']==false && (strlen($Phone)<10 || strlen($Phone)>11)){
            $response['error'] = true;
            $response['focus'] = 'Phone';
            $response['message'] = 'Số điện thoại không hợp lệ.';
        }
        if($Email!=''){
            if (!filter_var($Email, FILTER_VALIDATE_EMAIL)){
                $response['error'] = true;
                $response['focus'] = 'Email';
                $response['message'] = 'Email không hợp lệ.';
            }
        }
        if($response['error']==false && ((isset($cap['word']) && $cap['word']!=$Captcha) || !isset($cap['word']))){
            $response['error'] = true;
            $response['focus'] = 'Captcha';
            $response['message'] = 'Mã bảo vệ không hợp lệ. Vui lòng nhập lại.';
            $response['image'] = $this->reload_captcha();
        }
        if($response['error']==false){
            $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone'")->row();
            if(!$check){
                $customer = array(
                    'Name'  => $Name,
                    'Phone1'=> $Phone,
                    'Email' => $Email,
                    'UserID'=> 1
                );
                $this->db->insert("ttp_report_customer",$customer);
                $gift = $this->random_gift();
                $temp_gift = $gift;
                $response['gift'] = str_replace('%','phan-tram',$gift);
                $start = date('Y-m-d H:i:s');
                $stop = date('2017-09-30 23:59:00');
                if(strpos($gift,'%')!=''){
                    $gift = (int)$gift;
                    $voucher = $this->create_voucher($start,$stop,0,1,$gift,0);
                }else{
                    $gift = (int)$gift;
                    $voucher = $this->create_voucher($start,$stop,0,1,0,$gift);
                }
                if($voucher){
                    $response['voucher'] = $voucher;
                }
                $asi_order = '';
                switch ($temp_gift) {
                    case '5%' || '50000':
                        $order = " áp dụng cho đơn hàng trên 500.000đ";
                        $asi_order = " chi ap dung cho don hang tren 500.000";
                        break;
                    case '10%' || '100000':
                        $order = " áp dụng cho đơn hàng trên 1.000.000đ";
                        $asi_order = " chi ap dung cho don hang tren 1.000.000";
                        break;
                    case '15%' || '200000':
                        $order = " áp dụng cho đơn hàng trên 1.500.000đ";
                        $asi_order = " chi ap dung cho don hang tren 1.500.000";
                        break;
                    default:
                        $order;
                        break;
                }
                $response['message'] = 'Chúc mừng <b>'.$Name.'</b> ! Bạn vừa nhận được một mã ưu đãi giảm giá '.$temp_gift.$order.' từ chúng tôi. Mã ưu đãi sẽ được nhắn vào số điện thoại bạn vừa nhập vào. Cảm ơn bạn đã tham gia chương trình.';
                $Name = $this->lib->asciiCharacter($Name);
                $Phone = (int)$Phone;
                $data = array(
                    'u'     => 'hoathien',
                    'pwd'   => 'r5c8h',
                    'from'  => 'HoaThienPhu',
                    'phone' => '84'.$Phone,
                    'sms'   => 'Chuc mung '.$Name.' nhan duoc ma uu dai '.$voucher['Code'].' tu c.trinh dang ky nhan uu dai '.$asi_order.', hsd den het 30/09/2017. http://sacngockhang.com/'
                );
                $return = file_get_contents("http://sms.vietguys.biz/api/?".http_build_query($data));
                $sms = array(
                    "Message"   => 'Chuc mung '.$Name.' nhan duoc ma uu dai '.$voucher['Code'].' tu c.trinh dang ky nhan uu dai '.$asi_order.', hsd den het 30/09/2017. http://sacngockhang.com/',
                    "Phone"     => '84'.$Phone,
                    "Created"   => date('Y-m-d H:i:s'),
                    "CodeStatus"=> $return
                );
                $this->db->insert("ttp_report_sms",$sms);
            }else{
                $response['error'] = true;
                $response['focus'] = 'Phone';
                $response['message'] = 'Số điện thoại bạn vừa nhập đã có người đăng ký.';
            }
        }
        echo json_encode($response);
    }

    public function random_gift($email=''){
        if(strpos($email,'@hoathienphu.com')!=''){
            $this->gift = array(
                '15%','200000'
            );
        }else{
            $this->gift = array(
                '5%','10%','50000','100000'
            );
        }
        return $this->gift[rand(0,count($this->gift)-1)];
    }

    public function create_voucher($Startday='',$Stopday='',$UseOne=0,$Amount=1,$PercentReduce=0,$PriceReduce=0){
        if($PercentReduce>0 || $PriceReduce>0){
            $max = $this->db->query("select max(ID) as ID from ttp_report_voucher")->row();
            $max = $max ? $max->ID+1 : 1 ;
            $available = $this->db->query("select Code from ttp_report_voucher where Status=0")->result();
            $arr_available = array();
            if(count($available)>0){
                foreach($available as $row){
                    $arr_available[$row->Code] = '';
                }
            }
            $str = substr(strtoupper(sha1($max)),0,8);
            $data = array(
                'Code'          => $str,
                'Startday'      => $Startday,
                'Stopday'       => $Stopday,
                'PercentReduce' => $PercentReduce,
                'PriceReduce'   => $PriceReduce,
                'Created'       => date('Y-m-d H:i:s'),
                'UserID'        => 1,
                'UseOne'        => 0
            );
            $this->db->insert("ttp_report_voucher",$data);
            return $data;
        }else{
            return false;
        }
    }

    public function reload_captcha(){
        $this->load->helper('captcha');
        $vals = array(
            'img_path'   => APPPATH.'../assets/captcha/images/',
            'img_url'    => base_url().'assets/captcha/images/',
            'font_path'  => APPPATH.'../public/site/fonts/VNAVAN.TTF',
            'img_width'  => '170',
            'img_height' => '45',
            'expiration' => 7200
        );

        $cap = create_captcha($vals);
        $this->session->set_userdata("captcha",$cap);
        return $cap['image'];
    }

    public function event_luckydraw($phone=''){
        if($phone!=''){
            $phone = mysql_real_escape_string($phone);
            $cus = $this->db->query("select ID,CityID from ttp_report_customer where Phone1='$phone'")->row();
            if($cus){
                $this->session->set_userdata("customer",array('Phone'=>$phone,'ID'=>$cus->ID,'City'=>$cus->CityID,'OrderID'=>0,'Total'=>0));
            }
        }
        $this->template->write_view('content','luckydraw',$this->data);
        $this->template->render();
    }

    public function segmentNumber($phone=''){
//        $session = $this->session->userdata("customer");
//        $session = is_array($session) ? $session : array() ;
//        if(isset($session['ID'])){
//            $customer = $this->db->query("select ID,ScoreLuckydraw,Phone1,Name from ttp_report_customer where ID=".$session['ID'])->row();
//            if($customer){
//                $result = $this->db->query("select count(1) as Total from ttp_result_luckydraw")->row();
//                $arr_gift = array(2,3,7);
//                if(isset($session['City']) && $session['City']==30){$arr_gift[] = 5;}
//                if($result->Total>0 && ($result->Total%20)==0){$arr_gift[] = 4;$arr_gift[] = 8;}
//                if($result->Total>0 && ($result->Total%100)==0){$arr_gift[] = 6;}
//                $random_keys = array_rand($arr_gift,1);
//                $random_keys = $arr_gift[$random_keys];
//                if($customer->ScoreLuckydraw>0){
//                    $arr = array(
//                        'CustomerID'    => $customer->ID,
//                        'ResultCode'    => $random_keys,
//                        'Created'       => date('Y-m-d H:i:s')
//                    );
//                    $this->db->insert("ttp_result_luckydraw",$arr);
//                    $this->db->query("update ttp_report_customer set ScoreLuckydraw=ScoreLuckydraw-1 where ID=".$customer->ID);
//                    if($random_keys==8 || $random_keys==4){
//                        $start = date('Y-m-d H:i:s');
//                        $stop = date('2017-09-30 23:59:00');
//                        $asi_order = '';
//                        if($random_keys==4){
//                            $voucher = $this->create_voucher($start,$stop,0,1,15,0);
//                            $asi_order = ' giam 15% chi ap dung cho don hang tu 1.500.000';
//                        }
//                        if($random_keys==8){
//                            $voucher = $this->create_voucher($start,$stop,0,1,0,200000);
//                            $asi_order = ' giam 200.000 chi ap dung cho don hang tu 1.500.000';
//                        }
//                        $Name = $this->lib->asciiCharacter($customer->Name);
//                        $Phone = (int)$customer->Phone1;
//                        $data = array(
//                            'u'     => 'hoathien',
//                            'pwd'   => 'r5c8h',
//                            'from'  => 'HoaThienPhu',
//                            'phone' => '84'.$Phone,
//                            'sms'   => 'Chuc mung '.$Name.' nhan duoc ma uu dai '.$voucher['Code'].$asi_order.' hsd den 30/09/2017. http://sacngockhang.com/'
//                        );
//                        $return = file_get_contents("http://sms.vietguys.biz/api/?".http_build_query($data));
//                        $sms = array(
//                            "Message"   => 'Chuc mung '.$Name.' nhan duoc ma uu dai '.$voucher['Code'].$asi_order.' hsd den 30/09/2017. http://sacngockhang.com/',
//                            "Phone"     => '84'.$Phone,
//                            "Created"   => date('Y-m-d H:i:s'),
//                            "CodeStatus"=> $return
//                        );
//                        $this->db->insert("ttp_report_sms",$sms);
//                    }
//                    $response = array('error'=>false,'message'=>'','number'=>$random_keys);
//                }else{
//                    $response = array('error'=>true,'message'=>'Bạn đã hết lượt quay thưởng. Vui lòng đặt hàng để có thêm lượt quay mới.','number'=>0);
//                }
//            }
//        }else{
//            $response = array('error'=>true,'message'=>'','number'=>0);
//        }
//        echo json_encode($response);
    }

    public function showGift(){
//        $gift = isset($_GET['gift']) ? (int)$_GET['gift'] : 0 ;
//        if($gift>1 && $gift<9){
//            $prize = $this->db->query("select * from ttp_define where `group`='prize' and `type`='lucky_draw' and position=0 and code=$gift")->row();
//            if($prize){
//                $this->load->view("luckydraw_gift",array('gift'=>$prize));
//            }
//        }
        $this->load->view("luckydraw_gift","Bạn đã trúng thưởng!!!");
    }
}
