<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>System Authentication | Login</title> 
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>public/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>public/admin/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>public/admin/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>public/admin/css/style_login.css" rel="stylesheet">

    <style>
        .warning{text-align:center;padding:5px 10px;text-align:center;color:#FFF;background: rgba(0,0,0,0.6);position: relative;z-index: 1}
    </style>
</head>
<?php 
    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    $ip = get_client_ip();
    $u_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "UNKNOWN";
    $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "" ;
    if($referer!=''){
        $referer = explode('/',$referer);
        $referer = isset($referer[2]) ? $referer[2] : 'UNKNOWN' ;
    }else{
        $referer = ROOT_URL;
    }
?>
<body>
    <?php 
        $error = $this->uri->segment(3);
        if($error=='error'){
            echo "<div class='warning'>User hoặc Password không chính xác vui lòng kiểm tra lại !.</div>";
        }
    ?>
    <div class="box">
        <div class="left_box">
            <form method="post" action="<?php echo base_url().ADMINPATH ?>/check-login">
                <h1>XÁC THỰC TÀI KHOẢN</h1>
                <div><input type="text" class="form-control" placeholder="Tài khoản đăng nhập" name="username" required id="username" /></div>
                <div><input type="password" class="form-control" placeholder="Mật khẩu đăng nhập" required name="password" id="password" /></div>
                <?php 
                $str = file_get_contents('ipaccept.txt');
                $str = explode('|',$str);
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip = $_SERVER['REMOTE_ADDR'];
                }
                if(!in_array($ip,$str)){
                ?>
                <div>
                    <input type="password" class="form-control" placeholder="Mã số xác nhận" required name="ttp" />
                </div>
                <?php
                }
                ?>
                <div><input type="submit" name="commit" value="Đăng Nhập" class="btn btn-default submit" /></div>
            </form>
            <!-- form -->
        </div>
        <div class="right_box">
            <img src="<?php echo base_url() ?>public/login/images/logo.png" />
            <h2>AUCHAN RETAIL VIETNAM</h2>
            <p>Đây là trang thông tin nội bộ, mọi thông tin mà bạn nhập vào đây sẽ được chúng tôi ghi nhận và kiểm tra thường xuyên .</p>
            <h5>IP truy cập : <?php echo $ip ?> </h5>
            <h5>Referer : <?php echo $referer ?></h5>
            <h5><?php echo $u_agent ?></h5>
        </div>
    </div>
</body>

</html>