<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>System Authentication | Login</title> 
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>public/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>public/admin/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>public/admin/css/animate.min.css" rel="stylesheet">
    <script src="<?php echo base_url() ?>public/admin/js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <style>
        body{background: #ce3121;}
        .login_content{width:60%;margin:60px auto;}
        .login_content img{display: block;margin:10px auto;}
        .login_content .row{}
        .login_content .row p{margin-bottom:10px;color:#FFF;display: block;font-size: 18px;}
        .login_content .row input{margin-bottom:20px;}
        .login_content .row input[type="submit"]{width:100%;padding:10px 0px;text-transform: uppercase;margin-top:10px;font-weight: :bold;}
        .separator{margin-top:50px;text-align: center;}
        .separator p{color:#ccc;}
    </style>
</head>

<body>
<?php 
    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    $ip = get_client_ip();
    $u_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "UNKNOWN";
    $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "" ;
    if($referer!=''){
        $referer = explode('/',$referer);
        $referer = isset($referer[2]) ? $referer[2] : 'UNKNOWN' ;
    }else{
        $referer = "http://ucancook.vn";
    }
?>
    <div id="login" class="animate form">
        <section class="login_content">
            <form method="post" action="<?php echo base_url().ADMINPATH."/check-login" ?>">
                <div><img src='<?php echo base_url() ?>public/admin/images/logo_mobile.png' /></div>
                <div class='row'><p>Tài khoản</p></div>
                <div class='row'>
                    <input type="text" class="form-control" placeholder="Username" name="username" required id="username" />
                </div>
                <div class='row'><p>Mật khẩu</p></div>
                <div class='row'>
                    <input type="password" class="form-control" placeholder="Password" required name="password" id="password" />
                </div>
                <?php 
                $str = file_get_contents('ipaccept.txt');
                $str = explode('|',$str);
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip = $_SERVER['REMOTE_ADDR'];
                }
                if(!in_array($ip,$str)){
                ?>
                <div class='row'><p>Mã xác nhận</p></div>
                <div class='row'>
                    <input type="password" class="form-control" required name="ttp" />
                </div>
                <?php
                }
                ?>
                <div class='row'>
                    <input type="submit" name="commit" value="Đăng Nhập" class="btn btn-default submit" />
                </div>
                
            </form>
            <!-- form -->
        </section>
        <!-- content -->
    </div>
</body>

</html>