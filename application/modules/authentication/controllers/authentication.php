<?php 
class Authentication extends CI_Controller {     
	public function __construct() {
	    parent::__construct();
    }	

	public function index(){
        echo sha1("admin");
		$this->is_login();
		redirect(ADMINPATH. '/home');
	}	

	public function is_login(){		
		if($user==''){
			redirect(ADMINPATH. '/login');
		}
	}	

	public function logout(){		
		$this->session->unset_userdata('ttp_usercp');
		$this->session->sess_destroy();
		redirect(ADMINPATH);
	}	

	public function login(){
		$this->load->library('user_agent');
		if($this->agent->is_mobile()){
			$this->load->view('m_login');
		}else{
			$this->load->view('login');
		}
	}	

	public function login_now(){		
		if(isset($_POST['username']) && isset($_POST['password'])){			
			$username = $this->security->xss_clean($_POST['username']);			
			$password = $this->security->xss_clean($_POST['password']);
			$token = isset($_POST['ttp']) ? $_POST['ttp'] : '' ;
			$temp = $password;			
			$username = mysql_real_escape_string($username);
			$password = mysql_real_escape_string($password);
			$password = sha1($password);
			$result = $this->db->query("select a.ID,a.UserType,a.IsAdmin,a.AllowRemote,a.HomePage from ttp_user a,ttp_role b where a.UserName = '$username' and a.Password='$password' and a.Published=1 and b.Published=1 and a.RoleID=b.ID")->row();
			if($result){
				$this->accept_ip($result->IsAdmin,$token,$result->AllowRemote);
				$this->session->set_userdata('ttp_usercp',$username);
				if($temp=='123456'){
					redirect(ADMINPATH."/home/profile?mustchange=true");
				}
				if($result->HomePage!=''){
					redirect(ADMINPATH."/".$result->HomePage);
				}else{
					redirect(ADMINPATH."/home");
				}
			}
			redirect(ADMINPATH. '/login/error');
		}
	}

	public function accept_ip($admin=0,$token='',$AllowRemote=0){
        $str = file_get_contents('ipaccept.txt');
        $str = explode('|',$str);
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        if(!in_array($ip,$str)){
            if($admin==1 && $token=='842657931'){
        		return;
        	}
        	if($AllowRemote==1){
        		return;
        	}
			redirect('accept_deny');
        }
    }
}
?>