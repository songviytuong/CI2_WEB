<?php
$item = $_ci_vars;
?>
<div class="col-md-12 col-lg-12 product-id-<?=$item['ID'];?>">
    <div class="row">
        <div class="col-md-6">
            <div id="product-carousel" data-ride="carousel" class="carousel slide">
                <!-- Wrapper for slides-->
                <div role="listbox" class="carousel-inner">
                    <div class="carousel-item active"><img alt="ProDetail" title="Auchan" src="<?= UPLOAD_URL; ?>/<?= $PrimaryImage; ?>" data-magnify-src="<?= UPLOAD_URL; ?>/<?= $PrimaryImage; ?>" class="img-fluid img-rounded w-100 magnify" /></div>
                    <?php
                    if (count($item['AlbumImage']) > 0) {
                        foreach ($item['AlbumImage'] as $key => $image) {
                            ?>
                            <div class="carousel-item"><img alt="ProDetail" title="Auchan" src="<?= UPLOAD_URL; ?>/<?php echo $image; ?>" data-magnify-src="<?= UPLOAD_URL; ?>/<?php echo $image; ?>" class="img-fluid img-rounded w-100 magnify" /></div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <?php
                if (count($item['AlbumImage']) > 0) {
                    ?>
                    <!-- Indicators-->
                    <ul class="carousel-indicators">
                        <li data-slide-to="0" data-target="#product-carousel" class="active"><img alt="ProDetail" src="<?= UPLOAD_URL; ?>/<?= $PrimaryImage; ?>" class="img-fluid" /></li>
                        <?php
                        $i = 1;
                        foreach ($item['AlbumImage'] as $key => $image) {
                            ?>
                            <li data-slide-to="<?php echo $i++; ?>" data-target="#product-carousel"><img alt="ProDetail" src="<?= UPLOAD_URL; ?>/<?php echo $image; ?>" class="img-fluid" /></li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="product-info"><span class="badge badge-warning">Còn hàng</span>
                <h1 class="product-title"><?php echo $item['Title']; ?></h1>
                <div class="status">
                    <div class="star">
                        <i class="auc auc-star"></i>
                        <i class="auc auc-star"></i>
                        <i class="auc auc-star"></i>
                        <i class="auc auc-star"></i>
                        <i class="auc auc-star-empty"></i>
                    </div>
                    <?php if (BETAOFF && COMMENTOFF): ?>
                        <span class="badge badge-default">398 nhận xét</span>
                        <a href="" data-toggle="modal" data-target="#comment-modal" data-dismiss="modal" title="Gửi nhận xét của bạn" class="btn-sm"><i class="auc auc-pencil"> </i>Gửi nhận xét</a>
                    <?php endif; ?>
                </div>
                <p class="meta-info"><?php echo $item['Description']; ?></p>
		<?php if (BETAOFF): ?>
                <div class="row">
                    <div class="product-properties col-xs-12 col-sm-6"><b>Mã (SKU) : </b><span> <?= $item['SKU']; ?></span></div>
                    <div class="product-properties col-xs-12 col-sm-6"><b>Thương hiệu : </b><?php if(TRADEMARK_INFO):?><a href="brand.htm" class="btn-link"><?php endif; ?><?= $item['Trademark']; ?><?php if(TRADEMARK_INFO):?></a><?php endif; ?></div>
                </div>
                <?php endif; ?>
                <div class="product-properties form-inline">
                    <?php if (BETAOFF): ?>
                    <div class="form-group">
                        <label for="product-quantity"><b>Số lượng : </b></label>
                        <input id="product-quantity" type="number" value="1" min="1" class="form-control form-control-sm" style="width:80px;"> 
                        <?php if(in_array($item['Capacity'], array('kg','g'))) : ?>
                        <label> / </label>
                        <select class="form-control form-control-sm">
                            <?php
                                foreach($DataUnit as $unit){
                            ?>
                            <option value="<?=$unit;?>"><?=$unit;?></option>
                            <?php } ?>
                        </select>
                        <?php else: ?>
                        <?php echo $item['Unit'];?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>    
                    
                </div>
                <div class="price"><b><?php echo $item['Price']; ?></b>
                    <?php if (isset($item['SocialPrice']) && $item['SocialPrice'] != 0) : ?>
                        <span class="Social"><?php echo $item['SocialPrice']; ?></span>
                    <?php endif; ?>
                    <span><?php echo (!empty($item['UnitText'])) ? $item['UnitText'] : ''; ?></span>
                    <?php if (isset($item['is_saleoff'])) : ?>
                        <span class="badge badge-info">OFF <strong><?php echo $item['saleoff_value']; ?></strong></span>
                    <?php endif; ?>
                </div>
                <?php if (BETAOFF): ?>
                    <div class="product-cta">
                        <a href="cart.html" class="btn btn-primary btn-lg">
                            <i class="auc auc-cart"></i>
                            <span>Đặt hàng ngay<small>Giao tận nơi hoặc đến siêu thị lấy hàng</small></span>
                        </a>
                        <a href="" title="Yêu thích sản phẩm" class="btn btn-outline-success btn-lg">
                            <i class="auc auc-heart"></i>
                        </a>
                    </div>
                <?php else: ?>
                    <a href="<?= base_url()?>he-thong-sieu-thi-auchan/" class="btn btn-primary btn-lg btn-hotline"><?php echo $item['ButtonText']; ?></a>
                <?php endif; ?>
                <p class="delivery alert alert-info">
                    <span><i class="auc auc-delivery-truck"> </i>Miễn phí giao hàng cho hóa đơn trên 300.000 VNĐ.</span>
            </div>
        </div>
    </div>
</div>
<?php if ($item['Content']): ?>
    <div class="col-md-12 product-info-detail">
        <h3 class="content-box-title"><?php echo $this->lang->line('product_information'); ?></h3>
        <div class="post-content-wrap <?php if (_SHOWMORE): ?> tab-content-small<?php endif; ?>">
            <?php echo $item['Content']; ?>
        </div>
        <?php if (_SHOWMORE): ?><div class="text-center dis boxArticle"><a id="xemthem">Đọc thêm <i class="auc auc-angle-down"></i></a></div><?php endif; ?>
    </div>
<?php endif; ?>
