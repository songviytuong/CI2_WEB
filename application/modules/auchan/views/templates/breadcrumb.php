<?php
$data = isset($_ci_vars['data']) ? $_ci_vars['data'] : array();
?>
<ol class="breadcrumb">
    <?php
    if (in_array($data['page_template'], array('product_categories', 'e-brochure', 'cam-nang-auchan'))) {
        ?>
        <li class="breadcrumb-item"><i class="auc auc-shop"> </i> <a href="<?php echo base_url(); ?>"><?= $this->lang->line('home', 'Trang chủ'); ?></a></li>
    <?php
} else if ($data['page_template'] == 'product_list') {
    $ParentAlias = $this->uri->segment(1);
    $this->load->model('categories_model', 'category');
    $MenuParent = $this->category->getFromAlias('Title', $ParentAlias);
    ?>
        <li class="breadcrumb-item"><i class="auc auc-shop"> </i> <a href="<?php echo base_url(); ?>"><?= $this->lang->line('home', 'Trang chủ'); ?></a></li>
        <li class="breadcrumb-item"><a href="<?= prefix_parent_category . $ParentAlias . page_extension; ?>"><?= $MenuParent; ?></a></li>
    <?php
} else if ($data['page_template'] == 'page_details') {
    $alias = $this->uri->uri_string();
    $this->load->model('products_model', 'products');
    $arr = $this->products->getBreadCrumbData($alias);
    if (isset($arr['AliasCategory']) && isset($arr['AliasList'])) {
        $alias_category = 'danh-muc/' . $arr['AliasCategory'] . page_extension;
        $alias_list = $arr['AliasCategory'] . '/' . $arr['AliasList'] . page_extension;
    }
    ?>
        <li class="breadcrumb-item"><i class="auc auc-shop"> </i> <a href="<?php echo base_url(); ?>"><?= $this->lang->line('home', 'Trang chủ'); ?></a></li>
        <?php if (BETAOFF) : ?>
            <li class="breadcrumb-item"><a href="<?= $alias_category; ?>"><?= $arr['MenuCategory']; ?></a></li>
            <li class="breadcrumb-item"><a href="<?= $alias_list; ?>"><?= $arr['MenuList']; ?></a></li>
        <?php else : ?>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>khuyen-mai/">Khuyến mãi</a></li>
        <?php endif; ?>
    <?php } ?>
    <?php
    if ($data['menu_text_active']) {
        ?>
        <li class="breadcrumb-item active"><?php echo $data['menu_text_active']; ?></li>
    <?php } ?>
</ol>