<?php if ($item['link']) : ?><a href="<?= $item['link']; ?>" target="<?= $item['target']; ?>"><?php endif; ?>
    <img src="<?= UPLOAD_URL; ?>/<?= $item['image']; ?>" alt="<?= $item['title']; ?>" class="img-fluid img-thumbnail">
    <?php if ($item['link']) : ?></a><?php endif; ?>