<?php
$this->load->model('products_model', 'products');
$params = array(
    'cid' => !empty($_ci_vars['cid']) ? $_ci_vars['cid'] : 0,
    'this_id' => !empty($_ci_vars['this_id']) ? $_ci_vars['this_id'] : 0,
    'limit' => !empty($_ci_vars['limit']) ? $_ci_vars['limit'] : 10,
    'chimmoi' => !empty($_ci_vars['chimmoi']) ? $_ci_vars['chimmoi'] : 0,
    'discount' => !empty($_ci_vars['discount']) ? $_ci_vars['discount'] : FALSE,
    'shopping' => !empty($_ci_vars['SHOPPING']) ? $_ci_vars['SHOPPING'] : TRUE,
    'promotion' => !empty($_ci_vars['promotion']) ? $_ci_vars['promotion'] : FALSE,
    'hot' => !empty($_ci_vars['hot']) ? $_ci_vars['hot'] : FALSE,
    'created' => !empty($_ci_vars['created']) ? $_ci_vars['created'] : FALSE,
    'random' => !empty($_ci_vars['random']) ? $_ci_vars['random'] : FALSE,
    'class' => !empty($_ci_vars['class']) ? $_ci_vars['class'] : 'col-xs-6 col-sm-4 col-md-2 product-item',
    'truncate' => 50,
    'keyword' => !empty($_ci_vars['keyword']) ? $_ci_vars['keyword'] : '',
    'cataddon' => !empty($_ci_vars['cataddon']) ? $_ci_vars['cataddon'] : 0,
);
$ListProduct = $this->products->defineAllProducts($params['cid'], '', '', $params['limit'], 0, $params);
foreach ($ListProduct as $key => $item) {
    if (!lazyload) {
        $lazyload = "src='" . $item['main_picture'] . "'";
    } else {
        $lazyload = "src='data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=' data-src='" . $item['main_picture'] . "'";
    }
    ?>
    <div class="<?php echo $params['class']; ?> item-<?= $item['id']; ?>">
        <a href="<?php echo $item['url']; ?>" class="product-img" title="<?php echo $item['product_name']; ?>">
            <img <?php echo $lazyload; ?> class="<?= (lazyload) ? 'lazyload ' : ''; ?>img-fluid w-100" alt="<?php echo $item['product_name']; ?>"/>
        </a>
        <?= (isset($item['is_saleoff']) && $item['is_saleoff']) ? '<span class="badge badge-danger">' . $item['saleoff_value'] . '</span>' : '' ?>
        <?= isset($item['is_chimmoi']) ? '<span class="badge-warning badge-right">HOT</span>' : '' ?>
        <a href="<?php echo $item['url']; ?>" class="title-products" title="<?php echo $item['title']; ?>">
            <?php echo $item['product_name'] ?>
        </a>
        <p class="unit"><?php echo isset($item['unit_text']) ? $item['unit_text'] : '&nbsp;'; ?></p>
        <p class="star"><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star-empty"></i></p>
        <p class="price"><b><?php echo $item['price']; ?></b> <?php if ($item['social_price']) : ?><small><?php echo $item['social_price'] ?></small><?php endif; ?></p>
        <?php if ($params['shopping'] && BETAOFF): ?>
            <div class="form-horizontal text-center btnCart">
                <button type="button" class="btn btn-primary btn-sm" data-id="<?php echo $item['id']; ?>" data-token="<?=substr(md5($item['id'].'ARV'),27,06);?>" onclick="add_cart(this)"><i class="auc auc-cart"></i><?php echo $item['button_cart']; ?></button>
                <button type="button" class="btn btn-sm btn-outline-success auc auc-heart" data-id="<?php echo $item['id'] ?>" data-token="<?=substr(md5($item['id'].'ARV'),27,06);?>" onclick="add_wishlist(this)"></button>
            </div>
        <?php else: ?>
            <a href="<?php echo $item['url']; ?>" class="btn btn-primary btn-block btn-sm" title="<?php echo $item['product_name']; ?>">
                <?php echo $item['button_text']; ?>
            </a>
        <?php endif; ?>
    </div>
<?php } ?>