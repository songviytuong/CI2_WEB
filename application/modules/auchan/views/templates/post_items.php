<?php
$this->load->model('post_model', 'post');
$params = array(
    'class' => !empty($_ci_vars['class']) ? $_ci_vars['class'] : 'col-xs-6 col-md-4 col-lg-3',
    'keyword' => !empty($_ci_vars['keyword']) ? $_ci_vars['keyword'] : '',
    'maxheight' => !empty($_ci_vars['maxheight']) ? $_ci_vars['maxheight'] : '',
);
$ListPost = $this->post->defineAllPost($params);
foreach ($ListPost as $key => $item) {
    if (!lazyload) {
        $lazyload = "src='" . $item['Thumb'] . "'";
    } else {
        $lazyload = "src='data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=' data-src='" . $item['Thumb'] . "'";
    }
    ?>
    <div class="<?php echo $params['class']; ?>">
        <div class="card recipe-item"><a href="chi-tiet-cong-thuc.htm" class="recipe-img"><img <?php echo $lazyload; ?> alt="<?php echo $item['Title']; ?>" class="<?= (lazyload) ? 'lazyload ' : ''; ?>card-img-top w-100" <?php if ($params['maxheight']) : ?>style="max-height:<?=$params['maxheight'];?>px"<?php endif; ?>></a>
            <div class="card-block">
                <h4 class="card-title"><a href="" class="text"><?php echo $item['Title']; ?></a></h4>
                <div class="star"><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star-empty"></i></div>
                <p class="card-text">Sự khéo léo của người nội trợ sẽ được thể hiện qua bát bún chả thơm ngon, hài hòa. Với công thức cho thêm ít mắm tôm, ít chanh khi ướp và nướng thịt sẽ làm cho món này thêm phần ngon hơn. Cùng học cách làm bún chả Hà Nội ngon nổi tiếng này nhé!</p>
                <div class="function">
                    <p><i class="auc auc-view text-primary"></i><span>18</span> <span>lượt xem</span></p>
                    <p><i class="auc auc-commenting"></i><span>10</span> <span>nhận xét</span></p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>