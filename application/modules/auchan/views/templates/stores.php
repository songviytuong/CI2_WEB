<div class="col-md-2 store-sidebar hidden-md-down">
    <h4 class="sidebar-title"><?= $this->lang->line('find_store_at'); ?></h4>
    <select name="group_city" id="group_city" class="form-control form-control-sm">
        <option value="-1" disabled="disabled" selected="selected"><?= $this->lang->line('select_group_city'); ?></option>
        <?php
        $this->load->model('warehouse_model', 'warehouse');
        $GroupCity = $this->warehouse->defineGroupCity();
        foreach ($GroupCity as $row) {
            ?>
            <option value="<?= $row['city_id'] ?>"><?= $row['city_name'] ?></option>
        <?php } ?>
    </select>
    <select name="group_district" id="group_district" class="form-control form-control-sm">
        <option value="-1" disabled="disabled" selected="selected"><?= $this->lang->line('select_group_district'); ?></option>
    </select>
    <h6><i class="auc auc-shop"> </i><?= $this->lang->line('store_list'); ?></h6>
    <ul class="list-unstyled" id="loading_storelist">
        <li><a href="<?php echo base_url(); ?>he-thong-sieu-thi-auchan/">Xem thêm siêu thị <i class="auc auc-right"></i></a></li>
    </ul>
    <?php 
    if(isset($group4_banners)){
    foreach ($group4_banners as $key => $item) { ?>
            <?php if ($item['link']) : ?><a href="<?= $item['link']; ?>" target="<?= $item['target']; ?>" class="side-banner"><?php endif; ?>
                <img src="<?= UPLOAD_URL; ?>/<?= $item['image']; ?>" alt="<?= $item['title']; ?>" class="img-fluid img-rounded" />
                <?php if ($item['link']) : ?></a><?php endif; ?>
    <?php } } ?>
</div>