<?php
$arr = !empty($_ci_vars['data']) ? $_ci_vars['data'] : array();
$counter = count($arr);
if ($counter > 1) {
    ?>
    <link rel="stylesheet" href="public/auchan/assets/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="public/auchan/assets/owlcarousel/assets/owl.theme.default.min.css">
    <script src="public/auchan/assets/vendors/jquery.min.js"></script>
    <script src="public/auchan/assets/owlcarousel/owl.carousel.js"></script>
    <div id="mainslider" class="owl-carousel owl-theme">
        <?php
        foreach ($arr as $key => $item) {
            if (!lazyload) {
                $lazyload = "src='" . $item['image'] . "'";
            } else {
                $lazyload = "data-src='" . $item['image'] . "'";
            }
            ?>
            <div class="item">
                <?php if ($item['link']) : ?><a href="<?= $item['link']; ?>" target="<?= $item['target']; ?>"><?php endif; ?>
                    <img <?php echo $lazyload; ?> class="<?= (lazyload) ? 'owl-lazy ' : ''; ?>img-fluid w-100" alt="<?= $item['title']; ?>" />
                    <?php if ($item['link']) : ?></a><?php endif; ?>
            </div>
        <?php } ?>
    </div>
    <script>
        var mainslider = $("#mainslider");
        mainslider.owlCarousel({
            items: 1,
            slideSpeed: 2000,
            nav: true,
            lazyLoad: true,
            autoplay: true,
            dots: true,
            loop: true,
            responsiveRefreshRate: 200,
            navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
        }).on('changed.owl.carousel', syncPosition);

        function syncPosition(el) {
            //if you set loop to false, you have to restore this next line
            //var current = el.item.index;

            //if you disable loop you have to comment this block
            var count = el.item.count - 1;
            var current = Math.round(el.item.index - (el.item.count / 2) - .5);

            if (current < 0) {
                current = count;
            }
            if (current > count) {
                current = 0;
            }

            //end block
        }
    </script>
    <?php
} else {
    if (lazyload) {
        $lazyload = "data-src='" . UPLOAD_URL . '/' . (($counter == 1 && is_array($arr)) ? $arr[0]["image"] : (($arr) ? $arr : '')) . "'";
    } else {
        $lazyload = "src='" . UPLOAD_URL . '/' . (($counter == 1 && is_array($arr)) ? $arr[0]["image"] : (($arr) ? $arr : '')) . "'";
    }
    ?>
    <img <?php echo $lazyload; ?> class="<?= (lazyload) ? 'lazyload ' : ''; ?>img-fluid w-100" alt="" />
<?php }
?>

