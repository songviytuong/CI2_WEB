<?php
$this->load->model('post_model', 'post');
$params = array(
    'class' => !empty($_ci_vars['class']) ? $_ci_vars['class'] : 'col-xs-6 col-md-3',
    'limit' => !empty($_ci_vars['limit']) ? $_ci_vars['limit'] : '',
    'maxheight' => !empty($_ci_vars['maxheight']) ? $_ci_vars['maxheight'] : '',
);
$ListPost = $this->post->defineAllPost($params);
foreach ($ListPost as $key => $item) {
    if (!lazyload) {
        $lazyload = "src='" . $item['Thumb'] . "'";
    } else {
        $lazyload = "src='data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=' data-src='" . $item['Thumb'] . "'";
    }
    ?>
    <div class="<?php echo $params['class']; ?>">
        <div class="post-item"><a href="<?php echo $item['Url']; ?>" class="post-img" title="<?php echo $item['Title']; ?>"><img <?php echo $lazyload; ?> alt="<?php echo $item['Title']; ?>" class="<?= (lazyload) ? 'lazyload ' : ''; ?>img-rounded img-fluid" <?php if ($params['maxheight']) : ?>style="max-height:<?= $params['maxheight']; ?>px"<?php endif; ?>></a>
            <p class="meta"><a href="<?php echo $item['Url']; ?>" class="text" title="<?php echo $item['Title']; ?>"><?php echo $item['Title']; ?></a></p>
        </div>
    </div>
<?php } ?>