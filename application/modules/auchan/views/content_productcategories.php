<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <?php
                if (isset($off_menu))
                    $data_menu['class'] = 'col-md-2 hidden-md-down';
                $this->load->view('pages/includes/inc_mainmenu');
                ?>
                <div class="col-md-12 <?php echo isset($off_menu) ? 'col-lg-10' : 'col-lg-12' ?>">
                    <?php
                    $breadcrumb['data'] = array(
                        'page_template' => 'product_categories',
                        'menu_text_active' => $MenuTitle
                    );
                    $this->load->view('templates/breadcrumb', $breadcrumb);
                    ?>

                    <?php
                    $categories_slider['data'] = $MainPicture;
                    $categories_slider['class'] = 'cate-banner-item';
                    $this->load->view('templates/carousel', $categories_slider);
                    ?>
                    <div class="list-categories-child">
                        <div id="categories">
                            <div class="row">
                                <?php
                                foreach ($Sub_Categories as $key => $row) {
                                    $counter_byCategoriesID = $this->products->getProductCounterBy($row['id'], 'CategoriesID');
                                    if ($counter_byCategoriesID > 0) {
                                        ?>
                                        <div class="col-xs-6 col-sm-4 col-md-3 cate-item">
                                            <a href="<?php echo $current_url; ?>/#c<?php echo $row['id']; ?>">
                                                <b><?php echo $row['title']; ?></b>
                                                <span><?php echo $counter_byCategoriesID; ?> sản phẩm</span>
                                            </a>
                                            <img src="<?php echo $row['thumb']; ?>" alt="Categoies" class="img-fluid" />
                                        </div>
                                    <?php }
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="product-box">
                        <?php
                        foreach ($Sub_Categories as $key => $row) {
                            $counter_byCategoriesID = $this->products->getProductCounterBy($row['id'], 'CategoriesID');
                            if ($counter_byCategoriesID > 0) {
                                ?>
                                <a id="c<?php echo $row['id']; ?>"></a>
                                <div class="box-item">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h3 class="content-box-title">
                                                <a href="<?php echo $row['alias']; ?>" title="<?php echo $row['title']; ?>"><?php echo $row['title']; ?></a>
                                                <a class="read-all" href="<?php echo $row['alias']; ?>">Xem tất cả <i class="auc auc-angle-right"></i></a>
                                            </h3>
                                        </div>
                                        <?php
                                        $data['cid'] = $row['id'];
                                        $data['limit'] = 6;
                                        $this->view('templates/product_items', $data);
                                        ?>
                                    </div>
                                </div>
    <?php }
} ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>