<header class="header">
    <div class="top-head">
        <div class="container">
            <div class="row">
                <?php
                $this->load->model('languages_model', 'languages');
                $LangData = $this->languages->defineAllLanguage();
                $ActiveLang = $this->languages->getLanguageBySession($this->cur_lang);
                ?>
                <div class="col-xs-12 col-sm-4 <?= (DRIVEDRIECT) ? 'top-left-nav' : '' ?>">
                    <?php if (DRIVEDRIECT) : ?>
                        <a href="" class="nav-link"> <b>auchan</b><em>drive</em></a>
                        <a href="" class="nav-link"> <b>auchan:Direct</b></a>
                    <?php else : ?>
                        <?php
                        $alias = $this->uri->segment(1);
                        if ($alias != 'feedback') {
                            ?>
                            <a href="<?php echo base_url(); ?>feedback/?target=<?php echo urlencode(base_url() . $this->uri->uri_string()); ?>" class="nav-link"><i class="auc auc-lock-user"></i> <?= $this->lang->line('feedback', 'Thư góp ý'); ?></a>
                        <?php } ?>
                    <?php endif; ?>
                    <?php if (Globals::getConfig("Mle") == 1) : ?>
                        <a data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="nav-link pull-right hidden-md-up dropdown-toggle">
                            <img src="<?= $ActiveLang['LangFlag']; ?>" alt="<?= $ActiveLang['LangName']; ?>"/> <?= $ActiveLang['LangName']; ?>
                        </a>
                        <div aria-labelledby="Preview" class="dropdown-menu">
                            <?php
                            foreach ($LangData as $row) {
                                if ($this->cur_lang == $row['Local']) {
                                    continue;
                                }
                                ?>
                                <a href="<?= current_url(); ?>?lang=<?= $row['Local']; ?>" class="dropdown-item"><img src="public/auchan/v2/images/mle/<?= $row['Icon']; ?>.png" alt=""/> <?= $row['Name']; ?></a>
                            <?php } ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-xs-12 col-md-8 hidden-sm-down">
                    <ul class="nav navbar-nav pull-right nav-top">
                        <?php if (DRIVEDRIECT) : ?>
                            <?php
                            $alias = $this->uri->segment(1);
                            if ($alias != 'feedback') {
                                ?>
                                <li class="nav-item"><a href="<?php echo base_url(); ?>feedback/?target=<?php echo urlencode(base_url() . $this->uri->uri_string()); ?>" class="nav-link"><i class="auc auc-lock-user"></i> <?= $this->lang->line('feedback', 'Thư góp ý'); ?></a></li>
                            <?php } ?>
                        <?php endif; ?>
                        <!--<li class="nav-item"><button type="button" onclick="send_SMS();">Send</button></li>-->
                        <li class="nav-item"><a href="<?php echo base_url(); ?>e-brochure/" class="nav-link"><i class="icon-auchan-50 auchan"></i> <?= $this->lang->line('e-brochure', 'E-Brochure'); ?></a></li>
                        <li class="nav-item"><a href="<?php echo base_url(); ?>the-thanh-vien/" class="nav-link"><i class="icon-auchan-51 auchan"></i> <?= $this->lang->line('loyalty_card'); ?></a></li>
                        <li class="nav-item"><a href="<?php echo base_url(); ?>cam-nang-auchan/" class="nav-link"><i class="auc auc-book-album"></i> <?= $this->lang->line('handbook', 'Cẩm nang Auchan'); ?></a></li>
                        <li class="nav-item dropdown"><a data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i class="auc auc-question"></i> <?= $this->lang->line('support', 'Bạn cần hỗ trợ ?'); ?></a>
                            <div aria-labelledby="Preview" class="dropdown-menu">
                                <a href="<?php echo base_url(); ?>gioi-thieu-auchan/" class="dropdown-item"> <i class="auc auc-fw auc-info"></i><?= $this->lang->line('aboutus', 'Giới thiệu Auchan'); ?></a>
                                <a href="<?php echo base_url(); ?>khuyen-mai/" class="dropdown-item"> <i class="auc auc-fw auc-lightbulb"></i><?= $this->lang->line('promotion', 'Khuyến mãi'); ?></a>
                                <a href="<?php echo base_url(); ?>the-thanh-vien/" class="dropdown-item"> <i class="auc auc-fw auc-bookmark-empty"></i><?= $this->lang->line('loyalty_card', 'Thẻ thành viên'); ?></a>
                                <a href="<?php echo base_url(); ?>tuyen-dung/" class="dropdown-item"> <i class="auc auc-fw auc-user"></i><?= $this->lang->line('recruitment', 'Tuyển dụng'); ?></a>
                            </div>
                        </li>
                        <?php if (Globals::getConfig("Mle") == 1) : ?>
                            <li class="nav-item dropdown"><a data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">
                                    <img src="<?= $ActiveLang['LangFlag']; ?>" alt="<?= $ActiveLang['LangName']; ?>"/> <?= $ActiveLang['LangName']; ?>
                                </a>

                                <div aria-labelledby="Preview" class="dropdown-menu">
                                    <?php
                                    foreach ($LangData as $row) {
                                        if ($this->cur_lang == $row['Local']) {
                                            continue;
                                        }
                                        ?>
                                        <a href="<?= (current_url() != base_url()) ? current_url() . '/' : base_url(); ?>?lang=<?= $row['Local']; ?>" class="dropdown-item"><img src="public/auchan/v2/images/mle/<?= $row['Icon']; ?>.png" alt=""/> <?= $row['Name']; ?></a>
                                    <?php } ?>
                                </div>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="main-head">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-md-4 col-lg-2 logo">
                    <button type="button" data-toggle="collapse" data-target="#mainNavbar" aria-controls="mainNavbar" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler hidden-lg-up">☰<span class="hidden"><?= $this->lang->line('categories', 'Danh mục'); ?></span></button>
                    <h2><a title="Auchan" href="<?php echo base_url(); ?>">Auchan</a></h2>
                </div>
                <div class="col-xs-6 col-md-6 push-md-4">
                    <div id="desktopNavbar" class="pull-right">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a data-toggle="collapse" data-target="#mobileSearch" aria-controls="mobileSearch" aria-expanded="false" aria-label="Toggle navigation" href="#" class="nav-link"><i class="auc auc-search"> </i><span><?= $this->lang->line('search', 'Tìm kiếm'); ?></span></a></li>
                            <!--<li class="list-inline-item"><a class="nav-link" id="myLocation"></a></li>-->
                            <li class="list-inline-item"><a href="<?php echo base_url(); ?>he-thong-sieu-thi-auchan/" class="nav-link"> <i class="auc auc-location"> </i><span><?= $this->lang->line('auchan_location', 'Hệ thống siêu thị Auchan?'); ?></span></a></li>
                            <?php if (BETAOFF): ?>
                                <li class="list-inline-item">
                                    <?php
                                    if (isset($this->session->userdata['logged_in'])) {
                                        ?>
                                        <a data-toggle="dropdown" href="user-info.html" class="nav-link dropdown-toggle"><i class="auc auc-user-3 hidden-md-down"></i><span class="hidden-md-down">Chào </span>Nghĩa</a>
                                        <div class="user-info dropdown-menu"><a href="user-info.html" class="dropdown-item"><i class="auc auc-fw auc-user-3"> </i><span>Thông tin tài khoản</span></a><a href="alpoint.html" class="dropdown-item"><i class="auc auc-fw auc-financial-care"></i><span>Auchan Loyalty Point </span>
                                                <div class="tag tag-pill tag-warning">20</div></a><a href="order-history.html" class="dropdown-item"><i class="auc auc-fw auc-doc-text"></i><span>Đơn hàng của tôi</span></a><a href="fav-list.html" class="dropdown-item"><i class="auc auc-fw auc-heart"></i><span>Danh sách yêu thích</span></a><a href="gift-list.html" class="dropdown-item"><i class="auc auc-fw auc-gift-card"></i><span>Danh sách quà tặng</span></a><a href="notification.html" class="dropdown-item"><i class="auc auc-fw auc-bell"></i><span>Thông báo của tôi</span>
                                                <div class="tag tag-pill tag-warning">9</div></a>
                                            <div class="dropdown-divider"></div><a href="logout.html" class="dropdown-item"><i class="auc auc-fw auc-exit"></i><span>Đăng xuất</span></a>
                                        </div>
                                    <?php } else { ?>
                                        <a href="" title="Đăng nhập tài khoản" data-toggle="modal" data-target="#loginModal" data-dismiss="modal" class="nav-link"><i class="auchan icon-auchan-80 auc-2x"></i><span>Đăng nhập</span></a>
                                    <?php } ?>
                                </li>
                                <li class="list-inline-item wrap-shoping-cart"><a href="cart.html" title="Giỏ hàng" class="nav-link cart-button"> <i class="auc auc-cart"> </i><span>Giỏ hàng</span><span class="cart-count">2</span></a>
                                    <div class="cart-view">
                                        <div class="cart-body">
                                            <div class="alert alert-warning alert-sm">
                                                <button type="button" data-dismiss="alert" class="close">×</button> Quý khách có thể dùng <a href="" class="btn-link"><b>Voucher 	</b></a>hoặc  <a href="" class="btn-link"><b>Auchan Loyalty Point </b></a>để giảm giá trực tiếp cho đơn hàng.
                                            </div>
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td><a href="#" title=""><img src="http://placehold.it/80x80" alt=""></a></td>
                                                        <td class="w-100">
                                                            <h4><a href="#" title="SEPHORA FAVORITES">SEPHORA FAVORITES</a></h4>
                                                            <p class="prd-price pull-left"><span class="old-price">1.000.000 đ</span><span class="price">500.000 đ													</span></p><a href="#" class="group-icon pull-right"><i class="auc auc-trash-empty"></i></a>
                                                            <p class="prd-quanlity pull-right"> 
                                                                <input type="text" value="2" name="product-quality" class="form-control form-control-sm">
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="#" title=""><img src="http://placehold.it/80x80" alt=""></a></td>
                                                        <td class="w-100">
                                                            <h4><a href="#" title="SEPHORA FAVORITES">SEPHORA FAVORITES</a></h4>
                                                            <p class="prd-price pull-left"><span class="old-price">1.000.000 đ</span><span class="price">500.000 đ</span></p><a href="#" class="group-icon pull-right"><i class="auc auc-trash-empty"></i></a>
                                                            <p class="prd-quanlity pull-right"> 
                                                                <input type="text" value="2" name="product-quality" class="form-control form-control-sm">
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="#" title=""><img src="http://placehold.it/80x80" alt=""></a></td>
                                                        <td class="w-100">
                                                            <h4><a href="#" title="SEPHORA FAVORITES">SEPHORA FAVORITES</a></h4>
                                                            <p class="prd-price pull-left"><span class="old-price">1.000.000 đ</span><span class="price">500.000 đ</span></p><a href="#" class="group-icon pull-right"><i class="auc auc-trash-empty"></i></a>
                                                            <p class="prd-quanlity pull-right"> 
                                                                <input type="text" value="2" name="product-quality" class="form-control form-control-sm">
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="#" title=""><img src="http://placehold.it/80x80" alt=""></a></td>
                                                        <td class="w-100">
                                                            <h4><a href="#" title="SEPHORA FAVORITES">SEPHORA FAVORITES</a></h4>
                                                            <p class="prd-price pull-left"><span class="old-price">1.000.000 đ</span><span class="price">500.000 đ</span></p><a href="#" class="group-icon pull-right"><i class="auc auc-trash-empty"></i></a>
                                                            <p class="prd-quanlity pull-right"> 
                                                                <input type="text" value="2" name="product-quality" class="form-control form-control-sm">
                                                            </p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <p> <span>Tổng cộng:</span><b class="pull-right">1.000.000đ</b></p>
                                            <p><a href="cart.html" title="Thanh toán" class="btn btn-primary pull-right">Thanh toán</a></p>
                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
                <div id="mobileSearch" class="col-md-5 col-lg-4 search-box pull-md-6 collapse navbar-toggleable-sm">
                    <div class="input-group">
                        <input id="placeholder-type-writter" name="search" type="text" placeholder="Tìm kiếm cho sản phẩm hoặc thương hiệu" class="form-control">
                        <button id="btnGroupAddon2" type="button" class="input-group-addon btn"><i class="auc auc-search"></i></button>
                        <div class="completesearch"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="mobile-cate-nav">
        <div class="container">
            <div class="row">
                <?php
                $this->load->view('pages/includes/inc_mainmenu_m');
                ?>
            </div>
        </div>
    </div>
</header>
