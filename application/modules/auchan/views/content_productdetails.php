<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <?php
                if (isset($off_menu))
                    $this->load->view('pages/includes/inc_mainmenu');
                ?>
                <div class="col-md-12 <?= isset($off_menu) ? 'col-lg-8' : 'col-lg-10' ?>">
                    <?php
                    $breadcrumb_detail['data'] = array(
                        'page_template' => 'page_details',
                        'menu_text_active' => $item['Title']
                    );
                    $this->load->view('templates/breadcrumb', $breadcrumb_detail);
                    ?>
                    <div class="detail-box">
                        <div class="row">
                            <?php
                            $this->view('templates/product_details', $item);
                            $url = base_url() . $this->uri->uri_string() . page_extension;
                            ?>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?php if (COMMENTOFF) : ?>
                                    <?php if (COMMENT == 'FB' && COMMENTOFF) : ?>
                                        <div class="fb-comments" data-href="<?= $url; ?>" data-numposts="10" width="100%"></div>
                                    <?php else : ?>
                                        <div class="col-md-12 comment-box">
                                            <h3 class="content-box-title"><span>Hỏi đáp & cảm nhận người dùng </span><a href="" data-toggle="modal" data-target="#comment-modal" data-dismiss="modal" title="Gửi nhận xét của bạn" class="btn btn-primary btn-sm"><i class="auc auc-pencil"> </i>Gửi nhận xét</a><small>(398 lượt nhận xét)</small></h3>
                                            <ul class="media-list comment-content">
                                                <li class="media">
                                                    <div class="media-body">
                                                        <div title="Khuê Thiều" class="profile-photo"></div>
                                                        <h6 class="media-heading"><b>Khuê Thiều </b><span>18 ngày trước</span>
                                                            <div class="star"><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star-empty"></i></div>
                                                        </h6>
                                                        <p>Làm thế nào để chọn được quả bơ ngon mà ko bị đắng??</p>
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <div class="media-body">
                                                        <div title="Chung Hiền" class="profile-photo"></div>
                                                        <h6 class="media-heading"><b>Chung Hiền </b><span>một tháng trước</span>
                                                            <div class="star"><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star-empty"></i></div>
                                                        </h6>
                                                        <p>Ăn thử sẽ biết ngon mà đắng hay không. Còn để chọn một quả ngon thì chắc bạn biết rồi.</p>
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <div class="media-body">
                                                        <div title="Ha Lan Anh" class="profile-photo"></div>
                                                        <h6 class="media-heading"><b>Ha Lan Anh </b><span>3 tháng trước</span>
                                                            <div class="star"><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star-empty"></i></div>
                                                        </h6>
                                                        <p>Bơ này mình từng ăn, rất ngon, trái đồng đều. Cả nhà nên thử qua.</p>
                                                    </div>
                                                </li>
                                            </ul>
                                            <p class="text-center"><a href="" class="btn btn-secondary">Xem thêm nhận xét <i class="auc auc-right"></i></a></p>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <?php if (BETAOFF) : ?>
                    <div class="col-md-12 col-lg-12">
                        <div class="row">
                            <div class="product-box">
                                <div class="box-item">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <?php
                                            #SHOW LIST TITLE
                                            $alias = $this->uri->uri_string();
                                            $this->load->model('products_model', 'products');
                                            $arr = $this->products->getBreadCrumbData($alias);
                                            $alias_list = $arr['AliasCategory'] . '/' . $arr['AliasList'] . page_extension;
                                            ?>
                                            <?php if (BETAOFF) : ?>
                                                <h3 class="content-box-title">Sản phẩm tương tự thuộc "<b><?php echo $arr['MenuList']; ?></b>"<a href="<?php echo $alias_list; ?>" class="read-all" title="Xem tất cả sản phẩm thuộc <?php echo $arr['MenuList']; ?>">Xem tất cả <i class="auc auc-angle-right"></i></a></h3>
                                            <?php else: ?>
                                                <h3 class="content-box-title">Sản phẩm khác</h3>
                                            <?php endif; ?>
                                        </div>
                                        <?php
                                        $pid = $this->products->getIDFromAlias($alias);
                                        
                                        $data_top4['limit'] = 4;
                                        $data_top4['cid'] = $arr['ID'];
                                        $data_top4['random'] = TRUE;
                                        $data_top4['this_id'] = $pid;
                                        $data_top4['class'] = 'col-xs-6 col-sm-4 col-md-3 product-item';
                                        $this->view('templates/product_items', $data_top4);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                <?php
                $this->view('templates/stores');
                ?>
            </div>
            <?php
            $F = FALSE;
            if ($F) {
                ?>
                <div class="col-md-12 col-lg-12">
                    <div class="row">
                        <div class="product-box">
                            <div class="box-item">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <?php
                                        #SHOW LIST TITLE
                                        $alias = $this->uri->uri_string();
                                        $this->load->model('products_model', 'products');
                                        $arr = $this->products->getBreadCrumbData($alias);
                                        $alias_list = $arr['AliasCategory'] . '/' . $arr['AliasList'] . page_extension;
                                        ?>
                                        <?php if (BETAOFF) : ?>
                                            <h3 class="content-box-title">Sản phẩm tương tự thuộc "<b><?php echo $arr['MenuList']; ?></b>"<a href="<?php echo $alias_list; ?>" class="read-all" title="Xem tất cả sản phẩm thuộc <?php echo $arr['MenuList']; ?>">Xem tất cả <i class="auc auc-angle-right"></i></a></h3>
                                        <?php else: ?>
                                            <h3 class="content-box-title">Sản phẩm khác<a href="<?php echo $alias_list; ?>" class="read-all" title="Xem tất cả sản phẩm khác">Xem tất cả <i class="auc auc-angle-right"></i></a></h3>
                                                <?php endif; ?>
                                    </div>
                                    <?php
                                    $data_top10['limit'] = 6;
                                    $data_top10['class'] = 'col-xs-6 col-sm-4 col-md-2 product-item';
                                    $this->view('templates/product_items', $data_top10);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>