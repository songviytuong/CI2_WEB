<div class="auto-complete-search-box hidden" style="display: none;">
    <div class="item-products row">
        <div class="col-xs-3 col-lg-2"><a href="details.html"><img src="<?= THEMES_URL; ?>/images/asset/products-1.jpg" class="img-fluid"></a></div>
        <div class="col-xs-9 col-lg-10"><span class="badge badge-danger">OFF 10%</span><a href="details.html" class="title-products">Máy xay sinh tố công suất 250W (2 trong 1) MS-BL250</a>
            <p class="voted"><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i></p>
            <p class="price"><b>660.000 </b><span>750.000</span></p>
        </div>
    </div>
    <div class="item-products row">
        <div class="col-xs-3 col-lg-2"><a href="details.html"><img src="<?= THEMES_URL; ?>/images/asset/products-2.jpg" class="img-fluid"></a></div>
        <div class="col-xs-9 col-lg-10"><span class="badge badge-warning">NEW</span><a href="details.html" class="title-products">Máy hút bụi có thể sạc lại Morries Portable MS ...</a>
            <p class="voted"><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i></p>
            <p class="price"><b>760.000 </b><span>850.000</span></p>
        </div>
    </div>
    <div class="item-products row">
        <div class="col-xs-3 col-lg-2"><a href="details.html"><img src="<?= THEMES_URL; ?>/images/asset/products-3.jpg" class="img-fluid"></a></div>
        <div class="col-xs-9 col-lg-10"><span class="badge badge-danger">OFF 10%</span><a href="details.html" class="title-products">Bóng đèn LED Osram LED DL SCLA100F-14W / 865</a>
            <p class="voted"><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i></p>
            <p class="price"><b>60.000 </b><span>80.000</span></p>
        </div>
    </div>
    <div class="item-products row">
        <div class="col-xs-3 col-lg-2"><a href="details.html"><img src="<?= THEMES_URL; ?>/images/asset/products-4.jpg" class="img-fluid"></a></div>
        <div class="col-xs-9 col-lg-10"><a href="details.html" class="title-products">Máy nướng bánh thép không gỉ MS-2500SS</a>
            <p class="voted"><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star-empty"></i></p>
            <p class="price"><b>960.000 </b><span>1.180.000</span></p>
        </div>
    </div>
    <div class="view-more-search row"><a href=""><i class="auc auc-home"></i> Xem thêm kết quả cho từ khóa <b>"máy"</b></a></div>
</div>