<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <?php
                if (isset($off_menu)) {
                    $data_menu['class'] = 'col-md-2 hidden-md-down';
                    $this->load->view('pages/includes/inc_mainmenu');
                }
                ?>
                <div class="col-md-12 <?= isset($off_menu) ? 'col-lg-8' : 'col-lg-12' ?>">
                    <?php
                    $home_slider['data'] = $main_slider;
                    $this->load->view('templates/carousel', $home_slider);
                    ?>
                    <?php
                    #Display Banner
                    if (count($group1_banners) > 0):
                        ?>
                        <div class="content-box">
                            <div class="row">
                                <?php
                                foreach ($group1_banners as $key => $item) {
                                    ?>
                                    <div class="col-xs-12 col-sm-6 col-md-6 cate-banner-item">
                                        <?php if ($item['link']) : ?><a href="<?= $item['link']; ?>" target="<?= $item['target']; ?>"><?php endif; ?>
                                            <img src="<?= UPLOAD_URL; ?>/<?= $item['image']; ?>" alt="<?= $item['title']; ?>" class="img-fluid img-thumbnail"/>
                                            <?php if ($item['link']) : ?></a><?php endif; ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="content-box promo-box hidden-md-down">
                        <div class="row">
                            <div class="col-sm-6 col-md-3 item"><i class="auc auc-shield auc-3x text-success"></i>
                                <p>Đảm bảo<span>giá cả hợp lý</span></p>
                            </div>
                            <div class="col-sm-6 col-md-3 item"><i class="auc auc-delivery-truck auc-3x text-danger"></i>
                                <p>Giao hàng miễn phí<span>cho đơn hàng từ 300.000 VNĐ</span></p>
                            </div>
                            <div class="col-sm-6 col-md-3 item"><i class="auc auc-route auc-3x text-info"></i>
                                <p>Giao hàng<span>trong phạm vi 5km</span></p>
                            </div>
                            <div class="col-sm-6 col-md-3 item"><i class="auc auc-circular-clock-tool auc-3x text-warning"></i>
                                <p>7 ngày / tuần,<span>Từ 7:30 đến 22:00</span></p>
                            </div>
                        </div>
                    </div>
                    <?php
                    $this->load->model('products_model', 'products');
                    $r = $this->products->getCategoriesAddon();
                    foreach ($r as $key => $row) {
                        ?>
                        <div class="content-box product-box">

                            <div class="row">
                                <div class="col-xs-12">
                                    <h3 class="content-box-title"><?php echo $row['Title']; ?><a class="read-all" href="<?= base_url() . 'khuyen-mai/?tag=' . $row["Alias"]; ?>">Xem tất cả <i class="auc auc-angle-right"></i></a></h3>
                                </div>
                            </div>
                            <div class="row">
                                <?php
                                $data_top10['cataddon'] = $row['ID'];
                                if ($row['ID'] == 2) {
                                    $data_top10['limit'] = 5;
                                } else {
                                    $data_top10['limit'] = 10;
                                }
                                $data_top10['class'] = 'col-xs-6 col-sm-4 col-md-2 col-2 product-item';
                                $this->view('templates/product_items', $data_top10);
                                ?>
                            </div>

                        </div>
                    <?php } ?>
                    <?php
                    #Display Banner
                    if (count($group2_banners) > 0):
                        ?>
                        <div class="content-box hidden">
                            <div class="row">
                                <?php
                                foreach ($group2_banners as $key => $item) {
                                    ?>
                                    <div class="col-xs-12 col-sm-6 col-md-6 cate-banner-item">
                                        <div style="position: relative;">
                                            <?php if ($item['link']) : ?><a href="<?= $item['link']; ?>" target="<?= $item['target']; ?>"><?php endif; ?>
                                                <img src="<?= UPLOAD_URL; ?>/<?= $item['image']; ?>" alt="<?= $item['title']; ?>" class="img-fluid img-thumbnail"/>
                                                <div class="header_title"><div class="header_in"><?= $item['title']; ?></div></div>
                                                <?php if ($item['link']) : ?></a><?php endif; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (BETAOFF) : ?>
                        <div class="content-box product-box">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h3 class="content-box-title">Sản phẩm mới cập nhật<a class="read-all">Xem tất cả <i class="auc auc-angle-right"></i></a></h3>
                                </div>
                            </div>
                            <div class="row">
                                <?php
                                $data_created['limit'] = 5;
                                $data_created['created'] = TRUE;
                                $data_created['class'] = 'col-xs-6 col-sm-4 col-md-2 col-2 product-item';
                                $this->view('templates/product_items.php', $data_created);
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="content-box socials hidden-md-down hidden">
                        <div class="row">
                            <div class="col-xs-9">
                                <div class="facebook-social" data-facebook-api="" data-googleanalytics="{&quot;type&quot;: &quot;facebook&quot;, &quot;infoGA&quot;: [{&quot;category&quot;: &quot;Auchan Share&quot;, &quot;action&quot;: &quot;Facebook Button Like Click&quot;, &quot;label&quot;: &quot;Auchan Page&quot;}, {&quot;category&quot;: &quot;Auchan Share&quot;, &quot;action&quot;: &quot;Facebook Unlike Click&quot;, &quot;label&quot;: &quot;Auchan Page&quot;}, {&quot;category&quot;: &quot;Auchan Share&quot;, &quot;action&quot;: &quot;Facebook Button Share Click&quot;, &quot;label&quot;: &quot;Auchan Page&quot;}]}">
                                    <img src="<?= THEMES_URL ?>/images/like-us-facebook.png?v=1.0" alt="Your friend like us too!">
                                    <div class="fb-like fb_iframe_widget" data-href="https://www.facebook.com/Auchan.vn/" data-layout="standard" data-action="like" data-width="610" data-size="large" data-show-faces="true" data-share="true" fb-xfbml-state="rendered" fb-iframe-plugin-query="action=like&amp;app_id=893607087382185&amp;container_width=578&amp;href=https%3A%2F%2Fwww.facebook.com%2FAuchan.vn%2F&amp;layout=standard&amp;locale=en_US&amp;sdk=joey&amp;share=true&amp;show_faces=true&amp;size=large&amp;width=610"><span style="vertical-align: bottom; width: 610px; height: 28px;"><iframe name="fab903c2f331" width="610px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like Facebook Social Plugin" src="https://www.facebook.com/v2.8/plugins/like.php?action=like&amp;app_id=893607087382185&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FZ2duorNoYeF.js%3Fversion%3D42%23cb%3Df1d66dbb053e34c%26domain%3Dauchan.vn%26origin%3Dhttp%253A%252F%252Fauchan.vn%252Ff29ad4aa321328%26relation%3Dparent.parent&amp;container_width=578&amp;href=https%3A%2F%2Fwww.facebook.com%2FAuchan.vn%2F&amp;layout=standard&amp;locale=en_US&amp;sdk=joey&amp;share=true&amp;show_faces=true&amp;size=large&amp;width=610" style="border: none; visibility: visible; width: 610px; height: 28px;" class=""></iframe></span></div>
                                </div>

                            </div>
                            <div class="col-xs-3">
                                <div class="follow-zalo">
                                    <p class="heading heading-5">Theo dõi chúng tôi trên Zalo</p>
                                    <a href="http://page.zaloapp.com/" title="Zalo" data-googleanalytics="{&quot;type&quot;: &quot;button&quot;, &quot;category&quot;: &quot;Auchan Share&quot;, &quot;action&quot;: &quot;Zalo Button Click&quot;, &quot;label&quot;: &quot;Auchan Page&quot;}" target="_blank"><img src="public/auchan/v2/images/zalo.png" alt="Theo dõi chúng tôi trên Zalo"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    #Display Banner
                    if (count($group11_banners) > 0):
                        ?>
                        <?php
                        foreach ($group11_banners as $key => $item) {
                            ?>
                            <div class="cate-banner-item">
                                <?php if ($item['link']) : ?><a href="<?= $item['link']; ?>" target="<?= $item['target']; ?>"><?php endif; ?>
                                    <img src="<?= UPLOAD_URL; ?>/<?= $item['image']; ?>" alt="<?= $item['title']; ?>" class="img-fluid img-thumbnail"/>
                                    <?php if ($item['link']) : ?></a><?php endif; ?>
                            </div>
                        <?php } ?>
                    <?php endif; ?>

                    <div class="content-box product-box">
                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="content-box-title">Cẩm nang Auchan<a href="<?php echo base_url(); ?>cam-nang-auchan/" class="read-all">Xem tất cả <i class="auc auc-angle-right"></i></a></h3>
                            </div>
                        </div>
                        <div class="row">
                            <?php
                            $data_home_post['class'] = 'col-xs-6 col-md-3';
                            $data_home_post['limit'] = 4;
			    $data_home_post['maxheight'] = 143;
                            $this->view('templates/post_home_items', $data_home_post);
                            ?>
                        </div>
                    </div>
                    <div class="card subcript alert-warning">
                        <div class="card-block">					
                            <div class="row">		
                                <div class="col-md-6 col-lg-7">
                                    <h5 class="title"> <img src="<?= THEMES_URL ?>/images/email.svg" alt="Đăng ký bản tin"><b>Đăng ký nhận bản tin Auchan</b><small>Để không bỏ lỡ hàng ngàn sản phẩm và chương trình siêu hấp dẫn.</small></h5>
                                </div>
                                <div class="col-md-6 col-lg-5">
                                    <form class="form-inline">
                                        <div class="form-group">
                                            <input type="email" name="email_subscribe" id="email_subscribe" placeholder="Nhập E-mail của bạn" class="form-control" required="required"/>
                                            <button type="button" class="btn btn-primary btn-default btn_subscribe">Đăng ký</button>
                                        </div>
                                    </form>
                                    <div class="result_subscribe"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $this->view('templates/stores');
                ?>
            </div>
        </div>
    </div>
    <div class="container"></div>
</div>