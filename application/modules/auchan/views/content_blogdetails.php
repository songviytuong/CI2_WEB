<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><i class="auc auc-shop"> </i> <a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>cam-nang-auchan/">Cẩm nang</a></li>
                        <li class="breadcrumb-item active"><?= $post->Title; ?></li>
                    </ol>
                    <div class="post-box">
                        <div class="row">
                            <div class="main-content col-md-8">
                                <div class="detail-content">
                                    <h1 class="post-title"><?= $post->Title; ?></h1>
                                    <div class="top-meta">
                                        <div class="meta-info"><span class="post-date"><i class="auc auc-fw auc-calendar"></i><?= date_format(date_create($post->Created), 'd/m/Y H:i'); ?></span></div>
                                        <div class="default-share"></div>
                                    </div>
                                    <div class="post-content-wrap">
                                        <?php if ($post->Description) : ?>
                                            <p class="post-excerpt"><?= $post->Description; ?></p>
                                        <?php endif; ?>
                                        <div class="post-content">
                                            <?php
                                            if (!empty($post->Introtext)) {
                                                if (!lazyload) {
                                                    //$lazyload = "src='" . $item['main_picture'] . "'";
                                                    $Content = str_replace('src="', 'src="' . UPLOAD_URL, $post->Introtext);
                                                } else {
                                                    $Content = str_replace('src="', 'src=data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs= data-src="' . UPLOAD_URL, $post->Introtext);
                                                    $Content = str_replace('img', 'img class="lazyload"', $Content);
                                                    if (!empty($post->YoutubeID)) {
                                                        $Content = str_replace('[Youtube]', '<iframe src="https://www.youtube.com/embed/' . $post->YoutubeID . '" allowfullscreen="" class="embed-responsive-item" style="width:100%; height:450px;"></iframe>', $Content);
                                                    }
                                                    //$lazyload = "src='data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=' data-src='" . $item['main_picture'] . "'";
                                                }
                                            }
                                            ?>
                                            <?= $Content; ?>
                                        </div>
                                    </div>
                                    <!--div class="post-source-tags">
                                        <h6><span class="tag tag-default">TAGS </span><a href=""><small>art</small></a>, <a href=""><small>cool</small></a>, <a href=""><small>design</small></a>, <a href=""><small>tutorials</small></a></h6>
                                    </div-->
                                    <div id="fb-root"></div>
                                    <script>(function (d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0];
                                            if (d.getElementById(id))
                                                return;
                                            js = d.createElement(s);
                                            js.id = id;
                                            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10";
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));</script>
                                    <div class="post-sharing-bottom text-xs-center"><span>Nếu bạn thấy bài viết này hữu ích, hãy chia sẻ với bạn bè</span>
                                                <p><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= urlencode(base_url() . $this->uri->uri_string()); ?>&amp;picture=<?php echo UPLOAD_URL . '/' . $post->Thumb; ?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore social-sharing-buttons btn btn-info"><i class="auc auc-facebook-circled"></i><span>Chia sẻ Facebook</span></a><!--a href="" class="social-sharing-buttons btn btn-danger"><i class="auc auc-gplus"></i><span>Chia sẻ Google+</span></a--></p>
                                    </div>
                                </div>
                                <div class="relate-post">
                                    <h3 class="box-title"><span>Bài viết liên quan</span></h3>
                                    <div class="row">
                                        <?php
                                        $count = count($post_random) - 1;
                                        foreach ($post_random as $key => $item) {
                                            ?>
                                            <div class="col-xs-6 col-sm-4 <?= ($count == $key) ? ' hidden-sm-up' : '' ?>">
                                                <div class="post-item"><a href="<?= $item['Url']; ?>" class="post-img" title="<?= $item['Title']; ?>"><img alt="<?= $item['Title']; ?>" src="<?= $item['Thumb']; ?>" class="img-rounded img-fluid" /></a>
                                                    <p class="meta"><a href="<?= $item['Url']; ?>" class="text" title="<?= $item['Title']; ?>"><?= $item['Title']; ?></a></p>
                                                </div>
                                            </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>

                            <div class="sidebar col-md-4">
                                <div class="sidebar-box">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <iframe src="https://www.youtube.com/embed/53NgBMYLXwg" allowfullscreen="" class="embed-responsive-item" style="width:100%; height:250px;"></iframe>
                                        </div>
                                    </div>
                                </div>
                                <div class="sidebar-box">
                                    <h3 class="box-title"><span>Bài viết nổi bật</span></h3>
                                    <div class="row">
                                        <?php
                                        foreach ($post_featured as $key => $item) {
                                            ?>
                                            <div class="post-item col-xs-6 col-md-12"><a href="<?= $item['Url']; ?>" class="post-img" title="<?= $item['Title']; ?>"><img src="<?= $item['Thumb']; ?>" style="width: 113px" alt="Hoa cài mái tóc" class="img-fluid img-rounded"></a>
                                                <p class="meta"><a href="<?= $item['Url']; ?>" class="text" title="<?= $item['Title']; ?>"><?= $item['Title']; ?></a></p>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>