<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <?php
                if (isset($off_menu))
                    $data_menu['class'] = 'col-md-2 hidden-md-down';
                $this->load->view('pages/includes/inc_mainmenu');
                ?>
                <div class="col-md-12 col-lg-10">
                    <?php
                    $breadcrumb['data'] = array(
                        'page_template' => 'product_list',
                        'menu_text_active' => $MenuTitle
                    );
                    $this->load->view('templates/breadcrumb', $breadcrumb);
                    ?>
                    <?php
                    $categories_slider['data'] = $MainPicture;
                    $categories_slider['class'] = 'cate-banner-item';
                    $this->load->view('templates/carousel', $categories_slider);
                    ?>
                    <!--
                    <div class="list-categories-child">
                        <ul role="tablist" class="nav nav-tabs">
                            <li class="nav-item"><a data-toggle="tab" href="#categories" role="tab" class="nav-link active">Categories</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#brands" role="tab" class="nav-link">Brands</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#price" role="tab" class="nav-link">Price</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#country" role="tab" class="nav-link">Country <span class="hidden-sm-down">of Origin</span></a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="categories" role="tabpanel" class="tab-pane active">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-4 col-md-3 cate-item"><a href="sub-categories.html"><b>Fresh Fruits</b><span>1.984 sản phẩm</span></a><img src="../images/asset/list-cat-1.jpg" alt="Auchan" class="img-fluid"></div>
                                    <div class="col-xs-6 col-sm-4 col-md-3 cate-item"><a href="sub-categories.html"><b>Fresh Vegetables</b><span>1.984 sản phẩm</span></a><img src="../images/asset/list-cat-2.jpg" alt="Auchan" class="img-fluid"></div>
                                    <div class="col-xs-6 col-sm-4 col-md-3 cate-item"><a href="sub-categories.html"><b>Asian Vegetables</b><span>1.984 sản phẩm</span></a><img src="../images/asset/list-cat-3.jpg" alt="Auchan" class="img-fluid"></div>
                                    <div class="col-xs-6 col-sm-4 col-md-3 cate-item"><a href="sub-categories.html"><b>Organics</b><span>1.984 sản phẩm</span></a><img src="../images/asset/list-cat-4.JPG" alt="Auchan" class="img-fluid"></div>
                                    <div class="col-xs-6 col-sm-4 col-md-3 cate-item"><a href="sub-categories.html"><b>Salads</b><span>1.984 sản phẩm</span></a><img src="../images/asset/list-cat-5.jpg" alt="Auchan" class="img-fluid"></div>
                                    <div class="col-xs-6 col-sm-4 col-md-3 cate-item"><a href="sub-categories.html"><b>Ready-to-eat</b><span>1.984 sản phẩm</span></a><img src="../images/asset/list-cat-6.jpg" alt="Auchan" class="img-fluid"></div>
                                    <div class="col-xs-6 col-sm-4 col-md-3 cate-item"><a href="sub-categories.html"><b>Herbs & Spices</b><span>1.984 sản phẩm</span></a><img src="../images/asset/list-cat-7.jpg" alt="Auchan" class="img-fluid"></div>
                                    <div class="col-xs-6 col-sm-4 col-md-3 cate-item"><a href="sub-categories.html"><b>Cooking Styles</b><span>1.984 sản phẩm</span></a><img src="../images/asset/list-cat-8.jpg" alt="Auchan" class="img-fluid"></div>
                                </div>
                            </div>
                            <div id="brands" role="tabpanel" class="tab-pane">
                                <ul class="list-unstyled">
                                    <li>
                                        <input type="checkbox" checked="checked" value="spain"><a title="Spain">Spain</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="fruit-monkeys"><a title="Fruit Monkeys">Fruit Monkeys</a><span>6</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="generic"><a title="Generic">Generic</a><span>5</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="airflown"><a title="AirFlown">AirFlown</a><span>3</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="benelux-fruit"><a title="Benelux Fruit">Benelux Fruit</a><span>3</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" checked="checked" value="driscoll"><a title="Driscoll">Driscoll</a><span>3</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="kirei"><a title="Kirei">Kirei</a><span>3</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="benelux"><a title="Benelux">Benelux</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="coconut"><a title="Coconut">Coconut</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" checked="checked" value="dole"><a title="Dole">Dole</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="givvo"><a title="GIVVO">GIVVO</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" checked="checked" value="gala"><a title="Gala">Gala</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="granny"><a title="Granny">Granny</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="leong-tee"><a title="Leong Tee">Leong Tee</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="thygrace"><a title="Thygrace">Thygrace</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="usa"><a title="USA">USA</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="yuvvo"><a title="YUVVO">YUVVO</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="airflown"><a title="Airflown">Airflown</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="all-coco"><a title="All Coco">All Coco</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="always-perfect"><a title="Always Perfect">Always Perfect</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="banana"><a title="Banana">Banana</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="chinese"><a title="Chinese">Chinese</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="classic"><a title="Classic">Classic</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="dragonfruits"><a title="Dragonfruits">Dragonfruits</a><span>1									</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="fragrant"><a title="Fragrant">Fragrant</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="fuji"><a title="Fuji">Fuji</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="givvo"><a title="Givvo">Givvo</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="kiwi"><a title="Kiwi">Kiwi</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="lemons"><a title="Lemons">Lemons</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="mandarins"><a title="Mandarins">Mandarins</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="mangoes"><a title="Mangoes">Mangoes</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="mexican"><a title="Mexican">Mexican</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="morocco"><a title="Morocco">Morocco</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="navel"><a title="Navel">Navel</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="packham"><a title="Packham">Packham</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="papaya"><a title="Papaya">Papaya</a><span>1									</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="pisang-mas"><a title="Pisang Mas">Pisang Mas</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="ratnagiri"><a title="Ratnagiri">Ratnagiri</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="saute"><a title="Saute">Saute</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="siamcoco"><a title="SiamCoco">SiamCoco</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="valencia"><a title="Valencia">Valencia</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="vietnam"><a title="Vietnam">Vietnam</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="earthbound-natures-partner"><a title="Earthbound/ Nature's Partner">Earthbound/ Nature's Partner</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="pisang-berangan"><a title="Pisang Berangan">Pisang Berangan</a><span>1</span>
                                    </li>
                                </ul>
                            </div>
                            <div id="price" role="tabpanel" class="tab-pane">
                                <ul class="list-unstyled">
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain"><= 100.000đ</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">100.000đ <= 300.000đ</a><span>9</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">300.000đ <= 600.000đ</a><span>10</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">600.000đ <= 1 triệu</a><span>17</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">1 triệu <= 3 triệu</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">3 triệu <= 6 triệu</a><span>46</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">6 triệu <= 8 triệu</a><span>12</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">8 triệu <= 12 triệu</a><span>5</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">12 triệu <= 15 triệu</a><span>90</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">>= 15 triệu</a><span>82</span>
                                    </li>
                                </ul>
                            </div>
                            <div id="country" role="tabpanel" class="tab-pane">
                                <ul class="list-unstyled">
                                    <li>
                                        <input type="checkbox" value="United States"><a title="United States">United States</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="New Zealand"><a title="New Zealand">New Zealand</a><span>5</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="South Africa"><a title="South Africa">South Africa</a><span>5</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Thailand"><a title="Thailand">Thailand</a><span>5</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="China"><a title="China">China</a><span>4</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Malaysia"><a title="Malaysia">Malaysia</a><span>4</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Mexico"><a title="Mexico">Mexico</a><span>3</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Philippines"><a title="Philippines">Philippines</a><span>3</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Australia"><a title="Australia">Australia</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Chile"><a title="Chile">Chile</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Vietnam"><a title="Vietnam">Vietnam</a><span>2</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Egypt"><a title="Egypt">Egypt</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Hong Kong"><a title="Hong Kong">Hong Kong</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Indonesia"><a title="Indonesia">Indonesia</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Kenya"><a title="Kenya">Kenya</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Mauritius"><a title="Mauritius">Mauritius</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Morocco"><a title="Morocco">Morocco</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Spain"><a title="Spain">Spain</a><span>1</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="Turkey"><a title="Turkey">Turkey</a><span>1</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    -->
                    <div class="product-box">
                        <div class="filter-box">
                            <div class="row">
                                <div class="col-xs-6 col-sm-10">
                                    <a href="#" class="nav-link active btn-sm"><?= $this->lang->line('all'); ?></a>
                                </div>
                                <div class="col-xs-6 col-sm-2">
                                    <select name="" id="" class="form-control form-control-sm mb-2 mr-sm-2 mb-sm-0">
                                        <option>Mới cập nhật</option>
                                        <option>Giá thấp đến cao</option>
                                        <option>Giá cao đến thấp</option>
                                        <option>Sản phẩm A-Z</option>
                                        <option>Sản phẩm Z-A</option>
                                        <option>Thương hiệu A-Z</option>
                                        <option>Thương hiệu Z-A</option>
                                    </select>
                                </div>
                            </div>


                        </div>
                        <div class="row">
                            <?php
                            $data_productall['limit'] = 100;
                            $data_productall['cid'] = $cid;
                            $data_productall['class'] = 'col-xs-6 col-sm-4 col-md-2 product-item';
                            $this->view('templates/product_items', $data_productall);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>