<?php
$this->load->model('recruitment_model', 'recruitment');
?>
<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <?php
                if (isset($off_menu)) {
                    $data_menu['class'] = 'col-md-2 hidden-md-down';
                    $this->load->view('inc_mainmenu');
                }
                ?>
                <div class="col-md-12 <?= isset($off_menu) ? 'col-lg-10' : 'col-lg-12' ?>">
                    <div class="recruitment-box">
                        <div class="row recruitment-info">
                            <div class="col-xs-12 col-lg-6">
                                <h3 class="content-box-title">CƠ HỘI NGHỀ NGHIỆP</h3>
                                <p>Chào mừng bạn đến với trang giới thiệu Cơ hội nghề nghiệp tại Auchan Việt Nam.</p>
                                <p>Bạn có thể tìm thấy các công việc phù hợp được chia sẻ trong bảng thông tin việc làm và tìm hiểu cách nộp đơn ứng tuyển trực tuyến cùng Auchan Việt Nam tại link hướng dẫn hoặc gửi CV về hr@auchan.vn.</p>
                                <p>Auchan Việt Nam luôn chào đón bạn gia nhập vào đội ngũ năng động, đam mê thành công và vì khách hàng của công ty. Cùng nhau chúng ta mang đến dịch vụ tốt nhất và đóng góp vào sự thành công của Auchan và vì khách hàng Việt Nam. </p>
                                <p>Hãy gia nhập Auchan Việt Nam và cùng tỏa sáng tài năng của bạn!			</p>
                                <div class="text-center"><a href="" title="Xem hướng dẫn nộp đơn" class="btn-link"> <i class="auc auc-right"></i>Xem hướng dẫn nộp đơn</a></div>
                            </div>
                            <div class="col-xs-12 col-lg-6">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe src="https://www.youtube.com/embed/yXNzunmr0LM" allowfullscreen="" class="embed-responsive-item"></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="recruitment-content">
                            <?php
                            $total = $this->recruitment->getCount();
                            $total = ($total) ? $total : 0;
                            ?>
                            <h3 class="content-box-title">NHỮNG VỊ TRÍ ĐANG TUYỂN DỤNG<small>Có <?= $total; ?> vị trí đang tuyển dụng</small></h3>
                            <?php
                                $off = FALSE;
                                if($off):
                            ?>
                            <div class="recruitment-search-box">
                                <div class="row">
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Vị trí công việc cần tìm?" class="form-control">
                                    </div>
                                    <div class="col-sm-3 col-md-2">
                                        <select name="" id="" class="form-control">
                                            <option value="-1" disabled="disabled" selected="">Vị trí</option>
                                            <option value="0">Quản lý điều phối kho hàng</option>
                                            <option value="1">Nhân viên kiểm tra chất lượng</option>
                                            <option value="2">Chuyên gia đào tạo chế biến</option>
                                            <option value="3">Quản lý Siêu thị</option>
                                            <option value="4">Quản lý An Ninh</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 col-md-2">
                                        <select name="" id="" class="form-control">
                                            <option value="-1" disabled="disabled" selected="">Thành phố</option>
                                            <option>Hồ Chí Minh</option>
                                            <option>Hà Nội</option>
                                            <option>Tây Ninh</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 col-md-2">
                                        <select name="" id="" class="form-control">
                                            <option value="-1" disabled="disabled" selected="">Kinh nghiệm</option>
                                            <option>Chưa có kinh nghiệm</option>
                                            <option>Dưới 1 năm</option>
                                            <option>1 năm</option>
                                            <option>2 năm</option>
                                            <option>3 năm</option>
                                            <option>4 năm</option>
                                            <option>5 năm</option>
                                            <option>Trên 5 năm</option>
                                            <option>Không yêu cầu</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 col-md-2"><a href="" class="btn btn-primary"><i class="auc auc-search"></i> Tìm kiếm</a></div>
                                </div>
                            </div>
                            <?php
                                endif;
                            ?>
                            <div class="recruitment-position">
                                <div id="accordion" role="tablist" aria-multiselectable="true">
                                    <?php
                                    $recruitment = $this->recruitment->defineRecruitment();
                                    foreach ($recruitment as $key => $item) {
                                        ?>
                                        <div class="card item">
                                            <div id="headingOne" role="tab" class="card-header">
                                                <div class="row">
                                                    <h4 class="col-sm-5 col-md-6"><?= str_pad($item['Number'], 2, 0, STR_PAD_LEFT); ?> <?= strtoupper($item['Name']); ?><!--span><?= $item['Department'] ?> làm việc tại <?= strtolower($item['Location']) ?></span--></h4>
                                                    <p class="col-sm-2 hidden-xs"><?= $item['Timer']; ?></p>
                                                    <p class="col-sm-2"><?php
                                                        $data_city = json_decode($item['City']);
                                                        $this->load->model('city_model', 'city');
                                                        foreach ($data_city as $row) {
                                                            echo $cityName = $this->city->getBy('Title', 'ID', $row) . "</br>";
                                                        }
                                                        ?></p>
                                                    <div class="col-sm-3 col-md-2"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?= $item['ID']; ?>" aria-expanded="true" aria-controls="collapseOne" class="btn btn-success">Xem chi Tiết <i class="auc auc-right"></i></a></div>
                                                </div>
                                            </div>
                                            <div id="collapseOne<?= $item['ID']; ?>" role="tabpanel" aria-labelledby="headingOne" class="collapse show">
                                                <div class="card-block">
                                                    <div class="row">
                                                        <div class="col-xs-12 recruitment-header">
                                                            <div><b>Làm việc tại: </b><?= $item['Location']; ?></div>
                                                            <div><b>Số lượng: </b><?= str_pad($item['Number'], 2, 0, STR_PAD_LEFT); ?></div>
                                                            <div><b>Giới tính: </b><?= $item['Gender']; ?></div>
                                                            <div><b>Hình thức làm việc: </b><?= $item['Timer']; ?></div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <?= $item['Content']; ?>
                                                        </div>
                                                        <div class="col-md-3"></div>
                                                    </div>
                                                    <div class="card card-gray">
                                                        <div class="card-block">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <h5><i class="auc auc-circular-clock-tool"></i>Tiếp nhận hồ sơ đến hết ngày: <?= $item['End']; ?></h5>
                                                                    <p>Ứng viên quan tâm đến vị trí tuyển dụng. Vui lòng gửi CV trực tiếp đến văn phòng <a href="mailto:hr@auchan.vn" class="btn-link">HR Department</a></small></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>