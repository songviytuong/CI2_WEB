<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumb_loyalty_card['data'] = array(
                        'page_template' => 'product_categories',
                        'menu_text_active' => $this->lang->line('aboutus'),
                    );
                    $this->load->view('templates/breadcrumb', $breadcrumb_loyalty_card);
                    ?>
                    <div class="document-box">
                        <div class="row">
                            <?php
                            $MENU_LEFT['data'] = array(
                                'class' => 'col-md-4 col-xl-3',
                                'menuactive' => 'gioi-thieu-auchan'
                            );
                            $this->load->view('pages/includes/menu_left', $MENU_LEFT);
                            ?>
                            <div class="col-lg-8 col-xl-9">
                                <div class="detail-content">
                                    <h2 class="box-title"> <i class="auc auc-icon"> </i>Về chúng tôi</h2>
                                    <div class="post-content">
                                        <div class="alert alert-info">
                                            <p><strong>Hệ thống Auchan Việt Nam trực thuộc tập đoàn bán lẻ Auchan hàng đầu tại Pháp. Tập đoàn Auchan đã có mặt tại hơn 16 quốc gia trên toàn thế giới. </strong></p>
                                            <p>Với cam kết “GIÁ RẺ MỖI NGÀY”, bạn sẽ tha hồ mua sắm hơn 20.000 mặt hàng tại Auchan. Đặc biệt, có đến 80% sản phẩm xuất xứ Việt Nam thuộc nhiều ngành hàng như thực phẩm tươi sống, đồ gia dụng, thực phẩm đóng gói, hóa mỹ phẩm…<br> Với giá rẻ bất ngờ, nhiều ưu đãi hấp dẫn, chất lượng cao so với sản phẩm cùng loại trên thị trường, mọi nhu cầu mua sắm, tiêu dùng hàng ngày của bạn sẽ được Auchan đáp ứng. Đồng thời, Auchan cam kết luôn nỗ lực cải thiện chất lượng phục vụ để luôn làm hài lòng khách hàng đến tham quan mua sắm tại Auchan.</p>
                                            <p>Nếu có bất kỳ điều gì chưa hài lòng về sản phẩm hay dịch vụ của chúng tôi, bạn hãy cho chúng tôi biết nhé!</p>
                                        </div>
                                        <p>Tập đoàn Auchan được thành lập vào năm 1961 tại Lille – thành phố ở phía bắc nước Pháp. Auchan là nhà phân phối bán lẻ lớn thứ 2 tại Pháp, đồng thời là thành viên trong bảng xếp hạng 500 công ty, tập đoàn hàng đầu trên thế giới của tạp chí Fortune.</p>
                                        <div class="text-center"><img src="http://auchan.vn/wp-content/themes/auchanvn/assets/images/map-about-store.png?v=1.0" alt="VỀ TẬP ĐOÀN AUCHAN" class="img-fluid img-thumbnail"></div>
                                        <ul class="key-numbers list-unstyled">
                                            <li><b>16</b><span>Quốc gia</span></li>
                                            <li><b>774</b><span>Đại siêu thị</span></li>
                                            <li><b>817</b><span>Cửa hàng</span></li>
                                            <li><b>330.000</b><span>Nhân viên</span></li>
                                        </ul>
                                        <p>Năm 1981, với sự xuất hiện ngày càng tăng của các công ty con ở Pháp, tập đoàn Auchan thực hiện triển khai chiến lược phát triển toàn cầu. Để bắt kịp với nhu cầu thay đổi của thị trường, Auchan cam kết đa dạng hóa phát triển, bao gồm: Đại siêu thị, siêu thị, Banque Accord, trung tâm thương mại Immochan, thương mại điện tử, v..v…</p>
                                        <p>Hiện nay, Auchan đã có mặt tại 16 quốc gia và khu vực, với 774 đại siêu thị, 817 siêu thị và 362 trung tâm thương mại Immochan. Banque Accord phục vụ 7,6 triệu khách hàng tại 11 quốc gia. Auchan có 178 Drive ở 4 quốc gia, 26 Alinéa và 26 Little Extra ở lục địa Pháp, với tổng số hơn 330.000 nhân viên.</p>
                                        <p>Năm 2013, tập đoàn Auchan được xếp hạng thứ 11 trong số các nhà bán lẻ thực phẩm lớn nhất thế giới, và với doanh thu 62.1 tỉ đô bao gồm thuế cho các tất cả các chuỗi thương mại, Auchan được xếp hạng 149 trong danh sách 500 công ty của Fortune.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>