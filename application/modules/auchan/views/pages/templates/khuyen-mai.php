<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumb['data'] = array(
                        'page_template' => 'product_categories',
                        'menu_text_active' => $this->lang->line('promotion_page', 'Chương trình khuyến mãi')
                    );
                    $this->load->view('templates/breadcrumb', $breadcrumb);
                    ?>
                    <div class="product-box">
                        <div class="row">
                            <div class="col-xs-5 col-md-4 col-lg-3 hidden-sm-down">
                                <div class="main-filter card card-block">
                                    <h3 class="filter-title"><i class="auc auc-sort-descending"></i><span><?= $this->lang->line('promotion_filter_by', 'Tìm khuyến mãi theo'); ?></span></h3>
                                    <div id="accordion" role="tablist" aria-multiselectable="true" class="accordion">
                                        <div class="card">
                                            <div id="headingOne" role="tab" class="card-header">
                                                <h5 class="mb-0">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        <span><i class="auc auc-gift-card"></i><?= $this->lang->line('promotion_by_title', 'Tên chương trình'); ?></span><i class="auc auc-plus"></i><i class="auc auc-circle-minus"> </i>
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne" class="collapse show in">
                                                <div class="card-block">
                                                    <form id="checkboxes">
                                                        <?php
                                                        $this->load->model('promotions_model', 'promotions');
                                                        $params = array(
                                                            'status' => 0,
                                                            'between' => TRUE
                                                        );
                                                        $promotions = $this->promotions->definePromotions($params);
                                                        $checked_ = array(); //'10','13'
                                                        foreach ($promotions as $key => $row) {
                                                            $checked = (in_array($row['ID'], $checked_)) ? "checked='checked'" : "";
                                                            ?>
                                                            <div class="checkbox checkbox-primary">
                                                                <input id="checkbox1<?= $key; ?>" type="checkbox" <?= $checked; ?> name="filter_by_promotion" data-bind="<?= $row['ID']; ?>">
                                                                <label for="checkbox1<?= $key; ?>">
                                                                    <?= $row['Name']; ?>
                                                                </label>
                                                            </div>
                                                        <?php } ?>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if (BETAOFF) : ?>
                                            <div class="card">
                                                <div id="headingTwo" role="tab" class="card-header">
                                                    <h5 class="mb-0">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                            <span> <i class="auc auc-duplicate"></i><?= $this->lang->line('promotion_by_group_product', 'Nhóm sản phẩm'); ?></span><i class="auc auc-plus"></i><i class="auc auc-circle-minus"> </i>
                                                        </a>
                                                    </h5>
                                                </div>
                                                <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" class="collapse show in">
                                                    <div class="card-block">
                                                        <?php
                                                        $this->load->model('promotions_model', 'promotions');
                                                        $categories_addon = $this->promotions->defineProductCategoriesAddOn();
                                                        foreach ($categories_addon as $key => $row) {
                                                            ?>
                                                            <div class="checkbox checkbox-warning">
                                                                <input id="checkbox2<?= $key; ?>" type="checkbox" name="filter_by_group">
                                                                <label for="checkbox2<?= $key; ?>">
                                                                    <?= $row['Title']; ?>
                                                                </label>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card">
                                                <div id="headingThree" role="tab" class="card-header">
                                                    <h5 class="mb-0">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                            <span><i class="auc auc-location"></i><?= $this->lang->line('promotion_by_area', 'Khu vực'); ?></span><i class="auc auc-plus"></i><i class="auc auc-circle-minus"></i>
                                                        </a>
                                                    </h5>
                                                </div>
                                                <div id="collapseThree" role="tabpanel" aria-labelledby="headingThree" class="collapse show in">
                                                    <div class="card-block">
                                                        <?php
                                                        $this->load->model('warehouse_model', 'warehouse');
                                                        $filter_area = $this->warehouse->defineGroupCity();
                                                        foreach ($filter_area as $key => $row) {
                                                            ?>
                                                            <div class="checkbox checkbox-danger">
                                                                <input id="checkbox3<?= $key; ?>" type="checkbox" name="filter_by_area">
                                                                <label for="checkbox3<?= $key; ?>">
                                                                    <?= $row['city_name']; ?>
                                                                </label>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <?php
                                        $F = FALSE;
                                        if ($F) {
                                            ?>
                                            <div class="card">
                                                <div id="headingFour" role="tab" class="card-header">
                                                    <h5 class="mb-0"><a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour"><span><i class="auc auc-users-1"></i>Đối tượng áp dụng</span><i class="auc auc-plus"></i><i class="auc auc-circle-minus"> </i></a></h5>
                                                </div>
                                                <div id="collapseFour" role="tabpanel" aria-labelledby="headingFour" class="collapse show in">
                                                    <div class="card-block">
                                                        <ul class="list-unstyled">
                                                            <li class="form-check">
                                                                <label class="form-check-label"></label>
                                                                <input type="radio" name="option-04" checked class="form-check-input"> Có thẻ thành viên
                                                            </li>
                                                            <li class="form-check">
                                                                <label class="form-check-label"></label>
                                                                <input type="radio" name="option-04" class="form-check-input"> Mọi đối tượng
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div id="headingFive" role="tab" class="card-header">
                                                    <h5 class="mb-0"><a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive"><span><i class="auc auc-sun-weather"></i>Theo Mùa</span><i class="auc auc-plus"></i><i class="auc auc-circle-minus"> </i></a></h5>
                                                </div>
                                                <div id="collapseFive" role="tabpanel" aria-labelledby="headingFive" class="collapse show in">
                                                    <div class="card-block">
                                                        <ul class="list-unstyled">
                                                            <li class="form-check">
                                                                <label class="form-check-label"></label>
                                                                <input type="radio" name="option-05" class="form-check-input"> Trung thu
                                                            </li>
                                                            <li class="form-check">
                                                                <label class="form-check-label"></label>
                                                                <input type="radio" name="option-05" checked class="form-check-input"> Phụ nữ Việt Nam
                                                            </li>
                                                            <li class="form-check">
                                                                <label class="form-check-label"></label>
                                                                <input type="radio" name="option-05" class="form-check-input"> Halloween
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-8 col-lg-9">
                                <?php
                                $promotion_slider['data'] = $promotion_slider;
                                $this->load->view('templates/carousel', $promotion_slider);
                                ?>
                                <h3 class="full-box-title"><span><?= $this->lang->line('promotion_products_list', 'Danh sách sản phẩm'); ?></span>
                                    <?php
                                    $F = FALSE;
                                    if ($F) {
                                        ?>
                                        <form class="form-inline pull-lg-right">
                                            <div class="form-group">
                                                <label class="col-form-label-sm hidden-xs"><?= $this->lang->line('sort_by', 'Sắp xếp theo'); ?></label>
                                                <select class="form-control form-control-sm">
                                                    <option> Tin mới nhất</option>
                                                    <option> Lượt đánh giá</option>
                                                    <option> Lượt xem</option>
                                                    <option> Lượt thích</option>
                                                </select>
                                            </div>
                                        </form>
                                    <?php } ?>
                                </h3>
                                <div class="box-item">
                                    <div class="row text-center w-100">
                                        <div class="col-xs-12">
                                            <div class="ajax-loader">
                                                <img src="public/auchan/v2/images/loading.gif" class="img-responsive" />
                                            </div>
                                            <div class="ids" data-id=""></div>
                                        </div>
                                    </div>
                                    <div class="row showproduct">
                                        <?php
                                        $data_promotion['pid'] = array();
                                        $data_promotion['promotion'] = TRUE;
                                        $data_promotion['limit'] = 100;
                                        $data_promotion['class'] = 'col-xs-6 col-sm-4 col-lg-2 product-item';
                                        $this->view('templates/product_items', $data_promotion);
                                        ?>
                                    </div>
                                    <!--
                                    <nav class="text-center">
                                        <ul class="pagination">
                                            <li class="page-item disabled"><a href="#" tabindex="-1" aria-label="Previous" class="page-link"><span aria-hidden="true"><i class="auc auc-angle-left"></i></span><span class="sr-only"><i class="auc auc-angle-left"></i></span></a></li>
                                            <li class="page-item active"><a href="#" class="page-link">1 <span class="sr-only">(current)</span></a></li>
                                            <li class="page-item"><a href="#" class="page-link">2</a></li>
                                            <li class="page-item"><a href="#" class="page-link">3</a></li>
                                            <li class="page-item"><a href="#" class="page-link">4</a></li>
                                            <li class="page-item"><a href="#" class="page-link">5</a></li>
                                            <li class="page-item"><a href="#" aria-label="Next" class="page-link"><span aria-hidden="true"><i class="auc auc-angle-right"></i></span><span class="sr-only"><i class="auc auc-angle-right"></i></span></a></li>
                                        </ul>
                                    </nav>
                                    -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>