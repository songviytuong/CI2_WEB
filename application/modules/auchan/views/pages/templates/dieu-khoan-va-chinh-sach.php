<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumb_policy['data'] = array(
                        'page_template' => 'product_categories',
                        'menu_text_active' => $this->lang->line('policy'),
                    );
                    $this->load->view('templates/breadcrumb', $breadcrumb_policy);
                    ?>
                    <div class="document-box">
                        <div class="row">
                            <?php
                            $MENU_LEFT['data'] = array(
                                'class' => 'col-md-4 col-xl-3',
                                'menuactive' => 'dieu-khoan-va-chinh-sach'
                            );
                            $this->load->view('pages/includes/menu_left', $MENU_LEFT);
                            ?>
                            <div class="col-lg-8 col-xl-9">
                                <div class="detail-content">
                                    <h2 class="box-title"> <i class="auc auc-icon"> </i><?=$this->lang->line('policy');?>			</h2>
                                    <div class="post-content">
                                        <div class="alert alert-info">
                                            <p>
                                                <strong>Phát hành bởi: <?=$static->Title;?> </strong>
                                                <br> Địa chỉ đăng ký: <?=$static->FullAddress;?>
                                                <br> Số điện thoại: <?=$static->Hotline;?>
                                            </p>
                                        </div>
                                        <p><strong>Dữ liệu cá nhân:</strong></p>
                                        <p>Trang web này được khởi tạo và được đăng theo những điều khoản của Bộ luật Dân sự Việt Nam, Luật Công nghệ Thông tin, Nghị định 72/2013 / NĐ-CP ngày 15/7/2013 về việc quản lý, cung cấp và sử dụng dịch vụ internet và thông tin trực tuyến và các luật và quy định có liên quan của Việt Nam</p>
                                        <p>Mục đích của việc tự động xử lý dữ liệu cá nhân của bạn được thực hiện bởi Groupe Auchan là để trả lời các yêu cầu của bạn. Những mục trong dữ liệu cá nhân được đánh dấu sao (*) là bắt buộc để xử lý yêu cầu của bạn. Các dữ liệu cá nhân mà bạn nhập sẽ được chuyển đến Groupe Auchan.</p>
                                        <p>Bạn có quyền hợp pháp để sửa đổi, yêu cầu (với lý do chính đáng) và xóa dữ liệu cá nhân liên quan đến bạn, và có thể thực hiện quyền này bằng cách viết thư cho chúng tôi theo địa chỉ sau:</p>
                                        <p>Phòng Marketing<br> Lầu 3, tòa nhà Auchan, 11 Trường Sơn, P.15, Q.10, HCMC, Vietnam</p>
                                        <hr>
                                        <p><strong>Sở hữu trí tuệ:</strong></p>
                                        <p>Mỗi thành phần của trang web này được bảo vệ bởi quyền tác giả của Pháp và quốc tế và pháp luật sở hữu trí tuệ. Tất cả các quyền sao chép, đại diện và phóng tác được bảo vệ bởi Groupe Auchan S.A., bao gồm những quyền xin tài liệu tải về, các yếu tố đồ họa và nội dung hình ảnh.</p>
                                        <p>Việc sao chép và / hoặc phân phối trang web này, cho dù một phần hay toàn bộ, thông qua bất kỳ phương tiện nào mà không có sự cho phép của Giám đốc phát hành đều là  giả mạo, có thể phải chịu trách nhiệm dân sự và / hoặc hình sự đối với những người thực hiện.</p>
                                        <hr>
                                        <p><strong>Sử dụng trang web:</strong></p>
                                        <p>Groupe Auchan S.A. không chịu bất kì trách nhiệm nào liên quan đến các thông tin và / hoặc các trang có chứa tài liệu tham khảo đối với các sản phẩm, chương trình và dịch vụ hoặc bất kỳ trang web khác truy cập thông qua trang web <a href="" class="btn-link"><strong>www.auchan.vn</strong></a>.</p>
                                        <p>Groupe Auchan S.A. không chịu trách nhiệm về bất cứ nội dung của bất kỳ trang web nào truy cập từ trang web <a href="" class="btn-link"><strong>www.auchan.vn</strong></a>, hoặc việc sử dụng bất cứ nội dung nào nêu trên.</p>
                                        <p>Groupe Auchan S.A. áp dụng mọi biện pháp phòng ngừa để đảm bảo sự an toàn của trang web, và không chịu trách nhiệm về bất cứ cuộc tấn công mạng nào từ bên thứ ba với mục đích phá hủy và / hoặc thay đổi dữ liệu người dùng và / hoặc chương trình.</p>
                                        <p>Ngoài ra, Groupe Auchan S.A loại bỏ việc đăng tải các nội dung trái pháp luật, trái tiêu chuẩn đạo đức, và/hoặc các nội dụng có nguy cơ xâm phạm đến nhân phẩm con người, và cam kết xóa bỏ bất kì nội dung nào mang tính chất gian lận trên trang web, ngay khi nội dung đó được hiển thị, nhưng không chịu bất kì trách nhiệm nào cho việc hiện diện cũng những nội dung như vậy.</p>
                                        <hr>
                                        <p><strong>Photo credits: </strong></p>
                                        <p>Auchan, V.Damourette – SIPA PRESS, S.Dhote, G.Murat, F.Danel.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>