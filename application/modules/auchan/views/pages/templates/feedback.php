<div class="content nf-page">
    <div class="container">
        <div class="product-box">
            <div class="box-404 text-xs-center">
                <h3 class="box-title"><i class="auc auc-icon-2 auc-3x"></i><span><?= $this->lang->line('feedback_form', 'Hộp thư góp ý'); ?></span></h3>
                <div class="row">
                    <div class="col-xs-12 col-sm- col-lg-6 offset-lg-3">
                        <div class="card card-outline-success">
                            <div class="card-block" style="padding-bottom:10px;">
                                <p><?= $this->lang->line('feedback_description', 'Phản hồi cho Auchan biết bạn cần sản phẩm hoặc dịch vụ gì. Chúng tôi mong muốn tìm kiếm giải pháp, hoàn thiện dịch vụ & phục vụ bạn tốt nhất.'); ?></p>
                                <p>
                                    <?php
                                        $act = (isset($_GET['target'])) ? trim($_GET['target']) : '';
                                        echo $act;
                                    ?>
                                </p>
                                <form>
                                    <input type="hidden" name="url" value="<?=$act;?>"/>
                                    <div class="form-group">
                                        <?php
                                        $this->load->model('feedback_model', 'feedback');
                                        $feedback_arr = $this->feedback->defineFeedbackType($this->cur_langid);
                                        ?>
                                        <select name="feedback_option" id="feedback_option" class="form-control">
                                            <option value="-1" selected=""><?= $this->lang->line('feedback_select_title', '-- Tôi muốn góp ý về vấn đề -- '); ?></option>
                                            <?php
                                                foreach($feedback_arr as $item){
                                            ?>
                                            <option value="<?=$item['ID']?>"><?=$item['Name']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <textarea id="content" name="content" placeholder="<?= $this->lang->line('content', 'Nội dung'); ?> (*)" maxlength="100" rows="3" class="form-control" required="required"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6"><input id="fullname" name="fullname" placeholder="<?= $this->lang->line('fullname', 'Họ tên'); ?>" maxlength="100" type="text" aria-required="true" class="form-control"></div>
                                            <div class="col-lg-6">
                                                <input id="email" name="group" placeholder="<?= $this->lang->line('group', 'Thuộc bộ phận'); ?>" maxlength="100" type="text" aria-required="true" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6"><input id="phone" name="phone" placeholder="<?= $this->lang->line('phone', 'Điện thoại'); ?>" maxlength="13" type="text" aria-required="true" class="form-control"></div>
                                            <div class="col-lg-6"><input id="email" name="email" placeholder="Email" maxlength="100" type="email" aria-required="true" class="form-control"></div>
                                        </div>
                                    </div>
                                    <div class="text-xs-center">
                                        <button type="button" class="btn btn-success send_feedback">
                                            <?= $this->lang->line('send_feedback', 'Gửi thông tin'); ?> <i class="auc auc-right"></i>
                                        </button>
                                    </div>
                                 
                                </form>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-xs-center result" style="display: none;"><span class="text-danger"><?=$this->lang->line('please_fill_in_required_fields','Vui lòng nhập đủ thông tin');?></span></div>
            </div>
        </div>
    </div>
</div>