<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumb_loyalty_card['data'] = array(
                        'page_template' => 'product_categories',
                        'menu_text_active' => $this->lang->line('loyalty_card'),
                    );
                    $this->load->view('templates/breadcrumb', $breadcrumb_loyalty_card);
                    ?>
                    <div class="document-box">
                        <div class="row">
                            <?php
                            $MENU_LEFT['data'] = array(
                                'class' => 'col-md-4 col-xl-3',
                                'menuactive' => 'the-thanh-vien'
                            );
                            $this->load->view('pages/includes/menu_left', $MENU_LEFT);
                            ?>
                            <div class="col-lg-8 col-xl-9">
                                <div class="detail-content">
                                    <h2 class="box-title"> <i class="auc auc-icon"> </i>Thẻ thành viên			</h2>
                                    <div class="post-content">										
                                        <div class="row">
                                            <div class="col-xs-12 col-md-4">
                                                <div class="card card-info card-inverse">
                                                    <div class="card-block">
                                                        <h4><b>1</b><span> Ưu đãi Đặc Quyền</span></h4>
                                                        <p><strong>20 </strong><span>Sản phẩm với giá Cực kỳ Đặc Biệt chỉ dành riêng cho Khách Hàng Thành Viên.</span><small>(Giảm thêm 10% trên giá đã Khuyến Mãi).</small></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <div class="card card-warning card-inverse">
                                                    <div class="card-block">
                                                        <h4><b>2</b><span> Ưu đãi Gắn Kết</span></h4>
                                                        <p> <span>Tích lũy ngay <strong>1% </strong>giá trị mua sắm trên hóa đơn để sử dụng như tiền mặt cho những lần mua sắm sau.</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <div class="card card-success card-inverse">
                                                    <div class="card-block">
                                                        <h4><b>3</b><span> Đăng ký và làm lại thẻ (nếu mất) MIỄN PHÍ</span></h4>
                                                        <p><span>Vui lòng mang theo CMND gốc hoặc Hộ chiếu gốc (đối với người nước ngoài) để đăng ký và nhận thẻ sau 2 phút.</span><small>Mọi chi tiết xin liên hệ quầy DVKH của siêu thị.</small></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="card signup-rule">
                                                    <div class="card-block"><img src="<?= THEMES_URL; ?>/images/asset/membership-2.png" alt="Auchan" class="img-fluid img-rounded w-100">
                                                        <h4 class="card-title">CÁCH THỨC ĐĂNG KÝ?</h4>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-3">
                                                                <div class="card">
                                                                    <div class="card-block">
                                                                        <h6 class="badge badge-info badge-pill"><i class="auc auc-download"></i><span>Bước 1</span></h6>
                                                                        <p>Đến cửa hàng Auchan và mang theo CMND gốc hoặc.<a href="public/file/phieu-dang-ky-the-thanh-vien-2017.pdf" class="btn btn-primary"><i class="auc auc-download-1"> </i>Tải bản đăng ký</a></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3">
                                                                <div class="card">
                                                                    <div class="card-block">
                                                                        <h6 class="badge badge-warning badge-pill"><i class="auc auc-document"></i><span>Bước 2</span></h6>
                                                                        <p>Liên hệ quầy DVKH để nhận bản đăng ký.<small>(Nếu bạn chưa tải xuống).</small></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3">
                                                                <div class="card">
                                                                    <div class="card-block">
                                                                        <h6 class="badge badge-success badge-pill"><i class="auc auc-edit-task"></i><span>Bước 3</span></h6>
                                                                        <p>Điền đầy đủ thông tin vào bản đăng ký.<small>(Hoặc gửi bản đăng ký đã điền đầy đủ thông tin cho quầy DVKH).</small></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-3">
                                                                <div class="card">
                                                                    <div class="card-block">
                                                                        <h6 class="badge badge-danger badge-pill"><i class="auc auc-card"></i><span>Bước 4</span></h6>
                                                                        <p>Nhận ngay và sử dụng thẻ thành viên Auchan.													</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-9 item-work push-md-3">
                                                <h4 class="card-title">CÁCH THỨC HOẠT ĐỘNG CỦA THẺ THÀNH VIÊN AUCHAN?</h4>
                                                <ul class="list-unstyled">
                                                    <li><i class="auc auc-star"> </i><span>Tiền tích lũy trong thẻ thành viên có thể được khấu trừ vào hóa đơn mua hàng tiếp theo và không thể quy đổi thành tiền mặt.</span></li>
                                                    <li><i class="auc auc-star"> </i><span>Thời gian sử dụng tiền tích lũy trong thẻ thành viên được giới hạn đến hết tháng Hai của năm kế tiếp</span></li>
                                                    <li><i class="auc auc-star"> </i><span>Sau ngày 1 tháng 3 mỗi năm, tiền tích lũy trong thẻ thành viên của bạn trong năm trước đó sẽ không còn hiệu lực.</span></li>
                                                    <li><i class="auc auc-star"> </i><span>Bạn có thể sử dụng tiền tích lũy trong thẻ thành viên ở bất cứ chi nhánh nào của Auchan và bất cứ lúc nào bạn muốn.</span></li>
                                                    <li><i class="auc auc-star"> </i><span>Tiền tích lũy và thông tin của chủ thẻ sẽ được cập nhật sau 24h.</span></li>
                                                    <li><i class="auc auc-star"> </i><span>Thẻ thành viên cần được đưa cho thu ngân khi thanh toán để được tích lũy tiền vào tài khoản.</span></li>
                                                    <li><i class="auc auc-star"> </i><span>Thẻ thành viên Auchan có thể sử dụng và tích lũy tiền tại tất cả các chi nhánh siêu thị của Auchan.</span></li>
                                                    <li><i class="auc auc-star"> </i><span>Nếu thẻ thành viên Auchan của bạn bị mất, bạn có thể liên hệ quầy DVKH của tất cả các chi nhánh siêu thị Auchan để làm lại thẻ mới hoàn toàn miễn phí.</span></li>
                                                </ul>
                                            </div>
                                            <div class="col-xs-12 col-md-3 pull-md-9"><img src="<?= THEMES_URL; ?>/images/asset/membership.jpg" alt="Thẻ thành viên Auchan" class="img-rounded img-fluid w-100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>