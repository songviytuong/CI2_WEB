<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumb_detail['data'] = array(
                        'page_template' => 'product_categories',
                        'menu_text_active' => $this->lang->line('find_store', 'Tìm siêu thị') . ' ' . $this->lang->line('near', 'gần') . ' <span id="myLocal" data-add=""></span>'
                    );
                    $this->load->view('templates/breadcrumb', $breadcrumb_detail);
                    ?>
                </div>
                <?php
                if ($off_menu)
                    $this->load->view('inc_mainmenu');
                ?>
                <div class="col-md-12 <?= ($off_menu) ? 'col-lg-10' : 'col-lg-12' ?>">
                    <div class="location">
                        <div class="row no-gutters">
                            <div class="col-sm-4 box-location">
                                <h1 class="content-box-title"><?php echo $this->lang->line('store_list'); ?><span class="cls_shownearme"><a href="javascript:void(0)" onclick="getListStoreNearYou();" class="text-success store_near shownearme"><?=$this->lang->line('list_store_near_you','Xem danh sách siêu thị gần bạn');?></a></span></h1>
                                <div id="nearme"></div>
                                <select name="group_city" id="group_city" class="form-control custom-select">
                                    <option value="-1" disabled="disabled" selected="selected"><?= $this->lang->line('select_group_city'); ?></option>
                                    <?php
                                    $this->load->model('warehouse_model', 'warehouse');
                                    $GroupCity = $this->warehouse->defineGroupCity();
                                    foreach ($GroupCity as $row) {
                                        ?>
                                        <option value="<?= $row['city_id'] ?>"><?= $row['city_name'] ?></option>
                                    <?php } ?>
                                </select>
                                <ul class="list-unstyled" id="loading_storelist2"></ul>
                            </div>
                            <div class="col-sm-8">
                                <div id="map"></div>
                                <div id="getscript"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>