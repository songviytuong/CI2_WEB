<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumb_detail['data'] = array(
                        'page_template' => 'cam-nang-auchan',
                        'menu_text_active' => 'Cẩm nang Auchan'
                    );
                    $this->load->view('templates/breadcrumb', $breadcrumb_detail);
                    ?>
                    <div class="post-box">
                        <div class="row">
                            <div class="main-content col-md-8">
                                <div class="content-box">
                                    <div class="row">
                                        <?php
                                        foreach ($post_special as $key => $item) {
                                            ?>
                                            <div class="col-xs-12 col-md-12 col-lg-7">
                                                <div class="post-item"><a href="<?= $item['Url']; ?>" class="post-img" title="<?= $item['Title']; ?>"><img alt="<?= $item['Title']; ?>" src="<?= $item['Thumb']; ?>" class="img-rounded img-fluid w-100" /></a>
                                                    <p class="meta"><a href="<?= $item['Url']; ?>" class="text"><?= $item['Title']; ?></a></p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="col-xs-12 col-md-12 col-lg-5">
                                            <div class="row">
                                                <?php
                                                foreach ($post_4special as $key => $item) {
                                                    ?>
                                                    <div class="col-xs-6 col-md-6">
                                                        <div class="post-item"><a href="<?= $item['Url']; ?>" class="post-img" title="<?= $item['Title']; ?>"><img alt="<?= $item['Title']; ?>" src="<?= $item['Thumb']; ?>" class="img-rounded img-fluid" /></a>
                                                            <p class="meta"><a href="<?= $item['Url']; ?>" class="text" title="<?= $item['Title']; ?>"><?= $item['Title']; ?></a></p>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="content-box">
                                    <div class="row">
                                        <?php
                                        $this->load->model('global_model');

                                        foreach ($post as $key => $item) {
                                            ?>
                                            <div class="col-xs-12">
                                                <div class="post-item"><a href="<?= $item['Url']; ?>" class="post-img" title="<?= $item['Title']; ?>"><img alt="<?= $item['Title']; ?>" src="<?= $item['Thumb']; ?>" class="img-fluid img-rounded" /></a>
                                                    <div class="meta">
                                                        <a href="<?= $item['Url']; ?>" class="text" title="<?= $item['Title']; ?>"><?= $item['Title']; ?></a>
                                                        <div class="postdate"><i class="auc auc-fw auc-calendar"></i><?= $item['PostDate']; ?> <i class="auc auc-fw auc-doc-text"></i>Cẩm nang</div>
                                                        <div class="description"><?= (!empty($item['Description'])) ? $this->global_model->truncate($item['Description'], 300) : $this->global_model->truncate($item['Content'], 100); ?></div>

                                                    </div>

                                                </div>
                                            </div>
                                        <?php } ?>

                                    </div>
                                    <!--nav class="text-center">
                                        <ul class="pagination">
                                            <li class="page-item disabled"><a href="#" tabindex="-1" aria-label="Previous" class="page-link"><span aria-hidden="true"><i class="auc auc-angle-left"></i></span><span class="sr-only"><i class="auc auc-angle-left"></i></span></a></li>
                                            <li class="page-item active"><a href="#" class="page-link">1 <span class="sr-only">(current)</span></a></li>
                                            <li class="page-item"><a href="#" class="page-link">2</a></li>
                                            <li class="page-item"><a href="#" class="page-link">3</a></li>
                                            <li class="page-item"><a href="#" class="page-link">4</a></li>
                                            <li class="page-item"><a href="#" class="page-link">5</a></li>
                                            <li class="page-item"><a href="#" aria-label="Next" class="page-link"><span aria-hidden="true"><i class="auc auc-angle-right"></i></span><span class="sr-only"><i class="auc auc-angle-right"></i></span></a></li>
                                        </ul>
                                    </nav-->
                                </div>
                            </div>
                            <div class="sidebar col-md-4">
                                <div class="sidebar-box">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <iframe src="https://www.youtube.com/embed/ETCf0rgvCUA" allowfullscreen="" class="embed-responsive-item" style="width:100%; height:250px;"></iframe>
                                        </div>
                                    </div>
                                </div>
                                <div class="sidebar-box">
                                    <h3 class="box-title"><span>Bài viết nổi bật</span></h3>
                                    <div class="row">
                                        <?php
                                        foreach ($post_featured as $key => $item) {
                                            ?>
                                            <div class="post-item col-xs-6 col-md-12"><a href="<?= $item['Url']; ?>" class="post-img" title="<?= $item['Title']; ?>"><img src="<?= $item['Thumb']; ?>" style="width: 113px" alt="Hoa cài mái tóc" class="img-fluid img-rounded"></a>
                                                <p class="meta"><a href="<?= $item['Url']; ?>" class="text" title="<?= $item['Title']; ?>"><?= $item['Title']; ?></a></p>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>