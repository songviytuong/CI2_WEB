<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumb_loyalty_card['data'] = array(
                        'page_template' => 'product_categories',
                        'menu_text_active' => $this->lang->line('faqs'),
                    );
                    $this->load->view('templates/breadcrumb', $breadcrumb_loyalty_card);
                    ?>
                    <div class="document-box">
                        <div class="row">
                            <?php
                            $MENU_LEFT['data'] = array(
                                'class' => 'col-md-4 col-xl-3',
                                'menuactive' => 'hoi-dap'
                            );
                            $this->load->view('pages/includes/menu_left', $MENU_LEFT);
                            ?>
                            <div class="col-lg-8 col-xl-9">
                                <div class="detail-content">
                                    <h2 class="box-title"> <i class="auc auc-icon"> </i><?=$this->lang->line('faqs');?></h2>
                                    <div class="post-content">
                                        <div class="alert alert-info">
                                            <p>Dưới đây là các câu hỏi thường gặp của quý khách hàng trước và trong quá trình sử dụng sản phẩm & dịch vụ của Auchan. Chúng tôi xin đưa ra các câu trả lời gợi ý giúp bạn giải đáp những thắc mắc cơ bản và thường gặp nhất.</p>
                                        </div>
                                        <div id="accordion" role="tablist" aria-multiselectable="true" class="accordion">
                                            <div class="card">
                                                <div id="headingOne" role="tab" class="card-header">
                                                    <h5 class="mb-0"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="auc auc-question"> </i><span>Khi đặt hàng làm thế nào để kiểm tra phí giao hàng cho đơn hàng đó?</span><i class="auc auc-plus"> </i></a></h5>
                                                </div>
                                                <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne" class="collapse">
                                                    <div class="card-block">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div id="headingTwo" role="tab" class="card-header">
                                                    <h5 class="mb-0"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed"><i class="auc auc-question"> </i><span>Tôi có thể đặt hàng qua điện thoại không?</span><i class="auc auc-plus"> </i></a></h5>
                                                </div>
                                                <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" class="collapse show in">
                                                    <div class="card-block">
                                                        <p>Chúng tôi có thể hỗ trợ Quý khách đặt hàng qua điện thoại. Tuy nhiên chúng tôi luôn khuyến khích quý khách tìm hiểu thêm về cách mua hàng trực tiếp tại hệ thống siêu thị auchan.vn. </p>
                                                        <p>Nếu Quý khách chưa thể đặt hàng trực tuyến vì bất cứ lý do gì xin đừng ngần ngại liên hệ ngay với tổng đài 0914 086 993 của Auchan để được đặt hàng và hỗ trợ tư vấn sản phẩm. Chúng tôi rất hân hạnh được phục vụ quý khách!!!</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div id="headingThree" role="tab" class="card-header">
                                                    <h5 class="mb-0"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed"><i class="auc auc-question"> </i><span>Khi đặt hàng làm thế nào để kiểm tra phí hàng cồng kềnh cho đơn hàng đó?</span><i class="auc auc-plus"> </i></a></h5>
                                                </div>
                                                <div id="collapseThree" role="tabpanel" aria-labelledby="headingThree" class="collapse">
                                                    <div class="card-block">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div id="headingFour" role="tab" class="card-header">
                                                    <h5 class="mb-0"><a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour"><i class="auc auc-question"> </i><span>Làm thế nào để kiểm tra thời gian giao hàng của đơn hàng?</span><i class="auc auc-plus"> </i></a></h5>
                                                </div>
                                                <div id="collapseFour" role="tabpanel" aria-labelledby="headingFour" class="collapse show">
                                                    <div class="card-block">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div id="headingFive" role="tab" class="card-header">
                                                    <h5 class="mb-0"><a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive" class="collapsed"><i class="auc auc-question"> </i><span>Các loại thẻ tín dụng nào được áp dụng cho việc thanh toán?</span><i class="auc auc-plus"> </i></a></h5>
                                                </div>
                                                <div id="collapseFive" role="tabpanel" aria-labelledby="headingFive" class="collapse">
                                                    <div class="card-block">
                                                        <p>Chúng tôi có thể hỗ trợ Quý khách đặt hàng qua điện thoại. Tuy nhiên chúng tôi luôn khuyến khích quý khách tìm hiểu thêm về cách mua hàng trực tiếp tại hệ thống siêu thị auchan.vn. </p>
                                                        <p>Nếu Quý khách chưa thể đặt hàng trực tuyến vì bất cứ lý do gì xin đừng ngần ngại liên hệ ngay với tổng đài 0914 086 993 của Auchan để được đặt hàng và hỗ trợ tư vấn sản phẩm. Chúng tôi rất hân hạnh được phục vụ quý khách!!!</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div id="headingSix" role="tab" class="card-header">
                                                    <h5 class="mb-0"><a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix" class="collapsed"><i class="auc auc-question"> </i><span>Tôi có thể sử dụng thẻ tín dụng của người khác để đặt hàng không?</span><i class="auc auc-plus"> </i></a></h5>
                                                </div>
                                                <div id="collapseSix" role="tabpanel" aria-labelledby="headingSix" class="collapse">
                                                    <div class="card-block">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div id="headingSeven" role="tab" class="card-header">
                                                    <h5 class="mb-0"><a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven"><i class="auc auc-question"> </i><span>Giao dịch hoàn tiền sẽ được thực hiện trong bao lâu?</span><i class="auc auc-plus"> </i></a></h5>
                                                </div>
                                                <div id="collapseSeven" role="tabpanel" aria-labelledby="headingSeven" class="collapse show">
                                                    <div class="card-block">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div id="headingEight" role="tab" class="card-header">
                                                    <h5 class="mb-0"><a data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight" class="collapsed"><i class="auc auc-question"> </i><span>Làm thế nào khi tôi muốn đổi/trả hàng?</span><i class="auc auc-plus"> </i></a></h5>
                                                </div>
                                                <div id="collapseEight" role="tabpanel" aria-labelledby="headingEight" class="collapse">
                                                    <div class="card-block">
                                                        <p>Chúng tôi có thể hỗ trợ Quý khách đặt hàng qua điện thoại. Tuy nhiên chúng tôi luôn khuyến khích quý khách tìm hiểu thêm về cách mua hàng trực tiếp tại hệ thống siêu thị auchan.vn. </p>
                                                        <p>Nếu Quý khách chưa thể đặt hàng trực tuyến vì bất cứ lý do gì xin đừng ngần ngại liên hệ ngay với tổng đài 0914 086 993 của Auchan để được đặt hàng và hỗ trợ tư vấn sản phẩm. Chúng tôi rất hân hạnh được phục vụ quý khách!!!</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div id="headingNine" role="tab" class="card-header">
                                                    <h5 class="mb-0"><a data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine" class="collapsed"><i class="auc auc-question"> </i><span>Sau bao lâu tôi có thể nhận được kết quả trả hàng của tôi?</span><i class="auc auc-plus"> </i></a></h5>
                                                </div>
                                                <div id="collapseNine" role="tabpanel" aria-labelledby="headingNine" class="collapse">
                                                    <div class="card-block">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div id="headingTen" role="tab" class="card-header">
                                                    <h5 class="mb-0"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen" class="collapsed"><i class="auc auc-question"> </i><span>Làm thế nào để kiểm tra thông tin bảo hành của sản phẩm?</span><i class="auc auc-plus"> </i></a></h5>
                                                </div>
                                                <div id="collapseTen" role="tabpanel" aria-labelledby="headingTen" class="collapse">
                                                    <div class="card-block">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>