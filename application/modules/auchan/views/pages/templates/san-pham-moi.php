<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <?php
                if (isset($off_menu)) {
                    $data_menu['class'] = 'col-md-2 main-cate-nav hidden-md-down';
                    $this->load->view('inc_mainmenu');
                }
                ?>
                <div class="col-md-12 <?= isset($off_menu) ? 'col-lg-10' : 'col-lg-12' ?>">
                    <?php
                    $breadcrumb['data'] = array(
                        'page_template' => 'product_categories',
                        'menu_text_active' => 'Sản phẩm mới'
                    );
                    $this->load->view('templates/breadcrumb', $breadcrumb);
                    ?>
                    <!--
                    <div class="list-categories-child">
                        <ul role="tablist" class="nav nav-tabs">
                            <li class="nav-item"><a data-toggle="tab" href="#store" role="tab" class="nav-link active">Store</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#price" role="tab" class="nav-link">Price</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#gender" role="tab" class="nav-link">Gender</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="store" role="tabpanel" class="tab-pane active">
                                <ul class="list-unstyled">
                                    <li>
                                        <input type="checkbox" checked="checked" value="spain"><a title="Spain">AUCHAN PHẠM VĂN CHÍ</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="fruit-monkeys"><a title="Fruit Monkeys">AUCHAN LŨY BÁN BÍCH</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="generic"><a title="Generic">AUCHAN HOÀNG VĂN THỤ</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="airflown"><a title="AirFlown">AUCHAN ÂU CƠ (MAI LAN)</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="benelux-fruit"><a title="Benelux Fruit">AUCHAN USEFUL</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" checked="checked" value="driscoll"><a title="Driscoll">AUCHAN ERA</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="kirei"><a title="Kirei">AUCHAN MÃ LÒ</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="benelux"><a title="Benelux">AUCHAN ERA</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="coconut"><a title="Coconut">AUCHAN PHẠM VĂN ĐỒNG</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" checked="checked" value="dole"><a title="Dole">AUCHAN TRẦN BÌNH TRỌNG</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="givvo"><a title="GIVVO">AUCHAN TRƯỜNG SƠN</a><span>8</span>
                                    </li>
                                </ul>
                            </div>
                            <div id="price" role="tabpanel" class="tab-pane">
                                <ul class="list-unstyled">
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain"></a>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">100.000đ </a>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">300.000đ </a>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">600.000đ </a>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">1 triệu </a>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">3 triệu </a>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">6 triệu </a>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">8 triệu </a>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">12 triệu </a>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="spain"><a title="Spain">>= 15 triệu</a><span>82</span>
                                    </li>
                                </ul>
                            </div>
                            <div id="gender" role="tabpanel" class="tab-pane">
                                <ul class="list-unstyled">
                                    <li>
                                        <input type="checkbox" value="United States"><a title="United States">Male</a><span>8</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="New Zealand"><a title="New Zealand">Female</a><span>5</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" value="South Africa"><a title="South Africa">All</a><span>5</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    -->
                    <div class="product-box">
                        <div class="filter-box">
                            <ul class="nav nav-pills pull-left">
                                <!--<li class="nav-item"><a href="#" class="nav-link active btn-sm"><?= $this->lang->line('all') ?></a></li>-->
<!--                                <li class="nav-item"><a href="#" class="nav-link btn-sm"><?= $this->lang->line('newest') ?></a></li>
                                <li class="nav-item"><a href="#" class="nav-link btn-sm"><?= $this->lang->line('onsale') ?></a></li>
                                <li class="nav-item"><a href="" class="nav-link btn-sm"><?= $this->lang->line('instock') ?></a></li>-->
                            </ul>
                            <form class="form-inline pull-right"><span>Sắp xếp: </span>
                                <select name="" id="" class="form-control form-control-sm mb-2 mr-sm-2 mb-sm-0">
                                    <option>Mới cập nhật</option>
                                    <option>Giá thấp đến cao</option>
                                    <option>Giá cao đến thấp</option>
                                    <option>Sản phẩm A-Z</option>
                                    <option>Sản phẩm Z-A</option>
                                    <option>Thương hiệu A-Z</option>
                                    <option>Thương hiệu Z-A</option>
                                </select>
                            </form>
                        </div>
                        <div class="row">
                            <?php
                            $data_promotion['chimmoi'] = 1;
                            $data_promotion['limit'] = 100;
                            $data_promotion['class'] = 'col-xs-6 col-sm-4 col-md-2 product-item';
                            $this->view('templates/product_items.php', $data_promotion);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>