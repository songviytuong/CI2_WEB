<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <?php
                if (isset($off_menu))
                    $data_menu['class'] = 'col-md-2 hidden-md-down';
                $this->load->view('inc_mainmenu');
                ?>
                <div class="col-md-12 <?php echo isset($off_menu) ? 'col-lg-10' : 'col-lg-12' ?>">
                    <?php
                    $breadcrumb['data'] = array(
                        'page_template' => 'product_categories',
                        'menu_text_active' => $this->lang->line('search_result','Kết quả tìm kiếm')
                    );
                    $this->load->view('templates/breadcrumb', $breadcrumb);
                    ?>
                    <div class="card search-info">
                        <div class="card-block">
                            <ul role="tablist" class="nav tabs nav-inline">
                                <li class="nav-item"><a data-toggle="tab" href="#tab2" role="tab" class="nav-link btn active">Sản phẩm</a></li>
                                <li class="nav-item"><a data-toggle="tab" href="#tab3" role="tab" class="nav-link btn">Cẩm nang</a></li>
                            </ul>
                            <p><i class="auc auc-search"></i>Tìm kiếm với từ khóa: <strong>"<?= $keyword; ?>"</strong></p>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div id="tab2" role="tabpanel" class="tab-pane active">
                            <?php if ($total_product > 0) : ?>
                                <div class="product-box">
                                    <div class="box-item">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h3 class="content-box-title">Kết quả tìm kiếm sản phẩm: <span>(<?= $total_product; ?> kết quả)</span><!--a class="read-all">Xem tất cả <i class="auc auc-angle-right"></i></a--></h3>
                                            </div>
                                            <?php
                                            $data_search['keyword'] = $keyword;
                                            $data_search['limit'] = 100;
                                            $data_search['class'] = 'col-xs-6 col-sm-4 col-md-2 product-item';
                                            $this->view('templates/product_items', $data_search);
                                            ?>
                                        </div>
                                    </div>
                                    <!--
                                    <nav class="text-center">
                                        <ul class="pagination">
                                            <li class="page-item disabled"><a href="#" tabindex="-1" aria-label="Previous" class="page-link"><span aria-hidden="true"><i class="auc auc-angle-left"></i></span><span class="sr-only"><i class="auc auc-angle-left"></i></span></a></li>
                                            <li class="page-item active"><a href="#" class="page-link">1 <span class="sr-only">(current)</span></a></li>
                                            <li class="page-item"><a href="#" class="page-link">2</a></li>
                                            <li class="page-item"><a href="#" class="page-link">3</a></li>
                                            <li class="page-item"><a href="#" class="page-link">4</a></li>
                                            <li class="page-item"><a href="#" class="page-link">5</a></li>
                                            <li class="page-item"><a href="#" aria-label="Next" class="page-link"><span aria-hidden="true"><i class="auc auc-angle-right"></i></span><span class="sr-only"><i class="auc auc-angle-right"></i></span></a></li>
                                        </ul>
                                    </nav>
                                    -->
                                </div>
                            <?php endif; ?>
                        </div>
                        <div id="tab3" role="tabpanel" class="tab-pane">
                            <div class="product-box">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h3 class="content-box-title">Kết quả tìm kiếm cẩm nang: <span>(<?= $total_post; ?> kết quả)</span></h3>
                                    </div>
                                    <?php
                                    $data_search_post['keyword'] = $keyword;
                                    $data_search_post['class'] = 'col-xs-6 col-md-4 col-lg-3';
                                    $data_search_post['maxheight'] = '182';
                                    $this->view('templates/post_items', $data_search_post);
                                    ?>
                                </div>
<!--                                <nav class="text-center">
                                    <ul class="pagination">
                                        <li class="page-item disabled"><a href="#" tabindex="-1" aria-label="Previous" class="page-link"><span aria-hidden="true"><i class="auc auc-angle-left"></i></span><span class="sr-only"><i class="auc auc-angle-left"></i></span></a></li>
                                        <li class="page-item active"><a href="#" class="page-link">1 <span class="sr-only">(current)</span></a></li>
                                        <li class="page-item"><a href="#" class="page-link">2</a></li>
                                        <li class="page-item"><a href="#" class="page-link">3</a></li>
                                        <li class="page-item"><a href="#" class="page-link">4</a></li>
                                        <li class="page-item"><a href="#" class="page-link">5</a></li>
                                        <li class="page-item"><a href="#" aria-label="Next" class="page-link"><span aria-hidden="true"><i class="auc auc-angle-right"></i></span><span class="sr-only"><i class="auc auc-angle-right"></i></span></a></li>
                                    </ul>
                                </nav>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>