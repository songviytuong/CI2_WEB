<div class="content">
    <div class="content-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumb_detail['data'] = array(
                        'page_template' => 'e-brochure',
                        'menu_text_active' => 'Tờ rơi khuyến mãi'
                    );
                    $this->load->view('templates/breadcrumb', $breadcrumb_detail);
                    ?>
                    <div class="list-categories-child">
                        <link rel="stylesheet" href="public/auchan/assets/owlcarousel/assets/owl.carousel.min.css">
                        <link rel="stylesheet" href="public/auchan/assets/owlcarousel/assets/owl.theme.default.min.css">
                        <script src="public/auchan/assets/vendors/jquery.min.js"></script>
                        <script src="public/auchan/assets/owlcarousel/owl.carousel.js"></script>

                        <!-- Nav tabs-->
                        <ul role="tablist" class="nav nav-tabs">
                            <?php
                            foreach ($cities as $key => $city) {
                                $num = str_pad($key + 1, 2, 0, STR_PAD_LEFT);
                                ?>
                                <li class="nav-item"><a data-toggle="tab" href="#tab-<?= $num; ?>" role="tab" class="nav-link <?= ($num == 01) ? 'active' : ''; ?>" aria-expanded="true"><?= $city['city_name'] ?></a></li>
                            <?php } ?>
                        </ul>
                        <!-- Tab panes-->
                        <div class="tab-content e-brochure-tabs">
                            <?php
                            foreach ($cities as $key => $city) {
                                $num = str_pad($key + 1, 2, 0, STR_PAD_LEFT);
                                ?>
                                <div id="tab-<?= $num; ?>" role="tabpanel" class="tab-pane <?= ($num == 01) ? 'active' : ''; ?>" aria-expanded="true">
                                    <div id="tabCarousel-<?= $num; ?>" class="carousel">
                                        <div class="owl-carousel owl-theme">
                                            <?php
                                            $this->load->model('ebrochure_model', 'ebrochure');
                                            $d = $this->ebrochure->defineEBrochure(array('cityid' => $city['city_id']));
                                            foreach ($d as $key => $item) {
                                                ?>
                                            <div class="item openMaps" data-bind="<?= $item['ID'] ?>"><img class="owl-lazy" data-src="<?= $item['Image_S'] ?>" class="img-fluid img-rounded" style="cursor: pointer" alt=""/>
                                                    <div class="header_slider"><?= $item['Name'] ?>
                                                        <?php if (!empty($item['Description'])): ?>
                                                            <br/><?= $item['Description']; ?>
                                                        <?php else: ?>
                                                            <br/>Từ <?= $item['Start'] ?> đến <?= $item['End'] ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <script>
                                jQuery(document).ready(function ($) {
                                    $('.owl-carousel').owlCarousel({
                                        loop: true,
                                        lazyLoad: true,
                                        margin: 10,
                                        responsiveClass: true,
                                        responsive: {
                                            0: {
                                                items: 1,
                                                nav: true
                                            },
                                            600: {
                                                items: 3,
                                                nav: false
                                            },
                                            1000: {
                                                items: 5,
                                                nav: true,
                                                loop: false
                                            }
                                        }
                                    });
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="showmaps"></div>
        </div>
    </div>
</div>