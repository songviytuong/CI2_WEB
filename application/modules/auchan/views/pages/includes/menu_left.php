<?php
$params = array(
    'data' => !empty($_ci_vars['data']) ? $_ci_vars['data'] : array(),
);
$class = $params['data']['class'];
$menuactive = $params['data']['menuactive'];
?>
<div class="<?php echo $class; ?>">
    <div class="side-nav hidden-md-down">
        <h5 class="box-title"> <i class="auc auc-fw auc-question"> </i><?=$this->lang->line('auchan_informations');?></h5>
        <ul class="list-unstyled">
            <li <?=($menuactive == 'gioi-thieu-auchan') ? 'class="active"':''?>><a href="<?php echo base_url(); ?>gioi-thieu-auchan/"><i class="auc auc-fw auc-record-outline"></i><span><?=$this->lang->line('aboutus');?></span></a></li>
            <?php if (DRIVEDRIECT && BETAOFF) : ?>
            <li style="padding-left:30px;"><a href="<?php echo base_url(); ?>auchan-drive/"><i class="auc auc-fw auc-angle-right"></i><span>Auchan Drive</span></a></li>
            <li style="padding-left:30px;"><a href="<?php echo base_url(); ?>auchan-direct/"><i class="auc auc-fw auc-angle-right"></i><span>Auchan Direct</span></a></li>
            <?php endif; ?>
            <li <?=($menuactive == 'tuyen-dung') ? 'class="active"':''?>><a href="<?php echo base_url(); ?>tuyen-dung/"><i class="auc auc-fw auc-record-outline"></i><span><?=$this->lang->line('recruitment','Tuyển dụng');?></span></a></li>
            <?php if (BETAOFF) : ?>
            <li <?=($menuactive == 'dieu-khoan-va-chinh-sach') ? 'class="active"':''?>><a href="<?php echo base_url(); ?>dieu-khoan-va-chinh-sach/"><i class="auc auc-fw auc-record-outline"></i><span><?=$this->lang->line('policy');?></span></a></li>
            <li style="padding-left:30px;"><a href="<?php echo base_url(); ?>hoi-dap/"><i class="auc auc-fw auc-angle-right"></i><span>Điều khoản sử dụng</span></a></li>
            <li style="padding-left:30px;"><a href="<?php echo base_url(); ?>hoi-dap/"><i class="auc auc-fw auc-angle-right"></i><span>Chính sách đổi trả</span></a></li>
            <li style="padding-left:30px;"><a href="<?php echo base_url(); ?>hoi-dap/"><i class="auc auc-fw auc-angle-right"></i><span>Chính sách bảo mật</span></a></li>
            <li style="padding-left:30px;"><a href="<?php echo base_url(); ?>hoi-dap/"><i class="auc auc-fw auc-angle-right"></i><span>Ưu đãi doanh nghiệp (*)</span></a></li>
            <li style="padding-left:30px;"><a href="<?php echo base_url(); ?>hoi-dap/"><i class="auc auc-fw auc-angle-right"></i><span>Bán hàng cùng Auchan (*)</span></a></li>
        
            <li><a href="<?php echo base_url(); ?>hoi-dap/"><i class="auc auc-fw auc-record-outline"></i><span>Hướng dẫn đặt hàng</span></a></li>
            <li><a href="<?php echo base_url(); ?>hoi-dap/"><i class="auc auc-fw auc-record-outline"></i><span>Hướng dẫn mua hàng</span></a></li>
            <li><a href="<?php echo base_url(); ?>hoi-dap/"><i class="auc auc-fw auc-record-outline"></i><span>Hình thức thanh toán</span></a></li>
            <li><a href="<?php echo base_url(); ?>hoi-dap/"><i class="auc auc-fw auc-record-outline"></i><span>Phương thức vận chuyển</span></a></li>
            <?php endif; ?>
        </ul>
        <h5 class="box-title"> <i class="auc auc-fw auc-lightbulb"> </i>Khám phá</h5>
        <ul class="list-unstyled">
            <li <?=($menuactive == 'the-thanh-vien') ? 'class="active"':''?>><a href="<?php echo base_url(); ?>the-thanh-vien/"><i class="auc auc-fw auc-record-outline"></i><span><?=$this->lang->line('loyalty_card','Thẻ thành viên');?></span></a></li>
            <?php if (BETAOFF) : ?>
            <li <?=($menuactive == 'hoi-dap') ? 'class="active"':''?>><a href="<?php echo base_url(); ?>hoi-dap/"><i class="auc auc-fw auc-record-outline"></i><span><?=$this->lang->line('faqs','Hỏi đáp');?></span></a></li>
            <?php endif; ?>
        </ul>
        <div class="alert alert-warning support">
            <h5 class="alert-heading"> <i class="auc auc-cog"> </i>HỖ TRỢ KHÁCH HÀNG</h5>
            <ul class="list-unstyled">
                <li><i class="auc auc-fw auc-location"></i><?= Globals::getConfig('Address'); ?></li>
                <li><i class="auc auc-fw auc-mobile"></i>Liên hệ: <?= Globals::getConfig('Hotline'); ?> <?= Globals::getConfig('Ext'); ?></li>
            </ul>
        </div>
    </div>
    <select class="form-control hidden-md-up" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
        <option aria-expanded="false"><?=$this->lang->line('aboutus');?></option>
        <option selected="selected" value="<?php echo base_url(); ?>dieu-khoan-va-chinh-sach/"><?=$this->lang->line('policy');?></option>
        <option aria-expanded="false"><?=$this->lang->line('loyalty_card');?></option>
        <option aria-expanded="false"><?=$this->lang->line('faqs');?></option>
    </select>
</div>