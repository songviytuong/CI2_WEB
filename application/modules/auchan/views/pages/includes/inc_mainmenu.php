<?php
$params = array(
    'class' => !empty($_ci_vars['class']) ? $_ci_vars['class'] : 'col-md-2 hidden-md-down',
);
?>
<div class="<?php echo $params['class']; ?>">
    <div class="main-cate-nav">
        <?php if (BETAOFF): ?>
            <ul class="list-unstyled">
                <li><a href="<?php echo base_url(); ?>khuyen-mai/">Khuyến mãi <span class="badge badge-pill badge-danger"><?php echo $counter_promotion; ?></span></a></li>
                <li><a href="<?php echo base_url(); ?>san-pham-moi/">Mới <span class="badge badge-pill badge-success"><?php echo $counter_newest; ?></span></a></li>
                <?php
                foreach ($main_menu as $key => $row) {
                    ?>
                    <li><a data-id="<?= $row['id'] ?>" class="<?php if ($row['is_parent'] == 1) : ?>parent<?php endif; ?>" href="<?php echo base_url() . $row['alias'] ?>"><?= $row['title'] ?><?php if ($row['is_parent'] == 1) : ?><i class="auc auc-angle-right"></i><?php endif; ?></a>
                        <?php if ($row['is_parent'] == 1) { ?>
                            <ul class="list-unstyled sub-nav">
                                <li><a href="<?php echo base_url(); ?>danh-muc/<?= $row['alias'] ?>"><?= $row['title'] ?></a></li>
                                <?php
                                $this->load->model('categories_model');
                                $childs = $this->categories_model->defineAllCategories($row['id']);
                                foreach ($childs as $child) {
                                    ?>
                                    <li><a href="<?php echo base_url(); ?><?= $child["alias"]; ?>" title="<?= $child["title"]; ?>"><?= $child["title"]; ?></a></li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
        <?php else: ?>
            <ul class="list-unstyled beta navigation">
                <li><a href="<?php echo base_url(); ?>gioi-thieu-auchan/"><i class="icon-auchan-25 auchan"></i> <i class="auc auc-angle-right"></i><?php echo $this->lang->line('aboutus', 'Giới thiệu Auchan'); ?></a></li>
                <li><a href="<?php echo base_url(); ?>khuyen-mai/"><i class="icon-auchan-92 auchan"></i> <i class="auc auc-angle-right"></i><?php echo $this->lang->line('promotion', 'Khuyến mãi'); ?></a></li>
                <li><a href="<?php echo base_url(); ?>e-brochure/"><i class="icon-auchan-50 auchan"></i> <i class="auc auc-angle-right"></i><?php echo $this->lang->line('e-brochure', 'Tờ rơi khuyến mãi'); ?></a></li>
                <li><a href="<?php echo base_url(); ?>the-thanh-vien"><i class="icon-auchan-51 auchan"></i> <i class="auc auc-angle-right"></i><?php echo $this->lang->line('loyalty_card', 'Thẻ thành viên'); ?></a></li>
                <li><a href="<?php echo base_url(); ?>he-thong-sieu-thi-auchan/"><i class="icon-auchan-02 auchan"></i> <i class="auc auc-angle-right"></i><?php echo $this->lang->line('find_store', 'Tìm siêu thị'); ?></a></li>
                <li><a href="<?php echo base_url(); ?>tuyen-dung/"><i class="icon-auchan-08 auchan"></i> <i class="auc auc-angle-right"></i><?php echo $this->lang->line('recruitment', 'Tuyển dụng'); ?></a></li>
            </ul>
        <?php endif; ?>
        <div class="side">
            <?php
            if (isset($group3_banners)) {
                foreach ($group3_banners as $key => $item) {
                    ?>
                    <?php if ($item['link']) : ?><a href="<?= $item['link']; ?>" target="<?= $item['target']; ?>"><?php endif; ?>
                        <img src="<?= UPLOAD_URL; ?>/<?= $item['image']; ?>" alt="<?= $item['title']; ?>" class="img-fluid img-rounded" />
                        <?php if ($item['link']) : ?></a><?php endif; ?>
                        <?php
                    }
                }
                ?>
        </div>
    </div>
</div>

<?php
//echo "<ul>";
//for ($i = 0; $i < 100; $i++) {
//    echo "<li>icon-auchan-$i: <i class='icon-auchan-0$i'></i></li>";
//}
//echo "</ul>";
?>