<div id="mainNavbar" class="collapse navbar-toggleable-md hidden-lg-up col-md-3">
    <?php if (BETAOFF): ?>
        <ul class="list-unstyled">
            <li><a href="<?php echo base_url(); ?>khuyen-mai/">Khuyến mãi <span class="badge badge-pill badge-danger"><?php echo $counter_promotion; ?></span></a></li>
            <li><a href="<?php echo base_url(); ?>san-pham-moi/">Mới <span class="badge badge-pill badge-success"><?php echo $counter_newest; ?></span></a></li>
            <?php
            foreach ($main_menu as $key => $row) {
                ?>
                <li class="dropdown">
                    <a data-id="<?= $row['id'] ?>" href="<?php echo base_url() . $row['alias'] ?>"><?= $row['title'] ?><?php if ($row['is_parent'] == 1) : ?><i class="auc auc-angle-right hidden-sm-down"></i><?php endif; ?></a>
                    <?php if ($row['is_parent'] == 1) { ?>
                        <a href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                            <i class="auc auc-angle-down hidden-md-up"></i>
                        </a>
                        <div class="dropdown-menu">
                            <?php
                            $this->load->model('categories_model');
                            $childs = $this->categories_model->defineAllCategories($row['id']);
                            foreach ($childs as $child) {
                                ?>
                                <a href="<?php echo base_url(); ?><?= $child["alias"]; ?>" class="dropdown-item"><?= $child["title"]; ?></a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    <?php else: ?>
        <ul class="list-unstyled navigation beta">
            <li><a href="<?php echo base_url(); ?>gioi-thieu-auchan/"><i class="icon-auchan-25 auchan"></i> <i class="auc auc-angle-right"></i> <?php echo $this->lang->line('aboutus', 'Giới thiệu Auchan'); ?></a></li>
            <li><a href="<?php echo base_url(); ?>khuyen-mai/"><i class="icon-auchan-92 auchan"></i> <i class="auc auc-angle-right"></i> <?php echo $this->lang->line('promotion', 'Khuyến mãi'); ?></a></li>
            <li><a href="<?php echo base_url(); ?>e-brochure/"><i class="icon-auchan-50 auchan"></i> <i class="auc auc-angle-right"></i> <?php echo $this->lang->line('e-brochure', 'Tờ rơi khuyến mãi'); ?></a></li>
            <li><a href="<?php echo base_url(); ?>the-thanh-vien"><i class="icon-auchan-51 auchan"></i> <i class="auc auc-angle-right"></i> <?php echo $this->lang->line('loyalty_card', 'Thẻ thành viên'); ?></a></li>
            <li><a href="<?php echo base_url(); ?>he-thong-sieu-thi-auchan/"><i class="icon-auchan-02 auchan"></i> <i class="auc auc-angle-right"></i> <?php echo $this->lang->line('find_store', 'Tìm siêu thị'); ?></a></li>
            <li><a href="<?php echo base_url(); ?>tuyen-dung/"><i class="icon-auchan-08 auchan"></i> <i class="auc auc-angle-right"></i> <?php echo $this->lang->line('recruitment', 'Tuyển dụng'); ?></a></li>
        </ul>
    <?php endif; ?>
    <?php
    if (isset($group3_banners)) {
        foreach ($group3_banners as $key => $item) {
            ?>
                <?php if ($item['link']) : ?><a href="<?= $item['link']; ?>" target="<?= $item['target']; ?>"><?php endif; ?>
                <img src="<?= UPLOAD_URL; ?>/<?= $item['image']; ?>" alt="<?= $item['title']; ?>" class="img-fluid img-rounded" />
                <?php if ($item['link']) : ?></a><?php endif; ?>
            <?php }
        }
        ?>
</div>