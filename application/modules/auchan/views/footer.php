<?php
$this->load->model('warehouse_model', 'warehouse');
$counter_store = $this->warehouse->getTotalStore();
?>
<footer>
    <div class="foot-content">
        <div class="container">
            <div class="main-foot row">
                <div class="col-xs-12 col-md-4 logo">
                    <h3><a title="Auchan" href="<?php echo base_url(); ?>"><?= $this->lang->line('auchan', 'Auchan'); ?></a></h3>
                    <p><?= $this->lang->line('aboutus_description', 'Tập đoàn Auchan được thành lập vào năm 1961 tại nước Pháp và hiện nay đã phát triển với quy mô quốc tế và có mặt tại 16 quốc gia trên thế giới với hơn 330.000 nhân viên.'); ?></p>
                </div>
                <div class="col-xs-12 col-md-2">
                    <h5 class="title"><?= $this->lang->line('auchan_website', 'Website'); ?></h5>
                    <ul class="nav">
                        <li class="nav-item"><a href="<?php echo base_url(); ?>" class="nav-link"><?= $this->lang->line('home', 'Trang chủ'); ?></a></li>
                        <li class="nav-item"><a href="<?php echo base_url(); ?>cam-nang-auchan/" class="nav-link"><?= $this->lang->line('handbook', 'Cẩm nang'); ?></a></li>

                        <li class="nav-item"><a href="<?php echo base_url(); ?>khuyen-mai/" class="nav-link"><?= $this->lang->line('promotion', 'Khuyến mãi'); ?></a></li>
                        <li class="nav-item"><a href="<?php echo base_url(); ?>the-thanh-vien/" class="nav-link"><?= $this->lang->line('loyalty_card', 'Thẻ thành viên'); ?></a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-2">
                    <h5 class="title"><?= $this->lang->line('information', 'THÔNG TIN'); ?></h5>
                    <ul class="nav">
                        <li class="nav-item"><a href="<?php echo base_url(); ?>gioi-thieu-auchan/" class="nav-link"><?= $this->lang->line('aboutus', 'Giới thiệu Auchan'); ?></a></li>
                        <li class="nav-item"><a href="<?php echo base_url(); ?>tuyen-dung/" class="nav-link"><?= $this->lang->line('recruitment', 'Tuyển dụng'); ?></a></li>
                        <li class="nav-item"><a href="<?php echo base_url(); ?>he-thong-sieu-thi-auchan/" class="nav-link"><?= $this->lang->line('find_store', 'Tìm siêu thị'); ?> <span class="btn-link">(<?= $counter_store; ?>)</span></a></li>
                        <li class="nav-item"><a href="<?php echo base_url(); ?>dieu-khoan-va-chinh-sach/" class="nav-link"><?= $this->lang->line('policy', 'Điều khoản và chính sách'); ?></a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-4">
                    <h5 class="title"><?= $this->lang->line('head_office', 'TRỤ SỞ CHÍNH'); ?></h5>
                    <ul class="list-unstyled">
                        <li><i class="auc auc-fw auc-location"></i><?= Globals::getConfig("Address"); ?>.</li>
                        <li><i class="auc auc-fw auc-mail"> </i><a href="mailto:<?= Globals::getConfig("Email"); ?>" class="btn-link"><?= Globals::getConfig("Email"); ?></a> (<?= $this->lang->line('for_supplier', 'Dành cho Nhà cung cấp'); ?>)</li>
                        <li><i class="auc auc-fw auc-mail"></i><a href="mailto:cskh@auchan.vn" class="btn-link">cskh@auchan.vn</a> (<?= $this->lang->line('for_customer', 'Dành cho Khách hàng'); ?>)</li>
                        <li><i class="auc auc-fw auc-mobile"></i><?= $this->lang->line('Support', 'Liên hệ hỗ trợ'); ?>: <?= Globals::getConfig("Hotline"); ?> <?= Globals::getConfig("Ext"); ?> </li>
                        <li><i class="auc auc-fw auc-mobile"></i><?= $this->lang->line('Customer_Serives', 'Hotline dịch vụ CSKH'); ?>: 0914 086 993</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 p-t-1 col-sm-8">
                    <div class="certificate">
                        <?= Globals::getConfig("Certificate"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-foot">
        <div class="container">
            <p class="copyright">© 2017 Auchan.  All rights reserved.</p>
        </div>
        <!-- End footer-->
    </div>
</footer>
<!-- End footer-->
<div id="mailModal" tabindex="-1" role="dialog" class="modal fade">
    <div role="document" class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title"> <i class="auc auc-user-3"></i><span>Thư góp ý</span></h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <textarea type="text-area" placeholder="Nội dung góp ý..." rows="5" class="form-control"></textarea>
                </div>
                <p class="text-xs-center"><a href="" title="Gửi" class="btn btn-success btn-block btn-lg">Gửi</a></p>
            </div>
        </div>
    </div>
</div>
<div id="loginModal" tabindex="-1" role="dialog" class="modal fade">
    <div role="document" class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title"> <i class="auchan icon-auchan-80 auc-2x"></i><span>Đăng nhập tài khoản</span></h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="email" placeholder="Email hoặc số điện thoại" class="form-control">
                </div>
                <div class="form-group">
                    <input placeholder="Mật khẩu" type="password" class="form-control">
                </div>
                <p>
                    <label class="form-check-inline">
                        <input type="checkbox" value="option1" class="form-check-input"> Ghi nhớ mật khẩu 
                    </label><a href="" title="Quên mật khẩu" data-toggle="modal" data-target="#fogotPasswordModal" data-dismiss="modal" class="btn-link pull-right">Quên mật khẩu ?</a>
                </p>
                <p class="text-xs-center"><button class="btn btn-success btn-block btn-lg">Đăng nhập</button></p>
                <p class="text-xs-center"> Bạn chưa có tài khoản? <a href="" title="Đăng ký" data-toggle="modal" data-target="#signupModal" data-dismiss="modal" class="btn-link">Đăng ký</a>.</p>
            </div>
        </div>
    </div>
</div>
<div id="signupModal" tabindex="-1" role="dialog" class="modal fade">
    <div role="document" class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title"> <i class="auc auc-user-3"></i><span>Tạo tài khoản mới</span></h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="name" placeholder="Họ tên" class="form-control">
                </div>
                <div class="form-group">
                    <input type="email" placeholder="Email" class="form-control">
                </div>
                <div class="form-group">
                    <input placeholder="Mật khẩu" type="password" class="form-control">
                </div>
                <div class="form-group">
                    <input placeholder="Nhập lại mật khẩu" type="password" class="form-control">
                </div>
                <p>
                    <label class="form-check-inline">
                        <input id="inlineRadio1" type="radio" name="inlineRadioOptions" value="option1" checked class="form-check-input"> Nam
                    </label>
                    <label class="form-check-inline">
                        <input id="inlineRadio2" type="radio" name="inlineRadioOptions" value="option2" class="form-check-input"> Nữ
                    </label>
                </p>
                <p class="text-xs-center"><a href="" title="Đăng ký" class="btn btn-success btn-block btn-lg">Đăng ký</a></p>
                <p class="text-xs-center"> Bạn đã có tài khoản? <a href="" title="Đăng nhập tài khoản" data-toggle="modal" data-target="#loginModal" data-dismiss="modal" class="btn-link">Đăng nhập</a>.</p>
            </div>
        </div>
    </div>
</div>
<div id="fogotPasswordModal" tabindex="-1" role="dialog" class="modal fade">
    <div role="document" class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title"> <i class="auc auc-user-3"></i><span>Quên mật khẩu</span></h5>
            </div>
            <div class="modal-body">
                <div class="step-1">
                    <p>Để lấy lại mật khẩu bạn vui lòng nhập số điện thoại đăng ký.</p>
                    <div class="form-group">
                        <input type="text" placeholder="Số điện thoại" class="form-control">
                    </div>
                    <p class="text-xs-center"><a href="" title="Nhập lại mật khẩu" class="btn btn-success btn-block btn-lg">Nhận lại mật khẩu</a></p>
                </div>
                <div class="step-2">
                    <p> Mã xác nhận đã được gửi vào số điện thoại của bạn. Vui lòng nhập mã để tiếp tục thao tác .</p>
                    <div class="form-group">
                        <input type="text" placeholder="Mã xác nhận" class="form-control">
                    </div>
                    <p class="text-xs-center"><a href="" title="Xác nhận" class="btn btn-success btn-block btn-lg">Xác nhận</a></p>
                </div>
                <div class="step-2">
                    <p> Xác nhận thành công, vui lòng nhập mật khẩu mới.</p>
                    <div class="form-group">
                        <input type="text" placeholder="Mật khẩu mới" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="Nhập lại mật khẩu" class="form-control">
                    </div>
                    <p class="text-xs-center"><a href="" title="Hoàn tất" class="btn btn-success btn-block btn-lg">Hoàn tất</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="check-order-modal" tabindex="-1" role="dialog" class="modal fade">
    <div role="document" class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title"> <i class="auc auc-doc-text"></i><span>Kiểm tra đơn hàng</span></h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" placeholder="Số điện thoại" class="form-control">
                </div>
                <div class="form-group">
                    <input type="text" placeholder="Mã đơn hàng" class="form-control">
                </div>
                <p class="text-xs-center"><a href="detail-order.html" title="Tra cứu đơn hàng" class="btn btn-success btn-block btn-lg">Tra cứu</a></p>
            </div>
        </div>
    </div>
</div>
<div id="comment-modal" tabindex="-1" role="dialog" aria-labelledby="comment-modal-Label" class="modal fade">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title"><i class="auc auc-comment"></i><span>Hỏi đáp & cảm nhận người dùng</span></h5>
            </div>
            <div class="modal-body">
                <div class="row rate">
                    <div class="col-sm-6 col-md-3">Đánh giá của bạn</div>
                    <div class="col-sm-6 col-md-9">
                        <div class="star"><span class="auc auc-star"></span><span class="auc auc-star"></span><span class="auc auc-star"></span><span class="auc auc-star"></span><span class="auc auc-star"></span></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 form-group">
                        <input placeholder="Họ tên" type="text" class="form-control">
                    </div>
                    <div class="col-xs-6 form-group">
                        <input placeholder="Số điện thoại" type="text" class="form-control">
                    </div>
                    <div class="col-xs-12 form-group">
                        <textarea placeholder="Nội dung nhận xét..." rows="3" class="form-control"></textarea>
                        <div role="alert" class="alert alert-danger"><i class="snk snk-info-circled"></i><span>Vui lòng nhập đầy đủ thông tin.</span></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-primary btn-lg btn-block">Gửi</button>
            </div>
        </div>
    </div>
</div>
<div id="recruitment-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade">
    <div role="document" class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title"> <i class="auc auc-note"> </i><span>Ứng tuyển trực tiếp</span></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <h5>Thông tin công việc</h5>
                    </div>
                    <div class="col-xs-6">
                        <div class="input-group">
                            <input type="text" aria-label="Amount (to the nearest dollar)" value="09/21/2017" class="form-control"><span class="input-group-addon"><i class="auc auc-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <select class="form-control">
                            <option value="169934" selected="">Store Picking Manager - Quản Lý Điều Phối Kho Hàng</option>
                            <option value="169884">Quality Control Officer _ Nhân viên Kiểm Tra Chất Lượng (Long Bình Tân, Biên Hòa / VSIP Bắc Ninh)</option>
                            <option value="169834">In-house Legal Counsel</option>
                            <option value="169814">Logistic Manager (Long Binh Tan, Bien Hoa)</option>
                            <option value="167124">Social Campaign Marketing Executive</option>
                            <option value="167114">Market Research Leader cum Assistant to Real Estate Director</option>
                            <option value="167104">Digital Marketing Manager – Online Sale</option>
                            <option value="167084">Quản Lý An Ninh - Chief Security</option>
                            <option value="167054">Shift Manager - Quản Lý Ca Siêu Thị Tiện Lợi</option>
                            <option value="167044">Store Manager - Quản Lý Siêu Thị Tiện Lợi</option>
                            <option value="161774">NV Quầy Chế Biến - Siêu Thị Tiện Lợi Sắp Khai Trương (Hồng Bàng,Q5 - Tân Hòa Đông, Q6)</option>
                            <option value="161764">NV Thu Ngân - Siêu Thị Tiện Lợi Sắp Khai Trương (Hồng Bàng,Q5 - Tân Hòa Đông, Q6)</option>
                            <option value="161744">NV Quầy Bánh - NV Rau Củ : Siêu Thị Tiện Lợi Sắp Khai Trương (Hồng Bàng,Q5 - Tân Hòa Đông, Q6)</option>
                            <option value="151891">Chuyên Viên Đào Tạo Nghề -Rau Củ Quả</option>
                            <option value="151881">Chuyên Gia Đào Tạo Chế Biến</option>
                            <option value="151851">Store Manager - Giám Đốc Siêu Thị</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <select class="form-control">
                            <option value="-1">Nơi mong muốn làm việc</option>
                            <option value="ha-noi-vi">Hà Nội</option>
                            <option value="ho-chi-minh-vi">Hồ Chí Minh</option>
                            <option value="tay-ninh-vi">Tây Ninh</option>
                        </select>
                    </div>
                    <div class="col-xs-6 hidden-xs-down"></div>
                    <div class="col-xs-4 col-sm-3">
                        <select class="form-control">
                            <option value="-1">Địa điểm ưu tiên 1</option>
                        </select>
                    </div>
                    <div class="col-xs-4 col-sm-3">
                        <select class="form-control">
                            <option value="-1">Địa điểm ưu tiên 2</option>
                        </select>
                    </div>
                    <div class="col-xs-4 col-sm-3">
                        <select class="form-control">
                            <option value="-1">Địa điểm ưu tiên 3</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h5>Thông tin cá nhân</h5>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" placeholder="Họ tên (*)" class="form-control">
                    </div>
                    <div class="col-xs-6">
                        <input type="text" placeholder="Ngày sinh (*)" class="form-control">
                    </div>
                    <div class="col-xs-6">
                        <input type="text" placeholder="Nơi sinh (*)" class="form-control">
                    </div>
                    <div class="col-xs-6">
                        <input type="text" placeholder="Giới tính (*)" class="form-control">
                    </div>
                    <div class="col-xs-12">
                        <input type="text" placeholder="Nơi đăng ký hộ khẩu thường trú (*)" class="form-control">
                    </div>
                    <div class="col-xs-12">
                        <input type="text" placeholder="Địa chỉ hiện tại (*)" class="form-control">
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <input type="text" placeholder="Số CMND (*)" class="form-control">
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="input-group">
                            <input type="text" aria-label="Amount (to the nearest dollar)" placeholder="Ngày cấp (*)" class="form-control"><span class="input-group-addon"><i class="auc auc-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <input type="text" placeholder="Nơi cấp (*)" class="form-control">
                    </div>
                    <div class="col-xs-6">
                        <select class="select form-control">
                            <option value="-1">Thời gian bắt đầu làm việc</option>
                            <option value="1">Có thể đi làm ngay</option>
                            <option value="2">Thông báo trước</option>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" placeholder="Ngày bắt đầu làm việc (*)" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h5>Quá trình công tác</h5>
                        <input type="text" placeholder="Công ty" class="form-control">
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="input-group">
                            <input type="text" placeholder="Từ" class="form-control"><span class="input-group-addon"><i class="auc auc-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="input-group">
                            <input type="text" placeholder="Đến" class="form-control"><span class="input-group-addon"><i class="auc auc-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <input type="text" placeholder="Chức vụ" class="form-control">
                    </div>
                    <div class="col-xs-12">
                        <textarea placeholder="Mô tả công việc..." class="form-control"></textarea>
                    </div>
                    <div class="col-xs-12">
                        <button class="btn btn-success"><i class="auc auc-add"></i> Thêm công ty</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h5>Quá trình học tâp</h5>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <input type="text" placeholder="Trường lớp đào tạo" class="form-control">
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="input-group">
                            <input type="text" placeholder="Từ" class="form-control"><span class="input-group-addon"><i class="auc auc-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="input-group">
                            <input type="text" placeholder="Đến" class="form-control"><span class="input-group-addon"><i class="auc auc-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <input type="text" placeholder="Chuyên ngành" class="form-control">
                    </div>
                    <div class="col-xs-12">
                        <textarea placeholder="Mô tả công việc..." class="form-control"></textarea>
                    </div>
                    <div class="col-xs-12">
                        <button class="btn btn-success"><i class="auc auc-add"></i> Thêm quá trình</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h5>Đính kèm tập tin CV					</h5>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <p><i class="auc auc-question"></i>Với vị trí công việc tại Siêu Thị, bạn có thể làm việc 6 ngày/tuần theo ca xoay (sáng từ 5:00, chiều từ 13:30), làm vào Chủ Nhật, ngày lễ không?</p>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <label for="yes_button_v1">
                            <input id="yes_button_v1" type="radio" name="button_v1" checked="checked"> Có
                        </label>
                        <label for="no_button_v1">
                            <input id="no_button_v1" type="radio" name="button_v1"> Không
                        </label>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <p><i class="auc auc-question"></i>Bạn có từng làm việc trong siêu thị, cửa hàng bán lẻ trước đây không?</p>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <label for="yes_button_v2">
                            <input id="yes_button_v2" type="radio" name="button_v2" checked="checked"> Có
                        </label>
                        <label for="no_button_v2">
                            <input id="no_button_v2" type="radio" name="button_v2"> Không
                        </label>
                    </div>
                    <div class="col-xs-12 file-choose">
                        <input id="cv" type="file" class="hidden">
                        <label for="cv" class="btn btn-success"><i class="auc auc-doc-text auc-2x"></i><span>Chọn tệp tải lên</span><small>Tối đa 3MB (.xls, pdf, .doc)</small></label>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <input placeholder="Mức lương mong đợi (*)" class="form-control">
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <input placeholder="Điện thoại (*)" class="form-control">
                    </div>
                    <div class="col-xs-6 col-sm-6">
                        <input placeholder="Email (*)" class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <select name="" id="" class="form-control">
                            <option value="">Bạn biết thông tin tuyển dụng từ đâu ?</option>
                            <option value="">Internet</option>
                            <option value="">Bạn bè giới thiệu</option>
                            <option value="">Báo chí</option>
                            <option value="">Thông báo của AuchanVN</option>
                            <option value="">Khác</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <input id="PostID" type="hidden" name="PostID" value="70">
                <button type="button" class="btn btn-primary btn-lg"> <i class="auc auc-right"> </i>Ứng tuyển				</button>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->helper('cookie');
$this->input->cookie('show_popup', TRUE);
$cookie2 = get_cookie('show_popup');
?>
<div id="popup-modal" tabindex="-1" role="dialog" aria-labelledby="comment-modal-Label" class="modal fade">
    <div role="document" class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title"><i class="auc auc-location"></i><span>Lựa chọn khu vực của bạn</span></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <?= $this->lang->line('home', 'Trang chủ'); ?>
                        <img src="public/auchan/v2/images/asset/logo_auchan_super.jpg" style="width: 100%"/>
                        <select id="location" class="form-control">
                            <option>Ha Noi</option>
                        </select>
                    </div>
                    <div class="col-xs-12 form-group text-xs-center">
                        <button type="button" class="btn btn-success accept_location">
                            Bắt đầu trải nghiệm <i class="auc auc-right"></i>
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="bottom-function">
    <div id="back-to-top" class="scroll-up pull-right"><i class="auc auc-angle-up snk-2x"></i></div>
</div>