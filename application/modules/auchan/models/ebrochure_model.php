<?php

class Ebrochure_model extends CI_Model {

    var $tablename = 'ttp_ebrochure';
    var $tablename_flyer = 'ttp_ebrochure_flyer';

    function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    function getDetailMaps($params = array()) {
        $this->db->select('*');
        if (!empty($params['brochureid'])) {
            $this->db->where('BrochureID', $params['brochureid']);
        }
        $result = $this->db->get($this->tablename_flyer)->result_array();
        return $result;
    }

    function getEBrochure($params = array(), $orderby = 'Start', $sort = 'ASC') {
        $this->db->select('*');
        if (!empty($params['cityid'])) {
            $this->db->select('EbrochureID');
            $this->db->where('CityID', $params['cityid']);
            $query = $this->db->get('ttp_ebrochure_city');
            if (count($query->result_array()) > 0) {
                $ids = array();
                foreach ($query->result_array() as $sid) {
                    $ids[] = $sid['EbrochureID'];
                }
                $this->db->where_in('ID', $ids);
            }
        }
        $this->db->where('Published', 1);
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function defineEBrochure($params = array()) {

        $data_ebrochure = $this->getEBrochure($params);
        $res = array();
        foreach ($data_ebrochure as $key => $value) {
            $json = $value;
            $res[$key]["ID"] = isset($json->ID) ? $json->ID : 0;
            $res[$key]["Name"] = isset($json->Name) ? $json->Name : '';
            $res[$key]["Description"] = isset($json->Description) ? $json->Description : '';
            $res[$key]["Image_S"] = isset($json->Image_S) ? UPLOAD_URL . '/' . $json->Image_S : '';
            $res[$key]["Created"] = isset($json->Created) ? $json->Created : '';
            $res[$key]["LastEdited"] = isset($json->LastEdited) ? $json->LastEdited : '';
            $res[$key]["Start"] = isset($json->Start) ? date_format(date_create($json->Start), 'd/m/Y') : '';
            $res[$key]["End"] = isset($json->End) ? date_format(date_create($json->End), 'd/m/Y') : '';
        }
        return $res;
    }

}

?>