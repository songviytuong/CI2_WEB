<?php

class Pages_model extends CI_Model {

    var $tablename = 'ttp_pagelinks';

    function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    function getPages($orderby = 'ID', $sort = 'ASC') {
        $class = array('index', 'pages');
        $this->db->select('*');
        $this->db->where_in('Classname', $class);
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }
    
    function getPageRow($alias){
        $this->db->select('*');
        $this->db->where('Alias', $alias);
        $this->db->where('Published', 1);
        $result = $this->db->get($this->tablename)->row();
        return $result;
    }

}

?>