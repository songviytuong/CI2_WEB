<?php

class banner_model extends CI_Model {

    var $tablename = 'ttp_banner';

    function __construct() {

        parent::__construct();

        // Your own constructor code
    }

    function getSlider($orderby = 'ID', $sort = 'DESC') {
        $this->db->select('*');
        $this->db->where('Published', 1);
        $this->db->where('PositionID', 4);
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function defineSlider() {
        $data_slider = $this->getSlider();
        $arr = array();
        if ($data_slider) {
            foreach ($data_slider as $key => $row) {
                $json = json_decode($row->Data);
                $arr[$key]['title'] = isset($json->Title) ? $json->Title : "";
                $arr[$key]['description'] = isset($json->Description) ? $json->Description : "";
                $arr[$key]['image'] = isset($row->Thumb) ? UPLOAD_URL . '/' . $row->Thumb : "";
                $arr[$key]['link'] = isset($json->Link) ? $json->Link . page_extension : "";
                $arr[$key]['target'] = isset($json->Target) ? $json->Target : "";
            }
        }
        return $arr;
    }
    
    function getFromAlias($by, $alias) {
        $this->db->select($by);
        $this->db->where('Alias', $alias);
        $result = $this->db->get($this->tablename)->row()->$by;
        return $result;
    }

    function getFromID($by, $id) {
        $this->db->select($by);
        $this->db->where('ID', $id);
        $result = $this->db->get($this->tablename)->row()->$by;
        return $result;
    }

    function getBanner($group, $orderby = 'ID', $sort = 'DESC') {
        $this->db->select('*');
        $this->db->where('Published', 1);
        $this->db->where('NOW() > `Start`');
        $this->db->where('NOW() < `End`');
        $this->db->where('PositionID', $group);
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function defineBanner($group) {

        $data_banner = $this->getBanner($group);
        $arr = array();
        if ($data_banner) {
            foreach ($data_banner as $key => $row) {
                $json = json_decode($row->Data);
                $arr[$key]['title'] = isset($json->Title) ? $json->Title : "";
                $arr[$key]['description'] = isset($json->Description) ? $json->Description : "";
                $arr[$key]['image'] = isset($row->Thumb) ? $row->Thumb : "";
                $arr[$key]['link'] = isset($json->Link) ? $json->Link : "";
                $arr[$key]['target'] = isset($json->Target) ? $json->Target : "_self";
            }
        }
        return $arr;
    }

}
