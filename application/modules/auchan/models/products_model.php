<?php

class products_model extends CI_Model {

    var $tablename = 'ttp_report_products';
    var $tablename_promotions = 'ttp_report_products_promotions';

    function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    function checkUrl($segment) {
        
    }

    function getProductRow($alias = '') {
        $this->db->select('*');
        if ($alias) {
            $this->db->where('Alias', $alias);
        }
        $this->db->where_in('Published', array('0', '1'));
        $result = $this->db->get($this->tablename)->row();
        return $result;
    }

    function getCategoriesAddon($orderby = 'Position', $sort = 'ASC') {
        $this->db->select('*');
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get('ttp_report_products_categories_addon')->result_array();
        if ($result) {
            $res = array();
            foreach ($result as $key => $row) {
                $res[$key]["ID"] = $row["ID"];
                $res[$key]["Title"] = $row["Title"];
                $res[$key]["Alias"] = $this->lib->alias($row["Title"]);
            }
            return $res;
        } else {
            return false;
        }
    }

    function getBreadCrumbData($alias) {
        $this->db->select('ID')->from('ttp_report_products')->where('Alias', $alias);
        $pid = $this->db->get()->row()->ID;
        $this->db->select('CategoriesID')->from('ttp_report_products_categories')->where('ProductsID', $pid);
        $cid = $this->db->get()->row()->CategoriesID;

        $arr1 = $this->db->select('a.ID as ID, a.Title as MenuList, a.Alias as AliasList, a.ParentID as ParentID')
                        ->from('ttp_report_categories a')
                        ->where('a.ID', $cid)->get()->row_array();

        $arr2 = $this->db->select('a.Title as MenuCategory,a.Alias as AliasCategory')
                        ->from('ttp_report_categories a')
                        ->where('a.ID', $arr1['ParentID'])->get()->row_array();
        $arr = array_merge($arr1, $arr2);
        return $arr;
    }

    function getIDFromAlias($alias) {
        $this->db->select('ID');
        $this->db->where('Alias', $alias);
        $result = $this->db->get($this->tablename)->row()->ID;
        return $result;
    }

    function getProductCounter($params) {
        $this->db->select('COUNT(ID) as total');
        if ($params) {
            $params = $params . ' !=';
            $this->db->where($params, 0);
        }
        $result = $this->db->get($this->tablename)->row()->total;
        return $result;
    }

    function getProductCounterBy($cid, $params, $type = 'like') {
        $this->db->select('c.ProductsID as ProductsID');
        $this->db->from('ttp_report_products_categories c');
        $this->db->join('ttp_report_products p', 'p.ID=c.ProductsID');
        $this->db->where('c.CategoriesID', $cid);
        $this->db->where('p.Published', 1);
        $this->db->where('p.Status', 1);
        $query = $this->db->get();
        if (count($query->result_array()) > 0) {
            $ids = array();
            foreach ($query->result_array() as $sid) {
                $ids[] = $sid['ProductsID'];
            }

            $this->db->select('COUNT(ID) as total');
            if ($type == 'like') {
                $this->db->where_in('ID', $ids);
            }
            $result = $this->db->get($this->tablename)->row()->total;
            return $result;
        } else {
            return false;
        }
    }

    function getCountSearch($params) {
        $this->db->from($this->tablename);
        if (!empty($params['keyword'])) {
            $this->db->like('Title', $params['keyword']);
//            $this->db->or_like('ShortName', $params['keyword']);
//            $this->db->or_like('MetaTitle', $params['keyword']);
        }
        $this->db->where('Published', 1);
        $this->db->where('Status', 1);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    function getAllProductInPromotion() {
        $this->db->select('*');
        $result = $this->db->get($this->tablename_promotions)->result();
        return $result;
    }

    function getAllProducts($cid = null, $orderby = 'ID', $sort = 'ASC', $limit = 0, $start = 0, $params = array()) {
        $this->db->select('*');
        if ($cid) {
            $this->db->select('ProductsID');
            $this->db->where('CategoriesID', $cid);
            $query = $this->db->get('ttp_report_products_categories');
            if (count($query->result_array()) > 0) {
                $ids = array();
                foreach ($query->result_array() as $sid) {
                    $ids[] = $sid['ProductsID'];
                }
                $this->db->where_in('ID', $ids);
            }
        }

        #Promotions Page
        if (!empty($params['pid']) && $params['pid']) {
            $this->db->where_in('ID', $params['pid']);
        }
        if (isset($params['only_promotions']) == TRUE) {
            $this->db->where('SocialPrice !=', 0);
        }
        #End Promotions Page

        if (!empty($params['chimmoi']) == 1) {
            $this->db->where('Chimmoi', 1);
            $this->db->order_by('LastEdited', 'DESC');
        }

        if (!empty($params['cataddon'])) {
            $this->db->where('AddOnCategories', $params['cataddon']);
            $this->db->order_by('LastEdited', 'DESC');
        }

        if (!empty($params['created']) == TRUE) {
            $orderby = 'Created';
            $sort = 'DESC';
        }

        if (!empty($params['discount']) == TRUE) {
            $this->db->where('SocialPrice !=', 0);
        }

        if (!empty($params['promotion']) == TRUE) {
            $this->db->where('PromotionsID IS NOT NULL', null, false);
            $this->db->where('PromotionsID !=', '[]');
        }
        if (!empty($params['keyword'])) {
            $this->db->like('Title', $params['keyword']);
//            $this->db->or_like('ShortName', $params['keyword']);
//            $this->db->or_like('MetaTitle', $params['keyword']);
        }

        if (!empty($params['this_id'])) {
            $this->db->where_not_in('ID', $params['this_id']);
        }

        $this->db->order_by($orderby, $sort);
        $this->db->limit($limit, $start);
        $this->db->where('Published', 1);
        $this->db->where('Status', 1);
        $result = $this->db->get($this->tablename)->result();
        if (!empty($params['random']) == TRUE) {
            shuffle($result);
        }
        return $result;
    }

    function defineAllProducts($cid = '', $orderby = 'ID', $sort = 'ASC', $limit = 0, $start = 0, $params = array()) {
        $data_products = $this->getAllProducts($cid, $orderby = 'ID', $sort = 'DESC', $limit, $start, $params);
        $arr = array();
        if ($data_products) {
            foreach ($data_products as $key => $row) {
                $arr[$key]['is_saleoff'] = FALSE;
                $arr[$key]['is_chimmoi'] = FALSE;
                $arr[$key]['unit_text'] = "&nbsp;";
                $json = $row;
                $arr[$key]['id'] = isset($json->ID) ? $json->ID : "";
                $arr[$key]['title'] = isset($json->Title) ? $json->Title : "";
                $arr[$key]['product_name'] = isset($json->Title) ? $this->global_model->truncate($json->Title, isset($params['truncate'])) : "";
                $arr[$key]['alias'] = isset($json->Alias) ? $json->Alias : '';
                $arr[$key]['price'] = !empty($json->Price) ? number_format($json->Price, 0, ",", ".") . ((active_currency) ? currency : '') : 0;
                $arr[$key]['root_price'] = !empty($json->RootPrice) ? number_format($json->RootPrice, 0, ",", ".") . ((active_currency) ? currency : '') : 0;
                $arr[$key]['social_price'] = !empty($json->SocialPrice) ? number_format($json->SocialPrice, 0, ",", ".") . ((active_currency) ? currency : '') : 0;

                $PrimaryImage = explode("/", $json->PrimaryImage);
                $sx = array();
                for ($i = 0; $i < count($PrimaryImage); $i++) {
                    if ($i == count($PrimaryImage) - 1) {
                        $sx[] = "200x200_" . $PrimaryImage[$i];
                    } else {
                        $sx[] = $PrimaryImage[$i];
                    }
                }
                $main_picture = implode("/", $sx);

                $arr[$key]['main_picture'] = !empty($json->PrimaryImage) ? UPLOAD_URL . '/' . $main_picture : "public/auchan/v2/images/no-image.jpg";
                $arr[$key]['button_text'] = "CHI TIẾT <i class='auc auc-right'></i>";
                $arr[$key]['button_cart'] = "Đặt mua";
                $arr[$key]['url'] = !empty($json->Alias) ? $json->Alias . page_extension : $this->lib->alias($json->Title) . page_extension;
                $arr[$key]['unit'] = isset($json->Donvi) ? $json->Donvi : "";
                $arr[$key]['weight'] = isset($json->Weight) ? $json->Weight : "";

                if (!empty($json->SocialPrice) && $json->SocialPrice != 0 && $json->Price != 0) {
                    $arr[$key]['is_saleoff'] = TRUE;
                    $saleoff_value = ($json->SocialPrice - $json->Price) / $json->SocialPrice * 100;
                    $arr[$key]['saleoff_value'] = "-" . ceil($saleoff_value) . "%";
                }

                if ($json->Chimmoi == 1) {
                    $arr[$key]['is_chimmoi'] = TRUE;
                }

                if (!empty($json->Donvi) && !empty($json->Weight) && !empty($json->Capacity)) {
                    if (strtolower($json->Donvi) == $json->Capacity) {
                        $arr[$key]['unit_text'] = $json->Weight . " " . strtolower($json->Donvi);
                    } else {
                        $arr[$key]['unit_text'] = "1 " . strtolower($json->Donvi) . " " . $json->Weight . " " . $json->Capacity;
                    }
                }



                if (!empty($arr[$key]['price']) == 0) {
                    $arr[$key]['price'] = "Call";
                }

                if (!empty($arr[$key]['social_price']) == 0) {
                    $arr[$key]['social_price'] = "";
                }
            }
        }
        return $arr;
    }

    function getProductDetails($params) {
//        $this->db->cache_on();
        $this->db->select('*, '
                . 'a.ID as ID,'
                . 'a.MaSP as SKU,'
                . 'a.Title as Title,'
                . 'a.Alias as Alias,'
                . 'a.Donvi as Unit,'
                . 'a.Content as Content,'
                . 'a.Description as Description,'
                . 'a.MetaTitle as MetaTitle,'
                . 'a.MetaKeywords as MetaKeywords,'
                . 'a.MetaDescription as MetaDescription,'
                . 'a.PrimaryImage as PrimaryImage,'
                . 'b.Title as Trademark,'
        );
        $this->db->from('ttp_report_products a');
        $this->db->join('ttp_report_trademark b', 'a.TrademarkID=b.ID', 'inner');
        $this->db->where_in('a.Published', array('0', '1'));
        $this->db->where_in('a.Status', array('0', '1'));
        if ($params['alias']) {
            $this->db->where('a.Alias', $params['alias']);
        }
        $result = $this->db->get()->row();
        return $result;
    }

    function defineUnit($type = "") {
        $arr = array(
            'kg' => array(
                'kg',
                'gram'
            ),
            'g' => array(
                'gram',
                'kg'
            )
        );
        return isset($arr[$type]) ? $arr[$type] : $arr;
    }

    function defineProductDetails($params) {
        $json = $this->getProductDetails($params);
        if ($json) {
            $arr = array();
            $arr['MetaDescription'] = isset($json->MetaDescription) ? $json->MetaDescription : "";
            $arr['MetaKeywords'] = isset($json->MetaKeywords) ? $json->MetaKeywords : "";
            $arr['MetaTitle'] = isset($json->MetaTitle) ? $json->MetaTitle : "";

            $arr['ID'] = isset($json->ID) ? $json->ID : "";
            $arr['SKU'] = isset($json->SKU) ? $json->SKU : "";
            $arr['Title'] = isset($json->Title) ? $json->Title : "";
            $arr['Alias'] = isset($json->Alias) ? $json->Alias : "";
            if (!empty($json->Content)) {
                if (!lazyload) {
                    //$lazyload = "src='" . $item['main_picture'] . "'";
                    $Content = str_replace('src="', 'src="' . UPLOAD_URL, $json->Content);
                } else {
                    $Content = str_replace('src="', 'src=data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs= data-src="' . UPLOAD_URL, $json->Content);
                    $Content = str_replace('img', 'img class="lazyload"', $Content);
                    $Content = str_replace('alt=""', 'alt="' . $arr["Title"] . '"', $Content);
                    $Content = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $Content);
                    //$lazyload = "src='data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=' data-src='" . $item['main_picture'] . "'";
                }
            }
            $arr['Content'] = isset($Content) ? $Content : "";
            $arr['Description'] = !empty($json->Description) ? $json->Description : "";
            $arr['Price'] = !empty($json->Price) ? number_format($json->Price, 0, ",", ".") . ((active_currency) ? currency : '') : 0;
            $arr['RootPrice'] = !empty($json->RootPrice) ? number_format($json->RootPrice, 0, ",", ".") . ((active_currency) ? currency : '') : 0;
            $arr['SocialPrice'] = !empty($json->SocialPrice) ? number_format($json->SocialPrice, 0, ",", ".") . ((active_currency) ? currency : '') : 0;

            if (!empty($json->Donvi) && !empty($json->Weight) && !empty($json->Capacity)) {
                if (strtolower($json->Donvi) == $json->Capacity) {
                    $arr['UnitText'] = "/ " . $json->Weight . " " . strtolower($json->Donvi);
                } else {
                    $arr['UnitText'] = "/ " . strtolower($json->Donvi) . " " . $json->Weight . " " . $json->Capacity;
                }
            }

            $arr['Unit'] = !empty($json->Unit) ? strtolower($json->Unit) : "";

            $arr['Trademark'] = !empty($json->Trademark) ? $json->Trademark : "";
            $arr['Capacity'] = !empty($json->Capacity) ? $json->Capacity : "";
            $arr['DataUnit'] = $this->defineUnit(strtolower($json->Capacity));

            if (!empty($json->SocialPrice) && $json->SocialPrice != 0 && $json->Price != 0) {
                $arr['is_saleoff'] = TRUE;
                $saleoff_value = ($json->SocialPrice - $json->Price) / $json->SocialPrice * 100;
                $arr['saleoff_value'] = ceil($saleoff_value) . "%";
            }

            $AlbumImage = json_decode($json->AlbumImage);
            if (($key = array_search($json->PrimaryImage, $AlbumImage)) !== false) {
                unset($AlbumImage[$key]);
            }

            $arr['PrimaryImage'] = isset($json->PrimaryImage) ? $json->PrimaryImage : "public/auchan/v2/images/no-image.jpng";
            $arr['AlbumImage'] = $AlbumImage ? $AlbumImage : array();

            #ButtonText
            $this->load->model('static_model', 'static');
            $static = $this->static->getConfig();
            $static_data = ($static) ? json_decode($static->Data) : array();
            $arr['ButtonText'] = "<i class='auc auc-location'></i> Tới siêu thị gần nhất";

            return $arr;
        }
    }

}

?>