<?php

class Promotions_model extends CI_Model {

    var $tablename = 'ttp_report_promotions';

    function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    function getPromotions($params = array(), $orderby = 'Start', $sort = 'ASC') {
        $this->db->select('*');
        if(isset($params['between'])){
            $this->db->where('NOW() > `Start`');
            $this->db->where('NOW() < `End`');
        }
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function definePromotions($params = array()) {
        $data_promotions = $this->getPromotions($params);
        $res = array();
        foreach ($data_promotions as $key => $value) {
            $json = $value;
            $res[$key]["ID"] = isset($json->ID) ? $json->ID : 0;
            $res[$key]["Name"] = isset($json->Name) ? $json->Name : '';
            $res[$key]["Description"] = isset($json->Description) ? $json->Description : '';
            $res[$key]["Start"] = isset($json->Start) ? $json->Start : '';
            $res[$key]["End"] = isset($json->End) ? $json->End : '';
        }
        return $res;
    }
    
    function getProductCategoriesAddOn($params = array(), $orderby = 'Position', $sort = 'ASC') {
        $this->db->select('*');
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get('ttp_report_products_categories_addon')->result();
        return $result;
    }
    
    function defineProductCategoriesAddOn() {
        $data_categories_addon = $this->getProductCategoriesAddOn();
        $res = array();
        foreach ($data_categories_addon as $key => $value) {
            $json = $value;
            $res[$key]["ID"] = isset($json->ID) ? $json->ID : 0;
            $res[$key]["Title"] = isset($json->Title) ? $json->Title : '';
            $res[$key]["Position"] = isset($json->Position) ? $json->Position : 0;
        }
        return $res;
    }

}

?>