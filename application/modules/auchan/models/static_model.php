<?php

class Static_model extends CI_Model {

    var $tablename = 'ttp_static_config';
    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getConfig($id = 1) {
        $this->db->select('*');
        $this->db->where('ID', $id);
        $res = $this->db->get($this->tablename)->row();
        return $res;
    }
}
