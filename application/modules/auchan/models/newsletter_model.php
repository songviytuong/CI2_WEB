<?php

class Newsletter_model extends CI_Model {

    var $tablename = 'auc_newsletter';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getBy($name, $where, $value) {
        $this->db->select($name);
        $this->db->where($where, $value);
        $res = $this->db->get($this->tablename)->row()->$name;
        return $res;
    }

}
