<?php

class Feedback_model extends CI_Model {

    var $tablename = 'ttp_feedback';
    var $tablename_type = 'ttp_feedback_types';

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getFeedbackType($langid = 2) {
        $this->db->select('*');
        $this->db->where('LangID', $langid);
        $result = $this->db->get($this->tablename_type)->result();
        return $result;
    }

    function defineFeedbackType($langid) {
        $feedtype_data = $this->getFeedbackType($langid);
        $res = array();
        foreach ($feedtype_data as $key => $row) {
            $res[$key]['ID'] = isset($row->ID) ? $row->ID : '';
            $res[$key]['Name'] = isset($row->Name) ? $row->Name : '';
        }
        return $res;
    }
    
    function addFeedback($data){
        $this->db->insert($this->tablename,$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function getAllFeedback($orderby = 'ID', $sort = 'DESC') {
        $this->db->select('*');
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function defineAllFeedback() {
        $news_data = $this->getAllFeedback();
        $res = array();
        foreach ($news_data as $key => $row) {
            $res[$key]['ID'] = isset($row->ID) ? $row->ID : '';
            $res[$key]['Content'] = isset($row->Content) ? $row->Content : '';
            $res[$key]['Fullname'] = isset($row->Fullname) ? $row->Fullname : '';
            $res[$key]['Email'] = isset($row->Email) ? $row->Email : '';
            $res[$key]['Phone'] = isset($row->Phone) ? $row->Phone : '';
            $res[$key]['Created'] = isset($row->Created) ? $row->Created : '';
        }
        return $res;
    }

}
