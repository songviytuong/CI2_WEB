<?php

class categories_model extends CI_Model {

    var $tablename = 'ttp_report_categories';

    function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    function getAllCategories($id = 0, $orderby = 'STT', $sort = 'ASC') {
        $this->db->select('*');
        $this->db->where('Special', 1);
        $this->db->where('ParentID', $id);
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function defineAllCategories($id = 0) {
        $data_categories = $this->getAllCategories($id);
        $arr = array();
        $SQL_PATH = $this->db->query("select distinct(`ParentID`) from ttp_report_categories where Special=1 and ParentID != 0 and Path like '%/%'")->result_array();
        $res = array();
        foreach ($SQL_PATH as $key => $value) {
            $res[] = $value['ParentID'];
        }

        $SQL_ADD_PARENT = $this->db->query("SELECT ID from ttp_report_categories where Special=1 and ParentID = 0")->result_array();
        foreach ($SQL_ADD_PARENT as $key => $value) {
            $res2[] = $value['ID'];
        }
        $res = array_merge_recursive($res, $res2);

        if ($data_categories) {

            foreach ($data_categories as $key => $row) {
                $json = $row;
                #VERIFY PARENT
                #$this->db->query("SELECT ")->get()->row()->ID;
                if (in_array($json->ID, $res)) {
                    $is_parent = 1;
                    $arr[$key]['alias'] = isset($json->Url) ? $json->Url : (isset($json->Alias) ? "danh-muc/" . $json->Alias . page_extension : "");
                } else {
                    $is_parent = 0;
                    $ParentID = $this->getFromAlias('ParentID', $json->Alias);
                    $CurrentAlias = $this->getFromID('Alias', $ParentID);
                    $arr[$key]['alias'] = isset($json->Url) ? $json->Url : (isset($json->Alias) ? $CurrentAlias . '/' . $json->Alias . page_extension : "");
                }

                $arr[$key]['id'] = isset($json->ID) ? $json->ID : "";
                $arr[$key]['title'] = isset($json->Title) ? $json->Title : "";

                $arr[$key]['is_parent'] = $is_parent;
                $arr[$key]['parent_id'] = isset($json->ParentID) ? $json->ParentID : "";
            }
        }
        return $arr;
    }

    function getFromAlias($by, $alias) {
        $this->db->select($by);
        $this->db->where('Alias', $alias);
        $result = $this->db->get($this->tablename)->row()->$by;
        return $result;
    }

    function getFromID($by, $id) {
        $this->db->select($by);
        $this->db->where('ID', $id);
        $result = $this->db->get($this->tablename)->row()->$by;
        return $result;
    }

    function getSubCategoriesByID($alias, $orderby = 'STT', $sort = 'ASC') {
        $cid = $this->getFromAlias('ID', $alias);
        $this->db->select('*');
        $this->db->where('Special', 1);
        $this->db->where('ParentID', $cid);
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function defineSubCategoriesByAlias($alias) {
        $data_categories = $this->getSubCategoriesByID($alias);
        $arr = array();
        if ($data_categories) {
            foreach ($data_categories as $key => $row) {
                $json = $row;
                $arr[$key]['id'] = isset($json->ID) ? $json->ID : "";
                $arr[$key]['title'] = isset($json->Title) ? $json->Title : "";
                $ParentID = $this->getFromAlias('ParentID', $json->Alias);
                $CurrentAlias = $this->getFromID('Alias', $ParentID);
                $arr[$key]['alias'] = isset($json->Url) ? $json->Url : (isset($json->Alias) ? $CurrentAlias . '/' . $json->Alias . page_extension : "");
                $arr[$key]['parent_id'] = isset($json->ParentID) ? $json->ParentID : "";
                $arr[$key]['thumb'] = !empty($json->Thumb) ? UPLOAD_URL . '/' . $json->Thumb : "public/auchan/v2/images/no-image.jpg";
            }
        }
        return $arr;
    }

}

?>