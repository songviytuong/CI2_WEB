<?php

class Users extends CI_Controller {

    var $params = array();
    public $limit = 20;
    public $upload_to = '';
    public $classname = "auchan";
    public $seo_default = TRUE;
    public $cur_lang = 'english';
    public $cur_langid = 1;
    public $cur_local = 'en';

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $this->load->model('define_model');
        $app_conf = $this->define_model->getDefine();
        foreach ($app_conf as $row) {
            define("{$row['Keyword']}", $row['Value']);
        }

        if (UNDER) :
            #Load Static Config
            $this->load->model('global_model', 'global');
            $detect = $this->global->troubleshoot();

            if ($detect) {
                if (isset($detect['result']) == FALSE) {
                    $data['ip'] = $detect['ip'];
                    $this->template->set_template('under');
                }
            }
        endif;

        /*
         * BEGIN: LOAD LANGUAGE
         */
        $langs = $this->session->userdata('languageTT');
        $getLang = (isset($_GET['lang'])) ? trim($_GET['lang']) : '';
        if ($getLang != "") {
            $langrow = $this->languages_model->getLangNameByLocal($getLang);
            $this->session->set_userdata('languageTT', $langrow['LangName']);
        } else {
            $langsite = $this->config->item('language');
            $langrow = $this->languages_model->getLanguageBySession($langsite);
            $this->session->set_userdata('languageTT', $langsite);
        }
        $this->cur_lang = $langrow['LangName'];
        $this->cur_langid = $langrow['LangID'];
        $this->lang->load('translates', $this->classname);

        $this->params = array_merge($_GET, $_POST);
        $this->load->model('login_database');
    }
    
    function login(){
        
    }
}