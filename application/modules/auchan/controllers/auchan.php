<?php

class Auchan extends CI_Controller {

    public $limit = 20;
    public $upload_to = '';
    public $classname = "auchan";
    public $seo_default = TRUE;
    public $cur_lang = 'english';
    public $cur_langid = 1;
    public $cur_local = 'en';

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $this->load->library('template');
        $this->template->set_template('auchan');

        $this->load->model('define_model');
        $app_conf = $this->define_model->getDefine();
        foreach ($app_conf as $row) {
            define("{$row['Keyword']}", $row['Value']);
        }

        if (UNDER) :
            #Load Static Config
            $this->load->model('global_model', 'global');
            $detect = $this->global->troubleshoot();

            if ($detect) {
                if (isset($detect['result']) == FALSE) {
                    $data['ip'] = $detect['ip'];
                    $this->template->set_template('under');
                }
            }
        endif;

        #BEGIN: ACTIVE SMARTY
        #$this->load->library('parser');
        #$data['title'] = 'Hello Peace';
        #$this->parser->parse("demo.tpl",$data);

        /*
         * BEGIN: LOAD LANGUAGE
         */
        $langs = $this->session->userdata('languageTT');
        $getLang = (isset($_GET['lang'])) ? trim($_GET['lang']) : '';
        if ($getLang != "") {
            $langrow = $this->languages_model->getLangNameByLocal($getLang);
            $this->session->set_userdata('languageTT', $langrow['LangName']);
        } else {
            $langsite = $this->config->item('language');
            $langrow = $this->languages_model->getLanguageBySession($langsite);
            $this->session->set_userdata('languageTT', $langsite);
        }
        $this->cur_lang = $langrow['LangName'];
        $this->cur_langid = $langrow['LangID'];
        $this->lang->load('translates', $this->classname);

        #Lee (hiển thị menu categories)
        $this->load->model('categories_model');
        $define_categories = $this->categories_model->defineAllCategories();

        #Lee (hiển thị số lượng sản phẩm Promotion, mới MenuLeft)
        $this->load->model('products_model', 'products');
        $counter_promotion = $this->products->getProductCounter('SocialPrice');
        $counter_newest = $this->products->getProductCounter('Chimmoi');

        $data = array(
            "main_menu" => $define_categories,
            "counter_promotion" => $counter_promotion, //in MenuLeft
            "counter_newest" => $counter_newest, //in MenuLeft
        );
        $data['js_to_load'] = array();
        $this->template->write_view('header', 'header', $data);
        $this->template->write_view('footer', 'footer', $data);
        $this->template->add_doctype();
    }

    function googleForm() {
        $data = array();
        $this->template->write_view('content', 'googleForm', $data);
        $this->template->render();
    }

    public function index() {
        
        #Lee (hiển thị danh sách SP)
        $this->load->model('products_model', 'products');
        $define_products = $this->products->defineAllProducts($limit = 5);

        #Lee (hiển thị slider)
        $this->load->model('banner_model', 'slider');
        $MainSlider = $this->cache->get('MainSlider');
        if (!$MainSlider) {
            $MainSlider = $this->slider->defineSlider();
            $this->cache->write($MainSlider, "MainSlider");
        }

        #Lee (hiển thị banner dưới slider)
        $this->load->model('banner_model', 'banner');
        $define_banner5 = $this->banner->defineBanner(5);
        $define_banner6 = $this->banner->defineBanner(6);
        $define_banner7 = $this->banner->defineBanner(7);
        $define_banner8 = $this->banner->defineBanner(8);
        $define_banner11 = $this->banner->defineBanner(11);

        $data = $this->cache->get('Data');
        if (!$data) {
            $data = array(
                'products' => $define_products,
                'main_slider' => $MainSlider,
                'group1_banners' => $define_banner5,
                'group2_banners' => $define_banner6,
                'group3_banners' => $define_banner7,
                'group4_banners' => $define_banner8,
                'group11_banners' => $define_banner11,
            );
            $this->cache->write($data, "Data");
        }

        $data['page_class'] = "home-page show-nav-page";
        $data['js_to_load'] = array();
        $data['off_menu'] = TRUE;

        $this->template->write_view('content', 'content', $data);
        $this->template->render();
    }

    public function search() {
        $keyword = urldecode($_GET['q']);
        $this->load->model('products_model', 'products');
        $total_product = $this->products->getCountSearch(array('keyword' => $keyword));

        $this->load->model('post_model', 'post');
        $total_post = $this->post->getCountSearch(array('keyword' => $keyword));
        $data = array(
            'keyword' => $keyword,
            'total_product' => $total_product,
            'total_post' => $total_post,
        );

        $data['page_class'] = "search-page show-nav-page";
        $data['js_to_load'] = array();
        $data['off_menu'] = TRUE;

        $this->template->write_view('content', 'pages/templates/search', $data);
        $this->template->render();
    }

    public function make_folder($str, $folder = 'assets/') {
        if ($str != '') {
            if (!file_exists($folder . $str)) {
                mkdir("./$folder" . $str, 0777, true);
            }
        }
    }

    public function downloadimage_by_link($image = '') {
        $this->upload_to = date('Y') . '/' . date('Y-m');
        $this->make_folder($this->upload_to, 'assets/');
        @$file = file_get_contents($image);
        if ($file != '') {
            $name = explode('/', $image);
            $name = isset($name[count($name) - 1]) ? $name[count($name) - 1] : '';
            if ($name != '') {
                file_put_contents("assets/" . $this->upload_to . '/' . $name, $file);
                $cropimage = explode(",", IMAGECROP);
                foreach ($cropimage as $row) {
                    $size = explode("x", $row);
                    $width = isset($size[0]) ? (int) $size[0] : 0;
                    $height = isset($size[1]) ? (int) $size[1] : 0;
                    if ($width > 0 && $height > 0)
                        $this->lib->cropimage("assets/" . $this->upload_to . '/' . $name, $width, $height);
                }
                return "assets/" . $this->upload_to . '/' . $name;
            }
        }else {
            return '';
        }
    }

    public function checkstatus() {
        echo "true";
    }

    public function next_order() {
        $token = $this->session->userdata('token');
        if ($token == 1) {
            $page = isset($_POST['page']) ? (int) $_POST['page'] : 0;
            $limit = $page * 6;
            $order = $this->db->query("select Name,Ngaydathang,ID,Total,Chiphi,Chietkhau from ttp_report_order order by ID DESC limit $limit,6")->result();
            $orderlist = array();
            $productslist = array();
            if (count($order) > 0) {
                foreach ($order as $row) {
                    $orderlist[] = $row->ID;
                }
                $temp = implode(',', $orderlist);
                $products = $this->db->query("select a.Title,b.Amount,b.Total,b.OrderID from ttp_report_products a,ttp_report_orderdetails b where a.ID=b.ProductsID and b.OrderID in($temp)")->result();
                if (count($products) > 0) {
                    foreach ($products as $item) {
                        $gift = $item->Total == 0 ? 'tặng ' : '';
                        if (isset($productslist[$item->OrderID])) {
                            $productslist[$item->OrderID][] = $gift . $item->Amount . ' ' . $item->Title;
                        } else {
                            $productslist[$item->OrderID] = array();
                            $productslist[$item->OrderID][] = $gift . $item->Amount . ' ' . $item->Title;
                        }
                    }
                }
                foreach ($order as $row) {
                    echo '<div class="item-order">
                          <b>' . $row->Name . '</b>
                          <small> đặt hàng cách đây ' . $this->lib->get_times_remains($row->Ngaydathang) . '</small>';
                    $total = $row->Total - $row->Chietkhau + $row->Chiphi;
                    echo "<p>Đơn hàng trị giá <b>" . number_format($total, 0, '', '.') . "đ</b> bao gồm (";
                    echo isset($productslist[$row->ID]) ? implode(', ', $productslist[$row->ID]) : '';
                    echo ")</p>";
                    echo '</div>';
                }
            }
        } else {
            redirect("http://google.com");
        }
    }

    public function pages($object) {

        $segment = end($this->uri->segment_array());
        $data['off_menu'] = FALSE;

        $this->load->model('pages_model', 'pages');
        $pages_data = $this->pages->getPages();
        foreach ($pages_data as $row) {
            if ($segment == $row->Alias) {
                $data['page_class'] = $row->PageClass;
                if ($row->AddHeader) {
                    $this->template->write('header', $row->AddFooter);
                }
                if ($row->AddFooter) {
                    $this->template->write('footer', $row->AddFooter);
                }
            }
        }
        $data['js_to_load'] = array();

        #Lee (hiển thị slider)
        $this->load->model('banner_model', 'banner');
        $promotion_slider = $this->banner->defineBanner(9);
        $data['promotion_slider'] = $promotion_slider;

        #Lee (hiển thị banner dưới menu)
        $define_banner3 = $this->banner->defineBanner(7);
        $data['group3_banners'] = $define_banner3;

        #Lee (hiển thị thông tin trang cẩm nang)
        $this->load->model('post_model', 'post');
        $post_special = $this->post->defineAllPost(array('post_special' => TRUE));
        $post_4special = $this->post->defineAllPost(array('post_4special' => TRUE));
        $post_featured = $this->post->defineAllPost(array('post_featured' => TRUE));
        $post = $this->post->defineAllPost();
        $data['post_special'] = $post_special;
        $data['post_4special'] = $post_4special;
        $data['post_featured'] = $post_featured;
        $data['post'] = $post;

        #Lee (hiển thị thông tin trang E-Brochure)
        $this->load->model('warehouse_model', 'warehouse');
        $warehouse = $this->warehouse->defineGroupCity();
        $data['cities'] = $warehouse;

        #Lee (hiển thị SEO META - SEOPlugin)
        SEOPlugin::setTitle($object->MetaTitle);
        SEOPlugin::setKeywords($object->MetaKeywords);
        SEOPlugin::setDescription($object->MetaDescription);

        #Lee (hiển thị thông tin toàn cục)
        $this->template->write_view('content', 'pages/templates/' . $segment, $data);
        $this->template->render();
    }

    public function blogDetails() {
        $segment = end($this->uri->segment_array());
        $alias = str_replace('.html', '', $segment);
        $this->load->model('post_model', 'post');
        $data_post = $this->post->getPostRow($alias);

        $post_featured = $this->post->defineAllPost(array('post_featured' => TRUE));
        $post_random = $this->post->defineAllPost(array('random' => TRUE));
        $data['post_featured'] = $post_featured;
        $data['post_random'] = $post_random;
        $data['post'] = $data_post;

        $data['off_menu'] = FALSE;
        $data['page_class'] = 'post-category-page-sub post-detail-page show-nav-page';

        SEOPlugin::setTitle($data_post->Title);
        SEOPlugin::setDescription(($data_post->Description) ? $data_post->Description : strip_tags($data_post->Introtext));
        SEOPlugin::setSocialImage($data_post->Thumb);

        $this->template->write_view('content', 'content_blogdetails', $data);
        $this->template->render();
    }

    public function productDetails() {

        $segment = end($this->uri->segment_array());
        $alias = str_replace('.html', '', $segment);

        #Lee (hiển thị banner dưới slider)
        $this->load->model('banner_model', 'banner');
        $define_banner3 = $this->banner->defineBanner(7);
        $data['group3_banners'] = $define_banner3;

        $data['off_menu'] = FALSE;
        $data['page_class'] = 'product-detail-page show-nav-page';
        $data['js_to_load'] = array("public/auchan/v2/js/jquery.letterpic.min.js", "public/auchan/v2/js/jquery.magnify.js", "public/auchan/v2/js/magnify.details.js");

        $this->load->model('products_model', 'products');
        $params = array(
            'alias' => $alias
        );
        $products = $this->products->defineProductDetails($params);
        $data['item'] = $products;
        if ($products) {
            SEOPlugin::setTitle($products['Title']);
            SEOPlugin::setDescription(($products['Description']) ? $products['Description'] : $products['MetaDescription']);
            SEOPlugin::setSocialImage($products['PrimaryImage']);
            $this->template->write_view('content', 'content_productdetails', $data);
            $this->template->render();
        } else {
            redirect(base_url());
        }
    }

    public function checklink() {

        $segment = end($this->uri->segment_array());
        $alias = str_replace('.html', '', $segment);

        $this->load->model('global_model', 'global');
        $new_url = $this->global->getNewUrl($alias);
        if ($new_url) {
            #Lee (replace old url)
            redirect($new_url);
        } else {
            $this->load->model('post_model', 'post');
            $last_url_check = $this->post->getPostRow($alias);
            if ($last_url_check) {
                $this->blogDetails($last_url_check);
            }
        }
        $this->load->model('products_model', 'products');
        $check = $this->products->getProductRow($alias);
        if ($check) {
            $this->productDetails($check);
        } else {
            $this->load->model('pages_model', 'pages');
            $check = $this->pages->getPageRow($alias);
            if ($check) {
                if ($check->Classname == 'products') {
                    $this->products($check);
                } elseif ($check->Classname == 'news') {
                    $this->news($check);
                } elseif ($check->Classname == 'question_and_answer') {
                    $this->question_and_answer($check);
                } elseif ($check->Classname == 'pages') {
                    $this->pages($check);
                }
            }
        }
    }

    public function preview($id = 0) {
        if (!isset($_SESSION['data'])) {
            $_SESSION['data'] = $_POST;
        }
        print_r($_SESSION['data']);
        return;
        $this->template->add_title("PREVIEW POST");
        $data = array(
            'data' => ""
        );
        $this->template->write_view('content', 'detail', $data);
        $this->template->render();
    }

    public function errorpage() {
        $this->load->view('404');
    }

}

?>