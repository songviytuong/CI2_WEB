<?php

class Ajax extends CI_Controller {

    var $params = array();
    public $limit = 20;
    public $upload_to = '';
    public $classname = "auchan";
    public $seo_default = TRUE;
    public $cur_lang = 'english';
    public $cur_langid = 1;
    public $cur_local = 'en';

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $this->load->library('template');
        $this->template->set_template('auchan');

        $this->load->model('define_model');
        $app_conf = $this->define_model->getDefine();
        foreach ($app_conf as $row) {
            define("{$row['Keyword']}", $row['Value']);
        }

        if (UNDER) :
            #Load Static Config
            $this->load->model('global_model', 'global');
            $detect = $this->global->troubleshoot();

            if ($detect) {
                if (isset($detect['result']) == FALSE) {
                    $data['ip'] = $detect['ip'];
                    $this->template->set_template('under');
                }
            }
        endif;

        #BEGIN: ACTIVE SMARTY
        #$this->load->library('parser');
        #$data['title'] = 'Hello Peace';
        #$this->parser->parse("demo.tpl",$data);

        /*
         * BEGIN: LOAD LANGUAGE
         */
        $langs = $this->session->userdata('languageTT');
        $getLang = (isset($_GET['lang'])) ? trim($_GET['lang']) : '';
        if ($getLang != "") {
            $langrow = $this->languages_model->getLangNameByLocal($getLang);
            $this->session->set_userdata('languageTT', $langrow['LangName']);
        } else {
            $langsite = $this->config->item('language');
            $langrow = $this->languages_model->getLanguageBySession($langsite);
            $this->session->set_userdata('languageTT', $langsite);
        }
        $this->cur_lang = $langrow['LangName'];
        $this->cur_langid = $langrow['LangID'];
        $this->lang->load('translates', $this->classname);

        $this->params = array_merge($_GET, $_POST);
    }
    
    public function subscribe(){
        $email = (isset($_REQUEST['email'])) ? trim($_REQUEST['email']) : '';
        $data_insert = array(
            'Email' => $email,
            'Post' => 1,
            'Created' => date('Y-m-d H:i:m')
        );
        $res = array();
        if($this->db->insert('auc_newsletter',$data_insert)){
            $res['result'] = 'OK';
            $res['msg'] = '<span class="text-success"><small>Cảm ơn Bạn đã đăng ký nhận bản tin Auchan.</small></span>';
        } else {
            $res['msg'] = '<span class="text-danger"><small>Bạn vui lòng nhập đúng địa chỉ Email.</small></span>';
        }
        echo json_encode($res);
    }

    public function send_SMS() {
        $SMS = new SMSPlugin();
        $SMS->setClientUrl('http://210.211.109.118/apibrandname/send?wsdl');
        $SMS->setUsername('auchan');
        $SMS->setPassword('auchanvn!@#');
        $SMS->setBrandname('VTDD');
        $SMS->setMessage($_GET['message']);
        $SMS->setPhone($_GET['phone']);
        $SMS->SendSMS();
    }

    public function getLocation() {
        if ($_GET['position']) {
            $latitude = $_GET['position']['coords']['latitude'];
            $longitude = $_GET['position']['coords']['longitude'];
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://maps.googleapis.com/maps/api/geocode/json?latlng=$latitude,$longitude&sensor=true");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        curl_close($curl);
        echo $result;
    }

    public function getDataMaps() {
        $id = $_REQUEST['id'];
        $this->load->model('ebrochure_model', 'ebrochure');
        $data_brochure = $this->ebrochure->getDetailMaps(array('brochureid' => $id));
        $tmpl = '';
        foreach ($data_brochure as $key => $row) {
            if ($row['Data']) {
                $maps = json_decode($row['Data']);
                $count = count(json_decode($maps->coords));
                $tmpl .= '<map name="mn-shock-price-flyer' . ($key + 1) . '">';
                for ($i = 0; $i < $count; $i++) {
                    $tmpl .= '<area shape="circle" coords="' . json_decode($maps->coords)[$i] . '" alt="' . json_decode($maps->alt)[$i] . '" title="' . json_decode($maps->alt)[$i] . '" href="' . json_decode($maps->href)[$i] . '" />';
                }
                $tmpl .= '</map>';
            }
        }
        $tmpl .= '<div class="row">
    <div class="col-md-12">';
        foreach ($data_brochure as $key => $row) {
            $tmpl .= '<img data-id="aid" src="' . UPLOAD_URL . '/' . $row['Image'] . '" width="100%" alt="' . $row['Name'] . '" data-nicepicture="" usemap="#mn-shock-price-flyer' . ($key + 1) . '" />';
        }

        $tmpl .= '</div>
</div>';
        echo $tmpl;
    }

    public function feedback() {
        switch ($this->params['action']) {
            case 'send':
                $res = array();
                $rest = array();
                $data_req = $this->params['data'];

                foreach ($data_req as $key => $item) {
                    $res[$item['name']] = $item['value'];
                }
                $data = array();
                $data['TypeID'] = $res['feedback_option'];
                $data['Content'] = $res['content'];
                $data['Group'] = $res['group'];
                $data['Fullname'] = $res['fullname'];
                $data['Email'] = $res['email'];
                $data['Phone'] = $res['phone'];
                $data['Url'] = $res['url'];
                $data['Created'] = date('Y-m-d H:i:s');
                $this->load->model('feedback_model', 'feedback');
                $lastid = $this->feedback->addFeedback($data);
                if ($lastid > 0) {
                    echo $this->lang->line('feedback_alert_thanks');
                }
                break;
        }
    }

    public function dataPromotion($params = array()) {
        $this->load->model('products_model', 'products');

        $products_data = $this->products->getAllProductInPromotion();
        $ids = explode(",", $this->params['ids']);
        $arr = array();
        foreach ($products_data as $key => $row) {
            if (in_array($row->PromotionsID, $ids)) {
                $arr[] = $row->ProductsID;
            }
        }
        if ($ids) {
            $params['pid'] = $arr;
        }
//        $params['only_promotions'] = TRUE;
        $params['promotion'] = TRUE;

        $products_data = $this->products->defineAllProducts($id = '', $orderby = 'ID', $sort = 'ASC', $limit = 100, $start = 0, $params);
        $tmpl = array();
        $html = "";
        foreach ($products_data as $key => $item) {
            if (!lazyload) {
                $lazyload = "src='" . $item['main_picture'] . "'";
            } else {
                $lazyload = "src='data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=' data-src='" . $item['main_picture'] . "'";
            }
            $html .= '<div class="col-xs-6 col-sm-4 col-lg-2 product-item">
        <a href="' . $item['url'] . '" class="product-img" title="' . $item['product_name'] . '">
            <img ' . $lazyload . ' class="' . ((lazyload) ? 'lazyload ' : '') . ' img-fluid w-100" alt="' . $item['product_name'] . '" />
        </a>
        ' . (($item['is_saleoff']) ? '<span class="badge badge-danger">' . $item['saleoff_value'] . '</span>' : '') . (($item['is_chimmoi']) ? '<span class="badge-warning badge-right">HOT</span>' : '') . '<a href="nuoc-rua-chen-net-dam-dac-huong-chanh/" class="title-products" title="' . $item['product_name'] . '">' . $item['product_name'] . '</a>
    <p class="unit">' . ($item['unit_text']) . '</p>
    <p class="star"><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star"></i><i class="auc auc-star-empty"></i></p>
    <p class="price"><b>' . $item['price'] . '</b> ' . (($item['social_price']) ? '<span><small>' . $item['social_price'] . '</small></span>' : '') . '</p>
    <a href="' . $item['url'] . '" class="btn btn-primary btn-block btn-sm" title="' . $item['product_name'] . '">
        CHI TIẾT <i class="auc auc-right"></i>    </a>
    </div>';
        }
        $html .= '<script>$("img.lazyload").lazyload();</script>';
        $tmpl['product_items'] = $html;
        $tmpl['description'] = "ABC";
        echo json_encode($tmpl);
    }

    public function data() {
        switch ($this->params['position']) {
            case "getdistrictbycity": //Change City to District
                $this->ListDistrictByCity($this->params);
                break;
            case "getstorelistbydistrict":
                $this->ListStoreByDistrict($this->params);
                break;
            case "getallstorelist":
                $this->ListAllStoreList($this->params);
                break;
            case "getallstorelist2":
                $this->ListAllStoreList2($this->params);
                break;
            case "getstorelistbycity":
                $this->ListStoreListByCity($this->params);
                break;
            case "getstorelistbycity2":
                $this->ListStoreListByCity2($this->params);
                break;
            default:
                $this->ListAllStoreList(-1);
                break;
        }
    }

    function CompleteSearch() {
        $this->load->model('products_model', 'products');
        $keyword = $_GET['key'];
        $count = $this->products->getCountSearch(array('keyword' => $keyword));
        $html = "";
        if ($_GET['position'] == 'search') {
            $arr = $this->products->defineAllProducts('', '', '', $limit = 4, $start = 0, array('keyword' => $keyword));
            if (count($arr) > 0) {
                foreach ($arr as $key => $item) {
                    $html .= '<div class="item-products row">
                            <div class="col-xs-3 col-lg-2"><a href="' . $item['url'] . '"><img src=' . $item['main_picture'] . ' class="img-fluid" alt="' . $item['product_name'] . '" /></a></div>
                            <div class="col-xs-9 col-lg-10">' . (($item['is_saleoff']) ? '<span class="badge badge-danger">OFF ' . $item['saleoff_value'] . '</span>' : '') . '
                            <a href="' . $item['url'] . '" class="title-products">' . $item['product_name'] . '</a>
                                <p class="price"><b>' . $item['price'] . ' </b>' . (($item['social_price']) ? '<span>' . $item['social_price'] . '</span>' : '') . '</p>
                            </div>
                        </div>';
                }
                $html .= '<div class="view-more-search row"><a href="' . base_url() . 'search?q=' . urlencode($keyword) . '"><i class="auc auc-search"></i> Xem thêm' . (($count > $limit) ? ' (' . ($count - $limit) . ') ' : ' ') . 'kết quả cho từ khóa <b>"' . $keyword . '"</b></a></div>';
            }
        }

        echo $html;
    }

    function getListStoreNearYou() {
        $Lat = $_GET['Lat'];
        $Long = $_GET['Long'];

        $this->load->model('warehouse_model', 'warehouse');
        $data_maps = $this->warehouse->defineStoreByLatLong($Lat, $Long);

        $localtions = array();

        foreach ($data_maps as $key => $row) {
            $localtions[$key][] = $row['store_name'];
            $localtions[$key][] = $row['store_lat'];
            $localtions[$key][] = $row['store_long'];
            $localtions[$key][] = $row['store_code'];
            $localtions[$key][] = $row['store_address_map'];
            $localtions[$key][] = $key;
        }
        $localtions = json_encode($localtions);

        $res = array();
        $res['text_holder'] = $this->lang->line('enter_your_address_exactly', 'Vui lòng nhập địa chỉ chính xác của bạn');

        $tmpl = <<<EOT
<script>
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(UserLocation);
    } else {
        //NearestCity(38.8951, -77.0367);
    }    
// Callback function for asynchronous call to HTML5 geolocation
function UserLocation(position) {
    NearestCity(position.coords.latitude, position.coords.longitude);
}
                
                
// Convert Degress to Radians
function Deg2Rad(deg) {
    return deg * Math.PI / 180;
}

function PythagorasEquirectangular(lat1, lon1, lat2, lon2) {
    lat1 = Deg2Rad(lat1);
    lat2 = Deg2Rad(lat2);
    lon1 = Deg2Rad(lon1);
    lon2 = Deg2Rad(lon2);
    var R = 6371; // km
    var x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
    var y = (lat2 - lat1);
    var d = Math.sqrt(x * x + y * y) * R;
    return d;
}
                
    var cities = $localtions;

function NearestCity(latitude, longitude) {
    var mindif = 99999;
    var closest;
    var arr = [];
    var datahtml = '<ul class="list-unstyled" id="loading_storelist_near">';
    for (index = 0; index < cities.length; ++index) {
        var dif = PythagorasEquirectangular(latitude, longitude, cities[index][1], cities[index][2]);
        if (dif < mindif) {
            datahtml += '<li class="showmap add' + cities[index][5] + '">';
            closest = index;
            arr.push(dif);
            datahtml += '<p class="address-location" onclick="GetBranchAdd(\'' + cities[index][3] + '\')" onClick="appendInput(this)" data-id=' + cities[closest][5] + ' data-address="' + cities[closest][4] + '"><span class="auc auc-location"></span>' + cities[closest][0] + ' (<span class="text-danger">' + parseFloat(dif).toFixed(2) + 'km</span>)<br/><span style="font-size:12px">' + cities[closest][4] + '</span></p><a href="javascript:;" onClick="appendInput(this)" data-id=' + cities[closest][5] + ' data-address="' + cities[closest][4] + '" class="find_directions"><i class="auc auc-search"></i><span class="hidden-xs">Đi tới siêu thị</span></a></li>';
        }
        index++;
    }

    datahtml += '</ul>';
    $('#nearme').html(datahtml);
    // echo the nearest city
//    alert(cities[closest] + '---' + mindif);
}
    </script>
EOT;
        $res['script'] = $tmpl;
        echo json_encode($res);
    }

    function findStreet() {
        $from = $_GET['from'];
        $to = $_GET['to'];
        $tmpl = <<<EOT
<script>
    var mapCanvas;
var directionDisplay;
var directionsService;
var stepDisplay;
var markerArray = [];

function initialize(fromadd, toadd) {
    // Instantiate a directions service.
    directionsService = new google.maps.DirectionsService();

    // Create a map and center it on Manhattan.
    var myOptions = {
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    mapCanvas = new google.maps.Map(document.getElementById("map"), myOptions);

    // Create a renderer for directions and bind it to the map.
    var rendererOptions = {
        map: mapCanvas,
        suppressMarkers: true,
        animation: google.maps.Animation.DROP,
    }
    directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions)

    // Instantiate an info window to hold step text.
    stepDisplay = new google.maps.InfoWindow();

    calcRoute(fromadd, toadd);
}

function calcRoute(fromadd, toadd) {

    // First, remove any existing markers from the map.
    for (i = 0; i < markerArray.length; i++) {
        markerArray[i].setMap(null);
    }

    // Now, clear the array itself.
    markerArray = [];

    // Retrieve the start and end locations and create
    // a DirectionsRequest using WALKING directions.
    var start = fromadd;
    var end = toadd;
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };

    // Route the directions and pass the response to a
    // function to create markers for each step.
    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            showSteps(response);
        }
    });
}

function showSteps(directionResult) {
    // For each step, place a marker, and add the text to the marker's
    // info window. Also attach the marker to an array so we
    // can keep track of it and remove it when calculating new
    // routes.
    var myRoute = directionResult.routes[0].legs[0];

    for (var i = 0; i < myRoute.steps.length; i++) {
        var icon = "";
//      var icon = "https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=" + i + "|FF0000|000000";
        if (i == 0) {
            //icon = "https://chart.googleapis.com/chart?chst=d_map_xpin_icon&chld=pin_star|car-dealer|00FFFF|FF0000";
        }


        var marker = new google.maps.Marker({
            position: myRoute.steps[0].start_point,
            map: mapCanvas,
            icon: icon
        });
        //attachInstructionText(marker, myRoute.steps[i].instructions);
        markerArray.push(marker);
    }
    var marker = new google.maps.Marker({
        position: myRoute.steps[i - 1].end_point,
        map: mapCanvas,
        icon: "public/auchan/v2/images/fav/map_pointer.png",
        animation: google.maps.Animation.DROP,
    });
    markerArray.push(marker);
    google.maps.event.trigger(markerArray[0], "click");
}

function attachInstructionText(marker, text) {
    google.maps.event.addListener(marker, 'click', function () {
        // Open an info window when the marker is clicked on,
        // containing the text of the step.
        stepDisplay.setContent(text);
        stepDisplay.open(mapCanvas, marker);
    });
}
                initialize('$from', '$to');
    </script>
EOT;
        echo $tmpl;
    }

    function GetBranchAdd() {
        $this->load->model('warehouse_model', 'warehouse');
        $maps = $this->warehouse->defineStoreByCode($_GET['code'])[0];
        $maps = ($maps) ? $maps : array();

        $lat = $maps['store_lat'];
        $long = $maps['store_long'];
        $title = $maps['store_name'];
        $address = $maps['store_address'];
        $phone = $maps['store_phone'];
        $image = $maps['store_image'];
        $open = $maps['store_open'];

        $tmpl = <<<EOT
<script>
    var infowindow;
    function initialize() {
        var mapOptions = {
            zoom: 18,
            center: new google.maps.LatLng($lat,$long),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        
        var map = new google.maps.Map(document.getElementById('map'),mapOptions);
        rectangle = new google.maps.Rectangle();
	var rectOptions = {
            strokeColor: '#B3D1FF',
            strokeOpacity:1,
            strokeWeight: 1,
            fillColor: '#B3D1FF',
            fillOpacity: 1,
            map: map
        };
        
        rectangle.setOptions(rectOptions);
            setMarkers(map, beaches, message);
        }  
                            
        var beaches = [['$title', $lat,$long, 9999],] 
        var message = ['<div class="maps"><span class="maps_title"><img class="maps_image" src="public/auchan/v2/images/fav/map_pointer.png" alt=""/> $title</span><br/><img src="$image" class="images"/><br/><p class="open-time">Giờ mở cửa: $open</p><div class="info"><span class="maps_address"><span class="auc auc-location"></span> Địa chỉ: $address</span><br/><span class="maps_phone"><span class="auc auc-mobile"></span> Điện thoại: <a href="tel:$phone">$phone<a/></span></div></div>',];
                
    function setMarkers(map, locations,message) {
        var image = {
            url: 'public/auchan/v2/images/fav/map_pointer.png',
            size: new google.maps.Size(26, 26),origin: new google.maps.Point(0,0),anchor: new google.maps.Point(0, 32)
        };

        var shape = {
            coord: [1, 1, 1, 20, 18, 20, 18 , 1],
            type: 'poly'
        };
  
        infowindow = new google.maps.InfoWindow();
  
        for (var i = 0; i < locations.length; i++) {
            var beach = locations[i];
            var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image,
                shape: shape,
                title: beach[0],
                zIndex: beach[3],
                animation: google.maps.Animation.DROP,
        });
	
	infowindow = new google.maps.InfoWindow();
	attachSecretMessage(marker, i,message);
        }
                infowindow.setContent(message[0]);
                infowindow.open(marker.get('map'), marker);
    }

   function attachSecretMessage(marker, num,message) {
        //var message = ['<b>Title</b><br><img src=test.png>', 'is', 'the', 'secret', 'message'];
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(message[num]);
            infowindow.open(marker.get('map'), marker);
        });
    }
    
    //google.maps.event.addDomListener(window, 'load', initialize);
    initialize();
    </script>
EOT;
        echo $tmpl;
    }

    function ListDistrictByCity($object) {
        $this->load->model('warehouse_model', 'warehouse');
        $DT = $this->warehouse->defineListCityByCity($object);
        $html = "";
        $html .= '<option value="-1" disabled="disabled" selected="selected">' . $this->lang->line('select_group_district') . '</option>';
        foreach ($DT as $item) {
            $html .= '<option value="' . $item['district_id'] . '">' . $item['district_name'] . '</option>';
        }
        echo $html;
    }

    function ListStoreByDistrict($object) {
        $this->load->model('warehouse_model', 'warehouse');
        $DT = $this->warehouse->defineStoreListByDistrict($object);
        $html = "";
        foreach ($DT as $item) {
            $phone = $item['store_phone'];
            $phone = $this->global_model->strip_tags($phone);
            $html .= '<li>
            <p class="title-location"><i class="auc auc-shop"> </i>' . $item['store_name'] . '</p>
            <p class="city-location"><span class="badge badge-info">' . $item['store_cityname'] . '</span></p>
            <p class="address-location"><i class="auc auc-location"></i>' . $item['store_address'] . '</p>
            <p class="phone-location"><i class="auc auc-mobile"></i><a href=tel:' . $phone . '>' . $item['store_phone'] . '</a></p>
            <p style="text-align: center;"><a href="' . base_url() . 'he-thong-sieu-thi-auchan/" class="btn-link"><i class="auc auc-pie-chart"></i>Xem bản đồ</a></p>
        </li>';
        }
        echo $html;
    }

    function ListAllStoreList($object = array('id' => -1)) {
        $this->load->model('warehouse_model', 'warehouse');
        $DT = $this->warehouse->defineAllStoreList($object);
        $html = "";
        foreach ($DT as $item) {
            $phone = $item['store_phone'];
            $phone = $this->global_model->strip_tags($phone);
            $html .= '<li>
            <p class="title-location"><i class="auc auc-shop"> </i>' . $item['store_name'] . '</p>
            <p class="city-location"><span class="badge badge-info">' . $item['store_cityname'] . '</span></p>
            <p class="address-location"><i class="auc auc-location"></i>' . $item['store_address'] . '</p>
            <p class="phone-location"><i class="auc auc-mobile"></i><a href=tel:' . $phone . '>' . $item['store_phone'] . '</a></p>
            <p style="text-align: center;"><a href="' . base_url() . 'he-thong-sieu-thi-auchan/" class="btn-link"><i class="auc auc-pie-chart"></i>Xem bản đồ</a></p>
        </li>';
        }
        if ($object['limit']) {
            $html .= '<li><a href="' . base_url() . 'he-thong-sieu-thi-auchan/">Xem thêm siêu thị <i class="auc auc-right"></i></a></li>';
        }
        echo $html;
    }

    function ListAllStoreList2($object) {
        $this->load->model('warehouse_model', 'warehouse');
        $DT = $this->warehouse->defineAllStoreList($object);
        $html = "";
        foreach ($DT as $key => $item) {
            $phone = $item['store_phone'];
            $phone = $this->global_model->strip_tags($phone);
            $html .= '<li class="showmap" onclick="GetBranchAdd(\'' . $item['store_code'] . '\')">
                <p class="title-location"><i class="auc auc-shop"> </i>' . $item['store_name'] . '</p>
                <p class="city-location"><span class="badge badge-info">' . $item['store_cityname'] . '</span></p>
                <p class="address-location"><span class="auc auc-location"></span>' . $item['store_address'] . '</p>
                <p class="phone-location"><i class="auc auc-mobile"></i><a href=tel:' . $phone . '>' . $item['store_phone'] . '</a></p>
                <!--a onClick="click_location(this)" data-index="' . $key . '" data-lat="20.9813661" data-lng="105.78900329999999"><i class="fa fa-angle-right"></i>Xem vị trí</a-->
            </li>';
        }
        echo $html;
    }

    function ListStoreListByCity($object) {
        $this->load->model('warehouse_model', 'warehouse');
        $DT = $this->warehouse->defineStoreListByCity($object);
        $html = "";
        foreach ($DT as $item) {
            $phone = $item['store_phone'];
            $phone = $this->global_model->strip_tags($phone);
            $html .= '<li>
            <p class="title-location"><i class="auc auc-shop"> </i>' . $item['store_name'] . '</p>
            <p class="city-location"><span class="badge badge-info">' . $item['store_cityname'] . '</span></p>
            <p class="address-location"><i class="auc auc-location"></i>' . $item['store_address'] . '</p>
            <p class="phone-location"><i class="auc auc-mobile"></i><a href=tel:' . $phone . '>' . $item['store_phone'] . '</a></p>
            <p style="text-align: center;"><a href="" class="btn-link"><i class="auc auc-pie-chart"></i>Xem bản đồ</a></p>
        </li>';
        }
        if ($object['limit']) {
            $html .= '<li><a href="' . base_url() . 'he-thong-sieu-thi-auchan/">Xem thêm siêu thị <i class="auc auc-right"></i></a></li>';
        }
        echo $html;
    }

    function ListStoreListByCity2($object) {
        $this->load->model('warehouse_model', 'warehouse');
        $DT = $this->warehouse->defineStoreListByCity($object);

        $zoom = ($object['id'] == -1) ? 6 : 12;

        $localtions = array();
        foreach ($DT as $key => $row) {
            $localtions[$key][] = $row['store_name'];
            $localtions[$key][] = $row['store_lat'];
            $localtions[$key][] = $row['store_long'];
            $localtions[$key][] = $row['store_code'];
            $localtions[$key][] = $row['store_address_map'];
            $localtions[$key][] = $key;
        }
        $localtions = json_encode($localtions);
        $messages = array();
        foreach ($DT as $key => $row) {
            $messages[] = '\'<div class="maps"><span class="maps_title"><img class="maps_image" src="public/auchan/v2/images/fav/map_pointer.png" alt=""/> '
                    . $row['store_name'] . '</span><br/><p class="open-time">'
                    . 'Giờ mở cửa: ' . $row['store_open'] . '</p>'
                    . '<img src="' . $row['store_image'] . '" class="images"/><br/><div class="info"><span class="maps_address"><span class="auc auc-location"></span> '
                    . 'Địa chỉ: ' . $row['store_address'] . '</span><br/><span class="maps_phone"><span class="auc auc-mobile"></span> '
                    . 'Điện thoại: <a href="tel:' . $row['store_phone'] . '">'
                    . $row['store_phone'] . '<a/></span></div></div>\'';
        }
        $messages = implode(", ", $messages);

        $res[] = array();

        $html = "";
        foreach ($DT as $key => $item) {
            $html .= '<li class="showmap" onclick="GetBranchAdd(\'' . $item['store_code'] . '\')">
                <p class="title-location"><i class="auc auc-shop"> </i>' . $item['store_name'] . '</p>
                <p class="city-location"><span class="badge badge-info">' . $item['store_cityname'] . '</span></p>
                <p class="address-location"><span class="auc auc-location"></span>' . $item['store_address'] . '</p>
                <!--a onClick="click_location(this)" data-index="' . $key . '" data-lat="20.9813661" data-lng="105.78900329999999"><i class="fa fa-angle-right"></i>Xem vị trí</a-->
            </li>';
        }

        $res['content'] = $html;

        $tmpl = <<<EOT
<script>
    function initialize() {
    var locations = $localtions;

    window.map = new google.maps.Map(document.getElementById('map'), {
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();
    var message = [$messages];

    var bounds = new google.maps.LatLngBounds();
    var image = {
            url: 'public/auchan/v2/images/fav/map_pointer.png',
            size: new google.maps.Size(26, 26), origin: new google.maps.Point(0, 0), anchor: new google.maps.Point(0, 32)
    };
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
            icon: image,
            animation: google.maps.Animation.DROP,
        });

        bounds.extend(marker.position);

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(message[i]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }

    map.fitBounds(bounds);

    var listener = google.maps.event.addListener(map, "idle", function () {
        map.setZoom($zoom);
        google.maps.event.removeListener(listener);
    });
}
    initialize();
    </script>
EOT;
        $res['script'] = $tmpl;
        echo json_encode($res);
    }

}

?>