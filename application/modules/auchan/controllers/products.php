<?php

class Products extends CI_Controller {

    public $limit = 20;
    public $upload_to = '';
    public $classname = "auchan";
    public $seo_default = TRUE;
    public $cur_lang = 'english';
    public $cur_langid = 1;
    public $cur_local = 'en';

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $this->load->library('template');
        $this->template->set_template('auchan');

        $this->load->model('define_model');
        $app_conf = $this->define_model->getDefine();
        foreach ($app_conf as $row) {
            define("{$row['Keyword']}", $row['Value']);
        }

        if (UNDER) :
            #Load Static Config
            $this->load->model('global_model', 'global');
            $detect = $this->global->troubleshoot();

            if ($detect) {
                if (isset($detect['result']) == FALSE) {
                    $data['ip'] = $detect['ip'];
                    $this->template->set_template('under');
                }
            }
        endif;

        #BEGIN: ACTIVE SMARTY
        #$this->load->library('parser');
        #$data['title'] = 'Hello Peace';
        #$this->parser->parse("demo.tpl",$data);

        /*
         * BEGIN: LOAD LANGUAGE
         */
        $langs = $this->session->userdata('languageTT');
        $getLang = (isset($_GET['lang'])) ? trim($_GET['lang']) : '';
        if ($getLang != "") {
            $langrow = $this->languages_model->getLangNameByLocal($getLang);
            $this->session->set_userdata('languageTT', $langrow['LangName']);
        } else {
            $langsite = $this->config->item('language');
            $langrow = $this->languages_model->getLanguageBySession($langsite);
            $this->session->set_userdata('languageTT', $langsite);
        }
        $this->cur_lang = $langrow['LangName'];
        $this->cur_langid = $langrow['LangID'];
        $this->lang->load('translates', $this->classname);

        #Load Static Config
        $this->load->model('static_model');
        $static_config = $this->static_model->getConfig();

        #Lee (hiển thị menu categories)
        $this->load->model('categories_model');
        $define_categories = $this->categories_model->defineAllCategories();

        #Lee (hiển thị số lượng sản phẩm Promotion, mới MenuLeft)
        $this->load->model('products_model', 'products');
        $counter_promotion = $this->products->getProductCounter('SocialPrice');
        $counter_newest = $this->products->getProductCounter('Chimmoi');

        $data = array(
            "main_menu" => $define_categories,
            "counter_promotion" => $counter_promotion, //in MenuLeft
            "counter_newest" => $counter_newest, //in MenuLeft
        );
        $data['js_to_load'] = array();
        $this->template->write_view('header', 'header', $data);
        $this->template->write_view('footer', 'footer', $data);
        $this->template->add_doctype();
    }

    public function productCategories() {
        $data['page_class'] = 'product-cate-page categories-page show-nav-page';
        $data['off_menu'] = FALSE;

        #Load Model 
        $this->load->model('categories_model', 'menu');

        #Show List Product by Each Categories
        $ParentSubUrl = $this->uri->segment(1);
        $ParentAlias = $this->uri->segment(2);

        #Get Banner From CategoryID
        $this->load->model('categories_model', 'categories');
        $data['MainPicture'] = $this->categories->getFromAlias('Banner', $ParentAlias);

        #Get MenuTitle
        $data['MenuTitle'] = $this->menu->getFromAlias('Title', $ParentAlias);

        #Show Sub_Categories by Parent
        $Sub_Categories = $this->menu->defineSubCategoriesByAlias($ParentAlias);
        $data['Sub_Categories'] = $Sub_Categories;

        $data['current_url'] = $this->uri->uri_string();

        #Lee (hiển thị banner dưới slider)
        $this->load->model('banner_model', 'banner');
        $define_banner3 = $this->banner->defineBanner(7);
        $data['group3_banners'] = $define_banner3;

        #SEO

        $this->template->write_view('content', 'content_productcategories', $data);
        $this->template->render();
    }

    public function productList() {

        $old_url = $this->uri->segment(2);
        $this->load->model('global_model', 'global');
        $new_url = $this->global->getNewUrl($old_url);

        $arr = $this->uri->segment_array();
        if (count($arr) > 3) {
            redirect(ROOT_URL);
        } else if ($new_url) {
            redirect($new_url);
        }

        #Lee (hiển thị banner dưới slider)
        $this->load->model('banner_model', 'banner');
        $define_banner5 = $this->banner->defineBanner(5);
        $define_banner6 = $this->banner->defineBanner(6);
        $define_banner7 = $this->banner->defineBanner(7);
        $define_banner8 = $this->banner->defineBanner(8);

        $data = array(
            'group1_banners' => $define_banner5,
            'group2_banners' => $define_banner6,
            'group3_banners' => $define_banner7,
            'group4_banners' => $define_banner8,
        );

        #Show List Product by Each Categories
        $ParentSubUrl = $this->uri->segment(1);
        $ParentAlias = $this->uri->segment(2);

        #Get Banner From CategoryID
        $this->load->model('categories_model', 'categories');
        $data['MainPicture'] = $this->categories->getFromAlias('Banner', $ParentAlias);

        #Get MenuTitle
        $data['MenuTitle'] = $this->categories->getFromAlias('Title', $ParentAlias);

        #Show Sub_Categories by Parent
        $Sub_Categories = $this->categories->defineSubCategoriesByAlias($ParentAlias);
        $data['Sub_Categories'] = $Sub_Categories;

        $cid = $this->categories->getFromAlias("ID", $ParentAlias);
        $data['cid'] = $cid;

        $data['page_class'] = 'product-cate-page sub-categories-page show-nav-page';
        $data['js_to_load'] = array();
        $data['off_menu'] = FALSE;

        $this->template->write_view('content', 'content_productlist', $data);
        $this->template->render();
    }

}
