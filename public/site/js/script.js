var base_url = "";
var page_order = 1;

$('#box-item-order').on('scroll', function() {
    if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
        $.post(base_url+"next_order",{page:page_order},function(result){
            $('#box-item-order').append(result);
            page_order++;
        },"html");
    }
});

function event_form(ob,form,action){
    $("#"+form).find("#error-box").addClass("hidden");
    var html = $(ob).html();
    $(ob).html("Loading...");
    $.post(base_url+action,$("#"+form).serializeArray(),function(result){
        $(ob).html(html);
        if(result.error==true){
            $("#"+form).find("#error-box").removeClass("hidden");
            $("#"+form).find("#error-box").find(".alert span").html(result.message);
            $("#"+form).find("input[name='"+result.focus+"']").focus();
            if(result.focus=="Captcha"){
                $("#"+form).find("#imgcapt").html(result.image);
            }
        }else{
            $("#"+form).html("<img src='public/site/images/register/"+result.gift+".png' style='margin:15px auto' class='img-responsive' />"+result.message).addClass("alert alert-success");
        }
    },'json');
}

function load_box_check_order(ob,action){
    $.get(base_url+action,function(result){
        $("#order-modal").html(result);
    });
    $("#order-modal").modal("show");
}

function check_coupon_code(ob,action){
    $(ob).addClass("saving");
    var total = $("#order-modal").find("#total").attr("total");
    var fee = $("#order-modal").find("#fee").attr("fee");
    var coupon = $("#order-modal").find("#coupon_code").val();
    var status = $("#order-modal").find("#coupon_code").attr("readonly");
    if(status!="readonly" && coupon!=''){
        $.post(base_url+action,{fee:fee,total:total,coupon:coupon},function(result){
            $(ob).removeClass("saving");
            if(result.error==true){
                $(result.focus).focus();
                $("#order-modal").find("#coupon_message").removeClass("hidden").find('.alert').html(result.message);
            }else{
                $("#order-modal").find("#coupon_code").attr("readonly",true);
                $("#order-modal").find("#coupon_code").attr("coupon",result.coupon);
                $("#order-modal").find("#coupon_message").removeClass("hidden").find('.alert').addClass("alert-success").removeClass("alert-danger").html(result.message);
                getfee($("#order-modal").find("#selectDistrict"),"getfee");
            }
        },'json');
    }
}

function getfee(ob,action,totala){
    var total = $("#order-modal").find("#total").attr("total");
    var coupon = $("#order-modal").find("#coupon_code").attr("coupon");
    if($(ob).val()>0){
        $.post(base_url+action,{district:$(ob).val(),total:total,coupon:coupon},function(result){
            $("#order-modal").find("#fee").attr("fee",result.number_fee).removeClass("hidden").find('.alert').html(result.fee);
        },'json');
    }
}

function check_order(ob,form,action){
    $("#order-modal").find("#error-box-cart").addClass("hidden");
    $(ob).addClass("saving");
    $.post(base_url+action,$("#"+form).serializeArray(),function(result){
        $(ob).removeClass("saving");
        if(result.error==true){
            $("#order-modal").find("#error-box").removeClass("hidden");
            $("#order-modal").find("#error-box").find(".alert span").html(result.message);
            $("#order-modal").find("#"+form).find("input[name='"+result.focus+"']").focus();
        }else{
            $("#order-modal").load(base_url+"load_order_details");
        }
    },'json');
}

function add_to_cart(ob,action,id,amount){
    $.post(base_url+action,{products:id,amount:amount},function(result){
        $.get(base_url+"load_order",function(result){
            $("#order-modal").html(result);
        });
    },"json");
    $("#order-modal").modal("show");
}

function remove_cart(ob,action,id){
    $.post(base_url+action,{products:id},function(result){
        if(result.error==false){
            $.get(base_url+"load_order",function(result){
                $("#order-modal").html(result);
            });
        }else{
            $("#order-modal").find("#error-box").addClass("hidden");
            $("#order-modal").find("#error-box-cart").removeClass("hidden");
            $("#order-modal").find("#error-box-cart span").html(result.message);
        }
    },"json");
}

function set_address(ob,city,district){
    $("#order-modal").find("#selectCity").val(city);
    $.post(base_url+"load_district_from_city",{CityID:city},function(result){
        $("#order-modal").find("#selectDistrict").html(result);
        $("#order-modal").find("#selectDistrict").val(district);
    },"html");
    $("#order-modal").find("#AddressOrder").val($(ob).parent("label").text().trim());
}

function get_district(ob){
    var city = $(ob).val();
    $.post(base_url+"load_district_from_city",{CityID:city},function(result){
        $("#order-modal").find("#selectDistrict").html(result);
        $("#order-modal").find("#selectDistrict").change();
    },"html");
}

function tab(ob,tab){
    $("#order-modal").find(".tab").addClass("hidden");
    $("#order-modal").find("#tab"+tab).removeClass("hidden");
}

function send_order(ob,form,action){
    $("#order-modal").find("#error-box-cart").addClass("hidden");
    $(ob).addClass("saving");
    $.post(base_url+action,$("#"+form).serializeArray(),function(result){
        $(ob).removeClass("saving");
        if(result.error==true){
            $("#order-modal").find("#error-box").removeClass("hidden");
            $("#order-modal").find("#error-box").find(".alert span").html(result.message);
            $("#"+form).find("input[name='"+result.focus+"']").focus();
        }else{
            $("#order-modal").load(base_url+"thanks_page");
        }
    },'json');
}

function change_amount_cart(ob,action,id){
    var amount = parseInt($(ob).val());
    add_to_cart(ob,action,id,amount);
}

function send_comment(ob,form,action){
    $(ob).addClass("saving");
    $.post(base_url+action,$("#"+form).serializeArray(),function(result){
        $(ob).removeClass("saving");
        if(result.error==true){
            $("#"+form).find("#box-alert-comment").removeClass("hidden");
            $("#"+form).find("#box-alert-comment").find(".alert span").html(result.message);
            $("#"+form).find("input[name='"+result.focus+"']").focus();
        }else{
            $("#"+form).find("#box-alert-comment").addClass("hidden");
            $("#"+form).find("input[type='text']").val('');
            $("#"+form).find("textarea").val('');
            $("#comment-modal").modal("hide");
            $.post(base_url+'load_comment',{FunnelID:1},function(result){
               $(".comment-content").html(result);
            });
        }
    },'json');
}

function send_callme(ob,form,action){
    $(ob).addClass("saving");
    $.post(base_url+action,$("#"+form).serializeArray(),function(result){
        $(ob).removeClass("saving");
        if(result.error==true){
            $("#"+form).find("#box-alert-callme").removeClass("hidden");
            $("#"+form).find("#box-alert-callme").find(".alert span").html(result.message);
            $("#"+form).find("input[name='"+result.focus+"']").focus();
        }else{
            $("#"+form).find("#box-alert-callme").removeClass("hidden").find('.alert').removeClass("alert-danger").addClass("alert-success").html(result.message);
            $("#"+form).find("input[type='text']").val('');
            $("#"+form).find("textarea").val('');
        }
    },'json');
}

function show_more_comment(ob){
    $(ob).hide();
    $(ob).parents('div.comment-content').find(".hidden").removeClass("hidden");
}

function click_position(ob,action,position,funel,showform){
    $.post(base_url+action,{position:position,funel:funel,title:$(ob).attr('title')},function(result){
        if(showform!=''){
            $(showform).modal('show');
        }
    },'json');
}
