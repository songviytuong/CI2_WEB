class ERPPlugin{
	constructor(ob) {
	    this.URLExcute = $(ob).attr('erp-url-excute');
	    this.ObjectExcute = $(ob).attr('erp-object-excute');
	    this.URLNextExcute = $(ob).attr('erp-url-next-excute');
	    this.ObjectNextExcute = $(ob).attr('erp-object-excute');
	    this.MessageObject = $(ob).attr('erp-object-message');
	    this.PreviewDataTo = $(ob).attr('erp-object-preview');
	    this.URLDone = $(ob).attr('erp-url-done');
	    this.Data = {};
	    this.Method = "post";
	    this.object = ob;
	    $(this.MessageObject).addClass("hidden").html('');
	}

	submitform(){
		if(this.URLExcute!=''){
			var href = this.URLDone;
			var MessageObject = this.MessageObject;
			var ob = this.object;
			var URLNextExcute = this.URLNextExcute;
			this.Data = $(this.ObjectExcute).serialize();
			$(ob).find("i").hide();
			$(ob).addClass('saving');
			$.post(this.URLExcute,this.Data,function(result){
				$(ob).find("i").show();
				$(ob).removeClass('saving');
				if(result.error==false){
					if(URLNextExcute!='' && typeof(URLNextExcute)!='undefined'){
						console.log("URLNext="+URLNextExcute);
					}else{
						console.log("href = "+href);
						window.location = href;
					}
				}else{
					for( var i=0;i<result.message.length;i++){
						$(MessageObject).append("<p>- "+result.message[i]+"</p>");
					}
					$(MessageObject).removeClass("hidden");
				}
			},"json");
		}else{
			console.log("Missing URLExcute.");
		}
	}

	submitform_previewdata(){
		if(this.URLExcute!=''){
			var href = this.URLDone;
			var ob = this.object;
			var PreviewDataTo = this.PreviewDataTo;
			this.Data = $(this.ObjectExcute).serialize();
			$(ob).find("i").hide();
			$(ob).addClass('saving');
			$.post(this.URLExcute,this.Data,function(result){
				$(ob).find("i").show();
				$(ob).removeClass('saving');
				$(PreviewDataTo).html(result);
			},"html");
		}else{
			console.log("Missing URLExcute.");
		}
	}
}