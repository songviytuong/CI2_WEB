$(document).ready(function () {
    $(window).bind("load", function () {
        // Scroll-Then-Fix Top header  		
        $(window).scroll(function () {
            if ($(this).scrollTop() > 200) {

                $('.main-head').addClass('header-sticky');
                $('.side').addClass('header-sticky');
                $('.side').addClass('fix');
                $('.e-brochure').addClass('fix-e-brochure');
                if ($(window).width() > 992) {
                    // addClass main-cate-nav-sticky when hover
                    if ($('body').hasClass('show-nav-page')) {
                        $('.main-head').addClass('header-nav-sticky'); //Show Categories
                        var navWidth = $('.main-cate-nav').width();
                        $('.main-cate-nav').width(navWidth);

                        $('.navbar-toggler').hover(function () {
                            $('.main-cate-nav').addClass('main-cate-nav-sticky');
                            $('.side').removeClass('fix');
                        });
                        $('.main-cate-nav').mouseleave(function () {
                            //console.log('aaa');
                            //$('.main-cate-nav').removeClass('main-cate-nav-sticky');
                            //$('.side').addClass('fix');
                        });
                    }

                }
            } else {

                $('.main-head').removeClass('header-sticky');
                $('.side').removeClass('header-sticky');
                $('.side').removeClass('fix');
                $('.e-brochure').removeClass('fix-e-brochure');
                $('.main-cate-nav').removeClass('main-cate-nav-sticky');
            }



        });

        // Fix main-nav
        if ($(window).width() >= 768) {
            $('ul.nav li.dropdown').hover(function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(300);
            }, function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(300);
            });
        }


        // Auto pop-up promoModal
        // $('#recruitment-modal').modal('show')

        // Enable tooltips everywhere
        // $(function () {
        // $('[data-toggle="tooltip"]').tooltip()
        // })

        // scrollUp
        var scrollButton = $('#scrollUp');

        // Click on scrollUp button
        scrollButton.click(function () {
            $('html,body').animate({scrollTop: 0}, 600);

        });



    });
});

