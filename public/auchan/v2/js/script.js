$(document).ready(function(){
	$(window).bind("load", function() {


		// Scroll-Then-Fix Top header  		
		$(window).scroll(function() {
			if ($(this).scrollTop() > 300){  
				// console.log('abc');
				$('.main-head').addClass('header-sticky');	
				if ($(window).width() > 992){
					var navWidth = $('.main-cate-nav').width();
					$('.main-cate-nav').width(navWidth);
					$('.main-cate-nav').addClass('main-cate-nav-sticky');

					// addClass main-cate-nav-sticky when hover
					// if ($('body').hasClass('show-nav-page')) {
					// 	$('.main-head').addClass('header-nav-sticky');
					// 	var navWidth = $('.main-cate-nav').width();
					// 	$('.main-cate-nav').width(navWidth);

					// 	$('.navbar-toggler').hover(function() {
					// 	  $('.main-cate-nav').addClass('main-cate-nav-sticky');
					// 	});
		   //      $('.main-cate-nav').mouseleave(function() {
					//     $('.main-cate-nav').removeClass('main-cate-nav-sticky');
					//   });
					// }
						
				}
			}
			else{
				$('.main-head').removeClass('header-sticky');
				$('.main-cate-nav').removeClass('main-cate-nav-sticky');
			}
		}); 



		if($('body').hasClass('e-brochure-page')) {
			// Instantiate the Bootstrap carousel
			$('.multi-item-carousel').carousel({
			  interval: 4000
			});

			// for every slide in carousel, copy the next slide's item in the slide.
			// Do the same for the next, next item.
			$('.multi-item-carousel .carousel-item').each(function(){
			  var next = $(this).next();
			  if (!next.length) {
			    next = $(this).siblings(':first');
			  }
			  next.children(':first-child').clone().appendTo($(this));
			  
			  for (var i=0;i<2;i++) {
			    next=next.next();
			    if (!next.length) {
			    	next = $(this).siblings(':first');
			  	}			    
			    next.children(':first-child').clone().appendTo($(this));
			  }
			});
		}



		// Scroll-Then-Fix Top header  
		// if ($(window).width() <= 768) {
		// 	$(window).scroll(function() {
		// 		if ($(this).scrollTop() > 100){  
		// 			// console.log('abc');
		// 			$('.main-head').addClass('header-sticky');
		// 		}
		// 		else{
		// 			$('.main-head').removeClass('header-sticky');
		// 		}
		// 	}); 
		// } 
		

		


		// Fix main-nav
		if ($(window).width() >= 768) {
			$('ul.nav li.dropdown').hover(function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(300);
			}, function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(300);
			});
		}


		// Auto pop-up promoModal
		// $('#promoModal').modal('show')

		// Enable tooltips everywhere
		// $(function () {
		  // $('[data-toggle="tooltip"]').tooltip()
		// })

		// scrollUp
		var scrollButton = $('#scrollUp');
		// var bottomFuntion = $('.bottom-function');
		// $(window).scroll(function(){
		// 	$(this).scrollTop() >= 1000 ? bottomFuntion.show() : bottomFuntion.hide();			
		// 	// console.log($(this).scrollTop());
		// });

		// Click on scrollUp button
		scrollButton.click(function(){
			// console.log("click");
			$('html,body').animate({scrollTop:0},600);
			
		});

		

	});
});