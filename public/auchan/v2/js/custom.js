$(document).ready(function () {

    $("#xemthem").click(function () {
        $(".post-content-wrap").toggleClass("tab-content-small");
        if ($(".post-content-wrap").hasClass("tab-content-small")) {
            $("#xemthem").text("Mở rộng");
            $("html,body").animate({scrollTop: 300}, 1000);
        } else {
            //$("#xemthem").text("Thu gọn");
            $("#xemthem").remove();
            $(".dis").removeClass('boxArticle');
        }
    });




    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
                backToTop = function () {
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('#back-to-top').addClass('show');
                    } else {
                        $('#back-to-top').removeClass('show');
                    }
                };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    setTimeout(function () {
        /*Load Page Trang chủ*/
        loadAllStoreList(-1);
        /*Load Page Hệ thống siêu thị*/
        loadAllStoreList2(-1);
        loadStoreListByCity2(-1);
    }, 100);

    $('#group_city').change(function () {
        var city_id = $(this).val();
        $.ajax({
            url: "/auchan/ajax/data",
            dataType: "html",
            type: "POST",
            data: {position: 'getdistrictbycity', id: city_id},
            context: $(this),
            success: function (result) {
                setTimeout(function () {
                    $('#group_district').html(result);
                    loadStoreListByCity(city_id);
                    loadStoreListByCity2(city_id);
                }, 300);
            }
        });
    });

    $('#group_district').change(function () {
        var district_id = $(this).val();
        $.ajax({
            url: "/auchan/ajax/data",
            dataType: "html",
            type: "POST",
            data: {position: 'getstorelistbydistrict', id: district_id},
            context: $(this),
            success: function (result) {
                $('#loading_storelist').html(result);
                setTimeout(function () {

                });
            }
        });
    });
});

$('.btn_subscribe').click(function () {
    var email = $('#email_subscribe').val();
    if (email == "") {
        $('#email_subscribe').focus();
        return false;
    } else {
        if (!check_valid_email(email)) {
            alert('Email nhập vào không đúng.');
            $('#email_subscribe').select();
            return false;
        } else {
            $.ajax({
                url: "/auchan/ajax/subscribe",
                dataType: "JSON",
                type: "POST",
                data: {email: email},
                context: $(this),
                success: function (data) {
                    if (data.result == 'OK') {
                        $(':input').val('');
                        $('.result_subscribe').html(data.msg);
                    }
                }
            });
        }
    }
});

$('.openMaps').click(function () {
    var obj = $(this).attr('data-bind');
    $.ajax({
        url: "/auchan/ajax/getDataMaps",
        dataType: "html",
        type: "POST",
        data: {id: obj},
        context: $(this),
        success: function (result) {
            $('.showmaps').html(result);
            scrollToAnchor('aid');
        }
    });
});

function scrollToAnchor(aid) {
    var aTag = $("img[data-id='" + aid + "']");
    $('html,body').animate({scrollTop: aTag.offset().top - 130}, 'slow');
}

$('.nav-item').click(function () {
    $('.showmaps').html('');
});

function loadAllStoreList($obj) {
    var city_id = $obj;
    $.ajax({
        url: "/auchan/ajax/data",
        dataType: "html",
        type: "POST",
        data: {position: 'getallstorelist', id: city_id, limit: 2},
        context: $(this),
        success: function (result) {
            $('#loading_storelist').html(result);
        }
    });
}

function loadAllStoreList2($obj) {
    var city_id = $obj;
    $.ajax({
        url: "/auchan/ajax/data",
        dataType: "html",
        type: "POST",
        data: {position: 'getallstorelist2', id: city_id, limit: 100, type: 2},
        context: $(this),
        success: function (result) {
            $('#loading_storelist2').html(result);
        }
    });
}

function loadStoreListByCity2($obj) {
    var city_id = $obj;
    $.ajax({
        url: "/auchan/ajax/data",
        dataType: "json",
        type: "POST",
        data: {position: 'getstorelistbycity2', id: city_id, limit: 100},
        context: $(this),
        success: function (result) {
            $('#loading_storelist2').html(result.content);
            $('#getscript').html(result.script);
        }
    });
}

function loadStoreListByCity($obj) {
    var city_id = $obj;
    $.ajax({
        url: "/auchan/ajax/data",
        dataType: "html",
        type: "POST",
        data: {position: 'getstorelistbycity', id: city_id, limit: 100},
        context: $(this),
        success: function (result) {
            $('#loading_storelist').html(result);
        }
    });
}
$('#placeholder-type-writter').mouseup(function () {
    $('.completesearch').html('');
});
$('#placeholder-type-writter').keyup(function () {
    var len = $(this).val().length;
    if (len >= 3) {
        $.ajax({
            url: "/auchan/ajax/CompleteSearch",
            dataType: "html",
            data: {position: 'search', key: $(this).val()},
            context: $(this),
            success: function (result) {
                $('.completesearch').html('<div class="auto-complete-search-box">' + result + '</div>');
            }
        });
        return false;
    } else {
        $('.completesearch').html('');
    }

});

$('.send_feedback').click(function () {
    var type = $('#feedback_option');
    var content = $('#content');
    if (type.val() == -1) {
        type.focus();
        $('.result').css('display', 'block');
        return false;
    } else if (content.val() == "") {
        content.focus();
        $('.result').css('display', 'block');
        return false;
    } else {

        $.ajax({
            url: "/auchan/ajax/feedback",
            dataType: "html",
            data: {action: 'send', data: $("form").serializeArray()},
            context: $(this),
            success: function (result) {
                $('.result').css('display', 'block');
                $('.result').html(result);
                $(this).closest('form').find("input[type=text], textarea").val("");
                location.reload().delay(3000);
            }
        });
    }
});

$('#btnGroupAddon2').click(function () {
    var txtSearch = $('input[name="search"]').val();
    txtSearch = txtSearch.replace(' ', '+');
    window.location.href = "/search?q=" + encodeURI(txtSearch);
});

$('input[name="search"]').keypress(function (e) {
    if (e.which == 13) {
        $('#btnGroupAddon2').click();
    }
});

/*
 * Hiển thị khi click vào Store
 */
function GetBranchAdd(obj) {
    $.ajax({
        url: "/auchan/ajax/GetBranchAdd",
        dataType: "html",
        data: {code: obj},
        context: $(this),
        success: function (r) {
            $('#getscript').html(r);
        }
    });
}
/*
 * Hiển thị khi click vào danh sách gần bạn
 */
function getListStoreNearYou() {
    $('#your_address').remove();
    getLocation(function (position) {
        $.ajax({
            url: "/auchan/ajax/getListStoreNearYou",
            dataType: "json",
            data: {
                Lat: position.coords.latitude,
                Long: position.coords.longitude
            },
            cache: !0,
            context: $(this),
            success: function (r) {
                $('<input type="text" name="your_address" value="" id="your_address" placeholder="' + r.text_holder + '" class="form-control">').insertBefore("#nearme");
                $('#getscript').html(r.script);
            }
        });
    });
}

//example
$(document).ready(function () {
    if (navigator.geolocation) {
        getLocation(function (position) {
            $.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + ',' + position.coords.longitude + '&sensor=true', function (data) {
                if (data.status == "OK") {
//                    console.log(data);
                    $('#myLocal').html(data.results[0].formatted_address);
                    $('#myLocal').attr('data-add', data.results[0].formatted_address);
                    $('.store_near').removeClass("shownearme");
                    return false;
                }
            });
        });
    }

//    $('.store_near').removeClass("shownearme");
//    $('.store_near').removeAttr("onClick");
//    $('.store_near').html('Chưa xác định được vị trí của bạn');
//    $('.store_near').addClass('text-danger');

});

////////////////////////////////////////////////
function appendInput(obj) {
    var html = '<li class="enter_address" style="display:none;"><div class="form-control findto"></div></li>';
    $('.enter_address').remove();
    $('.showmap').css("background-color", '');

    $(html).insertAfter(".add" + $(obj).attr('data-id'));
    $(".add" + $(obj).attr('data-id')).css("background-color", 'rgba(255, 186, 81, 0.12)');
    $('.findto').attr("data-add", $(obj).attr('data-address'));
    $('.findto').html($(obj).attr('data-address') + '<div class="go_map"><a href="javascript:;" onClick="findStreet();" class="btn btn-sm text-success"><i class="auc auc-search" style="display:none;"></i></a></div>');
    findStreet();
}

function findStreet() {
    var fromadd = $('#your_address').val();
    var toadd = $('.findto').attr('data-add');
    if (fromadd == '') {
        fromadd = $('#myLocal').html();
    }
    $.ajax({
        url: "/auchan/ajax/findStreet",
        dataType: "html",
        data: {from: fromadd, to: toadd},
        context: $(this),
        success: function (r) {
            $('#getscript').html(r);
        }
    });
}

function send_SMS() {
    var phone = '0908689106';
    var message = 'Auchan - It is raining now means that it is raining right now and might continue or might not.';
    $.ajax({
        url: "/auchan/ajax/send_SMS",
        dataType: "html",
        data: {phone: phone, message: message},
        success: function (data) {
            console.log(data);
//            if (data.error == 0) {
//                alert("Đã gửi SMS thành công !");
//            } else {
//                alert("Có lỗi xảy ra trong quá trình gửi SMS .");
//            }
        }
    });
}

///////////////////////////////////// Promotions

$("#checkboxes").delegate("input[type='checkbox']", 'change', function () {
    var arr_sort = [];
    var listProductsCtrl = $('input[name=filter_by_promotion]');
    listProductsCtrl.html(''); //reset the values here
    $('#checkboxes input[type="checkbox"]:checked').each(function () {
        arr_sort.push($(this).attr('data-bind'));
        var showtimesAsString = arr_sort.join(', ');
        listProductsCtrl.html(showtimesAsString);
    });
//    console.log(arr_sort);
    $('.ids').attr('data-id', arr_sort);
    $.ajax({
        type: 'POST',
        dataType: "json",
        beforeSend: function () {
            $('.ajax-loader').css("visibility", "visible");
        },
        url: '/auchan/ajax/dataPromotion',
        data: {ids: $('.ids').attr('data-id')},
        success: function (result) {
            $('.showproduct').html(result.product_items);
        },
        complete: function () {
            $('.ajax-loader').css("visibility", "hidden");
        }
    });

});

