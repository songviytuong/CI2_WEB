﻿var CHAT_GL_ENABLED = 0;
var divVisible = '';
var pathToIcon = '/Content/desktop/images/site/';
var icontgdd = new google.maps.MarkerImage('' + pathToIcon + 'icon-tick-map.png',
        new google.maps.Size(22, 38),
        new google.maps.Point(0, 0),
        new google.maps.Point(0, 0));

var icontgddac = new google.maps.MarkerImage('' + pathToIcon + 'icon-tick-map.png',
        new google.maps.Size(22, 38),
        new google.maps.Point(0, 0),
        new google.maps.Point(0, 0));


var icondm = new google.maps.MarkerImage('' + pathToIcon + 'iconmapdm.png',
        new google.maps.Size(22, 37),
        new google.maps.Point(0, 0),
        new google.maps.Point(0, 0));

var icondmac = new google.maps.MarkerImage('' + pathToIcon + 'iconmapdm_ac.png',
        new google.maps.Size(22, 37),
        new google.maps.Point(0, 0),
        new google.maps.Point(0, 0));
var directionsService = new google.maps.DirectionsService;
var directionsDisplay = new google.maps.DirectionsRenderer;

var idplace = "storemap";
var isShowFull = 0;

$(document).ready(function () {
    var item = document.getElementById(idplace);
    var url = window.location.href;
    var STOREID = $(item).attr("idstoremap");
    GetFullMap();
    GetStoreDetail(parseInt(STOREID));
})

function GetStoreDetail(storeid) {
    if (isNaN(storeid)) return;
    $.ajax({
        url: '/store-ajax/GetStoreById',
        type: 'GET',
        data: { strStoreId: storeid },
        dataType: 'json',
        cache: true,
        success: function (data) {
            GenHTML2(data.values);
        }
    });
}

function GenHTML2(listObj) {
    if (listObj != null) {
        GenHTMLStoreMap2(listObj);
        //google.maps.event.trigger(gmarkers[0], "click");
    } else {
        $('#storelist').html('Rất tiếc, chúng tôi không tìm được thông tin bạn cần');
        GetMapDefaultNull();
    }
}
var lat = 0;
var lng = 0;
var map;
function GenHTMLStoreMap2(listObj) {

    if (listObj != null) {
        gmarkers = [];
        var intexit = 0;
        var locations = [];
        var LatLngCenter;
        if (listObj.LAT != undefined || listObj.LNG != null) {
            intexit = 0;
            lat = parseFloat(listObj.LAT.replace(',', '.'));
            lng = parseFloat(listObj.LNG.replace(',', '.'));
            if (lat != 0 & lng != 0)
                LatLngCenter = new google.maps.LatLng(lat, lng);

            var myLatlng = new google.maps.LatLng(lat, lng);

            var strInfo = CreateHTMLInfo(listObj.STOREFULLNAME, listObj.STOREADDRESS, listObj.IMAGEMAPLARGE, listObj.PHONE, listObj.FAX, listObj.EMAIL, listObj.OPENHOUR, listObj.SITEID, listObj.STOREID);
            var type = 0;
            var store = [strInfo, lat, lng, listObj.STOREID, listObj.SITEID];
            locations.push(store);
        }

        if (findaddress == 1) {
            calculateAndDisplayRoute(directionsService, directionsDisplay);
            return false;
        }
        $('.mapimg .imagemap').remove();
        $('.mapimg #storemap').show();
        map = new google.maps.Map(document.getElementById(idplace), {
            zoom: 17,
            center: LatLngCenter,
            mapTypeId: google.maps.MapTypeId.ROADMAP

        });

        var infowindow = new google.maps.InfoWindow();
        var marker;

        if (locations != null || locations != undefined) {
            for (var i = 0; i < locations.length; i++) {

                if (locations[i][4] == 1) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map,
                        icon: icontgdd
                    });
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                    google.maps.event.addListener(marker, 'click', (function (marker, i) {

                        return function () {
                            var storeid = locations[i][3];
                            infowindow.setContent(locations[i][0]);
                            infowindow.open(map, marker);
                            var fi = divVisible.replace('div', '');
                            if (fi != '') {
                                var fiIcon = gmarkers[fi].getIcon().size;
                                if (fiIcon.height == 38) {
                                    gmarkers[fi].setIcon(icontgdd);
                                }
                                else {
                                    gmarkers[fi].setIcon(icondm);
                                }
                            }
                            marker.setIcon(icontgddac);
                            divVisible = 'div' + i;
                            // $('#ulliststore li').removeClass('liac');
                            //  document.getElementById('div' + i).className = "liac";
                        }

                    })(marker, i));

                    google.maps.event.addListener(infowindow, 'closeclick', (function (marker, i) {

                        return function () {
                            flagMarker = 0;
                            divVisible = '';
                            infowindow.close();
                            marker.setIcon(icontgdd);
                            //   document.getElementById('div' + i).className = "";


                        }
                    })(marker, i));

                    google.maps.event.addListener(marker, "mouseover", (function (marker, i) {

                        return function () {
                            if (divVisible.replace('div', '') != i) {
                                marker.setIcon(icontgddac);
                                // document.getElementById('div' + i).className = "liac";
                            }
                        }
                    })(marker, i));

                    google.maps.event.addListener(marker, 'mouseout', (function (marker, i) {

                        return function () {
                            if (divVisible.replace('div', '') != i) {
                                marker.setIcon(icontgdd);
                                // document.getElementById('div' + i).className = "";
                            }
                        }
                    })(marker, i));
                }
                else {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map,
                        icon: icondm
                    });
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        alert(this.position);
                        return function () {
                            var storeid = locations[i][3];
                            flagMarker = 1;
                            infowindow.setContent(locations[i][0]);
                            infowindow.open(map, marker);
                            var fi = divVisible.replace('div', '');
                            if (fi != '') {

                                var fiIcon = gmarkers[fi].getIcon().size;

                                if (fiIcon.height == 38) {
                                    gmarkers[fi].setIcon(icontgdd);
                                }
                                else {
                                    gmarkers[fi].setIcon(icondm);
                                }
                            }
                            marker.setIcon(icondmac);
                            divVisible = 'div' + i;
                        }

                    })(marker, i));
                    google.maps.event.addListener(infowindow, 'closeclick', (function (marker, i) {

                        return function () {
                            flagMarker = 0;
                            divVisible = '';
                            infowindow.close();
                            //  document.getElementById('div' + i).className = "";
                            marker.setIcon(icondm);

                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {

                        return function () {
                            if (divVisible.replace('div', '') != i) {
                                marker.setIcon(icondmac);
                                //  document.getElementById('div' + i).className = "liac";
                            }
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', (function (marker, i) {

                        return function () {
                            if (divVisible.replace('div', '') != i) {
                                marker.setIcon(icondm);
                                //   document.getElementById('div' + i).className = "";
                            }
                        }
                    })(marker, i));
                }

                // gmarkers.push(marker);
            }
        }

    }
}

function CreateHTMLInfo(storename, storeaddress, imageurl, phone, fax, email, openhour, siteid, storeid) {
    var strImageIcon = '';
    imageurl = 'http://images.thegioididong.com/products/images/dtdd/khaitruong/sieuthi-nguyenoanh.jpg';
    if (siteid == 3) {
        strImageIcon = 'http://www.dienmay.com/favicon.ico';
        storename = 'Thế giới điện tử - ' + storename;
    }
    else {
        strImageIcon = 'http://www.thegioididong.com/favicon.ico';
        storename = 'Thế giới di động - ' + storename;
    }
    var strResult = '<div class="store-info">';
    strResult += '<div class="address">Địa chỉ: <span>' + storeaddress + '</span></div>';
    strResult += '<div class="phone">Điện thoại: <a href="tel:0838102102">028.38 102 102</a> - <a href="tel:18001060">1800 1060</a></div>';
    strResult += '<div class="openhour">Giờ mở cửa: <span>' + openhour + '</span></div>';
    if (isShowFull == 1) {
        strResult += '<a class="route" href="javascript:;" onclick="calculateAndDisplayRoute(directionsService, directionsDisplay)">Xem đường đi đến siêu thị này</a>';
    }
    strResult += '</div>';
    return strResult;
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var haight = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            var oceanBeach = new google.maps.LatLng(lat, lng);
            directionsDisplay = new google.maps.DirectionsRenderer();
            var mapOptions = {
                zoom: 17,
                center: haight
            }
            map = new google.maps.Map(document.getElementById(idplace), mapOptions);
            directionsDisplay.setMap(map);
            var request = {
                origin: haight,
                destination: oceanBeach,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            directionsService.route(request, function (response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });
            $('.mapimg .imagemap').remove();
            $('.mapimg #storemap').show();
        });
    } else {
        if (isMobile) {
            alert("Bạn vui lòng bật tính năng định vị trên thiết bị");
        }
    }
}

function GetFullMap() {
    var show = window.location.search.replace('?show=', '');
    if (show != '' && show !== undefined && show == 'full') {
        idplace = "storefullmap";
        $('section').attr('style', 'display: none');
        $('.fullmap a').attr('style', 'display: block');
        $('#storefullmap').height($(window).height() - $('header').height());
        isShowFull = 1;
    }
}